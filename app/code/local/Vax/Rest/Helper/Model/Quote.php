<?php

/**
 * Vax Rest Quote Helper.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Rest_Helper_Model_Quote extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieves a single quote.
     *
     * @param integer $id      Quote ID
     * @param integer $storeID Store ID
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote($id, $storeID)
    {
        $quote = Mage::getModel('sales/quote')
            ->setStoreId($storeID)
            ->load($id);

        if ( ! $quote->getId()) {
            throw new Mage_Api2_Exception('Quote does not exist', '404');
        }

        return $quote;
    }

    /**
     * Returns the totals address depending on the quote type.
     *
     * @param $quote
     */
    public function getTotalsAddress($quote)
    {
        if ($quote->isVirtual()) {
            return $quote->getBillingAddress();
        }

        return $quote->getShippingAddress();
    }
}