<?php

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

if ( ! $installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_order'), 'account_id')) {
    $installer->getConnection()->addColumn($installer->getTable('sales_flat_order'), 'account_id', 'varchar(255)');
    $installer->getConnection()->addColumn($installer->getTable('sales_flat_order_grid'), 'account_id', 'varchar(255)');
}

$installer->endSetup();