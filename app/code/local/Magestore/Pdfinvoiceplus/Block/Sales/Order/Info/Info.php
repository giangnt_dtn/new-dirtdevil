<?php

class Magestore_Pdfinvoiceplus_Block_Sales_Order_Info_Info extends Mage_Sales_Block_Order_Info {

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('pdfinvoiceplus/sales/order/info/info.phtml');
    }
    
    public function getCustomPrintUrl($order)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return $this->getUrl('pdfinvoiceplus/order/print', array('order_id' => $order->getId()));
        }
        return $this->getUrl('pdfinvoiceplus/order/print', array('order_id' => $order->getId()));
    }
}

?>
