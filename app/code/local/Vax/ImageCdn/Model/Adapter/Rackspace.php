<?php
/**
 * CDN adapter for Rackspace Could Files
 *
 * @category vax_uk
 * @package vax_uk
 * @author Craig Carnell
 * 
 */

set_include_path(Mage::getModuleDir('model', 'OnePica_ImageCdn').DS.'Model'.DS.'Adapter'.DS.'Rackspace'.PS.get_include_path());
/**
 * CDN adapter for Rackspace Could Files
 */
class Vax_ImageCdn_Model_Adapter_Rackspace extends OnePica_ImageCdn_Model_Adapter_Rackspace {
    
    /**
    * Creates a singleton connection handle
    *
    * @return CF_Connection
    */
    protected function auth() { 
        if(is_null($this->conn)) {
            $username = Mage::getStoreConfig('imagecdn/rackspace/username');
            $api_key = Mage::getStoreConfig('imagecdn/rackspace/api_key');
            $auth = new CF_Authentication($username, $api_key, NULL, UK_AUTHURL);
            $auth->ssl_use_cabundle();
            $auth->authenticate();
            
            if($auth->authenticated()) { 
                $this->conn = new CF_Connection($auth);
                $this->conn->ssl_use_cabundle();
                return $this->conn;
            } else {
                return false;
            }
        } else {
            return $this->conn;
        }
    }

    /**
     * Calls the adapter-specific save method and updates the cache
     *
     * @param string $relFilename	path (with filename) from the CDN root
     * @param string $tempfile		temp file name to upload
     * @return bool
     */
    public function save($filename, $tempfile) {
        
        if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('save() - fileName = ' . $filename, null, 'cdn.log');
        }
        
        if(Mage::helper('vax_imagecdn')->getRestrictionEnabled() && !Mage::helper('vax_imagecdn')->IsUploadAllowed()) {
            
            if(Mage::helper('vax_imagecdn')->isDebugging()) {
                Mage::log('save() - not allowed for ' . $filename, null, 'cdn.log');
            }
        
            return false;
        }
        
        if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('save() - allowed for ' . $filename, null, 'cdn.log');
        }
            
        // we could have called parent::save here, but we need to log the exceptions!
        $filename = $this->getRelative($filename);
        
        try {
            $result = $this->_save($filename, $tempfile);
        } catch (Exception $e) {
            Mage::log($e->__toString(), null, 'cdn.log');
            $result = false;
        }
        
        if($result) {
            $url = $this->getUrl($filename);
            Mage::getSingleton('imagecdn/cache')->updateCache($url);
        }

        return $result;
    }

    /**
     * Calls the adapter-specific remove method and updates the cache
     *
     * @param string $relFilename	path (with filename) from the CDN root
     * @return bool
     */
    public function remove($filename) {
        
        if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('remove() for ' . $filename, null, 'cdn.log');
        }
        
        if(Mage::helper('vax_imagecdn')->getRestrictionEnabled() && !Mage::helper('vax_imagecdn')->IsUploadAllowed()) {
            
            if(Mage::helper('vax_imagecdn')->isDebugging()) {
                Mage::log('remove() - not allowed for ' . $filename, null, 'cdn.log');
            }
        
            return false;
        }
        
        if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('remove() - allowed for ' . $filename, null, 'cdn.log');
        }
            
        parent::remove($filename);
    }

    /**
    * Tests to see if an image on the current CDN is available. These results are cached for
    * a configurable amount of time to reduce the number of cURLs needed.
    *
    * @param string $relFilename	path (with filename) from the CDN root
    * @return bool
    */
    public function fileExists($filename) {
        $filename = $this->getRelative($filename);
        $url = $this->getUrl($filename);

        $cached = Mage::getSingleton('imagecdn/cache')->checkCache($url);
        
        if($cached) {
            return true;
        }

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_HEADER, true);
        curl_setopt($c, CURLOPT_NOBODY, true);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, $this->curlFollowLocation);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($c);
        $httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
        $size = curl_getinfo($c, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($c);

        //should we test to see if file size greater than zero?
        $verifySize = Mage::getStoreConfig('imagecdn/general/cache_check_size');

        if ($httpcode == 200 && (!$verifySize || $size)) {
            
            if(Mage::helper('vax_imagecdn')->isDebugging()) {
                Mage::log('fileExists() - ' . $filename . ' size is ' . $size, null, 'cdn.log');
            }

            Mage::getSingleton('imagecdn/cache')->updateCache($url);
            return true;
        } else {
            
            if(Mage::helper('vax_imagecdn')->isDebugging()) {
                Mage::log('fileExists() not found - ' . $filename, null, 'cdn.log');
            }
            
            return false;
        }
    }
        
    /**
     * Clears the cache
     *
     * @return none
     */
    public function clearCache() {
        
        if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('clearCache()', null, 'cdn.log');
        }
        
        //get the admin session
        Mage::getSingleton('core/session', array('name'=>'adminhtml'));

        if(Mage::helper('vax_imagecdn')->getRestrictionEnabled() && !Mage::helper('vax_imagecdn')->IsUploadAllowed()) {
            
            if(Mage::helper('vax_imagecdn')->isDebugging()) {
                Mage::log('clearCache() - not allowed', null, 'cdn.log');
            }
        
            //verify if the user is logged in to the backend
            if(Mage::getSingleton('admin/session')->isLoggedIn()){
                Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('vax_imagecdn')->__('Image Cdn has not been cleared'));
            }
            
            return false;
        }
        
        if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('clearCache() - allowed', null, 'cdn.log');
        }
            
        parent::clearCache();
        
        //verify if the user is logged in to the backend
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            Mage::getSingleton('adminhtml/session')
            ->addSuccess(Mage::helper('vax_imagecdn')->__('Image Cdn Cache has been cleared'));
        }
    }
    
    /**
    * Creates a full URL to the image on the remote server
    *
    * @param string $relFilename	path (with filename) from the CDN root
    * @return string
    */
    public function getUrl($filename) {
        
        $type = Mage::app()->getStore()->isCurrentlySecure() ? 'secure_base_url' : 'base_url';
        $base_url = Mage::getStoreConfig('imagecdn/rackspace/' . $type); 
        
    	$filename = str_replace('\\', '/', $base_url . $this->getRelative($filename));
        
        /*if(Mage::helper('vax_imagecdn')->isDebugging()) {
            Mage::log('getUrl() - ' . $filename, null, 'cdn.log');
        }*/
        
	return $filename;
    }
    
    /**
     * Don't use parent method as that does not support SSL
     * 
     * @return bool
     */
    public function useCdn() {
        
        return Mage::getStoreConfig('imagecdn/general/status') ? true : false;
    }
}