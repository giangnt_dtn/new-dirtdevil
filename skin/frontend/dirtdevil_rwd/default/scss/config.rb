# note: this should never truly be refernced since we are using relative assets
http_path = "/skin/frontend/dirtdevil_rwd/default/"
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../images"
javascripts_dir = "../js"
relative_assets = true

add_import_path "../../../rwd/default/scss"

output_style = :expanded
environment = :production

# set default encoding to UTF-8
Encoding.default_external = "UTF-8"