<?php

/**
 * Vax Checkout Session for storing coupon messages.
 *
 * @copyright Vax Ltd 2014
 */
class Vax_Checkout_Model_CouponSession extends Mage_Core_Model_Session_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->init('coupon');
    }
}