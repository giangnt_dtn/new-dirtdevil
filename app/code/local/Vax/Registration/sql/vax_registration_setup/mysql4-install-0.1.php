<?php

$installer = $this;
$installer->startSetup();

$registrationTable = $installer->getConnection()
    ->newTable($installer->getTable('vax_registration/registration'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        20,
        array(
            'nullable' => false,
        ),
        'Salutation'
    )
    ->addColumn(
        'first_name',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        70,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'surname',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        70,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'address',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        120,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'address2',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        100,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'town',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        60,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'county',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        60,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'postcode',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        40,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'email',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'telephone',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        100,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'model',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        20,
        array(
            'nullable' => false,
        ),
        'GPID'
    )
    ->addColumn(
        'warranty',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        5,
        array(
            'nullable' => false,
        ),
        'No. of years'
    )
    ->addColumn(
        'serial_number',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        120,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'pur_date',
        Varien_Db_Ddl_Table::TYPE_DATE,
        null,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'pur_price',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        60,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'pur_location',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        120,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'mail_opt',
        Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        null,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ),
        'Store registered on'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        array(
            'nullable' => false,
        )
    )
    ->addColumn(
        'modified_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE
        ),
        'Update Time'
    )
    ->setComment('Vax Registration Table');

$installer->getConnection()->createTable($registrationTable);
$installer->endSetup();