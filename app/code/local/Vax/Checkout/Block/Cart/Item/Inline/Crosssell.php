<?php
/**
 * Shopping cart item render block
 *
 * @category    Vax
 * @package     Vax_Checkout
 * @author      Team Vax
 */
class Vax_Checkout_Block_Cart_Item_Inline_Crosssell extends Mage_Catalog_Block_Product_Abstract
{
	/**
	 * Items quantity will be capped to this value
	 *
	 * @var int
	 */
	protected $_maxItemCount = 2;

	/**
	 * Already rendered crosssells
	 *
	 * @var array
	 */
	protected $_renderedIds = null;

	protected function _construct()
	{
        if ($this->getBreakPoint() == 'small') {
            $this->setTemplate('checkout/cart/item/crosssell.small.phtml');
        } else {
            $this->setTemplate('checkout/cart/item/crosssell.phtml');
        }
        $this->_resetCrosssellIds();
		return parent::_construct();
	}

	/**
	 * Get crosssell items, filter by current product being rendered in basket
	 *
	 * @return array
	 */
	public function getItems()
	{
		$product = $this->getProduct();
        $items = $this->getData('items');
        if (is_null($items)) {
			$items = array();
			$ninProductIds = $this->_getCartProductIds();
			if ($ninProductIds) {
				if ($product) {
					$product = (int) $product->getId();

					// load crosssells for this product only
					$collection = $this->_getCollection()
						->addProductFilter($product);

					// don't list crosssells already in the basket
					if (!empty($ninProductIds)) {
						$collection->addExcludeProductFilter($ninProductIds);
					}

					// TODO: exclude duplicates
                    $storage = Mage::getSingleton('vax_checkout/cart_crosssell');
                    $crosssellIds = (array)$storage->getCrosssellIds();

                    if (count($crosssellIds)) {
                        $collection->addExcludeProductFilter($crosssellIds);
                    }

					$collection->setPositionOrder()->load();

					foreach ($collection as $item) {
						$ninProductIds[] = $item->getId();
						$items[] = $item;
                        $crosssellIds[] = $item->getId();
					}

                    $storage->setCrosssellIds($crosssellIds);
				}
			}

			$this->setData('items', $items);
		}
		return $items;
	}

	/**
	 * Get quote instance
	 *
	 * @return Mage_Sales_Model_Quote
	 */
	public function getQuote()
	{
		return Mage::getSingleton('checkout/session')->getQuote();
	}

	/**
	 * Get ids of products that are in cart
	 *
	 * @return array
	 */
	protected function _getCartProductIds()
	{
		$ids = $this->getData('_cart_product_ids');
		if (is_null($ids)) {
			$ids = array();
			foreach ($this->getQuote()->getAllItems() as $item) {
				if ($product = $item->getProduct()) {
					$ids[] = $product->getId();
				}
			}
			$this->setData('_cart_product_ids', $ids);
		}
		return $ids;
	}

	/**
	 * Get crosssell products collection
	 *
	 * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Link_Product_Collection
	 */
	protected function _getCollection()
	{
		$collection = Mage::getModel('catalog/product_link')->useCrossSellLinks()
			->getProductCollection()
			->setStoreId(Mage::app()->getStore()->getId())
			->addStoreFilter()
			->setPageSize($this->_maxItemCount);
        $collection->addAttributeToSelect('sku');
        $this->_addProductAttributesAndPrices($collection);

		Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

		return $collection;
	}

    /**
     * Resets crosssell ids stored in the singleton.
     */
    protected function _resetCrosssellIds()
    {
        Mage::getSingleton('vax_checkout/cart_crosssell')->unsCrosssellIds();
    }
}