<?php

/**
 * Vax Checkout Onepage Header Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Steps_Header extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the current header title.
     *
     * @return string
     */
    public function getHeader()
    {
        $steps = $this->getSteps();

        return $this->__($steps[$this->getStepIndex() - 1]['title']);
    }

    /**
     * Returns the current sub title.
     *
     * @return string
     */
    public function getSummary()
    {
        $steps = $this->getSteps();

        return $this->__($steps[$this->getStepIndex() - 1]['summary']);
    }

    /**
     * Do not display header if both header and summary are not set
     *
     * @return bool
     */
    public function canDisplayHeader()
    {
        $steps = $this->getSteps();
        $currentStep = $steps[$this->getStepIndex()-1];
        if (isset($currentStep['displayHeader']) && !$currentStep['displayHeader']) {
            return false;
        }
        return true;
    }
}