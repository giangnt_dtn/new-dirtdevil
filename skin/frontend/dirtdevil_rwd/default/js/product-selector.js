jQuery(document).ready(function($j){

    var category_url = "/category/index/index/";
    var product_url = "/product/index/category/";
    var productcode_url = "/product/index/code/";
    var subcategory_count = 0;
    var bodylocation = null;

    // Store all ajax requests in a pool
    $j.xhrPool = [];
    $j.ajaxSetup({
        beforeSend: function(jqXHR) {
            $j.xhrPool.push(jqXHR);
        },
        complete: function(jqXHR) {
            var index = $j.xhrPool.indexOf(jqXHR);
            if (index > -1) {
                $j.xhrPool.splice(index, 1);
            }
        }
    });

    // Abort all outstanding ajax requests
    $j.xhrPool.abortAll = function() {
        $j(this).each(function(index, jqXHR) {
            jqXHR.abort();
            $j.xhrPool.splice(index, 1);
        });
    };

    get_location();
    function get_location() {
        var bodyclass = $j(location).attr('pathname');

        if(bodyclass.indexOf('registration')>=0) {
            bodylocation = "registration";
        }
        else {
            bodylocation = "support";
        }

        return bodylocation;
    }

    function move_step_1_to_2 () {
        //$j("#step-1").removeClass("active").addClass("completed");
        //$j("#step-2").addClass("active");
    }

    function move_step_2_to_3 () {
        //$j("#step-2").removeClass("active").addClass("completed");
        //$j("#step-3").addClass("active");
    }

    $j("a.product-types-item").on('click', function() {
        var category = $j(this).data('category');
        var store_id = $j(this).data('store');
        var associatedProductId = $j(this).data('associated-product-id');

        if(typeof associatedProductId !== 'undefined') {
            return;
        }

        $j("ul.product-types").hide();
        move_step_1_to_2();
        $j("#ajax-loading-model-number").show();

        var url = category_url + "id/" + category;

        if(store_id !== null && typeof store_id != 'undefined') {
            url +=  "/store_id/" + store_id;
        }

        url += "/location/" + get_location();

        $j.ajax({
          url: url,
          context: document.body,
          success: function(data, status) {
            var categories = $j.parseJSON(data);
            subcategory_count = categories.length;

            if ( ! subcategory_count) {
                move_step_2_to_3();
                load_products(category);
                return false;
            }

            $j.each(categories, function(index, category) {
                var range = $j('ul.product-types li:first').clone(false);
                range.find('.title').text(category.name);
                range.find('a').removeClass('product-types-item').addClass('model-range-item').removeAttr('data-category').attr('data-range', category.id);
                if (category.image) {
                    range.find('img').attr('src', category.image);
                }
                else {
                    range.find('img').remove();
                }
                if (category.url && category.url != '#') {
                    range.find('a').attr('href', category.url);
                }
                range.appendTo('ul.model-range');
            });
            $j("#ajax-loading-model-number").hide();
            $j('ul.model-range').show();
          }
        });
        return false;
    });

    $j('ul.model-range').on('click', 'a.model-range-item', function() {
        var range = $j(this).data('range');

        $j("ul.model-range").hide();
        move_step_2_to_3();

        var url = $j(this).attr('href');

        if(url === '#') {
            $j("#ajax-loading-model-number").show();

            load_products(range);
            return false;
        }
    });

    function load_products(range) {
        var url = product_url + "id/" + range + "?location=" + bodylocation;

        $j.ajax({
          url: url,
          context: document.body,
          success: function(data, status) {
            var product_response = $j.parseJSON(data)
            $j.each(product_response.products, function(index, product) {
                var item = $j(product_response.template);
                item.find('.title').text(product.name);
                item.find('.model-no').text(product.modelNo);
                item.children('a').addClass('model-number-item').attr('data-model', product.id).attr('href', product.url);
                if (product.image) {
                    item.find('img').attr('src', product.image);
                }
                else {
                    item.find('img').remove();
                }
                item.appendTo('#model-number');
            });
            $j("#ajax-loading-model-number").hide();
            $j('#model-number').show();
          }
        });
    }

    function back_to_step_1() {
        $j('ol.steps li').removeClass('completed').removeClass('active');
        $j('#step-1').addClass('active');
        $j('ul.model-range').empty();
        $j('ul.model-number').empty();
        $j("#ajax-loading-model-number").hide();
        $j('ul.product-types').show();
    }

    function back_to_step_2() {
        $j('ol.steps li').removeClass('active');
        $j('#step-2').removeClass('completed').addClass('active');
        $j("#ajax-loading-model-number").hide();
        $j('ul.model-number').empty();
        $j('ul.model-range').show();
    }

    // Navigate to previous steps
    $j('ol.steps').on('click', '.completed', function() {
        $j.xhrPool.abortAll();
        if ($j(this).attr('id') == 'step-1' || subcategory_count == 0) {
            back_to_step_1();
        } else if ($j(this).attr('id') == 'step-2') {
            back_to_step_2();
        }
        return false;
    });

    //
    function loadProductsByCode(code)
    {
        var url = productcode_url + "?pcode=" + code + "&location=" + bodylocation;

        $j.ajax({
          url: url,
          context: document.body,
          success: function(data, status) {

            $j('#model-codes').empty();
            var product_response = $j.parseJSON(data);
            if (product_response.products.length > 0) {
                $j.each(product_response.products, function(index, product) {
                    var item = $j(product_response.template);
                    item.find('.title').text(product.name);
                    item.find('.model-no').text(product.modelNo);
                    item.children('a').addClass('model-number-item').attr('data-model', product.id).attr('href', product.url);
                    if (product.image) {
                        item.find('img').attr('src', product.image);
                    }
                    else {
                        item.find('img').remove();
                    }
                    item.appendTo('#model-codes');
                });
                $j("#ajax-loading-search-reg").hide();
                $j('#model-codes').show();
            }
            else {
                $j(product_response.template).appendTo('#model-codes');
                $j("#ajax-loading-search-reg").hide();
                $j('#model-codes').show();
            }
          }
        });
    }

    // to do link to unveil enabled in admin
    function lazyloadImages() {
        $j('img').unveil();
    }

    $j('#search-reg-go').click(function(ev){
        var input = $j('#search-reg');
            input.val($j.trim(input.val()));
    });    

    // Do product code search on registration search go
    $j('#product-web-search').on('submit', function(event) {
        $j('#product-selector-container').show();
        $j('#model-codes').hide();
        $j('#ajax-loading-search-reg').show();
        $j('#search-reg-content').show();
        loadProductsByCode($j("#search-reg").val());
        return false;
    });
})