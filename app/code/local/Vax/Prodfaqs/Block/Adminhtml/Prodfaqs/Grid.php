<?php
class Vax_Prodfaqs_Block_Adminhtml_Prodfaqs_Grid extends FME_Prodfaqs_Block_Adminhtml_Prodfaqs_Grid
{
	protected function _prepareColumns() {
		$return = parent::_prepareColumns();
		$this->getColumn('question_type')->setOptions(array(
              'general_question' => 'General Question',
              'product_question' => 'Product Question',
			  'maintenance_item' => 'Maintenance Question'
          ));
	}
	
}