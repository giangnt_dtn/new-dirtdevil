<?php

/**
 * Order Builder.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Builder_Order
{
    /**
     * Quote Converter.
     *
     * @var Mage_Sales_Model_Convert_Quote
     */
    protected $_convertor;

    /**
     * Constructor.
     *
     * @param Mage_Sales_Model_Convert_Quote $converter
     */
    public function __construct(Mage_Sales_Model_Convert_Quote $converter)
    {
        $this->_convertor = $converter;
    }

    /**
     * Builds a new order from a quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return Mage_Sales_Model_Order
     */
    public function build(Mage_Sales_Model_Quote $quote)
    {
        $order = $this->_getOrderInstance($quote);

        $this->_setAddresses($order, $quote);
        $this->_setItems($order, $quote);
        $this->_setPayment($order, $quote);
        $this->_setQuote($order, $quote);

        return $order;
    }

    /**
     * Returns a new order instance.
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _getOrderInstance(Mage_Sales_Model_Quote $quote)
    {
        if ($quote->isVirtual()) {
            return $this->_convertor->addressToOrder($quote->getBillingAddress());
        }

        return $this->_convertor->addressToOrder($quote->getShippingAddress());
    }

    /**
     * Sets the order's addresses.
     *
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _setAddresses($order, $quote)
    {
        $order->setBillingAddress($this->_convertor->addressToOrderAddress($quote->getBillingAddress()));

        if ($quote->getBillingAddress()->getCustomerAddress()) {
            $order->getBillingAddress()->setCustomerAddress($quote->getBillingAddress()->getCustomerAddress());
        }

        if ( ! $quote->isVirtual()) {
            $order->setShippingAddress($this->_convertor->addressToOrderAddress($quote->getShippingAddress()));
            if ($quote->getShippingAddress()->getCustomerAddress()) {
                $order->getShippingAddress()->setCustomerAddress($quote->getShippingAddress()->getCustomerAddress());
            }
        }
    }

    /**
     * Sets the order's items.
     *
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return void
     */
    protected function _setItems($order, $quote)
    {
        foreach ($quote->getAllItems() as $item) {
            $orderItem = $this->_convertor->itemToOrderItem($item);

            if ($item->getParentItem()) {
                $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
            }

            $order->addItem($orderItem);
        }
    }

    /**
     * Sets the order associated payment.
     *
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Quote $quote
     */
    protected function _setPayment($order, $quote)
    {
        $order->setPayment(
            $this->_convertor->paymentToOrderPayment($quote->getPayment())
        );
    }

    /**
     * Sets the order's associated quote.
     *
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Quote $quote
     */
    protected function _setQuote($order, $quote)
    {
        $order->setQuote($quote)
              ->setRelationParentId($quote->getParentOrderIncrementId());
    }
}