<?php

/**
 * Quote Builder.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Builder_Quote
{
    /**
     * Builds a quote from a data set.
     *
     * @param array $data
     *
     * @return Mage_Sales_Model_Quote
     */
    public function build(array $data)
    {
        return $this->_prepare(
            $this->_map($data)
        );
    }

    /**
     * Maps the quote.
     *
     * @param array $data
     *
     * @return Mage_Sales_Model_Quote
     */
    public function _map($data)
    {
        return Mage::getModel('checkout/mapper_checkout_request')->map(
            $this->_getQuote(),
            $data
        );
    }

    /**
     * Prepares the quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _prepare(Mage_Sales_Model_Quote $quote)
    {
        $oldQuoteOrderId = $quote->getReservedOrderId();

        $quote->setReservedOrderId(null)
                 ->deleteNominalItems()
                 ->reserveOrderId();

        Mage::dispatchEvent('quote_builder_prepare_after', array(
            'old_order_id' => $oldQuoteOrderId,
            'new_order_id' => $quote->getReservedOrderId())
        );

        return $quote;
    }

    /**
     * Returns the current quote.
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
}