<?php
/**
 * Catalog image helper
 */
class DirtDevil_Catalog_Helper_Image extends Mage_Catalog_Helper_Image
{
    /* if we are lazy loading we need to return the original as a data-src not src */
    public function getLazyLoadedImage($original) {

        if(!Mage::getStoreConfig('vax_minify/frontend/lazy_load')) {
            return $original;
        }

        return Mage::helper('catalog/product')->getPlaceHolderImage() .'" data-src="'.  $original;
    }

    public function getHomeBannerPlaceholderImage($original) {
        if(!Mage::getStoreConfig('vax_minify/frontend/lazy_load')) {
            return $original;
        }

        $placeholder = Mage::getBaseUrl('skin') . "frontend/dirtdevil_rwd/default/images/home-banner-placeholder.png";

        return $placeholder .'" data-src="'.  $original;
    }

    public function getSquareTransparentPlaceholderImage($original) {
        if(!Mage::getStoreConfig('vax_minify/frontend/lazy_load')) {
            return $original;
        }

        $placeholder = Mage::getBaseUrl('skin') . "frontend/dirtdevil_rwd/default/images/product-image-placeholder.png";

        return $placeholder .'" data-src="'.  $original;
    }
}