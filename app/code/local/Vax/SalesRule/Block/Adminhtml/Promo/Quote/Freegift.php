<?php

class Vax_SalesRule_Block_Adminhtml_Promo_Quote_Freegift extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup     = 'vax_salesrule';
        $this->_controller     = 'adminhtml_promo_quote_freegift';
        $this->_headerText     = Mage::helper('vax_salesrule')->__('Product Rules');
        $this->_addButtonLabel = Mage::helper('vax_salesrule')->__('Add New Rule');
        parent::__construct();
    }
}