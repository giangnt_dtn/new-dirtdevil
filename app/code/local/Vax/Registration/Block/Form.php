<?php
class Vax_Registration_Block_Form extends Mage_Core_Block_Template
{
    public function getRegistrationProduct()
    {
        $sku     = Mage::getSingleton('core/session')->getVaxRegistrationSku();
        $product = Mage::getModel('catalog/product');
        $id = $product->getResource()->getIdBySku($sku);

        if ($id) {
            $product->load($id);
        }
        
        if($id && $product) {
            return $product;
        } else {
            Mage::getSingleton('core/session')->addError('Invalid product with SKU '.$sku.' and ID '.$id.'. Please contact customer service');
            return false;
        }
    }
}
