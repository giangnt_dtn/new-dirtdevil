<?php
class Vax_LiveChat_Block_Sales extends Vax_LiveChat_Block_General
{
    /*
     * Returns the helper in use
     * @return Mage_Core_Helper_Abstract
     */
    public function getTheHelper() {
        return Mage::helper('vax_livechat/sales');
    }

    /**
     * Returns live chat button id
     *
     * @return string
     */
    public function getButtonId() {
        return "liveagent_invite_button_" . $this->getTheHelper()->getDeploymentID();
    }
}
