<?php
class Vax_Prodfaqs_FaqController extends Mage_Core_Controller_Front_Action
{
    public function viewAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        if (!$id) {
            $this->_redirect('/');
            return;
        }

        $faq = Mage::getModel('prodfaqs/prodfaqs')->load($id);
        if (!$faq->getId()) {
            $this->_redirect(Mage::getBaseUrl());
            return;
        }
        Mage::register('current_faq', $faq);
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs.new')->addCrumb('home', array(
            'label'=>Mage::helper('vaxfaqs')->__('Support Centre'),
            'title'=>Mage::helper('vaxfaqs')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs.new')->addCrumb('faqs', array(
            'label'=>Mage::helper('vaxfaqs')->__('FAQ\'s'),
            'title'=>Mage::helper('vaxfaqs')->__('Go to FAQ Page'),
            'link'=>Mage::getUrl('*/*/*', array('_current' => true))
        ));

        $this->getLayout()->getBlock('page.title')->setPageTitle(Mage::helper('core')->__('FAQ\'s'));
        $this->renderLayout();
    }

    public function troubleshootingAction()
    {
        if ($this->getRequest()->getParam('product')) {
            $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product'));
            Mage::register('current_product', $product);
            $params = $this->getRequest()->getParams();
            $params['id'] = $product->getId();
            Mage::getSingleton('core/session')->setLastFaqUrl(Mage::getUrl('catalog/product/view', $params));
        }

        $this->loadLayout();

        $this->renderLayout();
    }
}