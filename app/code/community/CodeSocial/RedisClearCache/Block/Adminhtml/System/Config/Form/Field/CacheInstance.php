<?php
/**
 * Adminhtml system config array field renderer
 *
 * @category   CodeSocial
 * @package    CodeSocial_RedisCacheClear
 * @author     CodeSocial Team <team@codesocial.co.uk>
 */
class CodeSocial_RedisClearCache_Block_Adminhtml_System_Config_Form_Field_CacheInstance extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    /**
     * Query type renderer
     *
     * @var Msat_Redisclearcache_Block_Select
     */
    protected $_typeRenderer;

    /**
     * Search field renderer
     *
     * @var Msat_redisclearcache_Block_Select
     */
    protected $_searchFieldRenderer;

    protected function _prepareToRender() {

        $this->_typeRenderer = null;
        $this->_searchFieldRenderer = null;

        $this->addColumn('server', array(
            'label' => Mage::helper('redisclearcache')->__('IP'),
            'style' => 'width:150px',
        ));

        $this->addColumn('port', array(
            'label' => Mage::helper('redisclearcache')->__('Port'),
            'style' => 'width:60px',
        ));

        $this->addColumn('database', array(
            'label' => Mage::helper('redisclearcache')->__('Database'),
            'style' => 'width:60px',
        ));

        // Disables "Add after" button
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('redisclearcache')->__('Add Server');
    }
}
