<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
require_once 'FME/Prodfaqs/controllers/IndexController.php';

class Vax_Prodfaqs_Rewrite_IndexController extends FME_Prodfaqs_IndexController
{
    public function ratingAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return;
        }

        if($data = $this->getRequest()->getPost()){
            try{
                $read_connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $faqsTable = Mage::getSingleton('core/resource')->getTableName('prodfaqs/prodfaqs');
                $select = $read_connection->select()->from($faqsTable, array('*'))->where('faqs_id=(?)', $data['faq_id']);
                $result_row =$read_connection->fetchRow($select);

                if($result_row != null){
                    $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write_connection->beginTransaction();
                    $fields = array();
                    $fields['rating_num']	= $result_row['rating_num']+$data['value'];
                    $fields['rating_count']	= $result_row['rating_count']+1;
                    $fields['rating_stars']	= $fields['rating_num']/$fields['rating_count'];

                    $where = $write_connection->quoteInto('faqs_id =?', $data['faq_id']);
                    $write_connection->update($faqsTable, $fields, $where);
                    $write_connection->commit();


                    //Check session for faqs id
                    $faqs_session_array = Mage::getSingleton('customer/session')->getRatedFaqsId();

                    if(!is_array($faqs_session_array)){
                        $faqs_session_array = array();
                    }

                    // check this array and increment the index to save next faq id

                    $faqs_session_array[] = $data['faq_id'];
                    Mage::getSingleton('customer/session')->setRatedFaqsId($faqs_session_array);

                    $this->getResponse()->setBody(Mage::helper('prodfaqs')->__('Thank you for Rating '));
                }
            }catch (Exception $e){
                $this->getResponse()->setBody(Mage::helper('prodfaqs')->__('Unable to process Rating '));
            }

        }
    }

    public function searchAction()
    {
        $faqsTable = Mage::getSingleton('core/resource')->getTableName('prodfaqs');
        $faqsProductTable = Mage::getSingleton('core/resource')->getTableName('prodfaqs_products');
        $faqsStoreTable = Mage::getSingleton('core/resource')->getTableName('prodfaqs_store');

        $sterm = $this->getRequest()->getParam('term');
        $productId = (int)$this->getRequest()->getParam('product_id');

        $post = $this->getRequest()->getPost();

        if($post && empty($sterm)){
            $sterm=$post['faqssearch'];
        }

        if(isset($sterm)){
            //Sorting condition for topic's detail page faqs
            $sort_by = Mage::helper('prodfaqs')->getGeneralFaqSorting();

            if($sort_by == 'helpful'):
                $condition = 'f.rating_stars DESC';
            elseif($sort_by == 'order'):
                $condition = 'f.faq_order ASC';
            else :
                $condition = 'f.created_time DESC';
            endif;

            $sqry = "select * from ".$faqsTable." f,".$faqsStoreTable." fs,".$faqsProductTable." fp where (f.title like '%$sterm%' or f.faq_answar like '%$sterm%') and (status=1)
			    and f.topic_id = fs.topic_id and f.visibility = 'public'
			    and fp.product_id = $productId and fp.faqs_id = f.faqs_id
			    and (fs.store_id =".Mage::app()->getStore()->getId()." OR fs.store_id=0) ORDER BY $condition";

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select = $connection->query($sqry);

            $sfaqs = $select->fetchAll();

            if($this->getRequest()->isAjax()) {
                $this->template = 'prodfaqs/_partial/faq.phtml';

                $afaqs = null;

                foreach($sfaqs as $faq) {

                    $afaqs[] = array(
                        'id'      => $faq["faqs_id"],
                        'title'   => $faq["title"],
                        'answer'  => $faq["faq_answar"]
                    );
                }

                $data = array(
                    'template' => $this->renderTemplate(),
                    'faqs' => $afaqs,
                );

                $this->getResponse()->setBody(json_encode($data));
            }
        }

        if(!$this->getRequest()->isAjax()) {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    /**
     * Renders the Template.
     *
     * @return string
     */
    protected function renderTemplate()
    {
        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'Vax_SupportPortal',
            array('template' => $this->template)
        );

        return $block->toHtml();
    }
}
