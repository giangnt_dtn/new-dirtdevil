<?php

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

$installer->updateAttribute('catalog_product', 'sales_chat_delay', 'apply_to', 'bundle,virtual,simple');
$installer->updateAttribute('catalog_product', 'is_sales_chat_enabled', 'apply_to', 'bundle,virtual,simple');
$installer->updateAttribute('catalog_product', 'is_support_chat_enabled', 'apply_to', 'bundle,virtual,simple');

$installer->endSetup();