<?php

class Yoma_RealexExtended_Model_RuleProcessor {

    protected $_modifiers = array('<','>');

    protected $_failed = array();
    protected $_successful = array();


    public function getResult($testCase) {

        foreach ($testCase as $test) {
            if (!$test->getResult()) {
                return false;
            }
        }

        return true;
    }

    public function newTestCase($testCase) {

        foreach ($testCase as $test) {
            $test->setResult(false);
            if ($this->_test($test)) {
                $test->setResult(true);
            }

        }

        return $testCase;
    }

    protected function _test($test) {

        $result = false;

        switch ($test->getType()) {
            case 'boolean':
                $value = (int)$test->getValue();
                $test  = (int)$test->getTest();
                if ($test & $value) {
                    $result = true;
                }
                break;
            case 'compare':
                 if(in_array(substr($test->getValue() , 0 , 1 ),$this->_modifiers)){
                     switch(substr($test->getValue() , 0 , 1 )){
                         case '<':
                             if ($test->getTest() < (int)substr($test->getValue(),1)) {
                                 $result = true;
                             }
                             break;
                         default:
                             if ((int)$test->getTest() > (int)substr($test->getValue(),1)) {
                                 $result = true;
                             }
                             break;
                     }
                 }else {
                     if ((int)$test->getTest() >= (int)$test->getValue()) {
                         $result = true;
                     }
                 }
                break;

        }

        return $result;
    }

    public function clear(){

        $this->_failed = array();
        $this->$_successful = array();
    }
}