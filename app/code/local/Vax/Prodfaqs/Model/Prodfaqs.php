<?php
class Vax_Prodfaqs_Model_Prodfaqs extends FME_Prodfaqs_Model_Prodfaqs
{
    public function getFaqsOfProduct($prod_id, $publicOnly = true, $questionType = null){
        $faqs = $this->getResource()->loadFaqsByProduct($prod_id, $publicOnly, $questionType);
        return $faqs;
    }

    public function getFaqProductIds()
    {
    	$resource = $this->getResource();
    	$select = $resource->getReadConnection()->select();
    	$select->from($resource->getTable('prodfaqs/prodfaqs_products'), 'product_id')
    		   ->where('faqs_id = ?', $this->getId())
    		   ->distinct();
    	return $resource->getReadConnection()->fetchCol($select);
    }

    public function getFaqIdsByProducts(array $productIds)
    {
        $resource = $this->getResource();
        $select = $resource->getReadConnection()->select();
        $select->from($resource->getTable('prodfaqs/prodfaqs_products'), 'faqs_id')
            ->where('product_id IN(?)', $productIds)
            ->distinct();
        return $resource->getReadConnection()->fetchCol($select);
    }
    
    protected function _afterSave()
    {
    	/*$helper = Mage::helper('vaxsearch');
    	try {
	    	$item = Vax_Search_Model_Indexer::getFaqForIndexing($this);
	    	$helper->index($item);
	    	$helper->optimizeIndex();
    	} catch (Exception $e) {
    		Mage::logException($e);
    	}*/
    }

    public function getTopicsByProduct($productId, $publicOnly = true, $questionType = null)
    {
        $resource = $this->getResource();
        $select = $resource->getReadConnection()->select();
        $select->from(array('rel' => $resource->getTable('prodfaqs/prodfaqs_products')), 'product_id')
            ->join(array('faq' => $resource->getTable('prodfaqs/prodfaqs')), 'rel.faqs_id=faq.faqs_id AND rel.product_id=' . (int)$productId . ' AND faq.status=1', 'faq.faqs_id')
            ->join(array('topic' => $resource->getTable('prodfaqs/topic')), 'faq.topic_id=topic.topic_id', array('topic_title' => 'topic.title', 'topic_id' => 'topic.topic_id'));
        if ($publicOnly || !Mage::helper('vaxfaqs')->isPrivateMode()) {
            $select->where('faq.visibility=?', 'public');
        }
        if ($questionType) {
            $questionType = (array)$questionType;
            $select->where('question_type IN(?)', $questionType);
        }
        $select->group('topic.topic_id')
               ->order('topic.topic_order DESC');
        return $resource->getReadConnection()->fetchAll($select);
    }

    public function getFaqsByTopic($topicId, $publicOnly = true, $questionType = null, $productId = null)
    {
        $collection = Mage::getResourceModel('prodfaqs/prodfaqs_collection')
                        ->addFieldToFilter('status', 1)
                        ->addFieldToFilter('topic_id', (int)$topicId);
        if ($publicOnly || !Mage::helper('vaxfaqs')->isPrivateMode()) {
            $collection->addFieldToFilter('visibility', 'public');
        }
        if ($questionType) {
            $questionType = (array)$questionType;
            $collection->addFieldToFilter('question_type', array('in' => $questionType));
        }
        if ($productId) {
            $collection->join(array('prod' => 'prodfaqs/prodfaqs_products'), 'prod.faqs_id=main_table.faqs_id AND prod.product_id=' . (int)$productId);
        }
        return $collection;
    }

    public function getCategoryFaqs($category = null)
    {
        if (null == $category) {
            $category = Mage::registry('current_category');
        }
        if (!$category instanceof Mage_Catalog_Model_Category) {
            return new Varien_Data_Collection();
        }
        $faqIds = $this->getFaqIdsByProducts(Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($category)->getAllIds());
        $collection = Mage::getResourceModel('prodfaqs/prodfaqs_collection')->addFieldToFilter('faqs_id', array('in' => $faqIds));
        $collection->join(array('topic' => 'prodfaqs/topic'), 'main_table.topic_id=topic.topic_id', array('topic_title' => 'topic.title'));
        $collection->setOrder('topic.topic_order', 'DESC');
        $collection->setOrder('main_table.faq_order', 'DESC');
        return $collection;
    }

    public function getData($key = '', $index = null) {
        if ($key == 'faq_answar' && Mage::app()->getStore()->getId() != Mage_Core_Model_App::ADMIN_STORE_ID) {
            $_filter = Mage::getModel('widget/template_filter');
            return $_filter->filter($this->_getData($key));
        }
        return parent::getData($key, $index);
    }
}