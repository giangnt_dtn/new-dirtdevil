<?php

/**
 * Vax Checkout Onepage Content Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Steps_Content extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the classes for a step.
     *
     * @param integer $index
     *
     * @return string
     */
    public function getStepClasses($index)
    {
        $classes  = 'step';
        $classes .= ' step-'.$index;

        if ($index == $this->getStepIndex()) {
            $classes .= ' step-active';
        } else {
            $classes .= ' step-inactive';
        }

        return $classes;
    }
}