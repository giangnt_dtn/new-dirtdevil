<?php
class Vax_Widget_Block_Widget_Products
                extends Mage_Catalog_Block_Product_List
                implements Mage_Widget_Block_Interface
{
    
    /**
     * Parses category set by widget helper and sets proper category id (int)
     */
    protected function _assignCategory()
    {
        $categoryArr = explode('/', $this->getCategoryId());
        if (is_array($categoryArr) && count($categoryArr)) {
            $categoryId = (int)$categoryArr[1];
            $this->setCategoryId($categoryId);
        }
    }
    
    /**
     * Returns selected category
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        return Mage::getModel('catalog/category')->load($this->getCategoryId());
    }
    
    /**
     * Depending on settings return either category name, custom title or null
     * @return string
     */
    public function getCategoryTitle() {
        switch($this->getData('use_category_title')) {
            case 2:
                return $this->getCategory()->getName();
                break;
            case 1:
                return $this->getData('category_title');
                break;
            default:
                return null;
        }
    }
    
    protected function _beforeToHtml() {
        $this->_assignCategory();
        parent::_beforeToHtml();
        $this->setTemplate('catalog/product/list_de.phtml');
        
    }
}