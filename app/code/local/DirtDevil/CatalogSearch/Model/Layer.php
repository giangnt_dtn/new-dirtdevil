<?php
/**
 * Vax Limited
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
 **/
class DirtDevil_CatalogSearch_Model_Layer extends Mage_CatalogSearch_Model_Layer
{
    /**
     * Retrieves The Current Product Collection.
     *
     * @return array
     */
    public function getProductCollection()
    {
        $collection = parent::getProductCollection();
        return $this->applyPriceFilter($collection);
    }
    /**
     * Adds min and max price filters if AJAX price slider is enabled
     *
     * @param $collection
     * @return mixed
     */
    public function applyPriceFilter($collection)
    {
        /* fetch and apply min and max price filter */
        if(Mage::helper('catalog/category')->useAjaxPriceSlider()) {
            $max = $this->getMaxPriceFilter();
            $min = $this->getMinPriceFilter();
            if($min && $max) {
                $collection->getSelect()->where('min_price >= "'.$min.'" AND max_price <= "'.$max.'" ');
            }
        } else {
            $this->prepareProductCollection($collection);
        }
        return $collection;
    }
    /**
     * Return Max Price Filter.
     *
     * @return boolean|integer
     */
    public function getMaxPriceFilter()
    {
        if (isset($_GET) && isset($_GET['max']) && is_numeric($_GET['max'])) {
            return round($_GET['max']);
        }
        return false;
    }
    /**
     * Return Min Price Filter.
     *
     * @return boolean|integer
     */
    public function getMinPriceFilter()
    {
        if (isset($_GET) && isset($_GET['min']) && is_numeric($_GET['min'])) {
            return round($_GET['min']);
        }
        return false;
    }
    public function getAllAttributes()
    {
        $collection = Mage::getResourceModel('catalog/product_attribute_collection');
        $collection
            ->setItemObjectClass('catalog/resource_eav_attribute')
            ->addStoreLabel(Mage::app()->getStore()->getId());
        $collection = $this->_prepareAttributeCollection($collection);
        $collection->load();
        return $collection;
    }
}
