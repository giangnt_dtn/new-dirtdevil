<?php
/**
 * Layer Model.
 *
 * @copyright Vax Ltd 2013
 */
class DirtDevil_Catalog_Model_Layer extends Mage_Catalog_Model_Layer
{
    /**
     * Retrieves The Current Product Collection.
     *
     * @return array
     */
    public function getProductCollection()
    {
        $collection = $this->applyPriceFilter(parent::getProductCollection());
        $collection = $this->applyRelatedFilter($collection);
        $collection = $this->applySnonymFilter($collection);

        return $collection;
    }

    /**
     * Adds min and max price filters if AJAX price slider is enabled
     *
     * @param $collection
     * @return mixed
     */
    public function applyPriceFilter($collection)
    {
        /* fetch and apply min and max price filter */
        if(Mage::helper('catalog/category')->useAjaxPriceSlider()) {
            $max = $this->getMaxPriceFilter();
            $min = $this->getMinPriceFilter();

            if($min && $max) {
                $collection->getSelect()->where('min_price >= "'.$min.'" AND max_price <= "'.$max.'" ');
            }
        } else {
            $this->prepareProductCollection($collection);
        }
        return $collection;
    }

    /**
     * Adds related product item filter
     *
     * @param $collection
     * @return mixed
     */
    public function applyRelatedFilter(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $additionalProductIds = false;

        /* fetch and apply related product IDs */
        if(Mage::registry('spares_product')) {
            $sparesProduct = Mage::getModel('catalog/product')->load(Mage::registry('spares_product'));

            if(!$sparesProduct) {
                return $collection;
            }

            $additionalProductIds = $sparesProduct->getRelatedProductIds();

            // also check for synoymns
            $synomns = Mage::helper('dirtdevil_spares')->getSpareMatchingSynonym($sparesProduct->getSku());

            if($synomns && $synomns->count() >= 1) {
                $additionalProductIds = array_unique(array_merge($additionalProductIds, $synomns->getAllIds()));
            }
        } elseif (Mage::registry('additional_spare_ids')) {
            $additionalProductIds = Mage::registry('additional_spare_ids');
        }

        if ($additionalProductIds) {
            $collection->addAttributeToFilter('entity_id', array('in' => $additionalProductIds));
        }

        return $collection;
    }

    /**
     * Adds related product item synonym filter where the machine does not exist
     *
     * @param $collection
     * @return mixed
     */
    public function applySnonymFilter($collection)
    {
        /* fetch and apply related product IDs */
        if(Mage::registry('spare_synonym_match')) {
            $productCollection = Mage::helper('dirtdevil_spares')->getSpareMatchingSynonym(Mage::registry('spare_synonym_match'));

            if(!$productCollection || ($productCollection && $productCollection->count() === 0)) {
                return $collection;
            }

            $productCollectionIds = $productCollection->getAllIds();

            if(!$productCollectionIds) {
                return $collection;
            }

            $collection->addAttributeToFilter('entity_id', array('in' => $productCollectionIds));
        }
        return $collection;
    }

    /**
     * Return Max Price Filter.
     *
     * @return boolean|integer
     */
    public function getMaxPriceFilter()
    {
        if (isset($_GET) && isset($_GET['max']) && is_numeric($_GET['max'])) {
            return round($_GET['max']);
        }

        return false;
    }

    /**
     * Return Min Price Filter.
     *
     * @return boolean|integer
     */
    public function getMinPriceFilter()
    {
        if (isset($_GET) && isset($_GET['min']) && is_numeric($_GET['min'])) {
            return round($_GET['min']);
        }

        return false;
    }

    public function getAllAttributes()
    {
        $collection = Mage::getResourceModel('catalog/product_attribute_collection');
        $collection
            ->setItemObjectClass('catalog/resource_eav_attribute')
            ->addStoreLabel(Mage::app()->getStore()->getId());
        $collection = $this->_prepareAttributeCollection($collection);
        $collection->load();
        return $collection;
    }
}