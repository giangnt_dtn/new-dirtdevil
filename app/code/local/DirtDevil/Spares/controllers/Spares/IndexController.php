<?php
/**
 * Category controller
 *
 * @category   Vax
 * @package    Vax_Catalog
 * @author     Vax
 */
class DirtDevil_Spares_Spares_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * sku action - returns array of related products to a particular product
     *
     * Responds to AJAX (XmlHttpRequest)
     */
    public function skuAction() {

        $sku = $this->getRequest()->getParam('sku');
        $categoryId = (int)$this->getRequest()->getParam('category');
        $sortOrder = $this->getRequest()->getParam('dir');
        $order = $this->getRequest()->getParam('order');

        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = $this->getSkusMatching($sku);

        if ($order && $sortOrder) {
            $productCollection->setOrder($order, $sortOrder);
        }

        // unset values
        Mage::unregister('spares_product');
        Mage::unregister('spares_product_sku');
        Mage::unregister('spare_synonym_match');

        // nothing found
        if(!$productCollection || ($productCollection && $productCollection->count() === 0)) {

            // check for spares anyway (in snonyms) in case the main machine has never been added
            $productCollection = Mage::helper('dirtdevil_spares')->getSpareMatchingSynonym($sku);

            // we found at least one snonym
            if($productCollection && $productCollection->count() >= 1) {

                Mage::register('spare_synonym_match', $sku);

                $list_block = Mage::app()->getLayout()->createBlock('catalog/product_list');
                $list_block->setCategoryId($categoryId)->setTemplate('catalog/product/list.phtml');

                $data = array(
                    'error' => null,
                    'productlist' => $list_block->toHtml(),
                );

                $this->getResponse()->setBody(json_encode($data));
                return true;
            }

            $data = array(
                'error' => $this->__("Sorry, no results were found for your search. Please try again or contact <a href=\"/contacts\">Dirt Devil</a>."),
                'productlist' => null,
            );

            $this->getResponse()->setBody(json_encode($data));
            return false;
        }

        // more than 1 found, give the user a choice
        if($productCollection->count() > 1) {
            $relatedProducts = Mage::helper('dirtdevil_spares')->getRelatedProducts($productCollection);
            if ($relatedProducts->count()) {
                Mage::register('additional_spare_ids', $relatedProducts->getAllIds());

                $list_block = Mage::app()->getLayout()->createBlock('catalog/product_list');
                $list_block->setCategoryId($categoryId)->setTemplate('catalog/product/list.phtml');

                $data = array(
                    'error' => null,
                    'productlist' => $list_block->toHtml(),
                );

                $this->getResponse()->setBody(json_encode($data));
                return true;
            }

            $data = array(
                'error' => $this->__("Sorry, more than one model number is found. Please try again or contact <a href=\"/contacts\">Dirt Devil</a>."),
                'productlist' => null,
            );

            // Return JSON encoded data
            $this->getResponse()->setBody(json_encode($data));

            return false;
        }

        // only 1 returned, let's do what we are supposed to do now
        $product = Mage::getModel('catalog/product')->load($productCollection->getFirstItem()->getId());
        if($product && $product->getRelatedProductIds()) {

            Mage::register('spares_product', $product->getId());
            Mage::register('spares_product_sku', $product->getSku());

            $list_block = Mage::app()->getLayout()->createBlock('catalog/product_list');
            $list_block->setCategoryId($categoryId)->setTemplate('catalog/product/list.phtml');

            $data = array(
                'error' => null,
                'productlist' => $list_block->toHtml(),
            );

        } else {

            $data = array(
                'error' => $this->__("Sorry, no results were found for your search. Please try again or contact <a href=\"/contacts\">Dirt Devil</a>."),
                'productlist' => null,
            );
        }

        // Return JSON encoded data
        $this->getResponse()->setBody(json_encode($data));
    }

    /**
     * get a sku matching
     *
     * Returns @collection of products
     */
    public function getSkusMatching($sku) {

        if(!$sku) {
            return false;
        }

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter(
                array(
                    array("attribute" => 'product_type_value', 'eq' => "Machine")
                ))
            ->addStoreFilter(Mage::app()->getStore()->getId());

        preg_match('/[0-9]+/', $sku, $matches);

        if (is_array($matches) && $matches[0]) {
            $productCollection->addAttributeToFilter(
                array(
                    array("attribute" => 'sku', 'like' => "%" . $matches[0] . "%"),
                ));
        } else {
            $productCollection->addAttributeToFilter(
                array(
                    array("attribute" => 'sku', 'eq' => "$sku"),
                ));
        }

        $productCollection->addAttributeToFilter('status', 1);

        // Group product by entity id, this stops duplicates!
        $productCollection->getSelect()->group('entity_id');
        $productCollection->load();

        if(!$productCollection || ($productCollection && $productCollection->count() === 0)) {
            $productCollection = Mage::helper('dirtdevil_spares')->getMachineMatchingSynonym($sku);
        }

        return $productCollection;
    }
}
