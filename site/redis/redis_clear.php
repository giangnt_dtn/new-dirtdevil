<?php
/**
 * Vax Redis Cache Clear Script
 *
 * @author ccarnell
 */
require_once 'app/Mage.php';
require_once 'Zend/Cache.php';
require_once "lib/Cm/Cache/Backend/Redis.php";

class redis_clear {

    // settings for ip address of redis server
    const XML_PATH_CACHE_BACKEND_OPTIONS_SERVER             = 'global/cache/backend_options/server';
    const XML_PATH_CACHE_BACKEND_OPTIONS_PORT               = 'global/cache/backend_options/port';
    const XML_PATH_CACHE_BACKEND_OPTIONS_DB                 = 'global/cache/backend_options/database';

    // fpc
    const XML_PATH_FULL_PAGE_CACHE_BACKEND_OPTIONS_SERVER   = 'global/full_page_cache/backend_options/server';
    const XML_PATH_FULL_PAGE_CACHE_BACKEND_OPTIONS_PORT   = 'global/full_page_cache/backend_options/port';
    const XML_PATH_FULL_PAGE_CACHE_BACKEND_OPTIONS_DB   = 'global/full_page_cache/backend_options/db';

    // our instances of the redis client, one for cache and another for FPC
    protected $_cache_instance;
    protected $_fpc_instance;

    /*
     * Setup redis connection options
     */
    public function __construct() {

        umask(0);

        // start up magento!
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $cache_ip = Mage::getConfig()->getNode(self::XML_PATH_CACHE_BACKEND_OPTIONS_SERVER);
        $cache_port = Mage::getConfig()->getNode(self::XML_PATH_CACHE_BACKEND_OPTIONS_PORT);
        $cache_db = Mage::getConfig()->getNode(self::XML_PATH_CACHE_BACKEND_OPTIONS_DB);

        $fpc_cache_ip = Mage::getConfig()->getNode(self::XML_PATH_FULL_PAGE_CACHE_BACKEND_OPTIONS_SERVER);
        $fpc_cache_port = Mage::getConfig()->getNode(self::XML_PATH_FULL_PAGE_CACHE_BACKEND_OPTIONS_PORT);
        $fpc_cache_db = Mage::getConfig()->getNode(self::XML_PATH_FULL_PAGE_CACHE_BACKEND_OPTIONS_DB);

        if(!empty($cache_ip)) {
            $this->_cache_instance = new Cm_Cache_Backend_Redis(array(
                'server' => $cache_ip,
                'port' => $cache_port,
                'database' => $cache_db,
                'notMatchingTags' => TRUE,
                'force_standalone' => false,
                'compress_threshold' => 100,
                'compression_lib' => 'lzf',
            ));
        }
        
        if(!empty($fpc_cache_ip)) {
            $this->_fpc_instance = new Cm_Cache_Backend_Redis(array(
                'server' => $fpc_cache_ip,
                'port' => $fpc_cache_port,
                'database' => $fpc_cache_db,
                'notMatchingTags' => TRUE,
                'force_standalone' => false,
                'compress_threshold' => 100,
                'compression_lib' => 'lzf',
            ));
        }
    }
    
    /*
     * Clear the redis cache
     */
    public function run() {

        try {
            if($this->_cache_instance) {
                $this->_cache_instance->clean(Zend_Cache::CLEANING_MODE_ALL);
                echo "Redis - Cache cleared\n";
            }
            
            if($this->_fpc_instance) {
                $this->_fpc_instance->clean(Zend_Cache::CLEANING_MODE_ALL);
                echo "Redis - FPC cleared\n";
            }
            
        } catch (Exception $e) {
            echo "Unable to clear cache - " . $e->getMessage();
        }
    }
    
    /**
     * Get the usage string
     * 
     * @return string
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f redis_clear.php
    
    Do the 'help' command to see the list of available commands

USAGE;
    }
}

$shell = new redis_clear();
$shell->run();

?>
