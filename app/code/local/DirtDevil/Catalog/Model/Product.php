<?php

/**
 * DirtDevil Catalog product model
 *
 * @method Mage_Catalog_Model_Resource_Product getResource()
 * @method Mage_Catalog_Model_Resource_Product _getResource()
 *
 * @category   DirtDevil
 * @package    DirtDevil_Catalog
 * @author     Vax
 */
class DirtDevil_Catalog_Model_Product extends Mage_Catalog_Model_Product
{
    const ATTR_SET_FEES_AND_WARRANTY = "Fees And Warranty";
    const ATTR_SET_ACCESSORY         = "Spares and accessories";
    const Vax_SA_Group = "Service Agents";

    const AVAILABILITY_RESTRICTION_NONE = 0;
    const AVAILABILITY_RESTRICTION_DISCONTINUED = 1;
    const AVAILABILITY_RESTRICTION_NOT_SALABLE = 2;
    
    /** @var  string */
    protected $_attributeSetName;
    
    /** @var  Mage_Eav_Model_Entity_Attribute_Set */
    protected $_attributeSetModel;

    /**
     * Returns whether or not it is the main product on the page.
     *
     * @return boolean
     */
    public function isMainProduct($product)
    {
        return Mage::registry('current_product') && Mage::registry('current_product')->getId() == $product->getId();
    }

    /**
     * Returns if it is a category page or not.
     *
     * @return boolean
     */
    public function isCategoryPage()
    {
        return Mage::app()->getFrontController()->getRequest()->getControllerName() == 'category';
    }

    /**
     * Returns the sku
     *
     * @return string
     */
    public function getUkProductNo() {

        if(!$this->getData("uk_product_no")) {
            return $this->getSku();
        }

        return $this->getData("uk_product_no");
    }

    /**
     * This function is the result of updating the 'inventory_discontinued' attribute
     * to accept stricter rule for preventing products from being sold. It checks
     * if the product should be salable at all based on setting stored in
     * 'inventory_discontinued' attribute.
     *
     * @return bool
     */
    public function isSaleDisabled()
    {
        if($this->getData("inventory_discontinued") && $this->getData("inventory_discontinued") == $this->getAvailabilityRestrictionNotSalable()) {
            return true;
        }

        if($this->isComingSoon()) {
            return true;
        }

        return false;
    }

    public function isComingSoon() {

        if($this->getData("inventory_discontinued") && $this->getData("inventory_discontinued") == $this->getAvailabilityRestrictionComingSoon()) {
            return true;
        }

        return false;
    }

    /*
     * Check if the product is allowed to be sold
     * @return bool
     */
    public function isSaleable() {

        if($this->getData("inventory_discontinued") && ($this->getData("inventory_discontinued") == $this->getAvailabilityRestrictionNotSalable() ||
            $this->getData("inventory_discontinued") == $this->getAvailabilityRestrictionDiscontinued())) {
            return false;
        }

        return parent::isSaleable();
    }

    /* Helpful getters */
    private function getAvailabilityRestrictionNone() {
        return Mage::getStoreConfig('disable_checkout/product_availability/avail_restr_none_attr_id');
    }

    private function getAvailabilityRestrictionDiscontinued() {
        return Mage::getStoreConfig('disable_checkout/product_availability/avail_restr_disc_attr_id');
    }

    private function getAvailabilityRestrictionNotSalable() {
        return Mage::getStoreConfig('disable_checkout/product_availability/avail_restr_not_sal_attr_id');
    }

    private function getAvailabilityRestrictionComingSoon() {
        return Mage::getStoreConfig('disable_checkout/product_availability/avail_restr_soon_attr_id');
    }

    /**
     * Checks whether the product is a machine (need this for integration on uk)
     *
     * @return boolean
     */
    public function isUkMachine()
    {
        $attributeSetName = $this->getAttrSetName();
        
        if (false !== stripos($attributeSetName, 'spare')) {
            return false;
        }

        return true;
    }

    /**
     * Checks whether the product is a bundle.
     *
     * @return boolean
     */
    public function isBundle()
    {
        return $this->getTypeId() == "bundle";
    }

    /**
     * Checks whether the product is an instalment.
     *
     * @return boolean
    */
    public function isInstalment()
    {
        if (false !== stripos($this->getName(), 'Easy Pay')) {
            return true;
        }

        return false;
    }

    /**
     * Indicates whether the product is an accessory.
     *
     * @return boolean
     */
    public function isAccessory()
    {
        if (false !== stripos($this->getAttrSetName(), self::ATTR_SET_ACCESSORY)) {
            return true;
        }

        return false;
    }

    /**
     * Is this product RTB Only?
     *
     * @return bool
     */
    public function isRTBOnly()
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {

            /*
             * Customer is logged in on the front end so now check to
             * see if they are a CSA or an SA
             */
            $group_o = Mage::getModel('customer/group')
                ->load(Mage::getSingleton('customer/session')
                    ->getCustomerGroupId());

            /*
             * SA can buy RTB only, CSA and Joe Public cannot
             */
            switch($group_o->getCode()) {

                case DirtDevil_Catalog_Model_Product::Vax_SA_Group:
                    return false;
                    break;

                default;
                    return $this->getData('inventory_rtb_only');
                    break;
            }
        } else {
            // Not logged in so Joe Public
            return $this->getData('inventory_rtb_only');
        }
    }

    /**
     * Indicates whether the product is a fee.
     *
     * @return boolean
     */
    public function isFee()
    {
        return false !== stripos($this->getAttrSetName(), self::ATTR_SET_FEES_AND_WARRANTY);
    }

    /**
     * Is this product Discontinued?
     *
     * @return bool
     */
    public function isDiscontinued()
    {
        $prodStock_o = Mage::getModel('cataloginventory/stock_item')->loadByProduct($this);
        $stockState = $prodStock_o->getIsInStock();
        $discontinued = $this->getData('inventory_discontinued');

        return ($stockState == Mage_CatalogInventory_Model_Stock_Status::STATUS_OUT_OF_STOCK) && ($discontinued == self::AVAILABILITY_RESTRICTION_DISCONTINUED);
    }

    /**
     * Checks whether the product has the required product attribute
     *
     *  @var String - attribute ID
     *  @return bool
     */
    public function hasAttribute($codeString) {
        $entity = 'catalog_product';
        $code = $codeString;
        $attr = Mage::getResourceModel('catalog/eav_attribute')
            ->loadByCode($entity,$code);

        if ($attr->getId()) {
            return true;
        }
        return false;
    }

    /**
     * Returns the name of the attribute set that product resides in.
     *
     * @return string
     */
    public function getAttrSetName()
    {
        if (is_null($this->_attributeSetName)) {
            $this->_attributeSetName = $this->getAttributeSetModel()->getAttributeSetName();
        }

        return $this->_attributeSetName;
    }
    /**
     * Returns attribute set model of the product.
     *
     * @return Mage_Eav_Model_Entity_Attribute_Set
     */
    public function getAttributeSetModel()
    {
        if (is_null($this->_attributeSetModel)) {
            $this->_attributeSetModel = Mage::getModel('eav/entity_attribute_set')->load($this->getAttributeSetId());
        }

        return $this->_attributeSetModel;
    }
}
