<?php

/**
 * OPC Order Success Response.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Response_Opc_Order_Success extends Vax_Checkout_Model_Response_Opc_Order
{
    /**
     * {@inheritdoc}
     */
    public function getOpcResponse()
    {
        $response = array(
            'success' => array('order' => array(
                'id'  => $this->_order->getIncrementId(),
            ))
        );

        $objResponse = new Varien_Object($response);
        Mage::dispatchEvent('opc_before_success_response', array('response' => $objResponse));

        return $objResponse->getData();
    }
}