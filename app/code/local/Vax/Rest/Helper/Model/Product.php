<?php

/**
 * Vax Rest Product Helper.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Rest_Helper_Model_Product extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieves a single product.
     *
     * @param integer $id      Product ID
     * @param integer $storeID Store ID
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct($id, $storeID)
    {
        $product = Mage::getModel('catalog/product')
            ->setStoreId($storeID)
            ->load($id);

        if ( ! $product->getId()) {
            throw new Mage_Api2_Exception('Product does not exist', '404');
        }

        return $product;
    }
}