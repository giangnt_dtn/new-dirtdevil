jQuery(document).ready(function(){

    //Slider settings
    sliderClickThroughButtons();
    sliderButtonsWidth();
    if(jQuery(window).width() < 960) {
        //keep slider image ratio when the slider is full width
        sliderHeightResize(620, 300, 130);
    } else {
        //save vertical space for the slider on load
        var sliderMaxHeight = 300;
        jQuery(".banner").css("height", sliderMaxHeight);
        jQuery(".banner ul").css("height", sliderMaxHeight);
        jQuery('.banner-content').css("height", sliderMaxHeight);
    }
    jQuery('.banner .banner-content').setAllToMaxHeight();
    jQuery('.banner .banner-content').css("height", 300);
    jQuery('div.homepage-banner div.cta-block .btn-cart').on('click touchend',function(e){
        jQuery(this).addClass('clicked');
    });
    jQuery('.banner').css({overflow: 'visible'});

    // Home Bestsellers carousel
    var bestsellers = '.bestsellers ul.mini-products-list';
    jQuery('.bestsellers .product-name').setAllToMaxMinHeight();

    enquire.register('screen and (min-width: ' + 481 + 'px) and (max-width: ' + 770 + 'px)', {
        match: function () {
            moreViewsThumbnails(4, bestsellers, 1);
        }, unmatch: function () {}
    });
    enquire.register('screen and (min-width: ' + 771 + 'px) and (max-width: ' + 1000 + 'px)', {
        match: function () {
            //bestsellers carousel, 3 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(3, bestsellers, 1);
        }, unmatch: function () {}
    });
    enquire.register('screen and (min-width: ' + 1001 + 'px)', {
        match: function () {
            //bestsellers carousel, 4 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(4, bestsellers, 1);
        }, unmatch: function () {}
    });
});

function resizeSliders()
{
    jQuery('.banner.has-dots, div .banner-container ul').css('height', (jQuery('.banner-image:visible').innerHeight() + jQuery('.banner-content').innerHeight()) + 'px');
}

jQuery(window).load(function() {
    resizeSliders();
});

jQuery(window).on('resize', function() {
    setTimeout(resizeSliders, 300);
});
