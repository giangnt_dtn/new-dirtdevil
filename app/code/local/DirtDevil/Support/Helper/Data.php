<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevil_Support_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Retrieves user guides for the given products.
     *
     * @param $products
     * @param string $lang
     * @return array
     */
    public function getUserguidesForProducts($products, $lang = 'English') {
        $guidesData = array();

        if (!Mage::helper('core')->isModuleEnabled('Vax_Document')) {
            return $guidesData;
        }

        foreach ($products as $_product) {
            $guidesData[] = $this->getUserguidesForProduct($_product, $lang);
        }

        return $guidesData;
    }

    /**
     * Retrieves user guides for the given product.
     *
     * @param $product
     * @param string $lang
     * @return array
     */
    public function getUserguidesForProduct($product, $lang = 'English') {
        if(!$product) {
            return false;
        }

        if (!Mage::helper('core')->isModuleEnabled('Vax_Document')) {
            return false;
        }

        $documents = Mage::helper('vax_document')->getDocuments($product->getSku(), $lang);
        $guidesData = array();

        foreach ($documents as $document) {
            if(!is_array($document) || is_array($document) && $document['fields']['document_type'][0] !== 'User Guide') {
                continue;
            }

            $guidesData[] = array(
                'title' => "User Guide",
                'sku' => $product->getSku(),
                'link' => "/document/document/retrieve?id=".$document['_id']."&sku=".$product->getSku(),
                'resourceid' => !empty($_doc['_id']) ? $_doc['_id'] : $product->getSku()
                //'link' => Mage::getUrl('document/userguide/retrieve', array('sku' => $product->getSku()))
            );
        }

        return $guidesData;
    }

    /**
     * Formats the link for a document.
     *
     * @param mixed $document
     *
     * @return string
     */
    public function getUserguideLink($document)
    {
        if (!Mage::helper('core')->isModuleEnabled('Vax_Document')) {
            return false;
        }

        if(!$document) {
            return false;
        }

        return $document['link'];
    }
}
