<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Checkout_Model_Onepage_Rwd_Steps extends Vax_Checkout_Model_Onepage_Steps
{
    /**
     * Initialize the $_steps array
     */
    protected function _construct() {

        parent::_construct();

        $this->_steps = array(
            array(
                'index'      => 1,
                'title'      => 'About You',
                'buttonTitle'      => 'About You',
                'summary'    => 'Enter your details',
                'htmlClass'  => 'contact-details',
                'blockClass' => 'contact_details',
                'alias'      => 'contact'
            ),
            array(
                'index'     => 2,
                'title'     => 'Delivery',
                'buttonTitle'   => 'Delivery Details',
                'summary'   => 'Enter your delivery address',
                'htmlClass' => 'delivery-details',
                'blockClass' => 'delivery_details',
                'alias'     => 'delivery',
            ),
            array(
                'index'     => 3,
                'title'     => 'Billing & payment',
                'buttonTitle'     => 'Billing & Payment',
                'summary'   => 'Enter your billing address',
                'htmlClass' => 'billing-payment-details',
                'blockClass' => 'billing_payment_details',
                'alias'     => 'billing'
            ),
        );

        return $this;
    }
}