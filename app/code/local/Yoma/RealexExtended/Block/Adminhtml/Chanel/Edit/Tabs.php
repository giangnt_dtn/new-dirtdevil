<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("chanel_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("realexExtended")->__("Advanced Fraud Rule"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("realexExtended")->__("Pre-Authoirsation Checks"),
            "title" => Mage::helper("realexExtended")->__("Pre-Authoirsation Checks"),
            "content" => $this->getLayout()->createBlock("realexExtended/adminhtml_chanel_edit_tab_form")->toHtml(),
        ));

        $this->addTab("post_section", array(
            "label" => Mage::helper("realexExtended")->__("Post Authorisation Checks"),
            "title" => Mage::helper("realexExtended")->__("Post Authorisation Checks"),
            "content" => $this->getLayout()->createBlock("realexExtended/adminhtml_chanel_edit_tab_post")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
