<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevil_Cms_Block_Block extends Mage_Cms_Block_Block
{
    /**
     * Allows for storing separate cache blocks for different store views.
     *
     * @return string
     */
    public function getCacheKey()
    {
        $cacheKey = parent::getCacheKey();
        return $cacheKey . '_' . Mage::app()->getStore()->getId();
    }
}