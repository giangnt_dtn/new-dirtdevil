<?php

/**
 * Vax Checkout Onepage Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage extends Mage_Checkout_Block_Onepage
{
    /**
     * Cached Quote.
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote;

    /**
     * Cache for steps singleton
     *
     * @var Vax_Checkout_Model_Onepage_Steps
     */
    protected $_stepsModel;

    /**
     * Returns the current checkout step index.
     *
     * @return integer
     */
    public function getStepIndex()
    {
        if ($this->hasData('step_index')) {
            return $this->getData('step_index');
        }

        $index = 1;

        if ($this->hasOrderFailed() || $this->hasOrderProcessed()) {
            $index = $this->getNumberOfSteps();
        }

        return $index;
    }

    /**
     * Checks whether we are on the first step.
     *
     * @return integer
     */
    public function isFirstStep()
    {
        return $this->getStepIndex() == 1;
    }

    /**
     * Checks whether we are on the last step.
     *
     * @return integer
     */
    public function isLastStep()
    {
        return $this->getStepIndex() == $this->getNumberOfSteps();
    }

    /**
     * Checks whether the order has processed.
     *
     * @return boolean
     */
    public function hasOrderProcessed()
    {
        if ($this->getCheckout()->getProcessedOrderId()) {
            return true;
        }

        return false;
    }

    /**
     * Checks whether the order has failed.
     *
     * @return boolean
     */
    public function hasOrderFailed()
    {
        if ($this->getCheckout()->getFailedOrderId()) {
            return true;
        }

        return false;
    }

    /**
     * Returns the last response.
     *
     * @return Vax_Checkout_Model_Response_Opc
     */
    public function getLastResponse()
    {
        return $this->getCheckout()->getLastResponse();
    }

    /**
     * Returns the number of steps.
     *
     * @return integer
     */
    public function getNumberOfSteps()
    {
        return count($this->getSteps());
    }

    /**
     * Returns the checkout steps.
     *
     * @return array
     */
    public function getSteps()
    {
        if (Mage::helper('vax_checkout')->isMagentoCheckout()) {
            return parent::getSteps();
        }
        return $this->getStepsModel()->getSteps();
    }

    /**
     * Returns the current quote.
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if ( ! $this->_quote){
            $this->_quote = $this->_getQuote();
        }

        return $this->_quote;
    }

    /**
     * Returns the current quote.
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        if ($this->getOrder() && $this->getOrder()->getQuote()) {
            return $this->getOrder()->getQuote();
        }

        if (Mage::getSingleton('checkout/session')->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
            return Mage::getModel('sales/quote')->load($order->getQuoteId());
        }

        if ($this->getCheckout()->getQuote()) {
            return $this->getCheckout()->getQuote();
        }

        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * Returns a completed order.
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if ($this->hasOrderProcessed()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($this->getCheckout()->getProcessedOrderId());

            if (!$order->getId()) {
                $order = Mage::getModel('sales/order')->load($this->getCheckout()->getProcessedOrderId());
            }

            return $order;
        }

        return $this->getCheckout()->getLastOrder();
    }

    /**
     * Returns the current order number.
     *
     * @return string
     */
    public function getOrderNumber()
    {
        $orderNumber = '';

        if ($this->getOrder()) {
            $orderNumber = $this->getOrder()->getIncrementId();
        }

        return $orderNumber;
    }

    /**
     * Returns the current confirmation title.
     *
     * @return string
     */
    public function getConfirmationTitle()
    {
        if ($this->hasOrderFailed()) {
            return $this->__('Order Failed');
        }

        return $this->__('Order Complete');
    }

    /**
     * Returns the current confirmation summary.
     *
     * @return string
     */
    public function getConfirmationSummary()
    {
        if ($this->hasOrderFailed()) {
            return $this->__('Your card has not been charged');
        }

        return $this->__('Your unique order number is: ').$this->getOrderNumber();
    }

    /**
     * Removes step specified by alias
     *
     * @param string $alias
     * @return $this
     */
    public function removeStep($alias)
    {
        return $this->getStepsModel()->removeStep($alias);
    }

    /**
     * Adds new checkout step
     *
     * @param string  $alias
     * @param string $title
     * @param string $summary
     * @param string $htmlClass
     * @param string $blockClass
     * @param string|null $before
     * @return $this
     */
    function addStep($alias, $title, $summary, $htmlClass, $blockClass, $displayHeader = 1, $before = null)
    {
        $step = array(
            'title'     => $title,
            'summary'   => $summary,
            'htmlClass' => $htmlClass,
            'blockClass' => $blockClass,
            'alias'     => $alias,
            'displayHeader' => (int)$displayHeader
        );
        return $this->getStepsModel()->addStep($step, $before);
    }

    /**
     * Gets model handling checkout steps.
     *
     * @return false|Mage_Core_Model_Abstract|Vax_Checkout_Model_Onepage_Steps
     */
    public function getStepsModel()
    {
        if (is_null($this->_stepsModel)) {
            $this->_stepsModel = Mage::getModel(Mage::getStoreConfig('checkout/options/steps_model'), array('onepage' => $this));

            // Fallback to default model
            if (!$this->_stepsModel instanceof Vax_Checkout_Model_Onepage_Steps) {
                $this->_stepsModel = Mage::getModel('vax_checkout/onepage_steps', array('onepage'   => $this));
            }
        }
        return $this->_stepsModel;
    }

    /**
     * Sets custom steps model.
     *
     * @param Vax_Checkout_Model_Onepage_Steps $stepModel
     */
    public function setStepsModel(Vax_Checkout_Model_Onepage_Steps $stepModel)
    {
        $this->_stepsModel = $stepModel;
    }

    /**
     * Returns the current shipping message block.
     *
     * @return mixed
     */
    public function getShippingMessage()
    {
        $blockID = 'shipping_message_'.Mage::app()->getStore()->getId();

        return Mage::app()->getLayout()
            ->createBlock('cms/block')
            ->setBlockId($blockID)->toHtml();
    }

    /**
     * Get step from model.
     *
     * @param $index
     * @return mixed
     */
    public function getStep($index) {
        return $this->getStepsModel()->getStep($index);
    }

    /**
     * Return specific information from step definition.
     *
     * @param $key
     * @param int|null $index
     * @return string
     */
    public function getStepData($key, $index = null) {
        if (null == $index) {
            $index = $this->getStepIndex();
        }

        $step = $this->getStep($index);

        if (is_array($step) && array_key_exists($key, $step)) {
            return $step[$key];
        }

        return '';
    }

    /* todo: check where delivery date is stored */
    public function getDeliveryDate()
    {
        try {
//            return Mage::helper('vax_deliverydate')->getDeliveryDateForOrder($this->getOrder());
        } catch(Exception $e) {
            return false;
        }
    }
}