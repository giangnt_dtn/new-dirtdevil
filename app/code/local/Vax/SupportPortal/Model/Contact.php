<?php

/**
 * Contact Model.
 *
 * @copyright Vax Ltd
 */
class Vax_SupportPortal_Model_Contact extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vax_supportportal/contact');
    }
}