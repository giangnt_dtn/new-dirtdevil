<?php

/**
 * Product Category Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_Rest_Model_Catalog_Api2_Category_Rest_Admin_V1 extends Mage_Catalog_Model_Api2_Product_Category
{
    /**
     * Retrieves all product categories.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $data       = array();
        $categories = $this->_loadCollection();

        foreach ($categories as $category) {
            $data[] = $this->_buildCategoryResponse($category);
        }

        return $data;
    }

    /**
     * Retrieves the categories collection.
     *
     * @return array
     */
    protected function _loadCollection()
    {
        $collection = Mage::getResourceModel('catalog/category_collection');
        $collection->addNameToResult();
        $collection->addOrderField('entity_id');
        $collection->setStore($this->_getStore());

        return $collection->load();
    }

    /**
     * Builds a single category response.
     *
     * @param mixed $category
     *
     * @return array
     */
    protected function _buildCategoryResponse($category)
    {
        return array(
            'id'        => (int) $category->getId(),
            'parent_id' => $category->getParentId(),
            'name'      => $category->getName(),
            'products'  => $this->_buildActiveProducts($category),
        );
    }

    /**
     * Builds the active products within a category.
     *
     * @param mixed $category
     *
     * @return array
     */
    protected function _buildActiveProducts($category)
    {
        $products = array();

        foreach ($category->getProductCollection()->load() as $product) {
            $products[$product->getId()] = array(
                'id'                 => $product->getId(),
                'sku'                => $product->getSku(),
                'cat_index_position' => $product->getCatIndexPosition(),
            );
        }

        return $products;
    }
}