$j = jQuery.noConflict();

function updateDiscountTotalStyle() {
    if ($j(window).width()>=600) {
        $j('#discount-total').width($j('.cart-totals-wrapper').first().width()-30);
    } else {
        $j('#discount-total').width($j('#discount-total').parent().width()-40);
    }
}

$j(document).ready(function() {
    // calculate total discount width
   updateDiscountTotalStyle();
});
$j(window).on('delayed-resize', function (e, resizeEvent) {
    updateDiscountTotalStyle();
    setTimeout(updateDiscountTotalStyle, 500);
    setTimeout(updateDiscountTotalStyle, 1500);
    setTimeout(updateDiscountTotalStyle, 2500);
});
