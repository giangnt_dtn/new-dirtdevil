<?php

/**
 * Vax Onepage Checkout Controller.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_OnepageController extends Mage_Checkout_Controller_Action
{
    /**
     * Index Action.
     *
     * Displays the one page checkout.
     *
     * @return void
     */
    public function indexAction()
    {
        $checkout = $this->_getCheckout($this->_getCheckoutType());
        $checkout->reset();

        if ( ! Mage::helper('checkout/checkout')->canCheckout()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $this->_loadLayout();
    }

    /**
     * {@inheritdoc}
     */
    public function submitAction()
    {
        $this->getResponse()->setRawHeader('Access-Control-Allow-Headers: x-requested-with');
        try
        {
            if ( ! Mage::helper('checkout/checkout')->canCheckout()) {
                $this->_redirect('checkout/cart');
                return;
            }
            Mage::getModel('checkout/mapper_checkout_request_validator')->validate(
                $this->_getRequest()->__toArray()
            );

            $checkout = $this->_getCheckout($this->_getCheckoutType());
            $order    = $this->_buildOrderFromRequest();
            $response = $checkout->submit($order);

            Mage::dispatchEvent('order_submit_after', array('order' => $order, 'response' => $response));

            if ($response->isRedirect() || $response->isFailure()) {
                $checkout->reset();
            }
            if ($response->isRedirect()) {
                $session = Mage::getSingleton('checkout/session');
                $session->setLastRealOrderId($order->getIncrementId());
                if ($this->_getRequest()->getData('paymentDetails', 'method') == 'paypal_standard' && $order->getId()) {
                    Mage::getSingleton('checkout/session')->setProcessedOrderId($order->getId());
                }

                $session->setQuoteId($order->getQuoteId());
                $session->setQuote(
                    Mage::getModel('sales/quote')->load($order->getQuoteId())
                );
            }
            $this->getResponse()->setBody(
                json_encode(
                    $response->getOpcResponse()
                )
            );
        }
        catch (Exception $ex)
        {
            $this->getResponse()->setBody(
                json_encode(
                    array('failure' => array('msg' => $ex->getMessage()))
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function failureAction()
    {
        Mage::getModel('vax_checkout/observer')->addShipping();

        $checkout = $this->_getCheckout($this->_getCheckoutType());

        if ($incrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
            Mage::getSingleton('checkout/session')->setFailedOrderId($order->getId());
        }

        if ( ! $checkout->hasFailedOrder()) {
            $this->_redirect('checkout/onepage');
            return;
        }

        $this->_loadLayout();

        $checkout->reset();
    }

    /**
     * {@inheritdoc}
     */
    public function successAction()
    {
        $checkout = $this->_getCheckout($this->_getCheckoutType());
        $session = Mage::getSingleton('checkout/session');

        if ($session->getLastRealOrderId() && !$checkout->hasProcessedOrder()) {
            $session->setProcessedOrderId($session->getLastRealOrderId());
            $session->setBVOrderId($session->getLastRealOrderId());
            $session->setOrder(Mage::getModel('sales/order')->load($session->getLastOrderId()));
        }

        if ( ! $checkout->hasProcessedOrder()) {
            $this->_redirect('checkout/onepage');
            return;
        }
        Mage::getSingleton('checkout/session')->clear();
        $this->_loadLayout(true);

    }

    /**
     * Loads the current layout.
     *
     * @param boolean $isSuccess
     *
     * @return void
     */
    protected function _loadLayout($isSuccess = false)
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));

        if ($isSuccess) {
            Mage::dispatchEvent(
                'checkout_onepage_controller_success_action',
                array(
                    'order_ids' => array(Mage::getSingleton('checkout/session')->getProcessedOrderId())
                )
            );
        }

        $this->renderLayout();

        Mage::getSingleton('checkout/session')
            ->unsProcessedOrderId()
            ->unsLastRealOrderId()
            ->unsOrder();
    }

    /**
     * Returns the current request.
     *
     * @return Varien_Object
     */
    protected function _getRequest()
    {
        $request = new Varien_Object;

        if ($this->getRequest()->isAjax()) {
            $request->setData(json_decode(urldecode($this->getRequest()->getRawBody()), true));
        } else {
            $request->setData($this->getRequest()->getPost());
        }

        return $request;
    }

    /**
     * Returns a checkout instance.
     *
     * @param string $type Checkout Type
     *
     * @return Vax_Realex_Model_Service_Checkout
     */
    protected function _getCheckout($type)
    {
        $factory = Mage::getModel('checkout/factory_checkout');

        return $factory->get($type);
    }

    /**
     * Returns the current checkout type.
     *
     * @return string
     */
    protected function _getCheckoutType()
    {
        return $this->_getRequest()->getData('paymentDetails', 'method');
    }

    /**
     * Builds a new order from a post request.
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _buildOrderFromRequest()
    {
        $builder = Mage::getModel(
            'checkout/builder_order',
            Mage::getModel('sales/convert_quote')
        );

        return $builder->build(
            Mage::getModel('checkout/builder_quote')->build($this->_getRequest()->__toArray())
        );
    }

    public function returnAction()
    {
        $quote = Mage::getModel('checkout/mapper_checkout_request')->map(
            Mage::getSingleton('checkout/session')->getQuote(),
            $this->_getRequest()->__toArray());
        $this->getResponse()->setBody(
            Zend_Json::encode(array(
                'return_url' => Mage::getUrl('checkout/cart')
            ))
        );
    }
}