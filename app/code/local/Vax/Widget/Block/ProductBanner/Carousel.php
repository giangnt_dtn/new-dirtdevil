<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_ProductBanner_Carousel extends Vax_Widget_Block_Abstract implements Mage_Widget_Block_Interface
{
    protected $_banners;

    /**
     * Initialise the carousel.
     */
    protected function _construct()
    {
        $this->setTemplate('widget/product_banner/carousel.phtml');
        parent::_construct();
    }

    /**
     * Returns list of banner slides.
     *
     * @return mixed
     */
    public function getBanners()
    {
        if (!$this->_banners) {
            for ($i = 1; $i <= 6; $i++) {
                if ($this->getData('slide_' . $i)) {
                    $widget = $this->getLayout()->createBlock('vax_widget/widget_instance', 'banner_slide_' . $i);
                    $widget->setWidgetId((int)$this->getData('slide_' . $i));

                    $this->_banners[] = $widget;
                }
            }
        }

        return $this->_banners;
    }
}