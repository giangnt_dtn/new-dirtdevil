<?php

/**
 * DirtDevil Newsletter Observer.
 *
 * @copyright DirtDevil Ltd 2013
 */
class DirtDevil_Newsletter_Model_Observer_Newsletter
{
    /**
     * Sets up newsletter subscription following a successful order.
     *
     * @param mixed $event
     *
     * @return void
     */
    public function handleSuccessfulOrder($event)
    {
        // quick hack to run this code on DD UK only
        if (Mage::helper('core')->isModuleEnabled('Yoma_Realex')) {
            $quote = $event->getQuote();

            if (!$quote->requiresNewsletter()) {
                $quote->setMarketingOptIn(0);
                $quote->save();
                return;
            }

            //generic newsletter - activated by the tickbox in checkout page
            Mage::getModel('newsletter/subscriber')->subscribe(
                $quote->getCustomerEmail(),
                $quote->getCustomerFirstname(),
                $quote->getCustomerLastname(),
                "Checkout Success"
            );

            $quote->setMarketingOptIn(1);
            $quote->save();
        }
    }

    /**
     * Sets up newsletter subscription following billing and shipping information saved (DD DE only)
     *
     * @param mixed $observer
     *
     * @return void
     */
    public function subscribeNewsletterOnPurchase($observer)
    {
        // quick hack to run this code on DD UK only
        if (Mage::helper('core')->isModuleEnabled('Yoma_Realex')) {
            return;
        }

        $email = $observer->getOrder()->getBillingAddress()->getEmail();
        $post  = Mage::app()->getRequest()->getPost();
        if (isset($post['newsletter'])) {
            Mage::getModel('newsletter/subscriber')->subscribe($email);
        }
    }
}