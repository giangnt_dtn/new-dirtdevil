<?php
class Yoma_RealexExtended_Model_Observer{

    const NOT_ENROLLED = 0x01;
    const UNABLE_VERIFY_ENROLLMENT = 0x02;
    const INVALID_RESPONSE = 0x04;
    const ENROLLED_ACS_INVALID_RESPONSE = 0x08;
    const SUCCESSFUL_AUTH = 0x10;
    const SERVER_ATTEMPT = 0x20;
    const INCORRECT_PASSWORD = 0x40;
    const AUTH_UNAVAILABLE = 0x80;
    const ACS_INVALID_RESPONSE = 0x100;
    const NO_RESPONSE = 0x200;

    /**
     * Response results from 3D Secure Verify Enrolled
     */
    const THREED_SECURE_VERIFY_ENROLLED_RESULT_ENROLLED             = '00';
    const THREED_SECURE_VERIFY_ENROLLED_RESULT_NOT_ENROLLED         = '110';
    const THREED_SECURE_VERIFY_ENROLLED_RESULT_INVALID_RESPONSE     = '5xx';
    const THREED_SECURE_VERIFY_ENROLLED_RESULT_FATAL_ERROR          = '220';

    /**
     * Response tags from 3D Secure Verify Enrolled
     */
    const THREED_SECURE_VERIFY_ENROLLED_TAG_ENROLLED            = 'Y';
    const THREED_SECURE_VERIFY_ENROLLED_TAG_UNABLE_TO_VERIFY    = 'U';
    const THREED_SECURE_VERIFY_ENROLLED_TAG_NOT_ENROLLED        = 'N';

    /**
     * Response results from 3D Secure Verify Signature
     */
    const THREED_SECURE_VERIFY_SIGNATURE_RESULT_VALIDATED                       = '00';
    const THREED_SECURE_VERIFY_SIGNATURE_RESULT_ENROLLED_INVALID_ACS_RESPONSE   = '110';
    const THREED_SECURE_VERIFY_SIGNATURE_RESULT_INVALID_ACS_RESPONSE            = '5xx';

    /**
     * Response statuses from 3D Secure Verify Signature
     */
    const THREED_SECURE_VERIFY_SIGNATURE_STATUS_AUTHENTICATED       = 'Y';
    const THREED_SECURE_VERIFY_SIGNATURE_STATUS_NOT_AUTHENTICATED   = 'N';
    const THREED_SECURE_VERIFY_SIGNATURE_STATUS_ACKNOWLEDGED        = 'A';
    const THREED_SECURE_VERIFY_SIGNATURE_STATUS_UNAVAILABLE         = 'U';

    protected $_paymentMethods = array('realexdirect','realvault');

    protected $_postAuthRuleSet = array(
        'avs_result' => array('value'=>'avspostcoderesponse','condition'=>'boolean'),
        'cvn_result' => array('value'=>'cvnresult','condition'=>'boolean'),
        'tss_result' => array('value'=>'tss_result','condition'=>'compare'),
        'tss_rule_specific' => array('value'=>'tss_check_','condition'=>'compare'),
        'three_d_secure_result' => array('value'=>'eci','condition'=>'boolean'),
    );

    public function appendRuleInfoPaymentBlock($payment,$data){

        $order = $payment->getOrder();
        $rule = $this->_getRules($payment,$order);

        if(!$rule){
            return $data;
        }

        if($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING){
            return $data;
        }

        $info = array();

        foreach($data as $key => $transaction) {

            $transactionId = $transaction['Transaction Reference'];
            $current = $payment->getTransaction($transactionId);
            $testCase = $this->_processPaymentInfo($rule,$current->getAdditionalInformation('raw_details_info'));
            foreach($testCase as $test){
                if(!$test->getResult()) {
                    switch ($test->getName()) {
                        case 'avspostcoderesponse' :
                            $transaction['AVS Address Result'] = $transaction['AVS Address Result'] . ' ( Not Matched )';
                            break;
                        case 'cvnresult' :
                            $transaction['CVN Result'] = $transaction['CVN Result'] . ' ( Not Matched )';
                            break;
                        case 'tss_result' :
                            $transaction['TSS Result'] = $transaction['TSS Result'] . ' ( Not Matched )';
                            break;
                    }
                }
            }
            $info[$key] = $transaction;

        }

        return $info;
    }

    public function processPostPlaceOrder(Varien_Event_Observer $observer){

        $payment = $observer->getPayment()->getInfoInstance();
        $order = $payment->getOrder();


        if($this->_isEnabled()) {

            $rule = $this->_getRules($payment, $order);

            $paymentActions = mage::getModel('realex/realex_source_paymentAction');
            $currentAction = $observer->getTransport()->getPaymentAction();
            if ($rule){
                $settleType = $rule['settle_type'];
                if($currentAction != $settleType && $paymentActions->isActionAllowed($settleType)) {
                    $observer->getTransport()->setPaymentAction($settleType);
                }
            }
        }
        return $this;

    }

    public function processPlaceOrderSuccess(Varien_Event_Observer $observer){

        $order = $observer->getOrder();
        $payment = $order->getPayment();


        $rule = $this->_getRules($payment,$order);

        if(!$rule){
            return $this;
        }

        if($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING){
            return $this;
        }

        try {
            $transaction = $payment->getTransactionAdditionalInfo('raw_details_info');

            if($this->_processPostAuth($rule,$transaction)){
                $transaction = $payment->getAuthorizationTransaction();
                if ($transaction && $order->canInvoice() && $rule['auto_settle'] == '1') {
                    $this->_settle($order);
                }
                if($rule['order_review'] == '1'){
                    $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                    $status = 'order_review';
                    $comment = 'Placed in Order Review due transaction rules';
                    $isCustomerNotified = false;
                    $order->setState($state, $status, $comment, $isCustomerNotified);
                    $order->save();
                }
            }
        }catch (Exception $e){
            mage::logException($e);
        }

        return $this;
    }

    protected function _processPaymentInfo($rule,$data){

        $availableRules = $this->_getAvailableRules($rule);

        $tests = new Varien_Data_Collection();

        foreach($availableRules as $key=>$test){

            switch(strtolower($test['condition']['value'])){
                case 'avspostcoderesponse':
                    $avs = mage::getModel('realexExtended/avs');
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$avs->getSelected(array($data[$test['condition']['value']]))));
                    }else {
                        if (isset($data['avspostcoderesult'])) {
                            $tests->addItem($this->_createTest($test, $avs->getSelected(array($data['avspostcoderesult']))));
                        }
                    }
                    break;
                case 'cvnresult':
                    $cvn = mage::getModel('realexExtended/cvn');
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$cvn->getSelected(array($data[$test['condition']['value']]))));
                    }
                    break;
                case 'tss_result':
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$data[$test['condition']['value']]));
                    }else {
                        if (isset($data['tss'])) {
                            $tests->addItem($this->_createTest($test, $data['tss']));
                        }
                    }
                    break;
                case 'eci':
                    $secure = mage::getModel('realexExtended/secure');
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$secure->getSelected(array($data[$test['condition']['value']]))));
                    }
                    break;
                case 'tss_check_':
                    $values = json_decode($test['value']);
                    foreach($values as $value){
                        $test['value'] = $value[1];
                        $name = 'tss_check_' . (string)$value[0];
                        $test['condition']['value'] = $name;
                        if(isset($data[$test['condition']['value']])){
                            $tests->addItem($this->_createTest($test,$data[$test['condition']['value']]));
                        }else{
                            $name = 'tss_' . (string)$value[0];
                            if(isset($data[$name])){
                                $tests->addItem($this->_createTest($test,$data[$name]));
                            }
                        }
                    }
                    break;
            }

        }

        $ruleProcessor = mage::getModel('realexExtended/ruleProcessor');

        return $ruleProcessor->newTestCase($tests);

    }

    protected function _processPostAuth($rule,$data,$action = false){

        $availableRules = $this->_getAvailableRules($rule);

        $tests = new Varien_Data_Collection();

        foreach($availableRules as $key=>$test){

            switch(strtolower($test['condition']['value'])){
                case 'avspostcoderesponse':
                    $avs = mage::getModel('realexExtended/avs');
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$avs->getSelected(array($data[$test['condition']['value']]))));
                    }else {
                        if (isset($data['avspostcoderesult'])) {
                            $tests->addItem($this->_createTest($test, $avs->getSelected(array($data['avspostcoderesult']))));
                        }
                    }
                    break;
                case 'cvnresult':
                    $cvn = mage::getModel('realexExtended/cvn');
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$cvn->getSelected(array($data[$test['condition']['value']]))));
                    }
                    break;
                case 'tss_result':
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$data[$test['condition']['value']]));
                    }else {
                        if (isset($data['tss'])) {
                            $tests->addItem($this->_createTest($test, $data['tss']));
                        }
                    }
                    break;
                case 'eci':
                    $secure = mage::getModel('realexExtended/secure');
                    if(isset($data[$test['condition']['value']])){
                        $tests->addItem($this->_createTest($test,$secure->getSelected(array($data[$test['condition']['value']]))));
                    }
                    break;
                case 'tss_check_':
                    $values = json_decode($test['value']);
                    foreach($values as $value){
                        $test['value'] = $value[1];
                        $name = 'tss_check_' . (string)$value[0];
                        $test['condition']['value'] = $name;
                        if(isset($data[$test['condition']['value']])){
                            $tests->addItem($this->_createTest($test,$data[$test['condition']['value']]));
                        }else{
                            $name = 'tss_' . (string)$value[0];
                            if(isset($data[$name])){
                                $tests->addItem($this->_createTest($test,$data[$name]));
                            }
                        }
                    }
                    break;
            }

        }

        $ruleProcessor = mage::getModel('realexExtended/ruleProcessor');

        $testCase = $ruleProcessor->newTestCase($tests);

        return $ruleProcessor->getResult($testCase);

    }

    protected function _createTest($test,$value){

        $instance = new Varien_Object();
        $instance->setName($test['condition']['value']);
        $instance->setType($test['condition']['condition']);
        $instance->setValue($test['value']);
        $instance->setTest($value);

        return $instance;
    }

    protected function _getAvailableRules($rule){

        $appliedRules = array();

        foreach($this->_postAuthRuleSet as $key=>$ruleSet){
            if(isset($rule[$key]) && !empty($rule[$key]) && $rule[$key] != '[[""]]'){
                array_push($appliedRules,array('value'=>$rule[$key],'condition'=>$ruleSet));
            }
        }

        return $appliedRules;
    }

    protected function _settle($order){

        $items = array();
        foreach ($order->getAllItems() as $item) {
            $items[$item->getId()] = $item->getQtyOrdered();
        }
        $invoiceId = Mage::getModel('sales/order_invoice_api')->create($order->getIncrementId(),$items,null,false,true);
        Mage::getModel('sales/order_invoice_api')->capture($invoiceId);
    }

    public function processCallBackRules(Varien_Event_Observer $observer){

        $payment = mage::getModel('sales/order_payment')->load($observer->getTransaction()->getPaymentId());
        $order = mage::getModel('sales/order')->load($observer->getTransaction()->getOrderId());
        $payment->setPaymentAmount($payment->getBaseAmountAuthorized());

        $rule = $this->_getRules($payment,$order);
        if($rule){
            $observer->getTransport()->setRequire3DSecure($rule['three_d_secure']);
            $observer->getTransport()->setProfile3DSecure($rule['three_d_secure_profile']);
            $observer->getTransport()->setTssResult($rule['tss_result']);
            $observer->getTransport()->setAvsResult($rule['avs_result']);
            $observer->getTransport()->setTssRuleSpecific($rule['tss_rule_specific']);
            Realex_Log::log(
                array(
                    'rule_match_callback' => array(
                        'increment_id' => $order->getIncrementId(),
                        'rule'         => $rule
                    )
                ),
                NULL,
                'rule_match_' . $payment->getMethod() . '.log'

            );
        }

        return $this;
    }

    public function process3dSecureVerifyRules(Varien_Event_Observer $observer){

        if($observer->getTransport()->getProfile3Dsecure()  && $observer->getTransport()->getRequire3DSecure()) {

            $code = null;

            if(substr($observer->getTransport()->getResult(),0,1) == '5'){
                $observer->getTransport()->setResult('5xx');
            }
            switch ($observer->getTransport()->getResult()) {
                case self::THREED_SECURE_VERIFY_SIGNATURE_RESULT_VALIDATED :
                    switch($observer->getTransport()->getStatus()){
                        case self::THREED_SECURE_VERIFY_SIGNATURE_STATUS_NOT_AUTHENTICATED :
                            $code = self::INCORRECT_PASSWORD;
                            break;
                        case self::THREED_SECURE_VERIFY_SIGNATURE_STATUS_UNAVAILABLE :
                            $code = self::AUTH_UNAVAILABLE;
                            break;
                    }
                    break;
                case self::THREED_SECURE_VERIFY_SIGNATURE_RESULT_ENROLLED_INVALID_ACS_RESPONSE :
                    $code = self::ENROLLED_ACS_INVALID_RESPONSE;
                    break;
                case self::THREED_SECURE_VERIFY_SIGNATURE_RESULT_INVALID_ACS_RESPONSE :
                    $code = self::AUTH_UNAVAILABLE;
                    break;
            }

            if (isset($code) && $observer->getTransport()->getProfile3Dsecure() & $code) {

                $observer->getTransport()->setRequireLiabilityShift(false);

            }
        }

        return $this;
    }

    public function process3dSecureEciRules(Varien_Event_Observer $observer){

        if($observer->getTransport()->getProfile3Dsecure() && $observer->getTransport()->getRequire3DSecure()) {

            $code = null;

            if(substr($observer->getTransport()->getResult(),0,1) == '5'){
                $observer->getTransport()->setResult('5xx');
            }

            switch ($observer->getTransport()->getResult()) {
                case self::THREED_SECURE_VERIFY_ENROLLED_RESULT_NOT_ENROLLED :
                    if ($observer->getTransport()->getEnrolled() == self::THREED_SECURE_VERIFY_ENROLLED_TAG_UNABLE_TO_VERIFY) {
                        $code = self::NOT_ENROLLED;
                    } elseif ($observer->getTransport()->getEnrolled() == self::THREED_SECURE_VERIFY_ENROLLED_TAG_NOT_ENROLLED) {
                        $code = self::UNABLE_VERIFY_ENROLLMENT;
                    }
                    break;
                case self::THREED_SECURE_VERIFY_ENROLLED_RESULT_FATAL_ERROR:
                case self::THREED_SECURE_VERIFY_ENROLLED_RESULT_INVALID_RESPONSE:
                    $code = self::INVALID_RESPONSE;
                    break;
            }

            if (isset($code) && $observer->getTransport()->getProfile3Dsecure() & $code) {

                $observer->getTransport()->setRequireLiabilityShift(false);

            }
        }

        return $this;
    }

    public function process3dSecureRules(Varien_Event_Observer $observer){

        $rule = $this->_getRules($observer->getPayment());

        if($rule){
            $observer->getTransport()->setRequire3DSecure($rule['three_d_secure']);
            $observer->getTransport()->setProfile3DSecure($rule['three_d_secure_profile']);
            Realex_Log::log(
                array(
                    'rule_match_pre_auth_secure' => array(
                        'increment_id' => $observer->getPayment()->getOrder()->getIncrementId(),
                        'rule'         => $rule
                    )
                ),
                NULL,
                'rule_match_' . $observer->getPayment()->getMethod() . '.log'

            );
        }

        return $this;
    }

    public function processSubAccountRules(Varien_Event_Observer $observer){

        $rule = $this->_getRules($observer->getPayment());

        if($rule){
            if($observer->getPayment()->getMethod() == 'realvault'){
                $value = $rule['rv_sub_account'];
            }else{
                $value = $rule['sub_account'];
            }
            $observer->getTransport()->setSubAccount($value);
            Realex_Log::log(
                array(
                    'rule_match_pre_auth_sub_account' => array(
                        'increment_id' => $observer->getPayment()->getOrder()->getIncrementId(),
                        'rule'         => $rule
                    )
                ),
                NULL,
                'rule_match_' . $observer->getPayment()->getMethod() . '.log'

            );
        }

        return $this;
    }

    protected function _getRules($payment, $order = null){

        if($this->_isEnabled()) {

            if(!isset($order)){
                $order = $payment->getOrder();
            }
            if(!$payment->getPaymentAmount()) {
                $payment->setPaymentAmount($payment->getBaseAmountOrdered());
            }
            $rule = mage::getResourceModel('realexExtended/chanel')->matchRule($payment, $order);
            if (isset($rule[0]['profile_name']) && $this->_isProfileActive($rule[0]['profile_name'],$order->getStoreId())) {
                return $rule[0];
            }

        }

        return false;
    }

    protected function _isEnabled(){

        return Mage::getStoreConfig(
            'realex/realex_transaction_profiles/active',
            Mage::app()->getStore()
        );
    }

    protected function _isAutoSettle(){

        return Mage::getStoreConfig(
            'realex/realex_transaction_profiles/auto_settle',
            Mage::app()->getStore()
        );
    }

    protected function _isProfileActive($profile,$store = null){

        $active = explode(',',Mage::getStoreConfig(
                'realex/realex_transaction_profiles/profiles',
                (isset($store)?$store:Mage::app()->getStore()))
            );

        if(in_array($profile,$active)){
            return true;
        }

        return false;
    }

}