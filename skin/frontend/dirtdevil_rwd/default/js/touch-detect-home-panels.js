jQuery(document).ready(function ($j) {
    if ("ontouchstart" in document.documentElement) {
        jQuery('.home-panel-element ').addClass("touchscreen");
    } else {
        jQuery('.home-panel-element ').addClass("no-tap");
    }
    jQuery('.home-panel-element .panel-content').on('touchstart', function () {
        jQuery(this).addClass('touchscreen');
    });
});
