<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Checkout_Block_Onepage_Steps_Rwd_Actions extends Vax_Checkout_Block_Onepage_Steps_Actions
{
    /**
     * Returns the next button title.
     *
     * @return string
     */
    public function getNextBtnTitle()
    {
        if ($this->hasOrderProcessed()) {
            return $this->__('Return to Shop');
        }

        if ($this->hasOrderFailed()) {
            return $this->__('Try Again');
        }

        $nextStepTitle = $this->getStepData('buttonTitle', $this->getStepIndex()+1);

        if ($this->isLastStep() || '' == $nextStepTitle) {
            return $this->__('Place order');
        }

        return $this->__('Next: %s', ucwords(strtolower($nextStepTitle)));
    }
}