<?php
class DirtDevil_Catalog_Block_Layer_State extends Mage_Catalog_Block_Layer_State
{
    /**
     * Retrieve Clear Filters URL
     *
     * @return string
     */
    public function getClearUrl()
    {
        $filterState = array();
        foreach ($this->getActiveFilters() as $item) {
            $filterState[$item->getFilter()->getRequestVar()] = $item->getFilter()->getCleanValue();
        }
        $filterState['min'] = 0;
        $filterState['max'] = 0;
        $params['_current']     = true;
        $params['_use_rewrite'] = true;
        $params['_query']       = $filterState;
        $params['_escape']      = true;

        return str_replace('isAjax=1', 'isAjax=0', Mage::getUrl('*/*/*', $params));
    }
}