<?php

/**
 * Registration Search Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Adminhtml_Search_Registration extends Varien_Object
{
    /**
     * Load search results
     *
     * @return Mage_Adminhtml_Model_Search_Registration
     */
    public function load()
    {
        $arr = array();

        if ( ! $this->hasStart() || ! $this->hasLimit() || ! $this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }

        $query      = $this->getQuery();
        $collection = Mage::getResourceModel('vax_registration/registration_collection');
        $collection->addFieldToSelect('*');

        $collection->getSelect()->where("entity_id like '%".$query."%' or postcode like '%".$query."%'");
        $collection
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();

        foreach ($collection as $registration) {
            $arr[] = array(
                'id'               => 'registration/1/'.$registration->getId(),
                'type'             => Mage::helper('adminhtml')->__('Registration'),
                'name'             => Mage::helper('adminhtml')->__('Registration #%s', $registration->getId()),
                'description'      => $registration->getFirstName().' '.$registration->getSurname(),
                'form_panel_title' => Mage::helper('adminhtml')->__('Registration #%s (%s)', $registration->getId(), $registration->getFirstName().' '.$registration->getSurname()),
                'url'              => Mage::helper('adminhtml')->getUrl('*/registration/view', array('registration_id'=>$registration->getId())),
            );
        }

        $this->setResults($arr);

        return $this;
    }
}