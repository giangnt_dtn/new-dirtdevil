<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Checkout_Model_Source_StepsModel
{
    public function toOptionArray()
    {
        $models = array(
            array(
                'value'     => 'original',
                'label'     => Mage::helper('vax_checkout')->__('Magento Checkout Steps')
            ),
            array(
                'value'     => 'vax_checkout/onepage_steps',
                'label'     => Mage::helper('vax_checkout')->__('Standard Steps')
            ),
            array(
                'value'     => 'vax_checkout/onepage_rwd_steps',
                'label'     => Mage::helper('vax_checkout')->__('RWD Steps')
            )
        );

        return $models;
    }
}