<?php 
/**
 * Category Related Product Setup
 *
 * @category    code
 * @package     dirtdevil
 * @copyright   Copyright (c) 2015 Vax Ltd.
 * @author      Vax
 */
$installer = $this;

$installer->startSetup();

$attribute = array(
    'type' => 'varchar',
    'label' => 'Category Title',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information"
);

$installer->addAttribute('catalog_category', 'category_title', $attribute);

$installer->endSetup();
