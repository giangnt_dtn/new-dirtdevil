<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
abstract class DirtDevilUk_Realex_Model_Payment_Direct extends Yoma_Realex_Model_Payment_Direct
{
    /**
     * Create url for gateway callback
     *
     * @return string
     */
    public function getCallbackUrl(){

        return str_replace('realexAdmin', 'realex', parent::getCallbackUrl());
    }
}