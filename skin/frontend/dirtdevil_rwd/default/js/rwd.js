$j(document).ready(function () {
    footeResize();
    //if there is an admin message, give it a Close button and hide it automatically after 10s
    if (!isEmpty($j('#admin_messages'))) {
        hideOnClick("#admin_messages");
    }

    var smallResizeTimer;
    $j(window).resize(function (e) {
        clearTimeout(smallResizeTimer);
        smallResizeTimer = setTimeout(function () {
            $j(window).trigger('delayed-resize', e);
        }, 50);
    });

    // scroll back to top when the user clicks a .back-to-top button
    backToTopMobile();
});

function hideOnClick(hideme) {
    //hide by default after some time
    $j(hideme).delay(10000).fadeOut();

    $j(hideme).append('<div class="close">X</div>');
    $j(".close").click(function () {
        $j(hideme).hide();
    });
}

function isEmpty( el ){
    return !$j.trim(el.html()) && el.length;
}

function footeResize() {
    //turn the footer columns into an accordion
    if ($j('.footer .footer-col').length) {
        // expand/unexpand footer cols
        jQuery('.footer-col div.title').click(function (e) {
            if (jQuery(window).width() > 771) {
                jQuery(this).removeClass('active');
                return false;
            }
            else {
                if (jQuery(this).hasClass('active')) {
                    jQuery(e.target).parent().find('.active').next('ul').hide('slow');
                    jQuery(this).removeClass('active');
                } else {
                    jQuery(this).addClass('active');
                    jQuery(e.target).parent().find('.active').next('ul').show('slow');
                }
            }
        });

        enquire.register('screen and (min-width: ' + 771 + 'px)', {
            match: function () {
                jQuery('.footer-col ul').show();
                jQuery('.footer-col div.title').removeClass('active');
            },
            unmatch: function () {
                jQuery('.footer-col ul').hide();
                jQuery('.footer-col.last ul').show();
            }
        });
    }
}

//share links in the footer
function share(anchor, link) {
    //attaches the current page url to the given social media's share link
    if(jQuery(anchor).length) {
        var shareUrl = link + window.location.href;
        jQuery(anchor).attr("href", shareUrl);
    }
}

// scroll back to top when the user clicks a .back-to-top button
function backToTopMobile() {
    var backButton = jQuery('.back-to-top');
    if(backButton.length) {
        backButton.on('click touchend', function(){
            jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
        });
    }
}

