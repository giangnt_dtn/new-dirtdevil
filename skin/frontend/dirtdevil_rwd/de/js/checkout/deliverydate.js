var $j = jQuery.noConflict();

$j('document').ready(function() {
    $j('select#address_delivery_shipping_date').change(function() {
        $j.ajax({
            url: '/vax-deliverydate/index/update',
            data: {'delivery_date': $j(this).val()}
        });
    });
});
