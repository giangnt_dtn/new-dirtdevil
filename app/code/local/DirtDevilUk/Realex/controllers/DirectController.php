<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
require_once 'Yoma/Realex/controllers/DirectController.php';

class DirtDevilUk_Realex_DirectController extends Yoma_Realex_DirectController
{
    /**
     * 3d Secure callback
     */
    public function threedSecureCaptureAction()
    {
        try {
            $this->_getService()->callBack();
            $success = true;
        }catch(Exception $e){
            $success = false;
            Mage::log($e->getMessage(), null, "custom_realex.log");
        }

        if ($this->getRequest()->getParam('reference')) {
            $order = $this->_getService()->getOrder();
            if (!$order || !$order->getId()) {
                $token = explode('-', $this->getRequest()->getParam('reference'));
                $order = Mage::getModel('sales/order')->loadByIncrementId($token[0]);
            }
        }

        if ($success) {
            Mage::getSingleton('checkout/session')->setLastRealOrderId($order->getIncrementId());
            Mage::getSingleton('checkout/session')->setLastOrderId($order->getId());
            $this->_redirect('checkout/onepage/success');
        } else {
            $this->_redirect('checkout/onepage/failure');
        }
        return;
    }
}
