<?php

/**
 * Vax Rest Quote Response Helper.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Rest_Helper_Response_Quote extends Mage_Core_Helper_Abstract
{
    /**
     * Builds the quote response.
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return array
     */
    public function buildResponse($quote)
    {
        $totalsAddress = Mage::helper('rest/model_quote')->getTotalsAddress($quote);
        $incrementId   = Mage::getModel('sales/order')->load($quote->getId(), 'quote_id')->getIncrementId();

        return array(
            'id'                   => $quote->getId(),
            'store_id'             => $quote->getStoreId(),
            'order_increment_id'   => $incrementId,
            'lines'                => $this->_buildItemsResponse($quote->getItemsCollection()),
            'billing_address'      => $this->_buildAddressResponse($quote->getBillingAddress()),
            'shipping_address'     => $this->_buildAddressResponse($quote->getShippingAddress()),
            'shipping_rates'       => $this->_buildShippingRatesResponse($quote->getShippingAddress()),
            'shipping_method'      => $quote->getShippingAddress()->getShippingMethod(),
            'tax_total'            => $totalsAddress->getTaxAmount(),
            'discount_total'       => abs($totalsAddress->getDiscountAmount()),
            'discount_description' => $totalsAddress->getDiscountDescription(),
            'shipping_total'       => $quote->getShippingAddress()->getShippingInclTax(),
            'sub_total'            => $quote->getSubtotal(),
            'grand_total'          => $quote->getGrandTotal(),
        );
    }

    /**
     * Builds the quote items response.
     *
     * @param Mage_Eav_Model_Entity_Collection_Abstract $items
     *
     * @return array
     */
    protected function _buildItemsResponse($itemsCollection)
    {
        $items = array();

        foreach ($itemsCollection as $item) {
            $items[] = array(
                'id'              => $item->getProductId(),
                'sku'             => $item->getSku(),
                'name'            => $item->getName(),
                'discount_amount' => $item->getDiscountAmount(),
                'unit_price'      => $item->getPriceInclTax(),
                'quantity'        => $item->getQty(),
                'total'           => $item->getRowTotalInclTax()
            );
        }

        return $items;
    }

    /**
     * Builds a single address response.
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return array
     */
    protected function _buildAddressResponse(Mage_Sales_Model_Quote_Address $address)
    {
        return array(
            'id'               => $address->getId(),
            'title'            => $address->getPrefix(),
            'first_name'       => $address->getFirstname(),
            'last_name'        => $address->getLastname(),
            'street'           => $address->getStreet(),
            'city'             => $address->getCity(),
            'county'           => $address->getRegion(),
            'post_code'        => $address->getPostcode(),
            'country_code'     => $address->getCountryId(),
            'email_address'    => $address->getEmail(),
            'telephone_number' => $address->getTelephone(),
        );
    }

    /**
     * Builds the shipping rates response.
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return array
     */
    protected function _buildShippingRatesResponse(Mage_Sales_Model_Quote_Address $address)
    {
        $shippingRates = array();

        foreach ($address->getShippingRatesCollection() as $rate) {
            $shippingRates[$rate->getCode()] = array(
                'price' => $rate->getPrice(),
                'title' => $rate->getMethodTitle(),
            );
        }

        return $shippingRates;
    }
}