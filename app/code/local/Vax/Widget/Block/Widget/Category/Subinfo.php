<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Category_Subinfo
    extends Vax_Widget_Block_Widget_Abstract implements Mage_Widget_Block_Interface
{
    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        $this->setTemplate('catalog/widget/category/subinfo.phtml');
        $this->setCategory(Mage::getModel('catalog/category')->load($this->_getCategoryId()));

        parent::_construct();
    }

    /**
     * Returns ID of the category selected in a widget.
     *
     * @return int
     */
    protected function _getCategoryId()
    {
        $categoryData = explode('/', $this->getCategoryId());
        if (sizeof($categoryData > 0)) {
            return (int)$categoryData[1];
        }
    }
}