<?php

/**
 * Vax Checkout Cart Summary Block.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Checkout_Block_Onepage_Cart_Summary extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the current cart items.
     *
     * @return mixed
     */
    public function getItems()
    {
        return $this->getQuote()->getAllVisibleItems();
    }

    /**
     * Returns the current subscription products.
     *
     * @return array
     */
    public function getSubscriptionProducts()
    {
        return $this->getQuote()->getSubscriptionProducts();
    }

    /**
     * Returns whether shipping currently applied to the cart.
     *
     * @return boolean
     */
    public function isShippingApplicable()
    {
        return ! $this->_quote->isVirtual();
    }

    /**
     * Returns the current shipping description.
     *
     * @return string
     */
    public function getShippingDescription()
    {
        $addr = $this->getQuote()->getShippingAddress();
        $desc = $addr->getShippingDescription();

        try
        {
            $shipping = Mage::getModel('vax_deliverydate/observer')->getShipping(
                new \DateTime,
                $this->getQuote()
            );

            if (strpos(strtolower($desc), 'nominated')) {
                $desc.=' - Delivered on '.$shipping->getDeliveryDate()->format('D jS F');
            } else {
                $desc.=' - Delivered on or before '.$shipping->getDeliveryDate()->format('D jS F');
            }
        }
        catch (Exception $e)
        {
            return $addr->getShippingDescription();
        }

        return $desc;
    }

    /**
     * Returns the current discount amount.
     *
     * @return float
     */
    public function getDiscountAmount()
    {
        $quote          = $this->getQuote();
        $discountAmount = $quote->getShippingAddress()->getDiscountAmount();

        foreach ($quote->getSubscriptionItems() as $item) {
            $discountAmount += $item->getRowTotalInclTax();
        }

        return $discountAmount;
    }

    /**
     * Returns the current discount amount.
     *
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->getQuote()->getShippingAddress()->getShippingInclTax();
    }

    /**
     * Returns the current grand total.
     *
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->getQuote()->getGrandTotal();
    }
}