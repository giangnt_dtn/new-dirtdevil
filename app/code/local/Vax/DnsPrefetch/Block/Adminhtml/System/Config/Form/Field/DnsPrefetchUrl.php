<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class Vax_DnsPrefetch_Block_Adminhtml_System_Config_Form_Field_DnsPrefetchUrl
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    /**
     * Add URL column.
     */
    protected function _prepareToRender()
    {
        $this->addColumn('url', array(
            'label' => Mage::helper('vax_dnsprefetch')->__('Url'),
            'style' => 'width:150px',
        ));

        // Disables "Add after" button
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('vax_dnsprefetch')->__('Add URL');
    }
}