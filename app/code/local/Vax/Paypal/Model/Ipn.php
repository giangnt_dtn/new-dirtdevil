<?php
/**
 * Vax PayPal Ipn Model
 *
 * Change order status to pending for export
 *
 * @copyright Vax Ltd 2014
 */
class Vax_Paypal_Model_Ipn extends Mage_Paypal_Model_Ipn
{
    /**
     * WARNING: This method is copied from parent class (Mage_Paypal_Model_Ipn). The only difference is
     * that in the function below the customer notification part is wrapped into try...catch block to prevent
     * incorrect email sending from affecting the rest of order processing.
     *
     * @param bool $skipFraudDetection
     */
    protected function _registerPaymentCapture($skipFraudDetection = false)
    {
        if ($this->getRequestData('transaction_entity') == 'auth') {
            return;
        }
        $parentTransactionId = $this->getRequestData('parent_txn_id');
        $this->_importPaymentInformation();
        $payment = $this->_order->getPayment();
        $payment->setTransactionId($this->getRequestData('txn_id'))
            ->setCurrencyCode($this->getRequestData('mc_currency'))
            ->setPreparedMessage($this->_createIpnComment(''))
            ->setParentTransactionId($parentTransactionId)
            ->setShouldCloseParentTransaction('Completed' === $this->getRequestData('auth_status'))
            ->setIsTransactionClosed(0)
            ->registerCaptureNotification(
                $this->getRequestData('mc_gross'),
                $skipFraudDetection && $parentTransactionId
            );
        $this->_order->save();

        $invoice = null;

        // notify customer
        try {
            $invoice = $payment->getCreatedInvoice();
            if ($invoice && !$this->_order->getEmailSent()) {
                $this->_order->queueNewOrderEmail()->addStatusHistoryComment(
                    Mage::helper('paypal')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
                )
                    ->setIsCustomerNotified(true)
                    ->save();
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }

        // set order status to pending to be exported to GP
        if ($invoice && Mage_Sales_Model_Order::STATE_PROCESSING == $this->_order->getState() &&
            Mage::helper('vax_realex/config')->getAuthorisedOrderStatus() !== $this->_order->getStatus()) {
            $this->_order->setState(
                Mage_Sales_Model_Order::STATE_PROCESSING,
                Mage::helper('vax_realex/config')->getAuthorisedOrderStatus(),
                Mage::helper('sales')->__('Order Status set to Pending for export to GP.'));

            $this->_order->save();
        }

        /*Mage::dispatchEvent(
            'checkout_submit_all_revert_stock',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );

        Mage::dispatchEvent(
            'checkout_submit_all_after',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );*/
    }
}
