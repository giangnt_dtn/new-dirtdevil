<?php

/**
 * Catalog layer price filter
 *
 * @category   Vax
 * @package    Vax_Catalog
 * @author     Vax
 */
class DirtDevil_Catalog_Block_Layer_Filter_Price extends Mage_Catalog_Block_Layer_Filter_Price {

    public $_currentCategory;
    public $_productCollection;
    public $_maxPrice;
    public $_minPrice;
    public $_currMinPrice;
    public $_currMaxPrice;

    private $view; /* Zend_View */

    /*
     *  Set all the required data that our slider will require
     */
    public function __construct() {

        if(Mage::helper('catalog/category')->useAjaxCheckboxes() && !Mage::helper('catalog/category')->useAjaxPriceSlider()) {
            parent::__construct();
            $this->setTemplate('catalog/layer/filter/checkbox.phtml');
            return;
        }

        if(!Mage::helper('catalog/category')->useAjaxPriceSlider()) {
            return parent::__construct();
        }

        $this->_currentCategory = Mage::registry('current_category');
        $this->setProductCollection();
        $this->setMinPrice();
        $this->setMaxPrice();
        $this->setCurrentPrices();

        $this->view = new Zend_View();

        parent::__construct();

        $this->setTemplate('catalog/layer/filter/price.phtml');
    }

    /*
     * Prepare query string that was in the original url
     *
     * @return queryString
     */
    public function prepareParams() {
        $url = "";

        $params = $this->getRequest()->getParams();

        foreach ($params as $key => $val) {
            if ($key == 'id') {
                continue;
            }
            if ($key == 'min') {
                continue;
            }
            if ($key == 'max') {
                continue;
            }
            $url.= '&' . $this->view->escape($key) . '=' . $this->view->escape($val);
        }
        return $url;
    }

    /*
     * Fetch Current Currency symbol
     *
     * @return currency
     */
    public function getCurrencySymbol() {
        return Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
    }

    /*
     * Fetch Start Minimum Price
     *
     * @return price
     */
    public function getStartMinPrice() {
        return 0;
    }

    /*
     * Fetch Current Minimum Price
     *
     * @return price
     */
    public function getCurrMinPrice() {
        if ($this->_currMinPrice > 0) {
            $min = $this->_currMinPrice;
        } else {
            $min = $this->_minPrice;
        }
        return $min;
    }

    /*
     * Fetch Current Maximum Price
     *
     * @return price
     */
    public function getCurrMaxPrice() {
        if ($this->_currMaxPrice > 0) {
            $max = $this->_currMaxPrice;
        } else {
            $max = $this->_maxPrice;
        }
        return $max;
    }

    /*
     * Gives you the current url without parameters
     *
     * @return url
     */
    public function getCurrentUrlWithoutParams() {
        $request = Mage::app()->getRequest();
        $url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getServer('REQUEST_URI');
        $baseUrl = explode('?', $url);
        $baseUrl = $baseUrl[0];
        return $baseUrl;
    }

    /*
     * Set the Actual Min Price of the search and catalog collection
     *
     * @use category | search collection
     */
    public function setMinPrice() {
        $this->_minPrice = floor($this->_productCollection->getMinPrice());
    }

    /*
     * Set the Actual Max Price of the search and catalog collection
     *
     * @use category | search collection
     */
    public function setMaxPrice() {
        $this->_maxPrice = ceil($this->_productCollection->getMaxPrice());
    }

    /*
     * Return min price (safe)
     *
     * @returns decimal
     */
    public function getMinPrice() {
        return htmlspecialchars($this->_minPrice, ENT_QUOTES, 'UTF-8');
    }

    /*
     * Return max price (safe)
     *
     * @returns decimal
     */
    public function getMaxPrice() {
        return htmlspecialchars($this->_maxPrice, ENT_QUOTES, 'UTF-8');
    }

    /*
     * Set the Product collection based on the page server to user
     * Might be a category or search page
     *
     */

    public function setProductCollection() {

        if ($this->_currentCategory) {
            $this->_productCollection = $this->_currentCategory
                ->getProductCollection();
        } elseif ($layer = Mage::registry('current_layer')) {
            $this->_productCollection = $layer->getProductCollection();
        }
    }

    /*
     * Set Current Max and Min Prices choosed by the user
     *
     * @set price
     */
    public function setCurrentPrices() {

        $this->_currMinPrice = (int)$this->getRequest()->getParam('min');
        $this->_currMaxPrice = (int)$this->getRequest()->getParam('max');
    }
}
