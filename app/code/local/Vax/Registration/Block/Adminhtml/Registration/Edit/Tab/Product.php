<?php
/**
 * Vax Registration Edit Product tab form block class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */

class Vax_Registration_Block_Adminhtml_Registration_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Add the form fields
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Edit_Tab_Product
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('product');
 
        $fieldset = $form->addFieldset('product_form', array(
            'legend' => $this->__('Product Information')
                ));
        
        $fieldset->addField('model', 'select', array(
            'label' => $this->__('Product'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'model',
            'values' => Mage::getModel('vax_registration/selector_product')
                    ->toOptionArray(),
        ));
 
        $fieldset->addField('serial_number', 'text', array(
            'label' => $this->__('Serial Number'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'serial_number',
        ));
        
        $fieldset->addField('override_warranty_box', 'checkbox', array(
            'name'      => 'override_warranty_box',
            'required'  => false,
            'label'    => 'Override Warranty',
            'onclick'   => "$('override_warranty').toggle();
                            $('" . $form->getHtmlIdPrefix() . "override_warranty').disabled = !$('" . $form->getHtmlIdPrefix() . "override_warranty').disabled;",
            'checked'   => false,
            'after_element_html' => '<label for="' . $form->getHtmlIdPrefix()
                . 'override_warranty_box">'
                . 'I wish to override this warranty length' . '</label><br><br><small><strong>Report</strong> incorrect warranty years to <a href="mailto:dlindsay@vax.co.uk">dlindsay@vax.co.uk</a><br>All activities are monitored for continuous mis-use.</small>',
            'value'     => 0,
        ));

        $fieldset->addField('override_warranty', 'text', array(
            'name'     => 'override_warranty',
            'label'    => 'New Warranty Duration',
            'class' => 'validate-number',
            'value'    => '',
            'container_id' => 'override_warranty',
            'after_element_html' =>
                '<script type="text/javascript">' .
                '$(\'override_warranty\').hide(); ' .
                '$(\''.$form->getHtmlIdPrefix() . 'override_warranty\').disabled = true; ' .
                '</script>',
        ));
        
        $fieldset->addField('pur_date', 'date', array( 
            'name' => 'pur_date', 
            'label' => 'Purchase Date', 
            'image' => $this->getSkinUrl('images/grid-cal.gif'), 
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT, 
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'required' => true,
        ));
        
        $fieldset->addField('pur_location', 'text', array(
            'label' => $this->__('Purchase Location'),
            'required' => false,
            'name' => 'pur_location',
        ));
        
        $fieldset->addField('pur_price', 'text', array(
            'label' => $this->__('Purchase Price'),
            'required' => false,
            'name' => 'pur_price',
        ));
 
        $form->addValues($this->_getFormData());
 
        $this->setForm($form);
 
        return parent::_prepareForm();
    }
 
    /**
     *
     * @return array
     */
    protected function _getFormData() {
        $data = Mage::getSingleton('adminhtml/session')->getFormData();
 
        if (!$data && Mage::registry('current_registration')->getData()) {
            $data = Mage::registry('current_registration')->getData();
        }
 
        return (array) $data;
    }
}