var $j = jQuery.noConflict();
$j(document).ready(function() {
    $j('.radio.delivery-method').change(function() {
        submitSubscriptionForm($j(this).closest('form'));
    });

    $j('.delivery-options select').change(function() {
        form = $j(this).closest('form');
        deliveryMethod = $j(form.find('.radio.delivery-method:checked')[0]);
        if (deliveryMethod && deliveryMethod.attr('value') == 'recurring') {
            submitSubscriptionForm(form);
        }
    });

    submitSubscriptionForm = function(form) {
        form.ajaxSubmit({
            dataType:"json",
            success: function(transport) {
                if (_.has(transport, '_failure')) {
                    alert(transport._failure);
                }

                if (_.has(transport, '_success')) {
                    alert(transport._success);
                }

                if (_.has(transport, '_redirect')) {
                    document.location.href = transport._redirect;
                }
            }
        });
    }
});