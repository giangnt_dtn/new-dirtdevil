<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevilUk_Pdfinvoiceplus_Helper_Product extends Magestore_Pdfinvoiceplus_Helper_Product
{
    /**
     * Get the product image - need to add the user options here
     * @param type $productId
     * @return array getPlaceholder no_selection
     */
    /* Change By Jack 19/01/2015 */
    public function convertImage($url){
        $type = pathinfo($url, PATHINFO_EXTENSION);
        $data = $this->url_get_contents($url);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    /* Alternative to file_get_contents */
    function url_get_contents ($Url) {
        if (!function_exists('curl_init')){
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
