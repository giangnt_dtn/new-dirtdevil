<?php

class Vax_SalesRule_Model_Observer
{
    /**
     * observe checkout_cart_product_add_after event
     *
     * @param Varien_Event_Observer $observer
     */
    public function addGiftProducts(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();
        /** @var Mage_Sales_Model_Quote_Item $item */
        $quoteItem = $observer->getEvent()->getQuoteItem();

        if ($product && $quoteItem) {
            $quote = $quoteItem->getQuote();
            $store = Mage::app()->getStore($quote->getStoreId());
            $rules = Mage::getSingleton('vax_salesrule/rule_freegift_validator')->getRules($store->getWebsiteId(), $quote->getCustomerGroupId(), $product->getId());
            if (count($rules)) {
                /** @var Mage_SalesRule_Model_Rule $rule */
                foreach ($rules as $rule) {
                    $toBeAddedProductIds = explode(',', $rule->getData('gift_product_ids'));
                    if (is_array($toBeAddedProductIds)) {
                        $cart = Mage::getSingleton('checkout/cart');
                        foreach ($toBeAddedProductIds as $productId) {
                            try {
                                /** @var Mage_Catalog_Model_Product $product */
                                $product = Mage::getModel('catalog/product')
                                    ->setStoreId(Mage::app()->getStore()->getId())
                                    ->load($productId)
                                    ->addCustomOption('is_gift_product', true);

                                $cart->addProduct($product, 1);
                            } catch (Exception $e) {
                                Mage::log('Error adding gift product to cart: ' . $e->getMessage(), null, 'gift-product.log');
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * observe sales_quote_address_collect_totals_before event
     * Update cart qtys to make sure the qty of auto added products are same with their main product
     *
     * @param Varien_Event_Observer $observer
     */
    public function updateCartQtys(Varien_Event_Observer $observer)
    {
        $quote     = $observer->getEvent()->getQuote();
        $store     = Mage::app()->getStore($quote->getStoreId());
        $cartItems = $quote->getItemsCollection();

        foreach ($cartItems as $item) {
            // Child item we check for parent
            if ($item->getParentItemId()) {
                continue;
            }

            $productId = $item->getProduct()->getId();
            $rules     = Mage::getSingleton('vax_salesrule/rule_freegift_validator')->getRules($store->getWebsiteId(), $quote->getCustomerId(), $productId);
            if (count($rules)) {
                $autoAddedProductIds = [];
                foreach ($rules as $rule) {
                    $ids                 = explode(',', $rule->getData('gift_product_ids'));
                    $autoAddedProductIds = array_merge($autoAddedProductIds, $ids);
                }
                if (count($autoAddedProductIds) == 0)
                    continue;

                $qty       = $item->getQty();
                $isDeleted = $item->isDeleted();

                // find the auto-added products that relates to this product
                /** @var Mage_Sales_Model_Quote_Item $_item */
                foreach ($cartItems as $_item) {
                    if ($_item->isDeleted())
                        continue;

                    if (in_array($_item->getProduct()->getId(), $autoAddedProductIds)
                        && $_item->getOptionByCode('is_gift_product')
                        && $_item->getOptionByCode('is_gift_product')->getValue()
                    ) {

                        $this->_setFreeGiftItemPrice($_item);
                        if ($_item->getChildren()) {
                            /**
                             * this fixed for bundle product as free gift
                             * Making bundle price type dynamic will force
                             * its price calculated by its children, which will be zero also
                             */
                            $_item->getProduct()->setPriceType(Mage_Bundle_Model_Product_Type::CALCULATE_CHILD);
                            foreach ($_item->getChildren() as $_child) {
                                $this->_setFreeGiftItemPrice($_child);
                            }
                        }

                        $_item->setQty($qty);
                        $_item->isDeleted($isDeleted);
                    }
                }
            }
        }
    }

    /**
     * Set gift item price to zero
     *
     * @param $item Mage_Sales_Model_Quote_Item
     * @return Mage_Sales_Model_Quote_Item
     */
    protected function _setFreeGiftItemPrice($item)
    {
        /**
         * reset product object to avoid setting price of
         * the same product which was added to cart manually
         */
        $item->setData('product', null);
        $item->getProduct()->setPrice(0.0);
        return $item;
    }
}