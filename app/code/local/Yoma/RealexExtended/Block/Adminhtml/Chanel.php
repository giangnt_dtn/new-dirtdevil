<?php
class Yoma_RealexExtended_Block_Adminhtml_Chanel extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

        $this->_controller = "adminhtml_chanel";
        $this->_blockGroup = "realexExtended";
        $this->_headerText = Mage::helper("realexExtended")->__("Advanced Fraud Settings");
        parent::__construct();
        $this->_removeButton('add');
	
	}

}