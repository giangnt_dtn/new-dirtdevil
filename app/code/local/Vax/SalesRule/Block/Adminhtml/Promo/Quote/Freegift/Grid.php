<?php

class Vax_SalesRule_Block_Adminhtml_Promo_Quote_Freegift_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize grid
     * Set sort settings
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('promo_quote_freegift_grid');
        $this->setDefaultSort('sort_order');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Add websites to sales rules collection
     * Set collection
     *
     * @return Mage_Adminhtml_Block_Promo_Quote_Grid
     */
    protected function _prepareCollection()
    {
        /** @var $collection Mage_SalesRule_Model_Mysql4_Rule_Collection */
        $collection = Mage::getModel('salesrule/rule')
            ->getResourceCollection()
            ->setFlag('include_free_gift_rules', true);

        $collection->addWebsitesToResult();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * Add grid columns
     *
     * @return Mage_Adminhtml_Block_Promo_Quote_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('rule_id', [
            'header' => Mage::helper('salesrule')->__('ID'),
            'align'  => 'right',
            'width'  => '50px',
            'index'  => 'rule_id',
        ]);

        $this->addColumn('name', [
            'header' => Mage::helper('salesrule')->__('Rule Name'),
            'align'  => 'left',
            'index'  => 'name',
        ]);

        $this->addColumn('from_date', [
            'header' => Mage::helper('salesrule')->__('Date Start'),
            'align'  => 'left',
            'width'  => '120px',
            'type'   => 'date',
            'index'  => 'from_date',
        ]);

        $this->addColumn('to_date', [
            'header'  => Mage::helper('salesrule')->__('Date Expire'),
            'align'   => 'left',
            'width'   => '120px',
            'type'    => 'date',
            'default' => '--',
            'index'   => 'to_date',
        ]);

        $this->addColumn('is_active', [
            'header'  => Mage::helper('salesrule')->__('Status'),
            'align'   => 'left',
            'width'   => '80px',
            'index'   => 'is_active',
            'type'    => 'options',
            'options' => [
                1 => 'Active',
                0 => 'Inactive',
            ],
        ]);

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('rule_website', [
                'header'   => Mage::helper('salesrule')->__('Website'),
                'align'    => 'left',
                'index'    => 'website_ids',
                'type'     => 'options',
                'sortable' => false,
                'options'  => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(),
                'width'    => 200,
            ]);
        }

        $this->addColumn('sort_order', [
            'header' => Mage::helper('salesrule')->__('Priority'),
            'align'  => 'right',
            'index'  => 'sort_order',
            'width'  => 100,
        ]);

        parent::_prepareColumns();
        return $this;
    }

    /**
     * Retrieve row click URL
     *
     * @param Varien_Object $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getRuleId()]);
    }

}
