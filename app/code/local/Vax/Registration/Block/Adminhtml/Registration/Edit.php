<?php
/**
 * Vax Registration Edit/Create class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */

class Vax_Registration_Block_Adminhtml_Registration_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Create the outer form wrapper
     */
    protected function _construct()
    {
        parent::_construct();
 
        $this->_objectId = 'id';
        $this->_blockGroup = 'vax_registration';
        $this->_controller = 'adminhtml_registration';
        $this->_mode = 'edit';
        // Vax/Registration/Block/Adminhtml/Registration/Edit.php
    }
 
    protected function _prepareLayout()
    {
        $this->_removeButton('save');
 
        $this->_addButton('save_and_continue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);
        
        $this->_addButton('resend_email', array(
            'label' => Mage::helper('adminhtml')->__('Resend Email'),
            'onclick'   => 'setLocation(\'' . Mage::helper("adminhtml")->getUrl("/registration/resendemail/",array("registration_id"=> $this->getRequest()->getParam('registration_id'))). '\')',
            'class' => 'resend_email',
                ), 0);
        
        $this->_formScripts[] = "
                        function saveAndContinueEdit(){
                                editForm.submit($('edit_form').action+'back/edit/');
                        }
                ";

         // Load Wysiwyg on demand and Prepare layout
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled() && ($block = $this->getLayout()->getBlock('head'))) {
            $block->setCanLoadTinyMce(true);
        }

        parent::_prepareLayout();
    }
 
    /**
     * Return the title string to show above the form
     *
     * @return string
     */
    public function getHeaderText()
    {
        $model = Mage::registry('current_registration');
        if ($model && $model->getId()) {
            return $this->__('Edit Registration %s (%s)', $this->htmlEscape($model->getId()), $this->htmlEscape($model->getModel())
            );
        } else {
            return $this->__('New Registration');
        }
    }
}