<?php
class Yoma_RealexExtended_Model_Resource_Chanel_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('realexExtended/chanel');
    }

    /**
     * Filter by distinct profile
     *
     * @return $this
     */
    public function addDistinctProfileFilter(){

        $this->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('profile_name')
            ->distinct(true);

        return $this;
    }

    /**
     * Filter by website
     *
     * @param int $websiteId
     * @return $this
     */
    public function addWebsiteFilter($websiteId){

        $this->addFieldToFilter('website_id',$websiteId);

        return $this;
    }
}
