<?php

class DirtDevil_Catalog_Block_Category_View extends Mage_Catalog_Block_Category_View {
    /**
     * Prints Spares and Accessories on a separate line in the title
     *
     * @return  void
     */
    public function getSparesTitle() {

        $_category = $this->getCurrentCategory();
        $title = Mage::helper('catalog/output')->getCategoryTitle($_category);

        //put Spares & Accessories on a new line, if this is a Spares page
        if (strpos($title, $this->__('Spares')) !== false) {
            //if Spares and Accessories is in the beginning
            if (strpos($title, $this->__('Spares')) == 0) {
                $accessories = $this->__('Accessories');
                $titles = explode($accessories, $title);
                $result_string = array_pop($titles);
                return $this->__('Spares & Accessories') . '<span class="spares-header">' . $result_string . '</span>';
            } //if Spares and Accessories is in th end
            else {
                $result_string = substr($title, 0, strpos($title, $this->__('Spares')));
                return $result_string . '<span class="spares-header">' . $this->__('Spares & Accessories') . '</span>';
            }
        }

        return $title;
    }
}
