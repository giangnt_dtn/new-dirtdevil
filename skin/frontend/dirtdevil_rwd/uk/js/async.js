function addLoadEvent(func) {
    if (window.addEventListener)
        window.addEventListener("load", func, false);
    else if (window.attachEvent)
        window.attachEvent("onload", func);
    else {
        var old = window.onload;
        window.onload = function() {
            if (old) old();
            func();
        };
    }
}

var ie = (function(){
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]
        );
    return v > 4 ? v : undef;
}());

function isBot()
{
    var userAgent = navigator.userAgent.toLowerCase();

    var Browser = {
        Version: (userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
        Googlebot: /googlebot/.test(userAgent),
        Bing: /bingbot/.test(userAgent),
        Check: function() { alert(userAgent); }
    };

    if(Browser.Googlebot || Browser.Bing) {
        return true;
    }

    return false;
}
