var dirtdevil = dirtdevil || {};


/* ===================================================
 *  Templates
 * ====================================================
 */


dirtdevil.templates = {};


dirtdevil.templates.category_listing = {
    init: function() {


        function categoryLayout() {
            displayWidth = jQuery(window).width();
            if (displayWidth > 599) {
                jQuery('.product-shop2').each(function() {
                    desc = jQuery(this).html();
                    that = jQuery(this).parent('.item').find('.product-shop .product-secondary.actions');
                    jQuery(this).remove();
                    jQuery(desc).insertBefore(that).wrap('<div class="product-shop2-alt">');
                });
            } else {
                jQuery('.product-shop2-alt').each(function() {
                    desc = jQuery(this).html();
                    that = jQuery(this).parent('.f-fix').parent('.product-shop').parent('.item').find('.product-primary');
                    jQuery(this).remove();
                    jQuery(desc).insertAfter(that).wrap('<div class="product-shop2">');
                });
            }
        };


        categoryLayout();

        jQuery(window).resize(function() {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function(){
                categoryLayout();
            }, 10);
        });



    }

}

dirtdevil.templates.my_product = {
    init: function() {

        function productSlider() {
            jQuery('.myproducts-list > .product-list-item:gt(0)').hide();
            jQuery('.myproduct-control').on('click', function(e){
                e.preventDefault();
                jQuery('.myproducts-list > .product-list-item:first')
                    .fadeOut(1000)
                    .next()
                    .fadeIn(1000)
                    .end()
                    .appendTo('.myproducts-list');
            })
        };

        productSlider();


    }

}

dirtdevil.templates.social_login = {
    init: function() {
        function loginLoad(){
            jQuery('.inchoo-socialconnect-login').appendTo('.account-login');
        }

        function registerLoad(){
            jQuery('.inchoo-socialconnect-register').appendTo('.account-create form');
        }

        //registerLoad();
        loginLoad();
    }
}



/* ===================================================
 *  END: Templates
 * ====================================================
 */


// Modules
dirtdevil.templates.init = function() {
    dirtdevil.templates.category_listing.init();
    dirtdevil.templates.my_product.init();
    dirtdevil.templates.social_login.init();
}

dirtdevil.init = function() {

    // Init Modules
    this.templates.init();

};

jQuery(document).ready(function() {
    dirtdevil.init();
});

// ====================================================================================
// Product Listing - Toggle div with "Suitable for" on spares and search pages in DE
// ====================================================================================
// this function is here in order to be possible to use it both on search pages and
// spares listers
// Shows the "Suitable for" section of the product
function showSuitableFor() {
    $j('body').on('click', '.also-available-for a', function(e){
        $j(this).closest('.also-available-for').find('.toggle').toggle();
        $j(this).closest('.also-available-for').find('span.skus').toggle();
        return false;
    });
}







