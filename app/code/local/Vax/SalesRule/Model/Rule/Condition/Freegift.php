<?php

class Vax_SalesRule_Model_Rule_Condition_Freegift extends Mage_Rule_Model_Condition_Product_Abstract
{
    public function getDefaultOperatorOptions()
    {
        if (null === $this->_defaultOperatorOptions) {
            $this->_defaultOperatorOptions = [
                '==' => Mage::helper('rule')->__('is')
            ];
        }
        return $this->_defaultOperatorOptions;
    }

    public function loadArray($arr)
    {
        $this->setType($arr['type']);
        $this->setAttribute(isset($arr['attribute']) ? $arr['attribute'] : false);
        $this->setOperator(isset($arr['operator']) ? $arr['operator'] : false);
        $this->setValue(isset($arr['value']) ? $arr['value'] : false);
        $this->setIsValueParsed(isset($arr['is_value_parsed']) ? $arr['is_value_parsed'] : false);
        $this->setFreeGifts(isset($arr['free_gifts']) ? $arr['free_gifts'] : false);

        return $this;
    }

    public function asArray(array $arrAttributes = [])
    {
        $out = [
            'type'               => $this->getType(),
            'attribute'          => $this->getAttribute(),
            'operator'           => $this->getOperator(),
            'value'              => $this->getValue(),
            'is_value_processed' => $this->getIsValueParsed(),
            'free_gifts'         => $this->getFreeGifts()
        ];
        return $out;
    }

    public function getFreegiftsElement()
    {
        $elementParams = [
            'name'               => 'rule[' . $this->getPrefix() . '][' . $this->getId() . '][free_gifts]',
            'value'              => $this->getFreeGifts(),
            'values'             => $this->getValueSelectOptions(),
            'value_name'         => $this->getFreeGifts() ? $this->getFreeGifts() : '...',
            'after_element_html' => $this->getValueAfterElementHtml(),
            'explicit_apply'     => $this->getExplicitApply(),
        ];

        return $this->getForm()->addField($this->getPrefix() . '__' . $this->getId() . '_free_gifts__value',
            $this->getValueElementType(),
            $elementParams
        )->setRenderer($this->getValueElementRenderer());
    }

    public function getFreegiftsElementHtml()
    {
        return ' then adds ' . $this->getFreegiftsElement()->getHtml() . ' for free';
    }

    public function asHtml()
    {
        $html = $this->getTypeElementHtml()
            . $this->getAttributeElementHtml()
            . $this->getOperatorElementHtml()
            . $this->getValueElementHtml()
            . $this->getFreegiftsElementHtml()
            . $this->getRemoveLinkHtml()
            . $this->getChooserContainerHtml();
        return $html;
    }
}