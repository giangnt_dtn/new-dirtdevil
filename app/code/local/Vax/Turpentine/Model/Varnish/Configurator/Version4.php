<?php
class Vax_Turpentine_Model_Varnish_Configurator_Version4
    extends Nexcessnet_Turpentine_Model_Varnish_Configurator_Version4 {

    /**
     * Get the hostname for cookie normalization
     *
     * @return string
     */
    protected function _getNormalizeCookieTarget() {
        return trim( Mage::getStoreConfig(
            'turpentine_vcl/normalization/cookie_target' ) );
    }

    /**
     * Get the regex for cookie normalization
     *
     * @return string
     */
    protected function _getNormalizeCookieRegex() {
        return trim( Mage::getStoreConfig(
            'turpentine_vcl/normalization/cookie_regex' ) );
    }

    /**
     * Build the list of template variables to apply to the VCL template
     *
     * @return array
     */
    protected function _getTemplateVars() {
        $vars = parent::_getTemplateVars();

        $vars['custom_debug_headers'] = $this->_getEnableDebugHeaders();

        if( Mage::getStoreConfig( 'turpentine_vcl/normalization/cookie_regex' ) ) {
            $vars['normalize_cookie_regex'] = $this->_getNormalizeCookieRegex();
        }
        if( Mage::getStoreConfig( 'turpentine_vcl/normalization/cookie_target' ) ) {
            $vars['normalize_cookie_target'] = $this->_getNormalizeCookieTarget();
        }

        return $vars;
    }
}
