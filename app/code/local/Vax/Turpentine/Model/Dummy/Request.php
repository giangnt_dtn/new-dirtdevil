<?php

/**
 * Vax Turpentine Dummy Request.
 *
 * @copyright Vax Ltd
 */
class Vax_Turpentine_Model_Dummy_Request extends Nexcessnet_Turpentine_Model_Dummy_Request
{
    /**
     * {@inheritdoc}
     */
    public function __construct( $uri=null ) {
        $uri = str_replace('|', urlencode('|'), $uri);

        try {
            return parent::__construct($uri);
        } catch (Exception $e) {
            Mage::log('Invalid URI supplied: ' . $uri, null, 'turpentine-error.log');
            throw($e);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function _fixupFakeSuperGlobals($uri)
    {
        $parsedUrl = parse_url($uri);

        if (isset($parsedUrl['path'])) {
            $this->SERVER['REQUEST_URI'] = $parsedUrl['path'];
        }
        if( isset( $parsedUrl['query'] ) && $parsedUrl['query'] ) {
            $this->SERVER['QUERY_STRING'] = $parsedUrl['query'];
            $this->SERVER['REQUEST_URI'] .= '?' . $this->SERVER['QUERY_STRING'];
        } else {
            $this->SERVER['QUERY_STRING'] = null;
        }
        parse_str( $this->SERVER['QUERY_STRING'], $this->GET );
        if( isset( $this->SERVER['SCRIPT_URI'] ) ) {
            $start = strpos( $this->SERVER['SCRIPT_URI'], '/', 9 );
            $sub = substr( $this->SERVER['SCRIPT_URI'], $start );
            $this->SERVER['SCRIPT_URI'] = substr(
                    $this->SERVER['SCRIPT_URI'], 0, $start ) .
                @str_replace(
                    $this->SERVER['SCRIPT_URL'], $parsedUrl['path'],
                    $sub, $c=1 );
        }
        if( isset( $this->SERVER['SCRIPT_URL'] ) ) {
            $this->SERVER['SCRIPT_URL'] = $parsedUrl['path'];
        }
    }
}