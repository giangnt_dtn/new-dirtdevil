$j = jQuery.noConflict();

function updateDiscountTotalStyle() {
    if ($j(window).width()>=600) {
        $j('#discount-total').width($j('.cart-totals-wrapper').first().width()-30);
    } else {
        $j('#discount-total').width($j('#discount-total').parent().width()-40);
    }
}

$j(document).ready(function() {
    // calculate total discount width
   updateDiscountTotalStyle();

    // Cart Bestsellers carousel

        var bestsellers = '.bestsellers ul.mini-products-list';
        jQuery('.product-name').setAllToMaxMinHeight();

        enquire.register('screen and (max-width: ' + 480 + 'px)', {
            match: function () {
                //set images to be tall 45.5% of the window width - equal to its width
                //jQuery('.bestsellers .product-image').css('height', (0.455 * jQuery(window).width()));
            }, unmatch: function () {}
        });
        enquire.register('screen and (min-width: ' + 481 + 'px) and (max-width: ' + 770 + 'px)', {
            match: function () {
                moreViewsThumbnails(3, bestsellers, 1);
            }, unmatch: function () {}
        });
        enquire.register('screen and (min-width: ' + 771 + 'px) and (max-width: ' + 1000 + 'px)', {
            match: function () {
                //spares carousel, 3 items per page, 1st page visible initially amd on resize
                moreViewsThumbnails(3, bestsellers, 1);
            }, unmatch: function () {}
        });
        enquire.register('screen and (min-width: ' + 1001 + 'px)', {
            match: function () {
                //spares carousel, 4 items per page, 1st page visible initially amd on resize
                moreViewsThumbnails(2, bestsellers, 1);
            }, unmatch: function () {}
        });

    jQuery('#gift-code-toggle').on('click', function(){
        jQuery('.aw_giftcard .desc').slideToggle();
    })
});

//delayed resize
$j(window).resize( function() {
    if( timer ) { clearTimeout(timer); }
    var timer = setTimeout( function() {
        jQuery('.bestsellers .product-name').setAllToMaxMinHeight();
        alignProductGridActions('.mini-products-list', 'li.product', '.product-details' );
    }, 100 );
});
$j(window).on('delayed-resize', function (e, resizeEvent) {
    updateDiscountTotalStyle();
    setTimeout(updateDiscountTotalStyle, 500);
    setTimeout(updateDiscountTotalStyle, 1500);
    setTimeout(updateDiscountTotalStyle, 2500);
});
