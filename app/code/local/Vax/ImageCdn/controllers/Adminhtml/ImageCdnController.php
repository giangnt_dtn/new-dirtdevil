<?php
/**
 * ImageCdn Adminhtml controller
 *
 * @category    vax_uk
 * @package     Vax_ImageCdn
 * @author      Craig Carnell
 */
class Vax_ImageCdn_Adminhtml_ImageCdnController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Run cache cleaning (locally)
     *
     * @return void
     */
    public function localCleanAction()
    {
        Mage::getSingleton('imagecdn/cache')->clearCache();
        
        //if ($result) {
            $this->_getSession()
                ->addSuccess(Mage::helper('vax_imagecdn')->__('The Local Image Cdn Cache has been deleted'));
        //} else {
        //    $this->_getSession()
        //        ->addError(Mage::helper('vax_imagecdn')->__('Unable to delete image cdn cache'));
        //}
        $this->_redirect('*/system_config/edit', array('section' => $this->getRequest()->getParam('section')));
    }
    
    /**
     * Run cache cleaning (remotely)
     *
     * @return void
     */
    public function remoteCleanAction()
    {
        if(!Mage::helper('vax_imagecdn')->useCdn()) {
            $this->_getSession()
                ->addError(Mage::helper('vax_imagecdn')->__('Remote CDN has not been cleared as CDN is flagged as not in use'));
            $this->_redirect('*/system_config/edit', array('section' => $this->getRequest()->getParam('section')));
            return;
    	}
        
        Mage::Helper('imagecdn')->factory()->clearCache();
        
        $this->_redirect('*/system_config/edit', array('section' => $this->getRequest()->getParam('section')));
    }
}
