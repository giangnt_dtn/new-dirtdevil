function resizeBanner(id) {
    if (jQuery(window).width() > 599) {
        jQuery('#product-banner-' + id).css({height: jQuery('#image-' + id).innerHeight()+'px'});
    }

    jQuery('.product-banner-main-image').each(function(){
        jQuery(this).css({'width': Math.round(jQuery(document).width()/2) + 'px'});
    });
}

//transfers data from the banner template to the dots navigation of the unslider
function sliderClickThroughButtons() {
    /*banner link text - array of all button labels*/
    var bannerLinkTag = jQuery(".slider-link-text");
    var allLinkTexts = new Array();

    /*banner link id for color - array of banner ids*/
    var bannerId = jQuery(".banner ul li .homepage-banner");
    var allLinkIds = new Array();

    //create identical arrays to associate bannerids with their appropriate buttons
    jQuery(".banner ul li").each(function() {
        //text
        var thisText = jQuery(this).find(bannerLinkTag).text();
        allLinkTexts.push(thisText);

        //id
        var id = jQuery(this).find(bannerId).attr('id');
        allLinkIds.push(id);
    });

    for(var i = 0; i < allLinkTexts.length; i++) {
        //text
        var child = ".dots li:nth-child("+(i+1)+")";
        var childText = allLinkTexts[i];
        jQuery(child).text(childText);

        //add the banner id to the dot(button) id, so that it can be recognized for color changing
        jQuery(child).attr('id', allLinkIds[i]);
    }
}

//set the width of the click-through buttons depending on how many banners are visible
function sliderButtonsWidth() {
    var count = $j(".banner ol.dots").children().length;
    if(count > 0) {
        var width = 100/count;
        width += '%';
        $j(".banner ol.dots li.dot").css("width", width );
    }
}


//slider jump fix  - keep the image ratio
// takes banner width and (banner height + Banner buttons height(usually 50px) ) and the banner content height on mobile
function sliderHeightResize(bannerWidth, bannerHeightPlusButtonHeight, mobileContentMinHeight) {
        // current screen width multiplied by slider image height, divided by slider image width keeps its ratio
        var sliderHeight = (jQuery(window).width() * bannerHeightPlusButtonHeight)/bannerWidth;

    if(jQuery(window).width() < 599) sliderHeight += mobileContentMinHeight;
    jQuery(".banner").css("height", sliderHeight);
    jQuery(".banner ul").css("height", sliderHeight);
}

// equalize banners to avoid the page moving on mobile
jQuery.fn.setAllToMaxHeight = function () {
    return this.height(Math.max.apply(this, jQuery.map(this, function (e) {
        return jQuery(e).height()
    })));
};
