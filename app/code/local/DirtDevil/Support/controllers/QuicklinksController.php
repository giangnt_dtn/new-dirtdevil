<?php
class DirtDevil_Support_QuicklinksController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function trackAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));

        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));

        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('order', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Order'),
            'title'=>Mage::helper('dirtdevil_support')->__('Order'),
        ));

        $this->renderLayout();
    }

    public function deliveryAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('delivery', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Delivery'),
            'title'=>Mage::helper('dirtdevil_support')->__('Delivery'),
        ));

        $this->renderLayout();
    }

    public function returnsAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('returns', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Returns'),
            'title'=>Mage::helper('dirtdevil_support')->__('Returns'),
        ));

        $this->renderLayout();
    }

    public function faultAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('fault', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Report a fault'),
            'title'=>Mage::helper('dirtdevil_support')->__('Report a fault'),
        ));

        $this->renderLayout();
    }

    public function orderAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('quicklinks.order.frame')->assign('cms_block', 'how_to_order');
        $this->renderLayout();
    }

    public function videosAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('register', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Help Videos'),
            'title'=>Mage::helper('dirtdevil_support')->__('Help Videos'),
        ));

        $this->renderLayout();
    }

    public function guidesAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('guides', array(
            'label'=>Mage::helper('dirtdevil_support')->__('User Guides'),
            'title'=>Mage::helper('dirtdevil_support')->__('User Guides'),
        ));

        $this->renderLayout();
    }

    public function downloaduserguideAction()
    {
        $lang = substr(Mage::app()->getLocale()->getLocaleCode(), 0, 2);
        $productId = $this->getRequest()->getParam('product-id');
        $vaxLibraryUrl = 'http://library.vax.co.uk/';

        $product = Mage::getModel('catalog/product')->load( $productId);
        if (!$product) {
            return null;
        }

        // Make cURL request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  $vaxLibraryUrl.'get.php?gpid='.$product->getSku().'&lang='.$lang);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Vax-Api-Key: 25b537055acfa61b3daa847dde47508dbf98c724ce58b908e063a5fedfcae5cb'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_status != 200) {
            return null;
        }
        $decoded = json_decode($data);

        header("Content-type:application/pdf");
        header('Location: ' . $vaxLibraryUrl . $decoded[0]->url);
        exit();
    }

    public function ajaxtrackingAction() {

        $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('order-id'));
        if (!$order->getId()) {
            $this->getResponse()->setBody('{"message": "Sorry, we could not find that order"}');
            $this->getResponse()->sendResponse();
            exit();
        }
        $shipmentCollection = $order->getShipmentsCollection();
        $jsonArray = array();
        $count = 0;
        foreach($shipmentCollection as $shipment) {

            $jsonArray[$count]['createdAt'] = date('jS \of F Y', strtotime($shipment->getCreatedAt()));
            foreach($shipment->getAllTracks() as $b) {
                // we have courier/tracking info - will be ups shipment
                $jsonArray[$count]['carrierCode'] = $b->getCarrierCode();
                $jsonArray[$count]['title'] = $b->getTitle();
                $jsonArray[$count]['trackingCode'] = $b->getNumber();
            }
            $count++;
        }
        $this->getResponse()->setBody(json_encode($jsonArray));
    }

    public function dpadisabilityAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('dpa-disability', array(
            'label'=>Mage::helper('dirtdevil_support')->__('DPA and Disability'),
            'title'=>Mage::helper('dirtdevil_support')->__('DPA and Disability'),
        ));

        $this->renderLayout();
    }

    public function complaintsAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('complaints', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Complaints'),
            'title'=>Mage::helper('dirtdevil_support')->__('Complaints'),
        ));

        $this->renderLayout();
    }

    public function ecoAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('home', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Home'),
            'title'=>Mage::helper('dirtdevil_support')->__('Go to Home Page'),
            'link'=>Mage::getBaseUrl()
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('support', array(
            'label'=>Mage::helper('dirtdevil_support')->__('Support'),
            'title'=>Mage::helper('dirtdevil_support')->__('Support'),
            'link'=>Mage::getUrl('support')
        ));
        $this->getLayout()->getBlock('breadcrumbs')->addCrumb('complaints', array(
            'label'=>Mage::helper('dirtdevil_support')->__('ECO Fiches'),
            'title'=>Mage::helper('dirtdevil_support')->__('ECO Fiches'),
        ));

        $this->renderLayout();
    }
}