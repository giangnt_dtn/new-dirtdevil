/**
 * Javascript for My Account pages
 */
jQuery(document).ready(function () {
    createAccountDropdown();
});

/**
 * Accordion for mobile registration form
 * */
function createAccountDropdown() {

    var button = jQuery('.mobile-accordion-title');
    var accountForm = jQuery('.account-create');

    if(button.length && accountForm.length) {

        button.on('click touchend', function () {
            button.toggleClass('active');
            accountForm.toggle();
        });

        enquire.register('screen and (min-width: ' + 771 + 'px)', {
            match: function () {
                if (button.hasClass('active')) {
                    button.removeClass('active');
                }
                accountForm.show();
            },
            unmatch: function () {
                accountForm.hide();
            }
        });
    } else {
        return false;
    }
}
