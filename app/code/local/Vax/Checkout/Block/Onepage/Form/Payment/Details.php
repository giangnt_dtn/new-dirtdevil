<?php

/**
 * Vax Checkout Onepage Form Payment Details.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Form_Payment_Details extends Mage_Checkout_Block_Onepage_Payment_Methods
{
    /**
     * Returns the number of available payments methods.
     *
     * @return integer
     */
    public function getNumberOfPaymentMethods()
    {
        return count($this->getMethods());
    }

    public function getMethods()
    {
        $methods = $this->getData('methods');
        $method = null;
        if ($methods === null) {
            $quote = $this->getQuote();
            $store = $quote ? $quote->getStoreId() : null;
            $methods = array();

            foreach ($this->helper('payment')->getStoreMethods($store, $quote) as $method) {
                if ($this->_canUseMethod($method) && $method->isApplicableToQuote(
                        $quote,
                        Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL
                    )) {
                    $this->_assignMethod($method);
                    $methods[] = $method;
                }
            }
            $this->setData('methods', $methods);
        }
        return $methods;
    }
}