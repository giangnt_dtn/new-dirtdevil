<?php
/**
 * Vax Limited
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
 **/
class Vax_Checkout_Model_Session extends Mage_Checkout_Model_Session
{
    /**
     * Fix the continue shopping url for nginx on port 80.
     *
     * @param bool $flag
     * @return string
     */
    public function getContinueShoppingUrl($flag = false)
    {
        $url = parent::getContinueShoppingUrl($flag);

        if($url && !empty($url)) {
            $url = str_replace(":8080", "", $url);
        }

        return $url;
    }
}
