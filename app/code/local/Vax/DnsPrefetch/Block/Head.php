<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class Vax_DnsPrefetch_Block_Head extends Mage_Core_Block_Template
{
    protected $_urlList;

    /**
     * Unserialize the list.
     *
     * @return array|mixed
     */
    public function getUrlList() {
        if (!$this->_urlList) {
            $list = Mage::getStoreConfig('vax_dnsprefetch/general/url_array');

            if (is_string($list) && $list) {
                $this->_urlList = unserialize($list);
            } else {
                $this->_urlList = (array)$list;
            }
        }

        return $this->_urlList;
    }
}
