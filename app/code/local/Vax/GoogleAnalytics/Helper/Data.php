<?php
/**
 * Google Analytics Data Helper.
 *
* @copyright Vax Ltd
*/
class Vax_GoogleAnalytics_Helper_Data extends Mage_GoogleAnalytics_Helper_Data
{
    /**
     * Config paths for using throughout the code
     */
    const XML_PATH_TAG_ENABLED           = 'vax_googleanalytics/other/enabled';
    const XML_PATH_TAG_ID                = 'vax_googleanalytics/other/container';

     /**
     * Checks whether tag manager needs to be inserted.
     *
     * @param mixed $store
     *
     * @return bool
     */
    public function isGoogleTagManagerAvailable($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_TAG_ENABLED, $store);
    }

    /**
     * Returns the container id value.
     *
     * @param integer $storeId
     *
     * @return string
     */
    public function getContainerId($storeId = null)
    {
       return (string) Mage::getStoreConfig(self::XML_PATH_TAG_ID, $storeId);
    }
}
