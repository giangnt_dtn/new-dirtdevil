<?php
class Vax_Prodfaqs_Block_ProductFaqs extends FME_Prodfaqs_Block_ProductFaqs
{
    public function getProductRelatedFaqs($publicOnly = true, $questionType = null){
		$product_id = $this->getProduct()->getId();
		$product_faqs = Mage::getModel('prodfaqs/prodfaqs')->getFaqsOfProduct($product_id, $publicOnly, $questionType);
		return $product_faqs; 
    }	
}