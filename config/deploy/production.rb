set  :user, "deploy"

set :scm,              :none
set :repository,       "."
set :local_repository, "."

set :copy_via,         :scp
set :deploy_via,       :copy
set :deploy_to,        '/var/www/vhosts/dirtdevil_uk'
set :copy_strategy,    :export
set :copy_exclude,     [".git/*", "var/*"]

#after('deploy:create_symlink', 'magento:redis:set')
