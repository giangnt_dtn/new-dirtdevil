<?php

/**
 * Vax Rest Payment Helper.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Rest_Helper_Payment extends Mage_Core_Helper_Abstract
{
    public function translateException($ex)
    {
        $message = '';

        switch ($ex->getCode()) {
            case 101:
            case 102:
                $message .= "Payment Failed <br /> <br /> The customer's card has been declined by their bank. Please try an alternative payment method.";
                break;
            case 103:
                $message .= "Payment Failed <br /> <br /> The customer's card has been reported lost or stolen. Please do not attempt this transaction again.";
                break;
            default:
                $message .= $ex->getMessage();
                break;
        }

        return $message;
    }
}