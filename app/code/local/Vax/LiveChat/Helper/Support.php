<?php

/**
 * Support Us Live Chat Helper.
 *
 * @copyright Vax Ltd
 */
class Vax_LiveChat_Helper_Support extends Vax_LiveChat_Helper_Data
{
    /**
     * Checks whether chat is enabled.
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return boolean
     */
    public function isEnabled($product)
    {
        if ( ! $this->isModuleEnabled()) {
            return false;
        }

        if ( ! Mage::getStoreConfigFlag('vax_livechat/support/is_enabled')) {
            return false;
        }

        if($product == null) {
            return false;
        }

        return (boolean) $product->getIsSupportChatEnabled();
    }

    /**
     * Returns the chat endpoint url.
     *
     * @return string
     */
    public function getEndpoint()
    {
        return Mage::getStoreConfig('vax_livechat/support/endpoint');
    }

    /**
     * Returns the script url.
     *
     * @return string
     */
    public function getScriptUrl()
    {
        return Mage::getStoreConfig('vax_livechat/support/js_script');
    }

    /**
     * Returns the deployment ID.
     *
     * @return string
     */
    public function getDeploymentID()
    {
        return Mage::getStoreConfig('vax_livechat/support/deployment_id');
    }

    /**
     * Returns the API username.
     *
     * @return string
     */
    public function getUsername()
    {
        return Mage::getStoreConfig('vax_livechat/support/username');
    }

    /**
     * Returns the API password.
     *
     * @return string
     */
    public function getPassword()
    {
        return Mage::getStoreConfig('vax_livechat/support/password');
    }

    /**
     * Returns the chat window height.
     *
     * @return string
     */
    public function getWindowHeight()
    {
        return Mage::getStoreConfig('vax_livechat/support/window_height');
    }

    /**
     * Returns the chat window width.
     *
     * @return string
     */
    public function getWindowWidth()
    {
        return Mage::getStoreConfig('vax_livechat/support/window_width');
    }

    /**
     * Returns whether the callbackHandler is required
     *
     * @return boolean
     */
    public function requiresCallback() {
        return false;
    }
}
