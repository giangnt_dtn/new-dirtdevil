<?php

/**
 * OPC Order Response.
 *
 * @copyright Vax Ltd
 */
abstract class Vax_Checkout_Model_Response_Opc_Order extends Vax_Checkout_Model_Response_Opc implements Serializable
{
    /**
     * Current Order.
     *
     * @var Mage_Sales_Model_Order
     */
    protected $_order;

    /**
     * Current Message.
     *
     * @var string
     */
    protected $_message;

    /**
     * Redirect URL.
     *
     * @var string
     */
    protected $_url;

    /**
     * Constructor
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @return void
     */
    public function __construct(Mage_Sales_Model_Order $order, $message = '', $url = null)
    {
        $this->_order   = $order;
        $this->_message = $message;
        $this->_url = $url;
    }

    /**
     * Returns the associated order.
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * {@inheritdoc}
     */
    public function getOpcMessage()
    {
        return $this->_message;
    }


    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(
            array(
                'msg'     => $this->_message,
                'orderID' => $this->_order->getId()
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($data)
    {
        $data = unserialize($data);

        $this->_message = $data['msg'];
        $this->_order   = Mage::getModel('sales/order')->load($data['orderID']);

        return $this;
    }
}