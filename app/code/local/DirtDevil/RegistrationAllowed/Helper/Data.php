<?php

/**
 * DirtDevil Customer Data Helper.
 *
 * @copyright Vax Ltd 2015
 */
class DirtDevil_RegistrationAllowed_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Checks whether an admin is logged in.
     *
     * @return boolean
     */
    public function isRegistrationAllowed()
    {
        return Mage::getStoreConfig('customer/create_account/is_allowed');
    }
}