jQuery(document).ready(function ($j) {


        var controller = "/support/service/sku/";
        //reset placeholders
        $j('.nav-group-dropdown .product-sku').empty();
        $j('.nav-group-dropdown .product-image').empty();
        $j('.nav-group-dropdown .product-sku').empty();
        $j('.nav-group-dropdown .product-url').hide();

        //hide the mini view before a product is selected
        // $j('.product-mini-view').hide();

        //hide links section before a product is selected
        $j('.nav-group-dropdown .product-links').hide();

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        function loadProduct(sku) {
            $j('.nav-group-dropdown #product-selector-container').show();

            sku = getUrlParameter('sku');
            var url = controller + "?sku=" + sku;

            $j.ajax({
                url: url,
                context: document.body,
                data:{},
                success: function (data, status) {

                    var response = $j.parseJSON(data);
                    if (response.productName !== null) {
                        $j('.search-box-error').empty();

                        //show miniview and product links
                        $j('.product-links').show();
                        $j('.product-mini-view').show();

                        //empty mini view elements
                        $j('.product-sku').empty();
                        $j('.product-image').attr('src', '');
                        $j('.product-url').attr('href', '');

                        //empty product sections
                        $j('.spares-li').hide();
                        $j('.features-li').hide();
                        $j('.whats-box-li').hide();
                        $j('.downloads-li').hide();
                        $j('.reviews-li').hide();

                        //empty product sections links
                        $j('.spares-link').attr('href', '');
                        $j('.downloads-link').attr('href', '');
                        $j('.features-link').attr('href', '');
                        $j('.reviews-link').attr('href', '');
                        $j('.whats-box-link').attr('href', '');
                        $j('.questions-link').attr('href', '');
                        $j('.specs-link').attr('href', '');

                        //==========================================

                        //set mini view data
                        $j('.product-url').show();

                        $j('.product-sku').html(response.productSku);
                        $j('.product-image').attr('src', response.productImage);
                        $j('.product-url').attr('href', response.productUrl);
                        $j('.product-sku').html(' ' + response.productSku + ' ');

                        //set "Search results ..." title
                        $j('.product-name').html(response.productName);

                        //show product sections if they appear on that product's page
                        if (response.hasSpares) {
                            $j('.spares-li').css('display', 'inline-block');
                        }
                        if (response.hasDownloads) {
                            $j('.downloads-li').css('display', 'inline-block');
                        }
                        if (response.hasFeatures) {
                            $j('.features-li').css('display', 'inline-block');
                        }
                        if (response.hasReviews) {
                            $j('.reviews-li').css('display', 'inline-block');
                        }
                        if (response.hasWhatsBox) {
                            $j('.whats-box-li').css('display', 'inline-block');
                        }

                        //add links to the product sections
                        $j('.spares-link').attr('href', response.productUrl + '#spares-and-accessories');
                        $j('.downloads-link').attr('href', response.productUrl + '#downloads');
                        $j('.features-link').attr('href', response.productUrl + '#features');
                        $j('.reviews-link').attr('href', response.productUrl + '#reviews');
                        $j('.whats-box-link').attr('href', response.productUrl + '#whats-in-the-box');
                        $j('.questions-link').attr('href', response.productUrl + '#faqs');
                        $j('.specs-link').attr('href', response.productUrl + '#specifications');

                    } else {
                        $j('.search-box-error').html('<p class="note-msg error"> No Product found!</p>');
                    }
                }
            });
        }

        $j('.nav-support #model-dropdown').change(function () {

            if ($j('.nav-support #model-dropdown option:selected').val() === '-') {
                return false;
            }

            $j('.nav-support #search-reg').val($j('#model-dropdown option:selected').val());

            $j('.nav-support #spares-model-number-search-menu').submit();
        });

        // Do product code search on submit
        $j('.nav-support #spares-model-number-search-menu').on('submit', function (event) {
            var input = $j('.nav-support #search-reg');
            input.val($j.trim(input.val()));


            if ($j.trim(input.val()) === '') {
                return false;
            }
            //sync dropdown with input box
            var element = $j('.nav-support #model-dropdown');
            element.value = input.val();

            loadProduct(input.val());

            return false;
        });

        $j('.nav-support #search-reg-go').on('click', function (event) {
            var input = $j('.nav-support #search-reg');
            input.val($j.trim(input.val()));

            if ($j.trim(input.val()) === '') {
                return false;
            }

            window.location.href = "/service?sku=" + input.val();

        });

        if (window.location.search.indexOf('sku') > -1) {
            loadProduct();
        }







});
