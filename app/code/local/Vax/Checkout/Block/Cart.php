<?php

/**
 * Vax Checkout Cart Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Cart extends Mage_Checkout_Block_Cart
{
    /**
     * Returns the continue shopping url.
     *
     * @return string
     */
    public function getContinueShoppingUrl()
    {
        $url = $this->getData('continue_shopping_url');

        if (is_null($url)) {
            $url = Mage::getSingleton('checkout/session')->getContinueShoppingUrl(true);
            if (!$url) {
                $url = Mage::getUrl();
            }
            $this->setData('continue_shopping_url', $url);
        }

        /* fix for nginx on port 80 */
        /* this code may be run twice, in getContinueShoppingUrl but needs to run if URL already set (not null) */
        if($url && !empty($url)) {
            $url = str_replace(":8080", "", $url);
        }

        return $url;
    }

    /**
     * Returns item HTML for small screen.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     */
    public function getItemSmallHtml(Mage_Sales_Model_Quote_Item $item)
    {
        $renderer = $this->getLayout()
            ->createBlock('checkout/cart_item_renderer')
            ->setTemplate('checkout/cart/item/small.phtml')
            ->setItem($item)
            ->setRenderedBlock($this);

        return $renderer->toHtml();
    }
}