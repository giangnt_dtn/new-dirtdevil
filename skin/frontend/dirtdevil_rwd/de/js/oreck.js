var $j = jQuery.noConflict();
$j(document).ready(function() {
    $j('.menu-container').css('max-height', ($j(window).height()-68) + 'px');
    /** Handle scrolling */
    $j('ul.menu-left li a').on('click',function(e) {
        e.preventDefault();

        $j(document).off("scroll");

        var $target = this.hash.substring(1);

        // Account for height of fixed header
        margin_top = $j(window).width() > 375 ? 10 : 75;
        var targetOffset = $j('a[name=' + $target + ']').offset().top -margin_top;

        $j('html, body').stop().animate({
            'scrollTop': targetOffset
        }, 900, 'swing', function () {
        });
    });

    $j('#arrow-up').click(function() {
        $j('html, body').stop().animate({
            'scrollTop': 0
        }, 900);
    });

    $j(window).scroll(function() {
        console.log($j(window).scrollTop());
        if ($j(window).scrollTop() > $j(window).height()*0.85) {

            $j('#arrow-up').stop().animate({right: 0 + 'px'}, 400);
        } else {
            $j('#arrow-up').stop().animate({right: -200 + 'px'}, 400);
        }
    })


    // Handling top menu
    $j('.menu-icon').click(function() {
        $j('.menu-container').fadeToggle(300, 'swing', function() {
            $j(this).toggleClass('expanded');
        });
    });

    positionAddToCart();

    $j(window).resize(function() {
        if ($j('body').width() > 375) {
            $j('.menu-container').show();
        } else {
            $j('.menu-container').hide();
        }

        positionAddToCart();
    });
});

function positionAddToCart()
{
    $j('.add-to-cart.hide-for-small').css({position: 'absolute', 'top': '10px', 'right': '10px', 'left': 'auto'});
    if ($j(window).width() > 940) {
        setTimeout("offset = $j('.add-to-cart.hide-for-small').offset();$j('.add-to-cart.hide-for-small').css({position: 'fixed', 'top': '215px', 'left': (offset.left-$j(window).scrollLeft()) + 'px', 'right':'auto'});", 500);
    } else {
        setTimeout("offset = $j('.add-to-cart.hide-for-small').offset();$j('.add-to-cart.hide-for-small').css({position: 'fixed', 'top': '215px', 'left': 'auto', 'right':'10px'});", 500);
    }
}
