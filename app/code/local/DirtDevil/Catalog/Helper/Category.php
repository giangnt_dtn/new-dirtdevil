<?php
/**
 * Catalog category helper
 *
 * @category   DirtDevil
 * @package    DirtDevil_Catalog
 * @author     Vax
 */
class DirtDevil_Catalog_Helper_Category extends Mage_Catalog_Helper_Category
{
    /**
     * Retrieve current spares by type categories
     *
     * @return  Varien_Data_Tree_Node_Collection|Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection|array
     */
    public function getPartsSubCategories()
    {
        $partsRootCategory = Mage::getModel('catalog/category')->load(25);
        $_partsSubCategories = $partsRootCategory->getChildrenCategories($partsRootCategory);

        return $_partsSubCategories;
    }

    /**
     * Retrieve current spares by type categories
     *
     * @return  Varien_Data_Tree_Node_Collection|Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection|array
     */
    public function getMachinesSubCategories()
    {
        $machinesRootCategory = Mage::getModel('catalog/category')->load(26);
        $_machinesSubCategories = $machinesRootCategory->getChildrenCategories($machinesRootCategory);

        return $_machinesSubCategories;
    }

    public function getThumbnailUrl($imageName)
    {
        return "/media/catalog/category/" . $imageName;
    }

    public function useAjaxCheckboxes() {

        if( Mage::getStoreConfig('catalog/ajax_category/enabled')) {
            return true;
        }

        return false;
    }

    public function useAjaxPriceSlider() {

        if( Mage::getStoreConfig('catalog/ajax_category/enabled') &&
            Mage::getStoreConfig('catalog/ajax_category/enable_price_slider')) {

            // check for 1column template = no filters
            $root = Mage::app()->getLayout()->getBlock('root');
            if($root && $root->getTemplate() === "page/1column.phtml") {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Ensures that sting can be used as HTML id.
     *
     * @param $string
     * @return string
     */
    public function stringToHtmlId($string)
    {
        return strtolower(str_replace(array(' ', '.', ';', ',', '!', '?', '#', '(', ')'), '-', $string));
    }
}
