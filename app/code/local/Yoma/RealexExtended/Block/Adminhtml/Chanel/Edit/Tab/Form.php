<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("realexExtended_form", array("legend" => Mage::helper("realexExtended")->__("Rule")));

        $fieldset->addType('boolean', 'Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Form_Renderer_Fieldset_Boolean');

        $fieldset->addType('tss', 'Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Form_Renderer_Fieldset_Tss');

        if($this->getRequest()->getParam('id')){

            $fieldset->addField('profile_name', 'text', array(
                'name'  => 'profile_name',
                'label'     => 'Profile Name',
                'disabled' => true,

            ));
        }else{
            $fieldset->addField('profile_name', 'text', array(
                'name'  => 'profile_name',
                'label'     => 'Profile Name',

            ));
        }

        $fieldset->addField('billing_country_id', 'select', array(
            'name'  => 'billing_country_id',
            'label'     => 'Billing Country',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_country')->toOptionArray(),
        ));

        $fieldset->addField('shipping_country_id', 'select', array(
            'name'  => 'shipping_country_id',
            'label'     => 'Shipping Country',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_country')->toOptionArray(),
        ));

        $fieldset->addField('currency', 'select', array(
            'name'  => 'currency',
            'label'     => 'Currency',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_currency')->toOptionArray(),
        ));

        $fieldset->addField('card_type', 'select', array(
            'name'  => 'card_type',
            'label'     => 'Card Types',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_cards')->toOptionArray(),
        ));

        $fieldset->addField('ticket_size_from', 'text', array(
            'name'  => 'ticket_size_from',
            'label'     => 'Min Order Value',
        ));

        $fieldset->addField('ticket_size_to', 'text', array(
            'name'  => 'ticket_size_to',
            'label'     => 'Max Order Value',
        ));

        $fieldset->addField('three_d_secure', 'select', array(
            'name'  => 'three_d_secure',
            'label'     => 'Use 3DSecure',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
        ));

        $fieldset->addField('three_d_secure_profile', 'boolean', array(
            'name'  => 'three_d_secure_profile',
            'label'     => '3DSecure Profile',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_profiles')->toOptionArray(),
        ));

        $fieldset->addField('sub_account', 'text', array(
            'name'  => 'sub_account',
            'label'     => 'Sub Account',
        ));

        $fieldset->addField('rv_sub_account', 'text', array(
            'name'  => 'rv_sub_account',
            'label'     => 'RealVault Sub Account',
        ));

        $fieldset->addField('settle_type', 'select', array(
            'name'  => 'settle_type',
            'label'     => 'Settlement Type',
            'values'   => Mage::getSingleton('realex/realex_source_paymentAction')->toOptionArray(),
        ));


        if (Mage::getSingleton("adminhtml/session")->getChanelData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getChanelData());
            Mage::getSingleton("adminhtml/session")->setChanelData(null);
        } elseif (Mage::registry("chanel_data")) {
            $form->setValues(Mage::registry("chanel_data")->getData());
        }

        return parent::_prepareForm();
    }

}