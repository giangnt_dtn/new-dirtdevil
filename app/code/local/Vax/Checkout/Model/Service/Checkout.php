<?php

/**
 * Realex Checkout Service.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Service_Checkout implements Vax_Checkout_Model_Interface_Service_Checkout
{
    /**
     * The Current Order.
     *
     * @var Mage_Sales_Model_Order
     */
    protected $_order;

    /**
     * Checkout Session.
     *
     * @var Mage_Checkout_Model_Session
     */
    protected $_session;

    /**
     * {@inheritdoc}
     */
    public function hasProcessedOrder()
    {
        if ($this->_getSession()->getProcessedOrderId()) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function hasFailedOrder()
    {
        if ($this->_getSession()->getFailedOrderId()) {
            return true;
        }

        return false;
    }

    /**
     * Returns the last order.
     *
     * @return Mage_Sales_Model_Order
     */
    public function getLastOrder()
    {
        if ($this->hasProcessedOrder()) {
            return Mage::getModel('sales/order')->load($this->_getSession()->getProcessedOrderId());
        }

        if ($this->hasFailedOrder()) {
            return Mage::getModel('sales/order')->load($this->_getSession()->getFailedOrderId());
        }

        return null;
    }

    /**
     * Returns the last response.
     *
     * @return Vax_Checkout_Model_Response_Opc
     */
    public function getLastResponse()
    {
        return $this->_getSession()->getLastResponse();
    }

    /**
     * Returns the current checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        if ( ! $this->_session) {
            $this->_session = Mage::getSingleton('checkout/session');
        }

        return $this->_session;
    }

    /**
     * {@inheritdoc}
     */
    public function submit(Mage_Sales_Model_Order $order)
    {
        $this->_order = $order;

        try
        {
            $this->_getSession()->setLastResponse($this->_place());
        }
        catch (\Vax_CheckoutLimit_Exception $e) {
            return $this->_getFailureResponse($e->getMessage());
        }
        catch (\Exception $ex)
        {
            return $this->_getFailureResponse('Unfortunately we cannot process your order at this time. Please try again later.');
        }

        return $this->getLastResponse();
    }

    /**
     * Places the order.
     *
     * @return array
     */
    protected function _place()
    {
        $this->_dispatchPreOrderEvents();

        try
        {
            $this->_order->place()->save();
            $redirectUrl = $this->_order->getQuote()->getPayment()->getOrderPlaceRedirectUrl();

            // Nasty hack for Yoma_Realex url rewrites.
            $redirectUrl = str_replace('realexAdmin', 'realex', $redirectUrl);

            if ($redirectUrl) {
                $response = $this->_getRedirectResponse($redirectUrl);

                return $response;
            }

            try
            {
                $this->_order->sendNewOrderEmail();
            }
            catch (Exception $ex)
            {
                Mage::log($ex->getMessage());
            }
        }
        catch (Exception $ex)
        {
            return $this->_getFailureResponse($ex->getMessage());
        }
        $this->_getSession()->clear();
        $this->_dispatchPostOrderEvents();

        $response =$this->_getSuccessResponse('Your order has been successfully placed.');
        return $response;
    }

    /**
     * Dispatches the post order events.
     *
     * @return void
     */
    protected function _dispatchPreOrderEvents()
    {
        $this->_dispatchEvent(
            'checkout_type_onepage_save_order',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );

        $this->_dispatchEvent(
            'sales_model_service_quote_submit_before',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );
    }

    /**
     * Dispatches the post order events.
     *
     * @return void
     */
    protected function _dispatchPostOrderEvents()
    {
        $this->_dispatchEvent(
            'sales_model_service_quote_submit_after',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );

        $this->_dispatchEvent(
            'checkout_type_onepage_save_order_after',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );

        $this->_dispatchEvent(
            'checkout_submit_all_after',
            array('order' => $this->_order, 'quote' => $this->_order->getQuote())
        );
    }

    /**
     * Dispatches a new event.
     *
     * @param string $name
     * @param array  $params
     *
     * @return void
     */
    protected function _dispatchEvent($name, $params = array())
    {
        Mage::dispatchEvent($name, $params);
    }

    /**
     * Returns a new opc failure response.
     *
     * @param string $message
     *
     * @return Vax_Checkout_Model_Response_Opc_Order_Failure
     */
    protected function _getFailureResponse($msg)
    {
        return $this->_getSession()
                    ->setLastResponse(new Vax_Checkout_Model_Response_Opc_Order_Failure($this->_order, $msg))
                    ->getLastResponse();

    }

    /**
     * Returns a new opc failure response.
     *
     * @param string $message
     *
     * @return Vax_Checkout_Model_Response_Opc_Order_Failure
     */
    protected function _getSuccessResponse($msg)
    {
        return $this->_getSession()
                    ->setLastResponse(new Vax_Checkout_Model_Response_Opc_Order_Success($this->_order, $msg))
                    ->setLastRealOrderId($this->_order->getIncrementId())
                    ->getLastResponse();
    }

    /**
     * Returns a new opc redirect response.
     *
     * @return Vax_Checkout_Model_Response_Opc_Order_Redirect
     */
    protected function _getRedirectResponse($url = null)
    {
        return $this->_getSession()
            ->setLastResponse(new Vax_Checkout_Model_Response_Opc_Order_Redirect($this->_order, '', $url))
            ->getLastResponse();
    }

    /**
     * Resets the checkout.
     *
     * @return Vax_Realex_Model_Service_Checkout
     */
    public function reset()
    {
        $this->_getSession()->setLastResponse(null);
        $this->_getSession()->setFailedOrderId(null);
        $this->_getSession()->setProcessedOrderId(null);

        return $this;
    }
}