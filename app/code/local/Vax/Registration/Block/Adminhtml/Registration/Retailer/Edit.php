<?php
/**
 * Vax Registration (Retailer) Edit/Create class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      C Carnell
 */

class Vax_Registration_Block_Adminhtml_Registration_Retailer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Create the outer form wrapper
     */
    public function __construct()
    {
        parent::__construct();
 
        $this->_blockGroup = 'vax_registration';
        $this->_objectId = 'retailer_id';
        $this->_controller = 'adminhtml_registration_retailer';
        
        if( $this->getRequest()->getParam($this->_objectId) ) {
            $retailerData = Mage::getModel('vax_registration/retailer')
                ->load($this->getRequest()->getParam($this->_objectId));
            Mage::register('retailer_data', $retailerData);
        }
    }

    /**
     * Return the title string to show above the form
     *
     * @return string
     */
    public function getHeaderText()
    {
        $model = Mage::registry('retailer_data');
        if ($model && $model->getId()) {
            return $this->__('Edit Retailer %s (%s)', $this->htmlEscape($model->getId()), $this->htmlEscape($model->getTitle())
            );
        } else {
            return $this->__('New Retailer');
        }
    }
}