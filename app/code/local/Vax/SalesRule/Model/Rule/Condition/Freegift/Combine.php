<?php

class Vax_SalesRule_Model_Rule_Condition_Freegift_Combine extends Mage_SalesRule_Model_Rule_Condition_Product_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('vax_salesrule/rule_condition_freegift_combine');
    }

    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml() .
            Mage::helper('rule')->__('Add free gift products using these conditions:');
        if ($this->getId() != '1') {
            $html .= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    /**
     * Generate a conditions data
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $conditions = [
            ['value' => '', 'label' => Mage::helper('rule')->__('Please choose a condition to add...')],
        ];

        $conditions = array_merge_recursive(
            $conditions,
            [
                [
                    'label' => Mage::helper('catalog')->__('Product Attribute'),
                    'value' => $this->_getAttributeConditions(self::PRODUCT_ATTRIBUTES_TYPE_PRODUCT),
                ]
            ]
        );
        return $conditions;
    }

    /**
     * CHeck whether the product attribute information exists and initialize it if missing
     * @return $this
     */
    protected function _initializeProductAttributesInfo()
    {
        if (is_null($this->_productAttributesInfo)) {
            $this->_productAttributesInfo = [];
            // only sku for now
            $productAttributes = ['sku' => Mage::helper('catalog')->__('SKU')];
            foreach ($productAttributes as $attributeCode => $attributeLabel) {
                $this->_addAttributeToConditionGroup(
                    self::PRODUCT_ATTRIBUTES_TYPE_PRODUCT,
                    'vax_salesrule/rule_condition_freegift',
                    $attributeCode,
                    $attributeLabel
                );
            }

            return $this;
        }
    }
}