<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$url_key_attribute = array(
    'type' => 'varchar',
    'label' => 'Buyers Guide URL Key',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information"
);
$title_attribute = array(
    'type' => 'varchar',
    'label' => 'Buyers Guide Title',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information"
);
$installer->addAttribute('catalog_category', 'category_buyers_guide_url_key', $url_key_attribute);
$installer->addAttribute('catalog_category', 'category_buyers_guide_title', $title_attribute);

$installer->endSetup();
