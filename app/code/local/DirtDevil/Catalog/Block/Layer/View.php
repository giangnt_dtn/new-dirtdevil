<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevil_Catalog_Block_Layer_View extends Mage_Catalog_Block_Layer_View
{
    /**
     * @return array of DirtDevil_Catalog_Block_Layer_View
     */
    public function getFilters() {
        $filters = array();

        $categoryFilter = $this->_getCategoryFilter();

        $filterableAttributes = $this->_getFilterableAttributes();
        foreach ($filterableAttributes as $attribute) {
            // Moves category filter below price
            if ($categoryFilter && 0 < $attribute->getPosition()) {
                $filters[] = $categoryFilter;
                $categoryFilter = null;
            }

            $filters[] = $this->getChild($attribute->getAttributeCode() . '_filter');
        }

        return $filters;
    }
}