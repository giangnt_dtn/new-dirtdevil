<?php

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

if ( ! $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, 'sales_chat_deployment_id')) {
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'sales_chat_deployment_id', array(
        'label'                      => 'Sales Chat Deployment Id',
        'sort_order'                 => 1,
        'backend'                    => '',
        'type'                       => 'text',
        'group'                      => 'LiveChat',
        'frontend'                   => '',
        'note'                       => null,
        'default'                    => 0,
        'wysiwyg_enabled'            => false,
        'input'                      => 'text',
        'source'                     => '',
        'required'                   => false,
        'user_defined'               => false,
        'unique'                     => false,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'                    => true,
        'visible_on_front'           => false,
        'used_in_product_listing'    => false,
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'filterable'                 => false,
        'filterable_in_search'       => false,
        'comparable'                 => false,
        'is_html_allowed_on_front'   => true,
        'apply_to'                   => 'bundle,virtual,simple',
        'is_configurable'            => false,
        'used_for_sort_by'           => false,
        'position'                   => 0,
        'used_for_promo_rules'       => false,
    ));
}

$installer->endSetup();