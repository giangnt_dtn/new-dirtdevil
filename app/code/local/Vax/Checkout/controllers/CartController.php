<?php
/**
 * Vax UK cart controller - extends default controller
 *
 * @category    code
 * @package     vax_uk
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */

// Autoload extended controller - Magento doesn't autoload controllers!
include_once('Mage/Checkout/controllers/CartController.php');

class Vax_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * {@inheritdoc}
     */
    public function indexAction()
    {
        Mage::getSingleton('checkout/session')->setShippingDate(null);

        parent::indexAction();
    }
    /**
     * Rewrites Mage_Checkout_CartController::couponPostAction()
     *
     * @see Mage_Checkout_CartController
     */
    public function couponApplyAction()
    {
        /**
         * No reason continue with empty shopping cart
         */
        $_session = Mage::getSingleton('vax_checkout/couponSession');

        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {
                    $_session->addSuccess(
                        $this->__('Coupon code was applied.')
                    );
                } else {
                    $_session->addError(
                        $this->__('Coupon code is not valid.')
                    );
                }
            } else {
                $_session->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $_session->addError($e->getMessage());
        } catch (Exception $e) {
            $_session->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }
        $this->getResponse()->setRedirect(Mage::getUrl('*/*/index'));
    }
    /**
     * Updates the quotes shipping method.
     *
     * @return void
     */
    public function updateShippingAction()
    {
        $deliveryMeth = (string) $this->getRequest()->getParam('estimate_method');
        $deliveryDate = (string) $this->getRequest()->getParam('address_delivery_shipping_date');

        $this->_getQuote()->updateShippingMethod(
            $deliveryMeth
        );

        if ($deliveryDate) {
            Mage::getSingleton('checkout/session')->setShippingDate(date('l jS F', $deliveryDate));
            Mage::getSingleton('vax_checkout/couponSession')->setShippingDate(date('l jS F', $deliveryDate));
        } else {
            Mage::getSingleton('checkout/session')->setShippingDate(null);
            Mage::getSingleton('vax_checkout/couponSession')->setShippingDate(null);
        }
    }
    /**
     * Gets the latest nominated day
     */
    public function getUpdateNominatedAction()
    {
        $delDate = Mage::getSingleton('vax_checkout/couponSession')->getShippingDate();

        if($delDate)
        {
            $dateObj['date'] = $delDate;
            $this->getResponse()->setHeader('Content-Type', 'application/json', true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($dateObj));
        }
    }
}