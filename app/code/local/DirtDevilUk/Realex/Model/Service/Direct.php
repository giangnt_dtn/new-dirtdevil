<?php
/**
 * Vax Limited
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevilUk_Realex_Model_Service_Direct extends Yoma_Realex_Model_Service_Direct
{
    /**
     * Initialize models after callback
     *
     * @return $this
     * @throws Exception
     */
    protected function _init(){

        $this->setTransactionType(Mage::app()->getRequest()->getParam('type'));
        $this->_setTransactionReference(Mage::app()->getRequest()->getParam('reference'));

        $this->_setMethod()->setResponse(
            $this->getTransactionType(),
            Mage::app()->getRequest()->getPost()
        );

        $transaction = Mage::getModel('realex/transaction')
            ->loadByServiceTransactionReference($this->_serviceCode,$this->getTransactionReference());

        $this->_setTransaction($transaction);

        if ($transaction->getTransactionType() != $this->getTransactionType()) {
            throw new Exception($this->_getHelper()
                ->__("Transaction type '{$this->getTransactionType()}' does not match expected '{$transaction->getTransactionType()}'."));
        }

        $order = Mage::getModel('sales/order')->load($transaction->getOrderId());
        if (!$order->getId()) {
            $tokens = explode('-', $this->getTransactionReference());
            $order = Mage::getModel('sales/order')->loadByIncrementId($tokens[0]);
            $transaction->setOrderId($order->getId())->save();
        }

        $this->_setOrder($order);

        if (!$order->getId()) {
            throw new Exception($this->_getHelper()
                ->__("Unable to load order for Transaction '{$this->getTransactionReference()}'"));
        }

        if (!$transaction->getPaymentId()) {
            $payment = Mage::getModel('sales/order_payment')->load($order->getId(), 'parent_id');
            $transaction->setPaymentId($payment->getId())->save();
        } else {
            $payment = Mage::getModel('sales/order_payment')->load($transaction->getPaymentId());
        }

        $payment->setOrder($order);
        $this->_setPayment($payment);

        if (!$payment->getId()) {
            throw new Exception($this->_getHelper()
                ->__("Unable to load payment for Transaction '{$this->getTransactionReference()}'"));
        }

        $this->setMethodInstance($payment->getMethodInstance());

        return $this;
    }
}
