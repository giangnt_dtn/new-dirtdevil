<?php
class Yoma_RealexExtended_Adminhtml_Model_System_Config_Source_Currency {

    protected $_options;

    public function toOptionArray($isMultiselect=false){
        if (!$this->_options) {

            $this->_options = array_merge(array(array('value'=>'*','label'=>'Any')),Mage::app()->getLocale()->getOptionCurrencies());
        }

        $options = $this->_options;

        if(!$isMultiselect){
            array_unshift($options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
        }

        return $options;
    }
}