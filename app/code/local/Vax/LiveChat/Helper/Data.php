<?php

/**
 * Live Chat Helper.
 *
 * @copyright Vax Ltd
 */
class Vax_LiveChat_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Checks whether the module is enabled.
     *
     * @return boolean
     */
    public function isModuleEnabled($moduleName = null)
    {
        return Mage::getStoreConfigFlag('vax_livechat/general/is_enabled');
    }

    /**
     * Returns the delay after document.ready
     *
     * @return int
     */
    public function getScriptDelay()
    {
        if (Mage::getStoreConfig('vax_livechat/general/script_delay') >= 0) {
            return Mage::getStoreConfig('vax_livechat/general/script_delay');
        }
        return 500;
    }

    /**
     * Returns the milliseconds of delay with which the Livechat Window should be opened
     *
     * @return int
     */
    public function getDelay($product = null)
    {
        if(!$product) {
            return 0;
        }

        $delay = Mage::getResourceModel('catalog/product')
            ->getAttributeRawValue(
                $product->getId(),
                'sales_chat_delay',
                Mage::app()->getStore()->getStoreId()
            );
        if(is_int($delay) && $delay >= 0) {
            return max(10000, (integer)$delay*1000);
        } else {
            return 1000;
        }
    }
}
