<?php
class Vax_Prodfaqs_Block_Faq_Abstract extends Mage_Core_Block_Template
{
    public function getCurrentFaq()
    {
        $_faq = Mage::registry('current_faq');
        if (!$_faq || !$_faq->getId()) {
            return false;
        }

        return $_faq;
    }

    /**
     * Gets faqs collection by products related to currently selected faq
     *
     * @return FME_Prodfaqs_Model_Mysql4_Prodfaqs_Collection
     */
    public function getRelatedFaqsByProduct()
    {
        $faq = $this->getCurrentFaq();
        if (!$faq) {
            return new Varien_Data_Collection();
        }
        $productIds = $faq->getFaqProductIds();
        $topicIds = $faq->getFaqIdsByProducts($productIds);
        $collection = Mage::getResourceModel('prodfaqs/prodfaqs_collection')->addFieldToFilter('main_table.faqs_id', array('in' => $topicIds));
        $collection->addFieldToFilter('main_table.faqs_id', array('neq' => $faq->getId()));
        $collection->join(array('topic' => 'prodfaqs/topic'), 'main_table.topic_id=topic.topic_id', array('topic_title' => 'topic.title'));
        $collection->setOrder('topic.topic_order', 'DESC');
        $collection->setOrder('main_table.faq_order', 'DESC');
        return $collection;
    }

    public function getRelatedFaqsByCategories()
    {
        $faq = $this->getCurrentFaq();
        if (!$faq) {
            return new Varien_Data_Collection();
        }
        $productIds = $faq->getFaqProductIds();

    }
}