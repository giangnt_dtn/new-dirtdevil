<?php

/**
 * Checkout Helper
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Helper_Checkout extends Mage_Core_Helper_Abstract
{
    const XML_PATH_CHECKOUT_STOCKTAKE = 'checkout/options/stocktake_message';

    /**
     * Checks whether a user can checkout.
     *
     * @return boolean
     */
    public function canCheckout()
    {
        $session = Mage::getSingleton('checkout/session');
        $quote   = $session->getQuote();

        if ( ! Mage::helper('checkout')->canOnepageCheckout()) {
            $session->addError($this->__('The onepage checkout is disabled.'));
            return false;
        }

        if ( ! Mage::helper('checkout')->isAllowedGuestCheckout($quote) && ! Mage::getSingleton('customer/session')->isLoggedIn()) {
            $session->addError($this->__('Only registered users are allowed to checkout.'));
            return false;
        }

        if ( ! $quote->hasItems() || $quote->getHasError()) {
            return false;
        }

        if ( ! $quote->validateMinimumAmount()) {
            Mage::getSingleton('checkout/session')->addError($this->getMinOrderMessage());
            return false;
        }

        $result = new Varien_Object(array('can_checkout' => true));
        Mage::dispatchEvent('can_checkout_after', array('result' => $result));


        return $result->getData('can_checkout');
    }

    /**
     * Returns the minimum order message.
     *
     * @return string
     */
    public function getMinOrderMessage()
    {
        $message = Mage::getStoreConfig('sales/minimum_order/error_message');

        if ( ! $message) {
            $message = Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');
        }

        return $message;
    }

    /**
     * Formats a price.
     *
     * @param float $price
     *
     * @return stsring
     */
    public function formatPrice($price)
    {
        if ($price == 0) {
            return '<span class="price">' . $this->__('Free') . '</span>';
        }

        return Mage::helper('core')->formatPrice($price);
    }

    /**
      * Checks whether stocktake message is enabled
      *
      * @return boolean
      */
     public function isStocktakeMessageEnabled()
     {
         $message = Mage::getStoreConfig(self::XML_PATH_CHECKOUT_STOCKTAKE);
         if($message) {
             $trimmed = rtrim($message);
             return !empty($trimmed);
         }

         return false;
     }


     /**
      * Gets stocktake message
      *
      * @return String
      */
     public function getStocktakeMessage()
     {
         return Mage::getStoreConfig(self::XML_PATH_CHECKOUT_STOCKTAKE);
     }
}
