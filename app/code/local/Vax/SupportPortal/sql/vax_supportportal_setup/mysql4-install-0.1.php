<?php
/**
 * Vax Contact SQL installer script - version 0.1
 * 
 * Version file name must match the version specified in config.xml to run
 * Once run the 'core_resource' table is updated with the new version number and script is not re-run
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd
 * @author      M Stephens
 * @version     0.1
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
 
$installer->startSetup();

/**
 * Create Vax Contact table
 */

$table = $installer->getConnection()
    // Get table name from resource model
    ->newTable($installer->getTable('vax_supportportal/contact'))
    // Name, type, size -opt, (options) -opt, comment -opt
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true, // auto increment
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true, // primary (unique) key
        ), 'Entity ID')
    ->addColumn('first_name', Varien_Db_Ddl_Table::TYPE_TEXT, 70, array(
        'nullable'  => false,
        ))
    ->addColumn('surname', Varien_Db_Ddl_Table::TYPE_TEXT, 70, array(
        'nullable'  => false,
        ))
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ))
    ->addColumn('mail_opt', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
        ))
    ->addColumn('form_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ))
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        ), 'Store registered on')
    ->addColumn('sent_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE
        ), 'Sent Time')
    ->setComment('Vax Contact Table');
 
$installer->getConnection()->createTable($table);

$installer->endSetup();