<?php

class DirtDevil_Widget_Block_HeaderInfoBanner extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        $this->setTemplate('widget/header_info_banner.phtml');
        parent::_construct();
    }
}