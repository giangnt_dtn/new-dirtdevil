<?php
class DirtDevilUk_Bazaarvoice_Model_ExportPurchaseFeed extends Bazaarvoice_Connector_Model_ExportPurchaseFeed
{
    /**
     * @param Varien_Io_File $ioObject
     * @param $orders
     * @return array exported Orders
     */
    protected function writeOrdersToFile(Varien_Io_File $ioObject, $orders)
    {
        // Get ref to BV helper
        /* @var $bvHelper Bazaarvoice_Connector_Helper_Data */
        $bvHelper = Mage::helper('bazaarvoice');

        // Initialize references to the object model accessors
        $orderModel = Mage::getModel('sales/order');

        // Gather settings for how this feed should be generated
        $triggeringEvent = Mage::getStoreConfig('bazaarvoice/feeds/triggering_event') ===
        Bazaarvoice_Connector_Model_Source_TriggeringEvent::SHIPPING ? self::TRIGGER_EVENT_SHIP : self::TRIGGER_EVENT_PURCHASE;

        // Hard code former settings
        $delayDaysSinceEvent = 1;

        Mage::log("    BV - Config {triggering_event: " . $triggeringEvent
                  . ", NumDaysLookback: " . $this->num_days_lookback
                  . ", NumDaysLookbackStartDate: " . $this->getNumDaysLookbackStartDate()
                  . ", DelayDaysSinceEvent: " . $delayDaysSinceEvent
                  . ', DelayDaysThreshold: ' . date('c', $this->getDelayDaysThresholdTimestamp($delayDaysSinceEvent)) . '}', Zend_Log::INFO, Bazaarvoice_Connector_Helper_Data::LOG_FILE);

        $ordersToExport = $this->_getOrdersToExport($orders, $triggeringEvent, $delayDaysSinceEvent);

        $exportedOrders = array();
        foreach ($ordersToExport as $orderId) {
            try{
                /* @var $order Mage_Sales_Model_Order */
                $order = Mage::getModel('sales/order')->load($orderId);
                $store = $order->getStore();


                $orderXml = '';

                $orderXml .= "<Interaction>\n";
                //                $orderXml .= '    <OrderID>' . $order->getIncrementId() . "</OrderID>\n";
                $orderXml .= '    <EmailAddress>' . $order->getCustomerEmail() . "</EmailAddress>\n";
                $orderXml .= '    <Locale>' . $store->getConfig('bazaarvoice/general/locale') . "</Locale>\n";
                $orderXml .= '    <UserName>' . $order->getCustomerName() . "</UserName>\n";
                if($order->getCustomerId()) {
                    $userId = $order->getCustomerId();
                } else {
                    $userId = md5($order->getCustomerEmail());
                }
                $orderXml .= '    <UserID>' . $userId . "</UserID>\n";
                $orderXml .= '    <TransactionDate>' . $this->getTriggeringEventDate($order, $triggeringEvent) . "</TransactionDate>\n";
                $orderXml .= "    <Products>\n";
                // if families are enabled, get all items
                if(Mage::getStoreConfig('bazaarvoice/feeds/families')){
                    $items = $order->getAllItems();
                } else {
                    $items = $this->_getOrderItems($order);
                }
                /* @var $item Mage_Sales_Model_Order_Item */
                foreach ($items as $item) {
                    // skip configurable items if families are enabled
                    if(Mage::getStoreConfig('bazaarvoice/feeds/families') && $item->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) continue;

                    $product = $bvHelper->getReviewableProductFromOrderItem($item);

                    // skip disabled products
                    //if($product->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_ENABLED) continue;

                    if (!is_null($product)) {
                        $productXml = '';
                        $productXml .= "        <Product>\n";
                        $productXml .= '            <ExternalId>' . $bvHelper->getProductId($product) .
                            "</ExternalId>\n";
                        $productXml .= '            <Name><![CDATA[' . htmlspecialchars($product->getName(), ENT_QUOTES, 'UTF-8', false) . "]]></Name>\n";

                        $imageUrl = Mage::helper('catalog/image')->init($product, 'image')->resize(265, 265);
                        $originalPrice = $item->getOriginalPrice();
                        if(Mage::getStoreConfig('bazaarvoice/feeds/families') && $item->getParentItem()) {
                            $parentItem = $item->getParentItem();
                            $parent = Mage::getModel('catalog/product')->load($parentItem->getProductId());

                            if(strpos($imageUrl, "placeholder/image.jpg")){
                                // if product families are enabled and product has no image, use configurable image
                                $imageUrl = Mage::helper('catalog/image')->init($parent, 'image')->resize(265, 265);
                            }
                            // also get price from parent item
                            $originalPrice = $parentItem->getOriginalPrice();
                        }

                        $productXml .= '            <ImageUrl>' . $imageUrl . "</ImageUrl>\n";
                        $productXml .= '            <Price>' . number_format((float)$originalPrice, 2) . "</Price>\n";
                        $productXml .= "        </Product>\n";

                        $orderXml .= $productXml;
                    }
                }
                $orderXml .= "    </Products>\n";
                $orderXml .= "</Interaction>\n";
                $ioObject->streamWrite($orderXml);
                $exportedOrders[] = $orderId;
            } Catch (Exception $e) {
                $this->flagOrders(array($orderId), 2);
                Mage::log($e->getMessage()."\n".$e->getTraceAsString());
            }

        }
        Mage::log("    BV - Exported " . count($exportedOrders) . " orders.", Zend_Log::INFO, Bazaarvoice_Connector_Helper_Data::LOG_FILE);

        return $exportedOrders;
    }

    private function _getOrdersToExport($orders, $triggeringEvent, $delayDaysSinceEvent) {
        $ordersToExport = array();
        foreach ($orders->getAllIds() as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if (!$order->getId() || !$this->shouldIncludeOrder($order, $triggeringEvent, $delayDaysSinceEvent)) {
                continue;
            }
            $ordersToExport[] = $orderId;
        }
        return $ordersToExport;
    }

    // get the correct order items list
    private function _getOrderItems($order) {
        $allItems = Mage::getResourceModel('sales/order_item_collection')
            ->setOrderFilter($order);

        $visibleItems = array();
        if ($order->getId()) {
            foreach ($allItems as $item) {
                $item->setOrder($order);
                if (!$item->isDeleted() && !$item->getParentItemId()) {
                    $visibleItems[] =  $item;
                }
            }
        }

        return $visibleItems;
    }
}

