<?php
/**
 * Vax Registration Edit Customer tab form block class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */

class Vax_Registration_Block_Adminhtml_Registration_Edit_Tab_Customer extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Add the form fields
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Edit_Tab_Customer
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('customer');
 
        $fieldset = $form->addFieldset('customer_form', array(
            'legend' => $this->__('Customer')
                ));
 
        if (Mage::registry('current_registration')->getId()) {
            $fieldset->addField('entity_id', 'label', array(
                'label' => $this->__('Registration ID')
            ));
        }
 
        $fieldset->addField('first_name', 'text', array(
            'label' => $this->__('First Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'first_name',
        ));
        
        $fieldset->addField('surname', 'text', array(
            'label' => $this->__('Surname'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'surname',
        ));
 
        $fieldset->addField('address', 'text', array(
            'label' => $this->__('Address'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'address',
        ));
        
        $fieldset->addField('address2', 'text', array(
            'label' => $this->__(''),
            'required' => false,
            'name' => 'address2',
        ));
        
        $fieldset->addField('town', 'text', array(
            'label' => $this->__('Town'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'town',
        ));
        
        $fieldset->addField('county', 'text', array(
            'label' => $this->__('County'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'county',
        ));
        
        $fieldset->addField('postcode', 'text', array(
            'label' => $this->__('Postcode'),
            'class' => 'required-entry validate-postcode',
            'required' => true,
            'name' => 'postcode',
        ));
        
        $fieldset->addField('email', 'text', array(
            'label' => $this->__('Email'),
            'class' => 'required-entry validate-email',
            'required' => true,
            'name' => 'email',
        ));
        
        $fieldset->addField('telephone', 'text', array(
            'label' => $this->__('Telephone'),
            'class' => 'required-entry validate-number',
            'required' => true,
            'name' => 'telephone',
        ));
 
        $form->addValues($this->_getFormData());
 
        $this->setForm($form);
 
        return parent::_prepareForm();
    }
 
    /**
     *
     * @return array
     */
    protected function _getFormData() {
        $data = Mage::getSingleton('adminhtml/session')->getFormData();
 
        if (!$data && Mage::registry('current_registration')->getData()) {
            $data = Mage::registry('current_registration')->getData();
        }
 
        return (array) $data;
    }
}