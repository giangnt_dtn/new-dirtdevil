<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Checkout_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Checks if Magento is configured to use the default checkout process.
     *
     * @return bool
     */
    public function isMagentoCheckout()
    {
        return Mage::getStoreConfig('checkout/options/steps_model') == 'original';
    }
}