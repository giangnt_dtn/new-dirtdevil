<?php
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('newsletter/subscriber'),
        'subscriber_source',
        array(
            'type'     =>  Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => true,
            'default'  => null,
            'comment'  =>  'Subscriber Source'
        )
    );
$installer->endSetup();
