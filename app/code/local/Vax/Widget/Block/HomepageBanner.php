<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class Vax_Widget_Block_HomepageBanner extends Vax_Widget_Block_ProductBanner implements Mage_Widget_Block_Interface
{
    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        $this->setTemplate('widget/homepage_banner.phtml');
        parent::_construct();
    }

    public function getSmallProductLogoUrl()
    {
        if(!$this->getProductLogoMobile()) {
            return false;
        }

        return $this->_getMediaUrl($this->getProductLogoMobile());
    }

	/**
	 * Returns banner navigation Click-through button text.
	 *
	 * @return string
	 */
	public function getBannerClickThroughText()
	{
		return $this->getData('click_through');
	}

    /**
     * Detects whether the banner should be shown on smaller screens.
     *
     * @return boolean
     */
    public function isEnabledForMobiles()
    {
        return $this->getData('enabled_for_mobiles');
    }

    /**
     * Returns banner link.
     *
     * @return string
     */
    public function getBannerUrl()
    {
        if ($this->getData('banner_url')) {
            $bannerUrl = $this->getData('banner_url');

            if (0 === strpos($bannerUrl, 'http')) {
                return $bannerUrl;
            }

            return Mage::getUrl($bannerUrl);
        }

        return null;
    }

	/**
	 * Returns custom price text
	 *
	 * @return string
	 */
	public function getCustomPrice() {
		return $this->getData('banner_custom_price', 0);
	}
}
