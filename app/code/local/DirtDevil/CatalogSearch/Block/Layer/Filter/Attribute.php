<?php
/**
 * Vax Limited
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
 **/
class DirtDevil_CatalogSearch_Block_Layer_Filter_Attribute extends Mage_CatalogSearch_Block_Layer_Filter_Attribute
{
    public function __construct()
    {
        parent::__construct();
        if(Mage::helper('catalog/category')->useAjaxCheckboxes()) {
            $this->setTemplate('catalog/layer/filter/checkbox.phtml');
        }
    }
}
