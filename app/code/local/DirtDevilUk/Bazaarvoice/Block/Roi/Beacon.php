<?php
class DirtDevilUk_Bazaarvoice_Block_Roi_Beacon extends Bazaarvoice_Connector_Block_Roi_Beacon
{
    /**
     * Current Order.
     *
     * @var Mage_Sales_Model_Order
     */
    protected $_order;

    /**
     * Current Address.
     *
     * @var Mage_Sales_Model_Order_Address
     */
    protected $_address;

    /**
     * Filteres Order Item
     *
     * @var array
     */
    protected $_filtered;

    /**
     * Checks whether the beacon is displayable.
     *
     * @return boolean
     */
    public function getIsEnabled() {

        if(!Mage::getStoreConfigFlag('bazaarvoice/general/enable_bv')) {
            return false;
        }

        if(!Mage::getStoreConfigFlag('bazaarvoice/general/enable_roibeacon')) {
            return false;
        }

        return count($this->getFilteredOrderItems()) > 0;
    }

    /**
     * Returns the items on the order.
     *
     * @return mixed
     */
    public function getFilteredOrderItems()
    {
        if ( ! $this->_filtered) {
            $this->_filtered = $this->_getFilteredOrderItems();
        }

        return $this->_filtered;
    }

    /**
     * Returns the items on the order.
     *
     * @return mixed
     */
    public function _getFilteredOrderItems()
    {
        $filteredItems = array();

        foreach ($this->getOrderItems() as $item) {
            if ($this->isProductDisplayable($item->getProduct())) {
                $filteredItems[] = $item;
            }
        }

        return $filteredItems;
    }

    /**
     * Returns the items on the order.
     *
     * @return mixed
     */
    public function getOrderItems()
    {
        $order = $this->getOrder();

        $allItems = Mage::getResourceModel('sales/order_item_collection')
                        ->setOrderFilter($order);

        if ($order->getId()) {
            foreach ($allItems as $item) {
                $item->setOrder($order);
            }
        }

        return $allItems;
    }

    /**
     * Returns the current order.
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if ( ! $this->_order) {
            if(Mage::getSingleton('checkout/session')->getProcessedOrderId()) {
                $this->_order = Mage::getModel('sales/order')->load(
                    Mage::getSingleton('checkout/session')->getProcessedOrderId(), 'increment_id'
                );
            }

            if(!$this->_order) {
                $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();

                if ($orderId) {
                    $this->_order = Mage::getModel('sales/order')->load($orderId);
                }
            }

            if(!$this->_order) {
                $orderId = Mage::getSingleton('checkout/session')->getBVOrderId();

                if ($orderId) {
                    $this->_order = Mage::getModel('sales/order')->load($orderId, 'increment_id');
                }
            }
        }

        return $this->_order;
    }

    /**
     * Checks whether the product is displayable.
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return boolean
     */
    public function isProductDisplayable(Mage_Catalog_Model_Product $product)
    {
        if ($product->isBundle()) {
            return false;
        }
        if ($product->isAccessory()) {
            return false;
        }
        return true;
    }

    /**
     * Returns the customer's full name.
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getOrder()->getCustomerPrefix().' '.$this->getOrder()->getCustomerFirstname().' '.$this->getOrder()->getCustomerLastname();
    }

    /**
     * Returns the order's shipping address.
     *
     * @return Mage_Sales_Model_Order_Address
     */
    public function getAddress()
    {
        if ( ! $this->_address) {
            $this->_address = $this->getOrder()->getShippingAddress();
        }
        return $this->_address;
    }

    /**
     * Returns the order's address state.
     *
     * @return string
     */
    public function getState()
    {
        return $this->getAddress()
            ->getRegion();
    }

    /**
     * Returns the order's creation date.
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return str_replace(' ', 'T', $this->getOrder()->getCreatedAt());
    }

    /**
     * returns serialized order details data for transmission to Bazaarvoice
     * @return string
     */
    public function getOrderDetails()
    {
        $orderDetails = array();

        $order = $this->getOrder();

        if ($order && $order->getId())
        {
            $address = $order->getBillingAddress();

            $orderDetails['orderId'] = $order->getIncrementId();
            $orderDetails['tax'] = number_format($order->getTaxAmount(), 2, '.', '');
            $orderDetails['shipping'] = number_format($order->getShippingAmount(), 2, '.', '');
            $orderDetails['total'] = number_format($order->getGrandTotal(), 2, '.', '');
            $orderDetails['city'] = $address->getCity();
            $orderDetails['state'] = $this->getState();
            $orderDetails['country'] = $address->getCountryId();
            $orderDetails['currency'] = $order->getOrderCurrencyCode();

            $orderDetails['items'] = array();

            foreach ($this->getFilteredOrderItems() as $itemId => $item)
            {
                $product = Mage::helper('bazaarvoice')->getReviewableProductFromOrderItem($item);
                $product = Mage::getModel('catalog/product')->load($product->getId());
                // skip configurable items if families are enabled
                if(Mage::getStoreConfig('bazaarvoice/feeds/families') && $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) continue;

                $itemDetails = array();
                $itemDetails['sku'] = Mage::helper('bazaarvoice')->getProductId($product);
                $itemDetails['name'] = $item->getName();
                // 'category' is not included.  Mage products can be in 0 - many categories.  Should we try to include it?
                $itemDetails['price'] = number_format($item->getPrice(), 2, '.', '');
                $itemDetails['quantity'] = number_format($item->getQtyOrdered(), 0);
                $itemDetails['imageUrl'] = $product->getImageUrl();

                if(Mage::getStoreConfig('bazaarvoice/feeds/families') && $item->getParentItem()) {
                    if(strpos($itemDetails['imageUrl'], "placeholder/image.jpg")) {
                        // if product families are enabled and product has no image, use configurable image
                        $parentId = $item->getParentItem()->getProductId();
                        $parent = Mage::getModel('catalog/product')->load($parentId);
                        $itemDetails['imageUrl'] = $parent->getImageUrl();
                    }
                    // also get price from parent item
                    $itemDetails['price'] = number_format($item->getParentItem()->getPrice(), 2, '.', '');
                }

                array_push($orderDetails['items'], $itemDetails);
            }

            if($order->getCustomerId()) {
                $userId = $order->getCustomerId();
            } else {
                $userId = md5($order->getCustomerEmail());
            }
            $orderDetails['userId'] = $userId;
            $orderDetails['email'] = $order->getCustomerEmail();
            $orderDetails['nickname'] = $order->getCustomerName();
            $orderDetails['deliveryDate'] = $this->getCreatedAt();

            $locale = Mage::getStoreConfig('bazaarvoice/general/locale', $order->getStoreId());

            if(!$locale || $locale && empty($locale)) {
                $locale = 'en_GB';
            }

            $orderDetails['locale'] = $locale;
        }
        Mage::log($orderDetails, Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
        $orderDetailsJson = json_encode( $orderDetails, JSON_UNESCAPED_UNICODE );

        Mage::getSingleton('checkout/session')
            ->unsBVOrderId();

        return urldecode(stripslashes($orderDetailsJson));
    }
}
