 <?php

/**
 * Checkout Request Mapper.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Model_Mapper_Checkout_Request
{
    /**
     * Maps the current checkout state.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param array                  $data
     */
    public function map(Mage_Sales_Model_Quote $quote, array $data)
    {
        $quote->setRequiresNewsletter(isset($data['contactDetails']['optIn']));

        if (isset($data['contactDetails']) && isset($data['deliveryAddress'])) {
            $this->mapAddress(
                $quote->getShippingAddress(),
                array_merge($data['contactDetails'], $data['deliveryAddress'])
            );
        }

        if (isset($data['contactDetails']) && isset($data['billingAddress'])) {
            $this->mapAddress(
                $quote->getBillingAddress(),
                array_merge($data['contactDetails'], $data['billingAddress'])
            );
        }

        $this->mapCustomer($quote);

        if (!empty($data['paymentDetails'])) {
            $this->mapPayment(
                $quote->getPayment(),
                $data['paymentDetails']
            );
        }

        $quote->setTotalsCollectedFlag(false)
              ->collectTotals()
              ->save();

        if (!empty($data['paymentDetails'])) {
            $this->mapPayment(
                $quote->getPayment(),
                $data['paymentDetails']
            );
        }

        return $quote;
    }

    /**
     * Maps an address.
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @param array                          $data
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function mapAddress(Mage_Sales_Model_Quote_Address $address, $data)
    {
        $address->setData(array_merge($address->toArray(), $data));

        if (isset($data['street_1']) && isset($data['street_2'])) {
            $address->setStreet(array($data['street_1'], $data['street_2']));
            $address->implodeStreetAddress();
        }


        Mage::dispatchEvent('after_quote_address_mapped', array('address' => $address));

        return $address;
    }

    /**
     * Maps a customer.
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return void
     */
    public function mapCustomer(Mage_Sales_Model_Quote $quote)
    {
        $totalsAddress = $quote->getTotalsAddress();

        $quote->setCustomerPrefix($totalsAddress->getPrefix());
        $quote->setCustomerFirstname($totalsAddress->getFirstname());
        $quote->setCustomerLastname($totalsAddress->getLastname());
        $quote->setCustomerEmail($totalsAddress->getEmail());
    }

    /**
     * Maps a payment.
     *
     * @param Mage_Sales_Model_Quote_Payment $payment
     * @param array                          $data
     *
     * @return void
     */
    public function mapPayment(Mage_Sales_Model_Quote_Payment $payment, $data)
    {
        $payment->importData($data)
                ->getQuote()
                ->getTotalsAddress()
                ->setCollectShippingRates(true);
    }
}