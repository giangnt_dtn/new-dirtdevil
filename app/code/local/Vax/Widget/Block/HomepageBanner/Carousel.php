<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class Vax_Widget_Block_HomepageBanner_Carousel
    extends Vax_Widget_Block_Abstract implements Mage_Widget_Block_Interface
{
    protected $_banners;

    /**
     * Returns list of banner slides.
     *
     * @return mixed
     */
    public function getBanners()
    {
        if (!$this->_banners) {
            $this->_banners = array();

            $collection = Mage::getResourceModel('widget/widget_instance_collection');
            $collection->addFieldToFilter('instance_type', 'vax_widget/homepageBanner');
            $collection->addStoreFilter(Mage::app()->getStore()->getStoreId());
            $collection->setOrder('sort_order', Mage_Core_Model_Resource_Db_Collection_Abstract::SORT_ORDER_ASC);

            foreach ($collection as $banner) {
                $parameters = $banner->getWidgetParameters();

                if ($parameters['enabled'] == '1') {
                    $widget = $this->getLayout()->createBlock('vax_widget/widget_instance', 'homepage_slide_' . $banner->getId());
                    $widget->setWidgetId($banner->getId());
                    $this->_banners[] = $widget;
                }
            }
        }

        return $this->_banners;
    }
}