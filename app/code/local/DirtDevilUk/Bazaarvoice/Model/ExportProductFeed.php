<?php
/**
 * Product Feed Export Class - Update Feed to 5.6
 */
class DirtDevilUk_Bazaarvoice_Model_ExportProductFeed extends Bazaarvoice_Connector_Model_ExportProductFeed
{
    /**
     * @param string $productFeedFileName Name of local product feed file to create and write
     * @param string $clientName BV Client name text
     * @return Varien_Io_File File object, opening <Feed> tag is already written
     */
    protected function createAndStartWritingFile($productFeedFileName, $clientName)
    {
        // Get ref to BV helper
        /* @var $bvHelper Bazaarvoice_Connector_Helper_Data */
        $bvHelper = Mage::helper('bazaarvoice');

        $ioObject = new Varien_Io_File();
        try {
            $ioObject->open(array('path' => dirname($productFeedFileName)));
        }
        catch (Exception $e) {
            $ioObject->mkdir(dirname($productFeedFileName), 0777, true);
            $ioObject->open(array('path' => dirname($productFeedFileName)));
        }

        if (!$ioObject->streamOpen(basename($productFeedFileName))) {
            Mage::throwException('Failed to open local feed file for writing: ' . $productFeedFileName, Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
        }

        $ioObject->streamWrite("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
        "<Feed xmlns=\"http://www.bazaarvoice.com/xs/PRR/ProductFeed/5.6\"" .
        " generator=\"Magento Extension r" . $bvHelper->getExtensionVersion() . "\"" .
        "  name=\"" . $clientName . "\"" .
        "  incremental=\"false\"" .
        "  extractDate=\"" . date('Y-m-d') . "T" . date('H:i:s') . ".000000\">\n");

        return $ioObject;
    }
}
