<?php

/**
 * Vax Checkout Onepage Address Form.
 *
 * @copyright Vax Ltd 2013
 */
abstract class Vax_Checkout_Block_Onepage_Form_Address extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the correct address.
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public abstract function getAddress();

    /**
     * Returns a list of available countries.
     *
     * @return array
     */
    public function getCountryOptions()
    {
        return Mage::getResourceModel('directory/country_collection')->loadByStore();
    }

    /**
     * Returns the current line 1.
     *
     * @return string
     */
    public function getLine1()
    {
        return $this->getAddress()->getStreet1();
    }

    /**
     * Returns the current line 2.
     *
     * @return string
     */
    public function getLine2()
    {
        return $this->getAddress()->getStreet2();
    }

    /**
     * Returns the current city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->getAddress()->getCity();
    }

    /**
     * Returns the current region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->getAddress()->getRegion();
    }

    /**
     * Returns the current postcode.
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->getAddress()->getPostcode();
    }
}