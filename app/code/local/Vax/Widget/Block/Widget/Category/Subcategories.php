<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Category_Subcategories
    extends Vax_Widget_Block_Category_Subcategories implements Mage_Widget_Block_Interface {

    /**
     * Initializes subcategory block.
     */
    protected function _construct()
    {
        $this->setData('current_category', Mage::getModel('catalog/category')->load($this->_getCategoryId()));
        $this->setTemplate('catalog/category/subcategories.phtml');
        parent::_construct();
    }

    /**
     * Returns ID of the category selected in a widget.
     *
     * @return int
     */
    protected function _getCategoryId()
    {
        $categoryData = explode('/', $this->getCategoryId());
        if (sizeof($categoryData > 0)) {
            return (int)$categoryData[1];
        }
    }
}
