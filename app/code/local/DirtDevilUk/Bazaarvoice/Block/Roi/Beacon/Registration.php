<?php

/**
 * Vax Bazaar Voice Registration ROI Beacon Block.
 *
 * @copyright Vax Ltd 2013
 */
class DirtDevilUk_Bazaarvoice_Block_Roi_Beacon_Registration extends Mage_Core_Block_Template
{
    /**
     * Registration.
     *
     * @var Mage_Sales_Model_Order
     */
    protected $_registration;

    /**
     * Registered Product.
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product;

    /**
     * Checks whether the beacon is displayable.
     *
     * @return boolean
     */
    public function isDisplayable()
    {
        if ( ! Mage::getStoreConfigFlag('bazaarvoice/general/enable_bv')) {
            return false;
        }

        if ($this->getProduct()->isBundle() || $this->getProduct()->isAccessory()) {
            return false;
        }

        if ( ! $this->getProduct()->isUk()) {
            return false;
        }

        return $this->getRegistration()->hasOptedIn();
    }

    /**
     * Returns the current registration.
     *
     * @return Vax_Registration_Model_Registration
     */
    public function getRegistration()
    {
        if ( ! $this->_registration) {
            $this->_registration = Mage::getModel('vax_registration/registration')->load(
                Mage::getSingleton('core/session')->getVaxRegistrationId()
            );
        }

        return $this->_registration;
    }

    /**
     * Returns the registered product.
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if ( ! $this->_product) {
            $this->_product = $this->getRegistration()->getRegisteredProduct();
        }

        return $this->_product;
    }

    /**
     * Returns the customer's email address.
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->getRegistration()
            ->getEmail();
    }

    /**
     * Returns the customer's full name.
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getRegistration()
            ->getCustomerName();
    }

    /**
     * Returns the registration city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->getRegistration()
            ->getTown();
    }

    /**
     * Returns the registration state.
     *
     * @return string
     */
    public function getState()
    {
        return $this->getRegistration()
            ->getCounty();
    }

    /**
     * Returns the purchase date.
     *
     * @return string
     */
    public function getPurchaseDate()
    {
        return $this->getRegistration()->getPurDate().'T00:00:00';
    }
}
