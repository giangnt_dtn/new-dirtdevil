set  :user, "deploy"

set :scm,              :none
set :repository,       "."
set :local_repository, "."

set :copy_via,         :scp
set :deploy_via,       :copy
set :deploy_to,        '/var/www/vhosts/dirtdevil_uk_staging'
set :copy_strategy,    :export
set :copy_exclude,     [".git/*", "var/*"]

namespace :deploy do
    desc "Sets the correct robots.txt file"
    task :set_robot_file, :roles => :web do
      run "sh -c 'cd #{latest_release} && ln -fs robots_staging.txt robots.txt'"
    end
end

after('deploy:create_symlink', 'deploy:set_robot_file')
#after('deploy:create_symlink', 'magento:redis:set')
