<?php

class Yoma_RealexExtended_Model_Secure extends Yoma_RealexExtended_Model_Result {

    const ECI_0 = 0x02;
    const ECI_1 = 0x04;
    const ECI_2 = 0x08;
    const ECI_5 = 0x10;
    const ECI_6 = 0x20;
    const ECI_7 = 0x40;

    protected function _getValuesAsArray(){

        $constants = array();
        $constants[0] = self::ECI_0;
        $constants[1] = self::ECI_1;
        $constants[2] = self::ECI_2;
        $constants[5] = self::ECI_5;
        $constants[6] = self::ECI_6;
        $constants[7] = self::ECI_7;
        return $constants;
    }

    public function getSelected($values = array()){

        $result = 0;

        $constants = $this->_getValuesAsArray();

        foreach($values as $selection){
            $result = $result | $constants[$selection];
        }

        return $result;

    }

    public function getProfileOptions($profile){
        $options = array();

        foreach($this->_getValuesAsArray() as $key=>$option){
            if($option & $profile){
                $options[] = $key;
            }
        }

        return $options;

    }


}