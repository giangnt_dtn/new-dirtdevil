<?php

/**
 * DirtDevil Newsletter Session for storing newsletter messages.
 *
 */
class DirtDevil_Newsletter_Model_NewsletterSession extends Mage_Core_Model_Session_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->init('newsletter');
    }
}