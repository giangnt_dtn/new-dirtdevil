<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

if (!$installer->getConnection()->isTableExists($installer->getTable('vax_salesrule/free_gift'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('vax_salesrule/free_gift'))
        ->addColumn(
            'rule_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true
            ],
            'Rule Id'
        )
        ->addColumn(
            'product_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true
            ],
            'Product Id'
        )
        ->addColumn(
            'gift_product_ids',
            Varien_Db_Ddl_Table::TYPE_VARCHAR,
            128,
            [],
            'Gift Product IDs (comma separated)'
        )
        ->addForeignKey(
            $installer->getFkName('vax_salesrule/free_gift', 'rule_id', 'salesrule/rule', 'rule_id'),
            'rule_id',
            $installer->getTable('salesrule/rule'),
            'rule_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        );

    $installer->getConnection()->createTable($table);
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('salesrule/rule'), 'is_free_gift_promotion')) {
    $installer->getConnection()->addColumn($installer->getTable('salesrule/rule'), 'is_free_gift_promotion', [
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'default' => 0,
        'comment' => 'Is Free Gift Promotion'
    ]);
}

$installer->endSetup();