<?php
/**
 * Vax Registration Admin controller
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */


class Vax_Registration_Adminhtml_RegistrationController extends Mage_Adminhtml_Controller_Action {
    public function indexAction() {
        /*
         * Redirect user via 302 http redirect (the browser url changes)
         */
        $this->_redirect('*/*/list');
    }

    /**
     * Display grid
     */
    public function listAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/list')) {
            $this->_redirect('/');
        }

        // null out the form data first
        $this->_getSession()->setFormData(array());

        // Title
        $this->_title($this->__('Registration'));

        $this->loadLayout();
        $this->_setActiveMenu('registration');
        $this->renderLayout();
    }

    /**
     * Check ACL permissions
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('registration');
    }

    /**
     * Grid action for ajax requests
     */
    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/view')) {
            $this->_redirect('*/*/list');
        }

        $registrationid = $this->getRequest()->getParam('registration_id');
        $registration = Mage::getModel('vax_registration/registration')->load($registrationid);

        try {
            if (!$registration->getId()) {
                Mage::throwException($this->__('No record with ID "%s" found.', $id));
            }

            // Set guarantee as current guarantee
            Mage::register('current_registration', $registration);

            $pageTitle = $this->__('View Registration %d', $registration->getId());
            $this->_title($this->__('Registration'))->_title($pageTitle);

            $this->loadLayout();

            $this->_setActiveMenu('registration');
            $this->renderLayout();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/list');
        }
    }

    /*
     * New registration
     */
    public function newAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/new')) {
            $this->_redirect('*/*/list');
        }
        /*
         * Redirect the user via a magento internal redirect
         */
        $this->_forward('edit');
    }

    /**
     * Edit or Create
     */
    public function editAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/edit')) {
            $this->_redirect('*/*/list');
        }

        $model = Mage::getModel('vax_registration/registration');
        Mage::register('current_registration', $model);
        $id = $this->getRequest()->getParam('registration_id');

        try {
            if ($id) {
                if (!$model->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }

            if ($model->getId()) {
                $pageTitle = $this->__('Edit Registration %s (%s)', $model->getID(), $model->getModel());
            } else {
                $pageTitle = $this->__('New Registration');
            }
            $this->_title($this->__('Registration'))->_title($pageTitle);

            $this->loadLayout();

            $this->_setActiveMenu('registration');
            $this->renderLayout();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/list');
        }
    }

    /**
     * Process form post
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            // Filter post data - date validation
            $data = $this->_filterPostData($data);
            $this->_getSession()->setFormData($data);
            $model = Mage::getModel('vax_registration/registration');
            $id = $this->getRequest()->getParam('registration_id');

            try {
                if ($id)
                    $model->load($id);

                // Set warranty number of years - if overriden use that value instead
                if(isset($data['override_warranty_box']) && $data['override_warranty_box'] == 0 && $data['override_warranty'] > 0) {
                    $data['warranty'] = $data['override_warranty'];
                }
                // Else use that from the database
                else {
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data['model']);
                    $data['warranty'] = substr($product->getUkGuarantee(), 0, 1);
                }

                $model->addData($data);
                $model->save();

                if (!$model->getId()) {
                    Mage::throwException($this->__('Error saving registration'));
                }

                if ($id) {
                    $this->_getSession()->addSuccess($this->__('Registration was successfully edited'));
                    $this->_logAdminAction($id, "update");
                }
                else {
                    $this->_getSession()->addSuccess($this->__('Registration was successfully created'));
                    $this->_logAdminAction($model->getId(), "create");
                }

                $this->_getSession()->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $params = array('registration_id' => $model->getId());
                    $this->_redirect('*/*/edit', $params);
                } else {
                    $this->_redirect('*/*/list');
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                if ($model && $model->getId()) {
                    $this->_redirect('*/*/edit', array('registration_id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/new');
                }
            }

            return;
        }

        $this->_getSession()->addError($this->__('No data found to save'));
        $this->_redirect('*/*');
    }

    /*
     * Send registration email
     */
    public function resendemailAction() {
        $id = $this->getRequest()->getParam('registration_id');

        if(!Mage::getSingleton('admin/session')->isAllowed('registration/edit')) {
            $this->_redirect('*/*/list');
        }

        // Load registration details
        $regdetails = Mage::getModel('vax_registration/registration')->load($id);
        // Load product details
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $regdetails->getModel());

        // Load product support email template
        $emailTemplate  = Mage::getModel('core/email_template')
                            ->loadDefault('product_registration_tpl')
                            ->setTemplateSubject('Vax '.$regdetails->getWarranty().' Year Guarantee');

        // Get store the user registered from, if no store (ie. it was created via admin) then use VaxUK (store 3)
        $storeid = ($regdetails->getStoreId() == 0)? "3" : $regdetails->getStoreId();

        // Get General email address (Admin->Configuration->General->Store Email Addresses) and set sender from contact form
        $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_support/name', $storeid));
        $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_support/email', $storeid));

        // Custom variables for email template
        $emailTemplateVariables['c_name']  = $regdetails->getFirstName();
        $emailTemplateVariables['c_code']  = $regdetails->getID();
        $emailTemplateVariables['c_model']  = $product->getName()." ".$product->getUkProductNo();
        $emailTemplateVariables['c_serial']  = $regdetails->getSerialNumber();
        $emailTemplateVariables['c_warranty']  = $regdetails->getWarranty();
        $emailTemplateVariables['c_purchase']  = date('dS F Y', strtotime($regdetails->getPurDate()));
        $emailTemplateVariables['vax_phone'] = Mage::getStoreConfig('general/store_information/phone', $storeid);
        $emailTemplateVariables['vax_email'] = Mage::getStoreConfig('trans_email/ident_support/email', $storeid);

        // Send email
        try {
            $emailTemplate->send($regdetails->getEmail(), $regdetails->getFirstName()." ".$regdetails->getSurname(), $emailTemplateVariables);
            $this->_getSession()->addSuccess($this->__('Registration email was successfully sent'));
        }
        catch(Exception $ex) {
            $this->_getSession()->addSuccess($this->__('Error sending registration email'));
            Mage::log("Error re-sending registration information to customer");
        }

        $this->_logAdminAction($id, "resend_email");
        /*
         * Redirect the user via a magento internal redirect
         */
        $this->_forward('edit', null, null, array("registration_id" => $id));
    }

    /*
     * Filter posted data - in this case we only need to filter dates!
     *
     * return array data
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('pur_date'));
        return $data;
    }

    protected function _logAdminAction ($registration_id, $action) {
        $eventcode = 'registration_edit';

        $userid = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $username = Mage::getSingleton('admin/session')->getUser()->getUsername();

        return Mage::getSingleton('enterprise_logging/event')->setData(array(
        'ip' => Mage::helper('core/http')->getRemoteAddr(),
        'user' => $username,
        'user_id' => $userid,
        'is_success' => true,
        'fullaction' => "adminhtml_registration_edit",
        'event_code' => $eventcode,
        'action' => $action,
        'info' => $registration_id,
        ))->save();
    }

    public function exportCsvAction()
    {
        $fileName   = 'registration.csv';
        $content    = $this->getLayout()->createBlock('vax_registration/adminhtml_registration_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'registration.xml';
        $content    = $this->getLayout()->createBlock('vax_registration/adminhtml_registration_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    /* required for export */
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.rawurlencode($fileName));
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', trim(preg_replace('/\s+/', ' ', $contentType)));
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}