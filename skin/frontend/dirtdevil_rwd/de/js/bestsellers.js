// Home Bestsellers carousel
jQuery(document).ready(function () {
    var bestsellers = '.bestsellers ul.mini-products-list';
    jQuery('.product-name').setAllToMaxMinHeight();

    enquire.register('screen and (max-width: ' + 480 + 'px)', {
        match: function () {
            //set images to be tall 45.5% of the window width - equal to its width
            //jQuery('.bestsellers .product-image').css('height', (0.455 * jQuery(window).width()));
        }, unmatch: function () {}
    });
    enquire.register('screen and (min-width: ' + 481 + 'px) and (max-width: ' + 770 + 'px)', {
        match: function () {
            moreViewsThumbnails(4, bestsellers, 1);
        }, unmatch: function () {}
    });
    enquire.register('screen and (min-width: ' + 771 + 'px) and (max-width: ' + 1000 + 'px)', {
        match: function () {
            //spares carousel, 3 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(3, bestsellers, 1);
        }, unmatch: function () {}
    });
    enquire.register('screen and (min-width: ' + 1001 + 'px)', {
        match: function () {
            //spares carousel, 4 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(4, bestsellers, 1);
        }, unmatch: function () {}
    });
});

//delayed resize
$j(window).resize( function() {
    if( timer ) { clearTimeout(timer); }
    var timer = setTimeout( function() {
        jQuery('.bestsellers .product-name').setAllToMaxMinHeight();
        alignProductGridActions('.mini-products-list', 'li.product', '.product-details' );
    }, 100 );
});

// carousel for displaying a number of images from a list
// used for product image gallery and spares slider
// takes the number of items per page, a reference to the list that contains the items and a way to reset to page 1 on resize
function moreViewsThumbnails(thumbsPerPage, list, currentPage) {
    if (thumbsPerPage === undefined) {
        thumbsPerPage = 5;
    }

    var listli = list + ' li';

    if (jQuery(list).length) {
        list = jQuery(list);
    } else {
        return false;
    }

    //round up the number of thumbnail pages in groups of 5
    // 1.6 pages is rounded to 2 pages
    var thumbs = jQuery(listli).length;
    var pages = parseInt(Math.ceil(thumbs / thumbsPerPage));
    if (currentPage == undefined) currentPage = 1;
    list.css("margin-left", 0);

    list.prev().addClass('nomore');
    list.next().addClass('nomore');
    if (thumbs > thumbsPerPage) {
        //if there are further pages the arrows are active
        list.prev().addClass('nomore');
        if (list.next().hasClass('nomore'))jQuery(list).next().removeClass('nomore');

        var margin = 0;

        list.on('movestart', function(e) {
            // If the movestart is heading off in an upwards or downwards
            // direction, prevent it so that the browser scrolls normally.
            if ((e.distX > e.distY && e.distX < -e.distY) ||
                (e.distX < e.distY && e.distX > -e.distY)) {
                e.preventDefault();
            }
        });

        list.parent().on('swipeleft', function (e) {
            GoRight();
        });
        list.parent().on('swiperight', function (e) {
            GoLeft();
        });

        list.next().click(function () {
            GoRight();
        });
        list.prev().click(function () {
            GoLeft();
        });


        function GoRight() {
            if (currentPage < pages) {
                list.animate({"margin-left": (margin - 100) + '%'}, 200);
                margin -= 100;

                currentPage++;
                if (list.prev().hasClass('nomore'))list.prev().removeClass('nomore');
                if (currentPage == pages) {
                    list.next().addClass('nomore');
                }
            }
        }

        function GoLeft() {
            if (currentPage > 1) {
                list.animate({"margin-left": (margin + 100) + '%'}, 200);
                margin += 100;

                currentPage--;
                if (list.next().hasClass('nomore'))list.next().removeClass('nomore');
                if (currentPage == 1) {
                    list.prev().addClass('nomore');
                }
            }
        }
    }
}

// equalize bestsellers heights
jQuery.fn.setAllToMaxMinHeight = function () {
    return this.css('min-height',(Math.max.apply(this, jQuery.map(this, function (e) {
        return jQuery(e).height()
    }))));
};

