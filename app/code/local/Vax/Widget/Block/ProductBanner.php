<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_ProductBanner
    extends Vax_Widget_Block_Abstract implements Mage_Widget_Block_Interface
{
    /** @var  Mage_Catalog_Model_Product */
    protected $_product;

    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        if (!$this->getTemplate()) {
            $this->setTemplate('widget/product_banner.phtml');
        }

        parent::_construct();
    }

    /**
     * Returns widget product.
     *
     * @return Mage_Catalog_Model_Product|Mage_Core_Model_Abstract
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::getModel('catalog/product')->load($this->_getProductId());
        }

        return $this->_product;
    }

    /**
     * Returns URL to product logo.
     * @return string
     */
    public function getProductLogoUrl()
    {
        if(!$this->getProductLogo()) {
            return false;
        }

        return $this->_getMediaUrl($this->getProductLogo());
    }

    /**
     * Returns URL to main product image.
     * @return string
     */
    public function getProductImageUrl()
    {
        return $this->_getMediaUrl($this->getMainImage());
    }

    /**
     * Returns URL of background image for large screens.
     * @return string
     */
    public function getLargeBgUrl()
    {
        return $this->_getMediaUrl($this->getLargeBgImage());
    }

    /**
     * Return URL of background image for small screens.
     *
     * @return string
     */
    public function getSmallBgUrl()
    {
        return $this->_getMediaUrl($this->getSmallBgImage());
    }

    /**
     * Returns ID of the product selected in a widget.
     *
     * @return int
     */
    protected function _getProductId()
    {
        $productData = explode('/', $this->getProductId());
        if (sizeof($productData > 0)) {
            return (int)$productData[1];
        }
    }
}