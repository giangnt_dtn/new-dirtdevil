<?php

/**
 * Registration Helper.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Attempts to save an entry.
     *
     * @param mixed $product
     * @param mixed $request
     *
     * @return void
     */
    public function saveRegistration($product, $request)
    {
        Mage::getSingleton('core/translate')->setTranslateInline(true);

        $data = $this->filterFormSubmission($request->getPost());

        if (!$this->isValidFormSubmission($data)) {
            throw new \Exception('Please ensure the form has been filled in correctly.');
        }

        return $this->_saveRegistration($product, $data);
    }

    /**
     * Filters the form submission.
     *
     * @param array $formData
     *
     * @return array
     */
    public function filterFormSubmission($formData)
    {
        foreach ($formData as $index => $data) {
            if(!is_array($data)) {
                $formData[$index] = trim($data);
            } else {
                $formData[$index] = trim($data[0]);
            }
        }

        return $formData;
    }

    /**
     * Checks whether the form has been submitted correctly.
     *
     * @param array $formData
     *
     * @return boolean
     */
    public function isValidFormSubmission($formData)
    {
        if (!isset($formData['title']) || !Zend_Validate::is($formData['title'], 'NotEmpty')) {
            return false;
        }
        if (!isset($formData['first_name']) || !Zend_Validate::is($formData['first_name'], 'NotEmpty')) {
            return false;
        }

        if (!isset($formData['surname']) || !Zend_Validate::is($formData['surname'], 'NotEmpty')) {
            return false;
        }

        if (!isset($formData['email']) || !Zend_Validate::is($formData['email'], 'EmailAddress')) {
            return false;
        }

        if (!isset($formData['telephone']) || !Zend_Validate::is($formData['telephone'], 'NotEmpty')) {
            return false;
        }

        if (!isset($formData['address']) || !Zend_Validate::is($formData['address'], 'NotEmpty')) {
            return false;
        }

        if (!isset($formData['town']) || !Zend_Validate::is($formData['town'], 'NotEmpty')) {
            return false;
        }

        if (!isset($formData['serial_number']) || !Zend_Validate::is($formData['serial_number'], 'NotEmpty')) {
            return false;
        }

        if (!isset($formData['pur_date']) || !Zend_Validate::is($formData['pur_date'], 'Date')) {
            return false;
        }

        if (!isset($formData['pur_price']) || !Zend_Validate::is($formData['pur_price'], 'NotEmpty')) {
//            Mage::log('wrong pur_date');
            return false;
        }

        if (!isset($formData['pur_location']) || !Zend_Validate::is($formData['pur_location'], 'NotEmpty')) {
            return false;
        }

        if (Mage::helper('vax_registration/form')->isRegistrationCountyEnabled()) {
            if (!isset($formData['county']) || !Zend_Validate::is($formData['county'], 'NotEmpty')) {
                return false;
            }
        }
        return true;
    }

    /**
     * Saves the registration.
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     *
     * @return Vax_Registration_Model_Registration
     */
    protected function _saveRegistration($product, array $data)
    {
        try {
            $registration = Mage::getModel('vax_registration/registration');
            $registration->setTitle($data['title']);
            $registration->setFirstName($data['first_name']);
            $registration->setSurname($data['surname']);
            $registration->setAddress($data['address']);
            $registration->setAddress2(isset($data['address2']) ? $data['address2'] : null);
            $registration->setTown($data['town']);

            if (Mage::helper('vax_registration/form')->isRegistrationCountyEnabled()) {
                $registration->setCounty($data['county']);
            }

            $registration->setPostcode($data['postcode']);

            $email = $data['email'];
            $registration->setEmail($email);
            $registration->setTelephone($data['telephone']);
            $registration->setModel(Mage::getSingleton('core/session')->getVaxRegistrationSku());
            $registration->setWarranty(substr($product->getUkGuarantee(), 0, 1));
            $registration->setSerialNumber($data['serial_number']);
            $registration->setPurDate($data['pur_date']);
            $registration->setPurPrice($data['pur_price']);
            $registration->setPurLocation($data['pur_location']);

            if (isset($data['mail_opt'])) {
                $registration->setMailOpt($data['mail_opt']);
                Mage::getModel('dirtdevil_newsletter/subscriber')->subscribe($email, $data['first_name'], $data['surname']);
            }

            if (isset($data['terms_agreed']) && $data['terms_agreed'] == 'on') {
                $registration->setTermsAgreed(true);
            }

            $extended_warranty_length = $this->_hasExtendedWarranty($data, $product);
            if ($extended_warranty_length && is_numeric($extended_warranty_length)) {
                $registration->setWarranty($extended_warranty_length);
            }

            $registration->setCreatedAt(date('Y-m-d H:i:s'));
            $registration->setStoreId(Mage::app()->getStore()->getStoreId());
            $registration->save();

            Mage::getSingleton('core/session')->unsVaxRegistrationSku();
            Mage::getSingleton('core/session')->setVaxRegistrationId($registration->getId());

            $registration_file = Mage::getSingleton('core/session')->getData('registration_file');

            if ($registration_file) {
                $registration_file = unserialize($registration_file);
            }

            if (isset($data['filename']) && $registration_file && is_object($registration_file)) {
                $registration_filename = Mage::getModel('vax_registration/attachment');
                $registration_filename->setFileName($registration_file->getFileName()); // $data['filename']
                $registration_filename->setFileNameOriginal($registration_file->getOriginalFileName()); // $data['original_filename']
                $registration_filename->setFileSize($registration_file->getSize()); // $data['filesize']
                $registration_filename->setRegistrationId($registration->getId());

                if ($registration_file->getThumbnail()) {
                    $registration_filename->setThumbnail($registration_file->getThumbnail()); // $data['thumbnail']
                }

                $registration_filename->save();

                // clear session object for uploaded file
                Mage::getSingleton('core/session')->unsetData('registration_file');
            }
        } catch (Exception $e) {
            Mage::log($e);
            throw new Exception($e->getMessage());
        }
        return $registration;
    }

    /**
     * Does the product have an extended warrant.
     *
     * @todo This is unlikely to apply to other stores
     *
     * @param array $post
     * @param Mage_Sales_Model_Product $product
     *
     * @return boolean
     */
    protected function _hasExtendedWarranty($post, $product)
    {
        try {
            $extended_warranty_offer = Mage::helper('vax_registration/form')->extendedWarrantyAvailable($product);
            $extended_warranty_length = Mage::helper('vax_registration/form')->extendedWarrantyLength($product);

            if (!$extended_warranty_offer || !$extended_warranty_length || !is_numeric($extended_warranty_length)) {
                return false;
            }

            if (isset($post['terms_agreed']) && $post['terms_agreed'] == 'on' && isset($post['filename'])) {
                return $extended_warranty_length;
            }

        } catch (Exception $ex) {
            Mage::log($ex->__toString(), null, 'registration.log');
        }

        return false;
    }
}