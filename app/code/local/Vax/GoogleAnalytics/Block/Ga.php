<?php
/**
* Google Analytics Block Override.
*
* @copyright Vax Ltd
*/
class Vax_GoogleAnalytics_Block_Ga extends Mage_GoogleAnalytics_Block_Ga
{
    protected $_order;

    /**
     * Returns the Tag Manager ID.
     *
     * @return string
     */
    public function getTagManagerId()
    {
        if (Mage::helper('googleanalytics')->isGoogleTagManagerAvailable()) {
            return Mage::helper('googleanalytics')->getContainerId();
        }
    }

    /**
     * Returns current order.
     *
     * @return false|Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->_order) {
            $this->_order = Mage::getModel('sales/order');

            if ($this->getOrderId()) {
                $this->_order->load($this->getOrderId());
            }
        }

        return $this->_order;
    }
}
