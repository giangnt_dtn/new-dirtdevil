<?php
/**
 * Vax Limited
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
 **/
class DirtDevilUk_Emails_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_MODULE_ENABLED               = 'dirtdeviluk_emails/general/is_enabled';
    const XML_PATH_MODULE_IS_LOGGING            = 'dirtdeviluk_emails/general/is_logging';
    const XML_PATH_MODULE_OVERRIDE_STORE_ID     = 'dirtdeviluk_emails/general/override_store_id';
    const XML_PATH_MODULE_FORCE_EMAILS          = 'dirtdeviluk_emails/config/force_emails';


    /**
     * Checks whether the module is enabled.
     *
     * @return boolean
     */
    public function isEnabled($storeId = null)
    {
        return (boolean) Mage::getStoreConfig(self::XML_PATH_MODULE_ENABLED, $storeId);
    }

    /**
     * Checks whether the logging is enabled.
     *
     * @return boolean
     */
    public function isLogging($storeId = null)
    {
        return (boolean) Mage::getStoreConfig(self::XML_PATH_MODULE_IS_LOGGING, $storeId);
    }

    /**
     * Checks whether the module is forcing emails.
     *
     * @return boolean
     */
    public function isForcingEmails($storeId = null)
    {
        return (boolean) Mage::getStoreConfig(self::XML_PATH_MODULE_FORCE_EMAILS, $storeId);
    }

    /**
     * Override store Id
     *
     * @return int
     */
    public function getOverrideStoreId($storeId = null)
    {
        return (int) Mage::getStoreConfig(self::XML_PATH_MODULE_OVERRIDE_STORE_ID, $storeId);
    }
}
