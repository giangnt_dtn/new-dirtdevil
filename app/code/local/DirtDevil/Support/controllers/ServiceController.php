<?php

class DirtDevil_Support_ServiceController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * sku action - returns the selected product
     *
     * Responds to AJAX (XmlHttpRequest)
     */
    public function skuAction() {

        $sku = $this->getRequest()->getParam('sku');

        // only 1 returned, let's do what we are supposed to do now
        $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
        $product = Mage::getModel('catalog/product')->load($id);

        $productHelper = Mage::helper('catalog/product');

        $data = array(
            'error'        => null,
            'product'      => $product,
            'productName'  => $product->getName(),
            'productSku'   => $sku,
            'productImage' => $product->getImageUrl(),
            'productUrl'   => $product->getProductUrl(),
            'hasSpares'    => $productHelper->hasSpares($product),
            'hasFeatures'  => $productHelper->hasFeatures($product),
            'hasWhatsBox'  => $productHelper->hasBox($product),
            'hasDownloads' => $productHelper->hasDownloads($product),
            'hasReviews'   => $productHelper->hasReviews(),
        );

        // Return JSON encoded data
        $this->getResponse()->setBody(json_encode($data));
    }
}
?>
