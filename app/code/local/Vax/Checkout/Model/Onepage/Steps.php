<?php
class Vax_Checkout_Model_Onepage_Steps extends Varien_Object
{
    protected $_steps;

    /**
     * Initialize the $_steps array
     */
    protected function _construct() {
        
        $this->_steps = array(
            array(
                'index'      => 1,
                'title'      => 'About You',
                'summary'    => 'Enter your details',
                'htmlClass'  => 'contact-details',
                'blockClass' => 'contact_details',
                'alias'      => 'contact'
            ),
            array(
                'index'     => 2,
                'title'     => 'Delivery',
                'summary'   => 'Enter your delivery address',
                'htmlClass' => 'delivery-details',
                'blockClass' => 'delivery_details',
                'alias'     => 'delivery',
            ),
            array(
                'index'     => 3,
                'title'     => 'Billing Details',
                'summary'   => 'Enter your billing address',
                'htmlClass' => 'billing-details',
                'blockClass' => 'billing_details',
                'alias'     => 'billing'
            ),
            array(
                'index'     => 4,
                'title'     => 'Payment Details',
                'summary'   => 'Please provide us with your payment details',
                'htmlClass' => 'payment-details',
                'blockClass' => 'payment_details',
                'alias'     => 'payment'
            ),
            array(
                'index'     => 5,
                'title'     => $this->getOnepage()->getConfirmationTitle(),
                'summary'   => $this->getOnepage()->getConfirmationSummary(),
                'htmlClass' => 'order-complete',
                'blockClass' => 'order_confirmation',
                'alias'     => 'summary'
            )
        );

        return parent::_construct();
    }

    public function getSteps()
    {
        return $this->_steps;
    }

    /**
     * Removes step with the given alias
     *
     * @param $alias
     * @return $this
     */
    public function removeStep($alias)
    {
        foreach ($this->_steps as $index => $step) {
            if (is_array($step) && array_key_exists('alias', $step) && $step['alias'] == $alias) {
                unset($this->_steps[$index]);
            }
        }
        $this->_reindexSteps();
        return $this;
    }

    /**
     * Get step by index value.
     *
     * @param $index
     * @return mixed
     */
    public function getStep($index) {
        foreach ($this->_steps as $idx => $step) {
            if ($index == $step['index']) {
                return $step;
            }
        }
    }

    /**
     * Adds new step.
     *
     * This method will ignore newly added step if the step with the same alias already exists.
     *
     * @param array $step
     * @param null $before
     * @return $this
     */
    public function addStep(array $step, $before = null)
    {
        if (!isset($step['alias'])) {
            return;
        }

        $added = false;
        $steps = array();
        foreach ($this->_steps as $currStep) {
            if (isset($currStep['alias']) && $before == $currStep['alias']) {
                $steps[] = $step;
                $added = true;
            } elseif ($step['alias'] == $currStep['alias']) {
                $added = true; // do not replace existed step with new data and ignore new step
            }
            $steps[] = $currStep;
        }
        if (!$added) {
            $steps[] = $step;
        }

        $this->_steps = $steps;
        $this->_reindexSteps();
        return $this;
    }


    /**
     * Sets consecutive integers as step indexes (starting from 1)
     *
     * @return $this
     */
    protected function _reindexSteps()
    {
        $index = 0;

        $stepsTmp = array();

        foreach ($this->_steps as $step) {
            $step['index'] = ++$index;
            $stepsTmp[] = $step;
        }

        $this->_steps = $stepsTmp;

        return $this;
    }

    public function getOnepage()
    {
        if ($this->_getData('onepage') &&  $this->_getData('onepage') instanceof Varien_Object) {
            return $this->_getData('onepage');
        } else {
            return new Varien_Object();
        }
    }
}