<?php
/**
 * Vax Registration (Retailer) Edit form block class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      C Carnell
 */
class Vax_Registration_Block_Adminhtml_Registration_Retailer_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare the form
     */
    protected function _prepareForm()
    {
        $retailer = Mage::registry('current_retailer');
        
        if($this->getRequest()->getParam('retailer_id')) {
            $form = new Varien_Data_Form(array(
                'id'        => 'edit_form',
                'action'    => $this->getUrl('*/*/save', array('retailer_id' => $this->getRequest()->getParam('retailer_id'))),
                'method'    => 'post'
            ));
        } else {
            $form = new Varien_Data_Form(array(
                'id'        => 'edit_form',
                'action'    => $this->getUrl('*/*/save'),
                'method'    => 'post'
            ));
        }

        $fieldset = $form->addFieldset('retailer_details', array('legend' => Mage::helper('vax_registration/form')->__('Retailer Details'), 'class' => 'fieldset-wide'));

        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('vax_registration/form')->__('Title'),
            'required'  => true,
            'text'      => 'title',
            'name'      => 'title'
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('select_stores', 'multiselect', array(
                'label'     => Mage::helper('vax_registration/form')->__('Store (select one only)'),
                'required'  => true,
                'name'      => 'stores[]',
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('select_stores', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $review->setSelectStores(Mage::app()->getStore(true)->getId());
        }
        
        $fieldset->addField('position', 'text', array(
            'label'     => Mage::helper('vax_registration/form')->__('Position'),
            'required'  => true,
            'name'      => 'position',
            'class'     => 'validate-digits'
        ));

        $fieldset->addField('is_enabled', 'select', array(
            'label'     => Mage::helper('vax_registration/form')->__('Enabled'),
            'required'  => false,
            'name'      => 'is_enabled',
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
            
        ));

        $form->setUseContainer(true);
        
        if($retailer) {
            $form->setValues($retailer->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);
        
        if($this->getRequest()->getParam('retailer_id')) {
            return $this;
        }
    }
}