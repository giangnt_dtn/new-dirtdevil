<?php
/**
 * Html page block
 *
 * @category   DirtDevil
 * @package    DirtDevil_Page
 * @author     Vax
 */
if (Mage::helper('core')->isModuleEnabled('Diglin_UIOptimization')) {
    class DirtDevil_Page_Block_Html_Head extends Diglin_UIOptimization_Block_Optimize_Head
    {
        /**
         * Get HEAD HTML with CSS/JS/RSS definitions
         * (actually it also renders other elements)
         *
         * @return string
         */
        public function getCssJsHtml()
        {
            // separate items by types
            $lines  = array();
            foreach ($this->_data['items'] as $item) {
                if (!is_null($item['cond']) && !$this->getData($item['cond']) || !isset($item['name'])) {
                    continue;
                }
                $if     = !empty($item['if']) ? $item['if'] : '';
                $params = !empty($item['params']) ? $item['params'] : '';
                switch ($item['type']) {
                    case 'external_js':  // external js/*.js
                    case 'external_css':  // external css/*.css
                    case 'inline_js':  // inline js/*.js
                    case 'js':        // js/*.js
                    case 'skin_js':   // skin/*/*.js
                    case 'js_css':    // js/*.css
                    case 'skin_css':  // skin/*/*.css
                        $lines[$if][$item['type']][$params][$item['name']] = $item['name'];
                        break;
                    default:
                        $this->_separateOtherHtmlHeadElements($lines, $if, $item['type'], $params, $item['name'], $item);
                        break;
                }
            }

            // prepare HTML
            $shouldMergeJs = Mage::getStoreConfigFlag('dev/js/merge_files');
            $shouldMergeCss = Mage::getStoreConfigFlag('dev/css/merge_css_files');
            $html   = '';
            foreach ($lines as $if => $items) {
                if (empty($items)) {
                    continue;
                }
                if (!empty($if)) {
                    // open !IE conditional using raw value
                    if (strpos($if, "><!-->") !== false) {
                        $html .= $if . "\n";
                    } else {
                        $html .= '<!--[if '.$if.']>' . "\n";
                    }
                }

                // static and skin css
                $html .= $this->_prepareStaticAndSkinElements('<link rel="stylesheet" type="text/css" href="%s"%s />' . "\n",
                    empty($items['js_css']) ? array() : $items['js_css'],
                    empty($items['skin_css']) ? array() : $items['skin_css'],
                    $shouldMergeCss ? array(Mage::getDesign(), 'getMergedCssUrl') : null
                );

                $html .= $this->_prepareInlineAndExternalSkinElements('<link rel="stylesheet" type="text/css" href="%s" />' . "\n",
                    empty($items['external_css']) ? array() : $items['external_css']);

                $html .= $this->_prepareInlineAndExternalSkinElements('<script type="text/javascript" src="%s"></script>' . "\n",
                    empty($items['external_js']) ? array() : $items['external_js']);

                $html .= $this->_prepareInlineAndExternalSkinElements('<script type="text/javascript">%s</script>' . "\n",
                    empty($items['inline_js']) ? array() : $items['inline_js']);

                // static and skin javascripts
                $html .= $this->_prepareStaticAndSkinElements('<script type="text/javascript" src="%s"%s></script>' . "\n",
                    empty($items['js']) ? array() : $items['js'],
                    empty($items['skin_js']) ? array() : $items['skin_js'],
                    $shouldMergeJs ? array(Mage::getDesign(), 'getMergedJsUrl') : null
                );

                // other stuff
                if (!empty($items['other'])) {
                    $html .= $this->_prepareOtherHtmlHeadElements($items['other']) . "\n";
                }

                if (!empty($if)) {
                    // close !IE conditional comments correctly
                    if (strpos($if, "><!-->") !== false) {
                        $html .= '<!--<![endif]-->' . "\n";
                    } else {
                        $html .= '<![endif]-->' . "\n";
                    }
                }
            }
            return $html;
        }

        /**
         * Insert external jscript files into script tag
         *
         * @return string
         */
        protected function &_prepareInlineAndExternalSkinElements($format, array $staticItems)
        {
            $html = '';

            foreach ($staticItems as $idx => $jscript) {

                foreach ($jscript as $src) {
                    $jscript_trimmed = trim($src);
                    $html .= sprintf($format, $src);
                }
            }

            return $html;
        }

        /**
         * Retrieve title element text (encoded)
         *
         * @return string
         */
        public function getTitle()
        {
            if (empty($this->_data['title']) ||
                !empty($this->_data['title']) && trim($this->_data['title']) == '') {
                $this->_data['title'] = $this->getDefaultTitle();
            }

            return htmlspecialchars(html_entity_decode(trim($this->_data['title']), ENT_QUOTES, 'UTF-8'));
        }
    }
} else {
    class DirtDevil_Page_Block_Html_Head extends Mage_Page_Block_Html_Head
    {
        /**
         * Get HEAD HTML with CSS/JS/RSS definitions
         * (actually it also renders other elements)
         *
         * @return string
         */
        public function getCssJsHtml()
        {
            // separate items by types
            $lines  = array();
            foreach ($this->_data['items'] as $item) {
                if (!is_null($item['cond']) && !$this->getData($item['cond']) || !isset($item['name'])) {
                    continue;
                }
                $if     = !empty($item['if']) ? $item['if'] : '';
                $params = !empty($item['params']) ? $item['params'] : '';
                switch ($item['type']) {
                    case 'external_js':  // external js/*.js
                    case 'external_css':  // external css/*.css
                    case 'inline_js':  // inline js/*.js
                    case 'js':        // js/*.js
                    case 'skin_js':   // skin/*/*.js
                    case 'js_css':    // js/*.css
                    case 'skin_css':  // skin/*/*.css
                        $lines[$if][$item['type']][$params][$item['name']] = $item['name'];
                        break;
                    default:
                        $this->_separateOtherHtmlHeadElements($lines, $if, $item['type'], $params, $item['name'], $item);
                        break;
                }
            }

            // prepare HTML
            $shouldMergeJs = Mage::getStoreConfigFlag('dev/js/merge_files');
            $shouldMergeCss = Mage::getStoreConfigFlag('dev/css/merge_css_files');
            $html   = '';
            foreach ($lines as $if => $items) {
                if (empty($items)) {
                    continue;
                }
                if (!empty($if)) {
                    // open !IE conditional using raw value
                    if (strpos($if, "><!-->") !== false) {
                        $html .= $if . "\n";
                    } else {
                        $html .= '<!--[if '.$if.']>' . "\n";
                    }
                }

                // static and skin css
                $html .= $this->_prepareStaticAndSkinElements('<link rel="stylesheet" type="text/css" href="%s"%s />' . "\n",
                    empty($items['js_css']) ? array() : $items['js_css'],
                    empty($items['skin_css']) ? array() : $items['skin_css'],
                    $shouldMergeCss ? array(Mage::getDesign(), 'getMergedCssUrl') : null
                );

                $html .= $this->_prepareInlineAndExternalSkinElements('<link rel="stylesheet" type="text/css" href="%s" />' . "\n",
                    empty($items['external_css']) ? array() : $items['external_css']);

                $html .= $this->_prepareInlineAndExternalSkinElements('<script type="text/javascript" src="%s"></script>' . "\n",
                    empty($items['external_js']) ? array() : $items['external_js']);

                $html .= $this->_prepareInlineAndExternalSkinElements('<script type="text/javascript">%s</script>' . "\n",
                    empty($items['inline_js']) ? array() : $items['inline_js']);

                // static and skin javascripts
                $html .= $this->_prepareStaticAndSkinElements('<script type="text/javascript" src="%s"%s></script>' . "\n",
                    empty($items['js']) ? array() : $items['js'],
                    empty($items['skin_js']) ? array() : $items['skin_js'],
                    $shouldMergeJs ? array(Mage::getDesign(), 'getMergedJsUrl') : null
                );

                // other stuff
                if (!empty($items['other'])) {
                    $html .= $this->_prepareOtherHtmlHeadElements($items['other']) . "\n";
                }

                if (!empty($if)) {
                    // close !IE conditional comments correctly
                    if (strpos($if, "><!-->") !== false) {
                        $html .= '<!--<![endif]-->' . "\n";
                    } else {
                        $html .= '<![endif]-->' . "\n";
                    }
                }
            }
            return $html;
        }

        /**
         * Insert external jscript files into script tag
         *
         * @return string
         */
        protected function &_prepareInlineAndExternalSkinElements($format, array $staticItems)
        {
            $html = '';

            foreach ($staticItems as $idx => $jscript) {

                foreach ($jscript as $src) {
                    $jscript_trimmed = trim($src);
                    $html .= sprintf($format, $src);
                }
            }

            return $html;
        }

        /**
         * Retrieve title element text (encoded)
         *
         * @return string
         */
        public function getTitle()
        {
            if (empty($this->_data['title']) ||
                !empty($this->_data['title']) && trim($this->_data['title']) == '') {
                $this->_data['title'] = $this->getDefaultTitle();
            }

            return htmlspecialchars(html_entity_decode(trim($this->_data['title']), ENT_QUOTES, 'UTF-8'));
        }
    }
}

