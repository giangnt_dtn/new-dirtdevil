<?php
class Vax_Prodfaqs_Block_Faq extends Mage_Core_Block_Template
{
    public function getFaq()
    {
        if (!$this->hasFaq()) {
            $faq = Mage::registry('current_faq');
            if ($faq && $faq instanceof Varien_Object) {
                $topic = Mage::getModel('prodfaqs/topic')->load($faq->getTopicId());
                if ($topic && $topic instanceof Varien_Object) {
                    $faq->setTopic($topic);
                }
                $this->setData('faq', $faq);

            }
        }
        return $this->getData('faq');
    }
}