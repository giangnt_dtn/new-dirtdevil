<?php

/**
 * Attachment Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Attachment extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vax_registration/attachment');
    }

    /**
     * Loads the model by registration ID.
     *
     * @param integer $registrationId
     *
     * @return \Vax_Registration_Model_Attachment
     */
    public function loadByRegistrationId($registrationId)
    {
        $this->_getResource()->loadByRegistrationId($this, $registrationId);
        $this->_afterLoad();

        return $this;
    }
}