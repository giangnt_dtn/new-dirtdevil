<?php
/**
 * Manuals block
 *
 * @category   DirtDevil
 * @package    DirtDevil_Page
 * @author     Vax
 */
class DirtDevil_Support_Block_Manuals extends Mage_Core_Block_Template
{
    protected $product;
    protected $category;

    protected function _construct() {
        parent::_construct();

        if(Mage::registry("current_category")) {
            $this->category = Mage::registry("current_category");
        }

        if($this->category && $this->category->getCategoryProductId()) {
            $this->product = Mage::getModel('catalog/product')->load($this->category->getCategoryProductId());
        }
    }

    public function getDocuments() {
        if(!$this->product) {
            return false;
        }

        if(!Mage::helper('vax_document')->hasUserguide($this->product->getSku())) {
            return false;
        }

        return Mage::helper('dirtdevil_support')->getUserguidesForProduct($this->product);
    }

    public function getDocumentLink($document) {
        return Mage::helper('dirtdevil_support')->getUserguideLink($document);
    }

    public function getDocumentTitle($document) {
        return "User Guide";
        //return $document['fields']['document_type'][0];
    }

    public function getDocumentType($document) {
        return "User Guide";
        //return $document['fields']['document_type'][0];
    }
}
