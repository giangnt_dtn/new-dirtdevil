<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/quote'),
        'marketing_opt_in',
        array(
            'type'     =>  Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => true,
            'default'  => null,
            'comment'  =>  'Subscriber Opt In'
        )
    );
$installer->endSetup();
