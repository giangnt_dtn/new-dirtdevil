<?php
/**
 * Catalog Observer
 *
 * @category   DirtDevil
 * @package    DirtDevil_Catalog
 * @author     Vax
 */
class DirtDevil_Catalog_Model_Observer extends Mage_Catalog_Model_Observer
{
    /**
     * Recursively adds categories to top menu
     *
     * @param Varien_Data_Tree_Node_Collection|array $categories
     * @param Varien_Data_Tree_Node $parentCategoryNode
     * @param Mage_Page_Block_Html_Topmenu $menuBlock
     * @param bool $addTags
     */
    protected function _addCategoriesToMenu($categories, $parentCategoryNode, $menuBlock, $addTags = false)
    {
        $categoryModel = Mage::getModel('catalog/category');
        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $categoryModel->setId($category->getId());
            if ($addTags) {
                $menuBlock->addModelTags($categoryModel);
            }

            $tree = $parentCategoryNode->getTree();
            $thumbnail = $categoryModel->load($category->getId())->getThumbnail();

            if(!empty($thumbnail)) {
                $thumbnail = Mage::getBaseUrl('media') . 'catalog/category/' . $thumbnail;
            }

            $name = $category->getName();

            if($categoryModel->getCategoryMenuName()) {
                $name = $categoryModel->getCategoryMenuName();
            }

            $categoryData = array(
                'name' => $name,
                'id' => $nodeId,
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'is_active' => $this->_isActiveMenuCategory($category),
                'image_url' => $thumbnail, /* compatibility with old DD DE theme */
                'thumbnail' => $thumbnail
            );
            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            $parentCategoryNode->addChild($categoryNode);

            $flatHelper = Mage::helper('catalog/category_flat');
            if ($flatHelper->isEnabled() && $flatHelper->isBuilt(true)) {
                $subcategories = (array)$category->getChildrenNodes();
            } else {
                $subcategories = $category->getChildren();
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);
        }
    }
}
