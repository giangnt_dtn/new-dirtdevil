<?php
/**
 * Extend the Vax Toolbar block not Turpentines
 *
 * @category    code
 * @package     vax_uk
 * @copyright   Copyright (c) 2013 Vax Ltd.
 * @author      C Carnell
 */
class Vax_Turpentine_Model_Observer_Varnish extends Nexcessnet_Turpentine_Model_Observer_Varnish {
    /**
     * Add a rewrite for catalog/product_list_toolbar if config option enabled
     *
     * @param Varien_Object $eventObject
     * @return null
     */
    public function addProductListToolbarRewrite( $eventObject ) {
        if( Mage::helper( 'turpentine/varnish' )->shouldFixProductListToolbar() ) {
            Mage::getSingleton( 'turpentine/shim_mage_core_app' )
                ->shim_addClassRewrite( 'block', 'catalog', 'product_list_toolbar',
                    'Vax_Turpentine_Block_Catalog_Product_List_Toolbar' );
        }
    }
}