<?php

/**
 * Checkout Request Validator.
 * 
 * @copyright Vax Ltd 2014
 */
class Vax_Checkout_Model_Mapper_Checkout_Request_Validator
{
    /**
     * Required Sections.
     * 
     * @var array
     */
    protected $_requiredSections = array(
        'contactDetails' => array(
            'prefix',
            'firstname',
            'lastname',
            'email',
            'telephone'
        ),
        'deliveryAddress' => array(
            'postcode',
            'street_1',
            'city',
            'region',
            'country_id',
            'shipping_method'
        ),
        'billingAddress' => array(
            'postcode',
            'street_1',
            'city',
            'region',
            'country_id'
        ),
        'paymentDetails' => array(
            'method'
        )
    );

    /**
     * Payment Parameters.
     * 
     * @var array
     */
    protected $_requiredPaymentParams = array(
        'realex' => array(
            'cc_type',
            'cc_number',
            'cc_owner',
            'cc_cid',
            'cc_exp_month',
            'cc_exp_year'
        )
    );

    /**
     * Validates Request Data.
     *
     * @param array $data Request data to validate
     * 
     * @return boolean
     * @throws Exception
     */
    public function validate(array $data)
    {
        try {
            $this->_validateRequired($data);
            $this->validateContactDetails($data['contactDetails']);
            $this->validateDeliveryAddress($data['deliveryAddress']);
            $this->validateBillingAddress($data['billingAddress']);
            $this->validatePaymentDetails($data['paymentDetails']);
            return true;
        } catch (Exception $e) {
            Mage::logException($e);
            throw ($e);
        }
    }

    /**
     * Checks if contact details were filled in correctly
     *
     * @param array $data
     *
     * @return boolean
     */
    public function validateContactDetails(array $data) {
        $this->_validateRequired($data, 'contactDetails');
        $emailValidator = new Zend_Validate_EmailAddress();

        if (!$emailValidator->isValid($data['email'])) {
            Mage::log($data);
            Mage::throwException(
                Mage::helper('checkout')->__('%s is not valid email address', $data['email'])
            );
        }
        return true;
    }

    /**
     * Check fields in delivery address
     *
     * @param array $data
     *
     * @return boolean
     */
    public function validateDeliveryAddress(array $data)
    {
        $this->_validateRequired($data, 'deliveryAddress');
        return true;
    }

    /**
     * Validate entries in billing address
     *
     * @param array $data
     *
     * @return boolean
     */
    public function validateBillingAddress(array $data)
    {
        $this->_validateRequired($data, 'billingAddress');
        return true;
    }

    /**
     * Validates payment details. Calls separate validation for each payment method (if implemented)
     *
     * @param array $data
     *
     * @return boolean
     */
    public function validatePaymentDetails(array $data)
    {
        $this->_validateRequired($data, 'paymentDetails');

        if (!array_key_exists($data['method'], $this->_requiredPaymentParams)) {
            return true;
        }

        $this->_validateRequired($this->_requiredPaymentParams[$data['method']], $data['method']);
        $this->_validateNotEmptyFields($this->_requiredPaymentParams[$data['method']], $data);

        // auto discover implemented validation for given payment method
        $methodName = '_validate' . ucfirst($data['method']);

        if (method_exists($this, $methodName)) {
            call_user_func(array($this, $methodName), $data);
        }

        return true;
    }

    /**
     * Checks whether given array contains required data
     *
     * @param array $sections
     *
     * @return boolean
     */
    protected function _validateRequired(array $data, $section = false)
    {
        if ($section === false || $section === '') {
            $this->_validateRequiredKeys(array_keys($this->_requiredSections), $data, 'Missing required step data for: %s');
            return true;
        }

        if (!array_key_exists((string)$section, $this->_requiredSections)) {
            return true;
        }

        $this->_validateRequiredKeys($this->_requiredSections[$section], $data);
        $this->_validateNotEmptyFields($this->_requiredSections[$section], $data);

        return true;
    }

    /**
     * Checks whether given array contains all required keys
     *
     * @param array $keys
     * @param array $data
     * @param string $errorMsg
     * 
     * @return boolean
     */
    protected function _validateRequiredKeys(array $keys, array $data, $errorMsg = 'Missing required data: %s')
    {
        $diff = array_diff($keys, array_keys($data));

        if (count($diff)) {
            Mage::throwException(
                Mage::helper('checkout')->__($errorMsg, implode(', ', $diff))
            );
        }

        return true;
    }

    /**
     * Checks if array's fields with given keys are empty
     *
     * @param array $keys
     * @param array $data
     *
     * @return boolean
     */
    protected function _validateNotEmptyFields(array $keys, array $data)
    {
        $notEmptyValidator = new Zend_Validate_NotEmpty();

        foreach ($keys as $key) {
            if (!$notEmptyValidator->isValid($data[$key])) {
                Mage::throwException(
                    Mage::helper('checkout')->__('%s must not be empty', $key)
                );
            }
        }

        return true;
    }
}