<?php
/**
 * Catalog attribute layer filter
 *
 * @category   Vax
 * @package    Vax_Catalog
 * @author     Vax
 */
class DirtDevil_Catalog_Block_Layer_Filter_Attribute extends Mage_Catalog_Block_Layer_Filter_Attribute
{
    public function __construct()
    {
        parent::__construct();
        
        if(Mage::helper('catalog/category')->useAjaxCheckboxes()) {
            $this->setTemplate('catalog/layer/filter/checkbox.phtml');
        }
    }
}
