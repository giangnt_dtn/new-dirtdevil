<?php

class DirtDevilUk_Bazaarvoice_Helper_Data extends Bazaarvoice_Connector_Helper_Data
{
    /**
     * Get the uniquely identifying product ID for a catalog product.
     *
     * This is the unique, product family-level id (duplicates are unacceptable).
     * If a product has its own page, this is its product ID. It is not necessarily
     * the SKU ID, as we do not collect separate Ratings & Reviews for different
     * styles of product - i.e. the 'Blue' vs. 'Red Widget'.
     *
     * @static
     * @param  $product a reference to a catalog product object
     * @return The unique product ID to be used with Bazaarvoice
     */
    public function getProductId($product)
    {
        if(!$product) {
            return false;
        }

        $rawProductId = $product->getSku();

        if(!$rawProductId) {
            $rawProductId = Mage::getModel('catalog/product')->load($product->getId())->getSku();
        }

        // >> Customizations go here
        $rawProductId = preg_replace_callback('/\./s', create_function('$match','return "_bv".ord($match[0])."_";'), $rawProductId);
        $rawProductId = str_replace('-SL', '', $rawProductId);

        // << No further customizations after this
        
        return $this->replaceIllegalCharacters($rawProductId);

    }

    /* override ftp */
    public function getSFTPHost($store = null)
    {
        $environment = Mage::getStoreConfig('bazaarvoice/general/environment', $store);
        $sftpHostOverride = trim(Mage::getStoreConfig('bazaarvoice/bv_config/sftp_host_name', $store));
        if(strlen($sftpHostOverride)) {
            $sftpHost = $sftpHostOverride;
        }
        else if ($environment == 'staging') {
            $sftpHost = 'sftp7-stg.bazaarvoice.com';
        }
        else {
            $sftpHost = 'sftp7.bazaarvoice.com';
        }
        return $sftpHost;
    }
}
