<?php
class Vax_SupportPortal_Block_Html_Accordion_Step extends Mage_Core_Block_Abstract
{
    public function getId()
    {
        if (!$this->getData('id')) {
            $id = preg_replace('/[^a-z0-9_]/', '_', strtolower($this->getNameInLayout()));
            $this->setData('id', $id);
        }
        return $this->getData('id');
    }

    protected function _beforeToHtml()
    {
        $blockName = $this->getDataSetDefault('name', '');
        $data = $this->getData();
        if ($this->hasData('cms_block_id')) {
            $data['block_id'] = $this->getCmsBlockId();
            $this->setContentRenderer($this->getLayout()->createBlock('cms/block', $blockName, $data));
        }
        if ($this->hasData('content_template')) {
            $data['template'] = $this->getContentTemplate();
            $blockType = $this->getDataSetDefault('block_type', 'core/template');
            $this->setContentRenderer($this->getLayout()->createBlock($blockType, $blockName, $data));
        }
    }

    protected function _toHtml()
    {
        $parentId = $this->getParentBlock()->getId();
        //empty if the block isn't there or isn't enabled
        if ( !$this->getContentRenderer() || trim($this->getContentRenderer()->toHtml()) == '' || !$this->getContentRenderer() instanceof Mage_Core_Block_Abstract) {
            return '';
        }
        $html = '<div class="accordion-step' . $parentId . ' accordion-step section ' . strtolower($this->getNameInLayout()) . '" id="' . $this->getId() . '">';
        $html .= '<div class="icon ' . $this->getId() . ' "></div> <h5 class="accordion-title">' . $this->__($this->getTitle());
        $html .= '</h5></div>';
        $html .= '<div class="accordion-content' . $parentId . ' accordion-content a-item" style="display: none">' . $this->getContentRenderer()->toHtml() . '</div>';
        return $html;
    }
}