<?php

/**
 * OPC Response.
 *
 * @copyright Vax Ltd
 */
abstract class Vax_Checkout_Model_Response_Opc
{
    /**
     * Returns the applicable OPC response.
     *
     * @return array
     */
    public abstract function getOpcResponse();

    /**
     * Returns an applicable OPC message.
     *
     * @return string
     */
    public abstract function getOpcMessage();

    /**
     * Returns whether the response is an error.
     *
     * @return boolean
     */
    public function isFailure()
    {
        return key($this->getOpcResponse()) == 'failure';
    }

    /**
     * Returns whether the response is a redirect.
     *
     * @return boolean
     */
    public function isRedirect()
    {
        return key($this->getOpcResponse()) == 'redirect';
    }
}