<?php

/**
 * Newsletter Helper.
 *
 * @copyright DirtDevil
 */
class DirtDevil_Newsletter_Helper_Data extends Mage_Newsletter_Helper_Data
{
    const XML_PATH_NEWSLETTER_FORM_ENABLED = 'newsletter/subscription/display_newsletter_form';
    const XML_PATH_NEWSLETTER_FORM_KEY_VALIDATION = 'newsletter/subscription/form_key_validation';

    /**
     * Checks whether the newsletter is enabled.
     *
     * @return boolean
     */
    public function isNewsletterFormEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWSLETTER_FORM_ENABLED);
    }

    /**
     * Checks whether form key validation is enabled
     *
     * @return boolean
     */
    public function isFormKeyValidationEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWSLETTER_FORM_KEY_VALIDATION);
    }
    /**
     *  @return text
     */
    public function getMessageNewsletter($type = null)
    {
        $html = '';

        foreach (Mage::getSingleton('dirtdevil_newsletter/newsletterSession')->getMessages()->getItems() as $type => $message) {
            $html= '<ul id="admin_messages">'
                . $message->getText()
                . '</ul>';
        }

        Mage::getSingleton('dirtdevil_newsletter/newsletterSession')->getMessages()->clear();

        return $html;
    }
}