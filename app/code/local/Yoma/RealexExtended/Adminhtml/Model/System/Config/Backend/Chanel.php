<?php

class Yoma_RealexExtended_Adminhtml_Model_System_Config_Backend_Chanel extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getResourceModel('realexExtended/chanel')->uploadAndImport($this);
    }
}
