<?php
/**
 * Catalog layer category filter
 *
 * @category    Vax
 * @package     Vax_Catalog
 * @author      Vax
 */
class DirtDevil_Catalog_Block_Layer_Filter_Category extends Mage_Catalog_Block_Layer_Filter_Category
{
    public function __construct()
    {
        if(!Mage::helper('catalog/category')->useAjaxCheckboxes()) {
            return parent::__construct();
        }

        parent::__construct();
        $this->setTemplate('catalog/layer/filter/checkbox.phtml');
    }
}
