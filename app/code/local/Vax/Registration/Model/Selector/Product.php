<?php

/**
 * Product Selector Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Selector_Product
{
    /**
     * Retrieves a current list of products and outputs in the admin control panel
     *
     * @return array products
     */
    public function toOptionArray()
    {
        $array_return = array();
        $collection   = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('attribute_set_id',array('nin' => array(26, 27)))
                ->addAttributeToFilter('status','1')
                ->addAttributeToFilter('uk_guarantee', array('gt' => 0))
                ->addAttributeToSelect(array('name', 'sku', 'uk_product_no', 'uk_guarantee'))
                ->addAttributeToSort('uk_product_no', 'ASC');

        foreach ($collection as $prod_model) {
            array_push($array_return, array("label" => $prod_model->getUkProductNo()." - ".$prod_model->getName()." (".$prod_model->getUkGuarantee().")", "value" => $prod_model->getSku()));
        }

        array_unshift($array_return, array("label" => "Select product...", "value" => 0));

        return $array_return;
    }
}