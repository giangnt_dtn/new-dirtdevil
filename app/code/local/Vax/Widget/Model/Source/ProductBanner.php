<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Model_Source_ProductBanner
{
    public function toOptionArray()
    {
        $collection = Mage::getResourceModel('widget/widget_instance_collection');
        $collection->addFieldToFilter('instance_type', 'vax_widget/productBanner');

        $options = array(
            array(
                'value' => 0,
                'label' => Mage::helper('vax_widget')->__('-- Select banner --')
            )
        );

        foreach ($collection as $item) {
            $options[] = array(
                'value' => $item->getId(),
                'label' => $item->getTitle()
            );
        }

        return $options;
    }
}
