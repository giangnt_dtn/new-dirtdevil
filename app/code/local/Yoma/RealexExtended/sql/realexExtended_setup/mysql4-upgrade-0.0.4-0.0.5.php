<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `{$this->getTable('realexExtended/chanel')}`
    CHANGE COLUMN `tss_result` `tss_result` varchar(255) DEFAULT NULL,
    CHANGE COLUMN `avs_result` `avs_result` varchar(255) DEFAULT NULL;
");
$installer->endSetup();