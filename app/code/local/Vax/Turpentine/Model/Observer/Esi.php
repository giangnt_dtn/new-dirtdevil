<?php

/**
 * @category    code
 * @package     vax_uk
 * @copyright   Copyright (c) 2016 Vax Ltd.
 */
class Vax_Turpentine_Model_Observer_Esi extends Nexcessnet_Turpentine_Model_Observer_Esi
{
    /**
     * Allow custom database layout updates to be added to ESI data
     *
     * @param  Mage_Core_Block_Template $blockObject
     * @param  array $esiOptions
     * @return Varien_Object
     */
    protected function _getEsiData($blockObject, $esiOptions)
    {
        $esiData = parent::_getEsiData($blockObject, $esiOptions);
        if ($layoutHandles = $esiData->getLayoutHandles()) {
            if (is_array($esiOptions['registry_keys'])) {
                foreach ($esiOptions['registry_keys'] as $key => $options) {
                    $value = Mage::registry($key);
                    if ($value && is_object($value)) {
                        if ($value instanceof Mage_Catalog_Model_Category) {
                            $layoutHandles[] = 'CATEGORY_' . $value->getId();
                        } else if ($value instanceof Mage_Catalog_Model_Product) {
                            $layoutHandles[] = 'PRODUCT_' . $value->getId();
                        }
                    }
                }
                $esiData->setLayoutHandles($layoutHandles);
            }
        }

        return $esiData;
    }
}