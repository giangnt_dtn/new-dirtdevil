<?php
/**
 * Newsletter subscribers collection
 *
 * @category    DirtDevil
 * @package     DirtDevil_Newsletter
 */
class DirtDevil_Newsletter_Model_Resource_Subscriber_Collection extends Mage_Newsletter_Model_Resource_Subscriber_Collection
{
    protected function _construct()
    {
        parent::_construct();

        // defining mapping for fields represented in several tables
        $this->_map['fields']['subscriber_lastname'] = 'subscriber_lastname_table.value';
        $this->_map['fields']['subscriber_firstname'] = 'subscriber_firstname_table.value';
    }
}
