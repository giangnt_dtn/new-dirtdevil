$j(document).ready(function () {
    function handleScroll() {
        var divPosition = $j('#checkout-steps').offset();
        var blockHeight = $j('#order-summary').height();


        if ($j(window).width() <= 770) {
            $j('#order-summary').css({position: 'static'});
            return;
        }
        footerHeight = $j($j('.footer-container')[0]).height() + $j($j('.pre-footer-container')[0]).height() +40;

        curOffset = $j(window).scrollTop();
        maxScrollBottom = $j(document).height() - footerHeight;
        if (curOffset >= divPosition.top - 10) {
            if (curOffset <= maxScrollBottom - blockHeight) {
                $j('#order-summary').css({position:'fixed', top: '10px', bottom: 'auto'});
            } else {
                bottomPos = $j(window).height() - ($j(document).height() - curOffset) + footerHeight;
                $j('#order-summary').css({position:'fixed', bottom: bottomPos + 'px', top: 'auto'});
            }
        } else {
            $j('#order-summary').css({position: 'fixed', top: (divPosition.top - curOffset) + 'px'});
        }
    }

    divPos = $j('#checkout-steps').offset();
    curOffset = $j(window).scrollTop();

    $j('#order-summary').css({position: 'fixed', top: (divPos.top - curOffset) + 'px'});

    $j(window).scroll(function(event) {
        handleScroll();
    });

    $j(window).on('small-delayed-resize', function (e, resizeEvent) {
        handleScroll();
    });

    handleScroll();

    // make sure that the payment method is selected upon clicking its label
    $j('.payment-method label').click(function() {
        $j(this).parent().find('input[type=radio]').click();
    });
});
