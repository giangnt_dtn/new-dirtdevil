<?php

/**
 * Contact Collection Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_SupportPortal_Model_Resource_Contact_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vax_supportportal/contact');
    }
}