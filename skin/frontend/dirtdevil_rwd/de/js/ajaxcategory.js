var oldFeature;
var currFeature;
var oldFeatureCat;
jQuery(document).ready(function(){
    jQuery('.block-layered-nav-container').on("change", 'input', function(e) {
        oldFeature = currFeature;
        oldFeatureCat = jQuery(this).parents('dd').prev('dt').text();
        currFeature = jQuery(this).parents('label').text().trim();
        currFeature = currFeature.substr(0,30).trim();
    });
    showSuitableFor();
});


/* open filters that have been checked by default */
function openSelectedLayeredNavElements()
{
    enquire.register('(max-width: ' + bp.medium + 'px)', {
        setup: function () {
        },
        match: function () {
            jQuery('.block-layered-nav .filter-checkbox-on').each(function() {

                if(!jQuery('.toggle-content').hasClass('accordion-open')) {
                    jQuery('.toggle-content').addClass('accordion-open');
                }

                jQuery(this).closest('dt').addClass('current');
                jQuery(this).closest('dd').addClass('current');
            });
        },
        unmatch: function () {
        }
    });
}

/* as the layered nav is returned via Ajax, reattach event listeners */
function updateLayeredNavToggleJs() {
    jQuery('.block-layered-nav .toggle-content').each(function ($j) {
        var wrapper = jQuery(this);

        var dl = wrapper.children('dl:first');
        var dts = dl.children('dt');
        var panes = dl.children('dd');
        var groups = new Array(dts, panes);

        //Add "last" classes.
        var i;
        for (i = 0; i < groups.length; i++) {
            groups[i].filter(':last').addClass('last');
        }

        function toggleClasses(clickedItem, group) {
            var index = group.index(clickedItem);
            var i;
            for (i = 0; i < groups.length; i++) {
                groups[i].removeClass('current');
                groups[i].eq(index).addClass('current');
            }
        }

        //Toggle on tab (dt) click.
        dts.on('click', function (e) {
            //They clicked the current dt to close it. Restore the wrapper to unclicked state.
            if (jQuery(this).hasClass('current') && wrapper.hasClass('accordion-open')) {
                wrapper.removeClass('accordion-open');
            } else {
                //They're clicking something new. Reflect the explicit user interaction.
                wrapper.addClass('accordion-open');
            }
            toggleClasses(jQuery(this), dts);
        });
    });
}

var index = '';
var filterApplied = [];
var filterCats = [];
var formKey = '';

function updateCategory(url, type, closed) {
    jQuery(function($j) {
        $j('#progress-dialog').fadeIn(300);
        $j('body').css('cursor', 'wait');

        url += '&isAjax=1';
        var scrollTo = 0;

        // refresh the form key
        $j.ajax({
            url: '/turpentine/esi/getFormKey',
            method: 'POST',
            cache: false,
            async: false,
            success: function(transport) {
                formKey = transport;
            }
        });

        $j.ajax({
            url: url,
            dataType: 'json',
            data: null,
            type: type,
            async: false,
            success: function(response) {
                 if (response.layerednav) {
                    if ($j('.block-layered-nav')) {
                        $j('.block-layered-nav-container').empty();
                        $j('.block-layered-nav-container').append(response.layerednav);
                        updateLayeredNavToggleJs();

                        if(typeof closed !== 'undefined' && !closed) {
                            openSelectedLayeredNavElements();
                        }
                    }
                }

                if (response.productlist) {
                    re = new RegExp('{{dd_form_key_placeholder}}', 'g');
                    response.productlist = response.productlist.replace(re, formKey);
                }

                if(window.hasOwnProperty('dataLayer')){
                    var prod     = jQuery(response.productlist).filter('.category-products').find('ol.products-list li.item');
                    var products = [];
                    var prodIds = [];
                    var prodTotal = jQuery(response.productlist).filter('.category-products').find('.toolbar .total .amount .desktop').first().text().substr(0,2).trim();
                    var list = jQuery('.breadcrumbs li strong').text();
                    var filter = '';
                    var filterRes = '';
                    var filterCat = '';
                    var orderBy = jQuery('.toolbar .sorter select option:selected:first').text().trim();
                    var orderByRes = orderBy.substr(0,13).trim();
                    var removedFilter = '';
                    var i = 0;

                    jQuery(prod).each(function(){
                        prodIds.push(jQuery(this).attr('data-product-id'));
                    });


                    jQuery.ajax({
                        url: '/googletagmanager/datalayer/datalayer',
                        dataType: 'json',
                        data: {ids : prodIds, list : list},
                        type: 'POST',
                        async: false,
                        success: function(data) {
                            products = data;
                        }
                    });


                    //If a filter is applied, add it to the list of applied filters
                    jQuery('.block-layered-nav-container ol li a.filter-checkbox-on').each(function(){
                        if(jQuery(this).parents('label').text().trim().length > 0)
                        {
                            filter = jQuery(this).parents('label').text().trim();
                            filterRes = filter.substr(0,30).trim();
                            indexFilterRes = filterApplied.indexOf(filterRes);
                            if(indexFilterRes === -1){
                                filterApplied.push(filterRes);
                                i = 1;
                            }
                            filterCat = jQuery(this).parents('dd').prev('dt').text();
                            potential = filterCat.concat(" : ").concat(filterRes);
                            indexPotential = filterCats.indexOf(potential);
                            if(indexPotential === -1) {
                                filterCats.push(potential);
                            }
                        }
                    });

                    indexCurr = filterApplied.indexOf(currFeature);
                    if(indexCurr > -1 && i == 0){
                        removedFilter = oldFeatureCat.concat(" : ").concat(filterApplied[indexCurr]);
                        indexCats = filterCats.indexOf(removedFilter);
                        filterCats.splice(indexCats,1);
                        filterApplied.splice(index, 1);
                    }

                    // Passes price as the name for filter By
                    var priceFilter = jQuery('.ui-slider-range').attr('style');
                    if(!filterRes && priceFilter){
                        var priceFilterRes = priceFilter.split('width: ')[1];
                        if(priceFilterRes.indexOf("100%") == -1){
                            filterRes = 'Price ' + oldMinPrice + ' - ' + oldMaxPrice;
                            filterCats.push(filterRes);
                        } else {
                            var index;
                            for (index = 0; index < filterCats.length; ++index){
                                if(filterCats[index].indexOf('Price') > -1){
                                    filterCats.splice(index,1);
                                }
                            }
                        }
                    }

                    pageRes = "page " + page;

                    var o = {
                        "event" : "productList",
                        "products_displayed" : products.length,
                        "results_found" : prodTotal,
                        "pagination" : pageRes,
                        "filter_by" : filterCats.toString(),
                        "order_by" : orderByRes,
                        "ecommerce" : {
                            "impressions" : products
                            }
                        };

                    dataLayer.push(o);
                }


                if (response.productlist) {
                    $j('.category-products').empty();
                    $j('.category-products').append(response.productlist);

                    /** [KS] Restored scrolling top upon AJAX load **/
                    if($j('.products-grid').length) { scrollTo = $j('.products-grid').offset().top - 200; }
                    if($j('.products-list').length) { scrollTo = $j('.products-list').offset().top - 200; }
                    if ($j('.toolbar').length) { scrollTo = $j('.toolbar').offset().top - 200}

                    $j('html, body').animate({
                        scrollTop: scrollTo
                    }, 1);
                }

                $j("div.sorter select").attr("onchange", "");
                $j('body').css('cursor', 'default');
                $j("img").unveil(200);
                $j('#progress-dialog').fadeOut(300);
                if (typeof moveCartIcons === 'function') {
                    moveCartIcons();
                }
                //in app.js
                alignProductGridActions('.products-grid', 'li.item', '.height-box' );
                categoryLayout();

                jQuery(window).resize(function() {
                    clearTimeout(window.resizedFinished);
                    window.resizedFinished = setTimeout(function(){
                        categoryLayout();
                    }, 250);
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr + '-' + xhr.status + ' - options: - '+ ajaxOptions + ' - error message : ' + thrownError);
            }
        });
    });

    return false;
}

var page = 1;
var oldPage = 1;
var lazyUpdateCategory = _.debounce(updateCategory, 500);

function attachAjaxCategoryListener(post_type) {
    jQuery("div.sorter select").attr("onchange", "");
    jQuery(".category-products").on("change", 'div.sorter select', function(e) {
        e.preventDefault();

        if (typeof datalayerPushOrder == 'function') {
            datalayerPushOrder();
        }

        lazyUpdateCategory(this.value, post_type);
        return false;
    });

    jQuery(".block-layered-nav-container").on('click', 'a', function(e) {
        e.preventDefault();
        lazyUpdateCategory(this.href, post_type);
        return false;
    });

    jQuery(".view-mode > *").on('click', function(e) {
        e.preventDefault();
        lazyUpdateCategory(this.href, post_type);
        return false;
    });

    jQuery(".category-products").on('click', '.pager a', function(e) {
        oldPage = page;
        page = jQuery(this).text();
        if(page.indexOf('Next') > -1 || page.indexOf('Previous') > -1){
            page = jQuery(this).attr('href').split('p=')[1].substr(0,1);
        }
        e.preventDefault();
        lazyUpdateCategory(this.href, post_type);
        return false;
    });
}

function categoryLayout() {
    displayWidth = jQuery(window).width();
    if (displayWidth > 599) {
        jQuery('.product-shop2').each(function() {
            desc = jQuery(this).html();
            that = jQuery(this).parent('.item').find('.product-shop .product-secondary.actions');
            jQuery(this).remove();
            jQuery(desc).insertBefore(that);
        });
    }
};


