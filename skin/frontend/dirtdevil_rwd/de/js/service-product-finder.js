jQuery(document).ready(function ($j) {

    var controller = "/support/service/sku/";
    //reset placeholders
    //$j('.product-sku').empty();
    //$j('.product-image').empty();
    //$j('.product-sku').empty();
    //$j('.product-url').hide();

    //hide the mini view before a product is selected
    // $j('.product-mini-view').hide();

    //hide links section before a product is selected
    //$j('.product-links').hide();


    function loadProduct(sku) {
        $j('#product-selector-container').show();

        var url = controller + "?sku=" + sku;


        $j.ajax({
            url: url,
            context: document.body,
            success: function (data, status) {

                var response = $j.parseJSON(data);
                if (response.productName !== null) {
                    $j('.search-box-error').empty();

                    //show miniview and product links
                    $j('.product-links').show();
                    $j('.product-mini-view').show();

                    //empty mini view elements
                    $j('.product-sku').empty();
                    $j('.product-image').attr('src', '');
                    $j('.product-url').attr('href', '');

                    //empty product sections
                    $j('.spares-li').hide();
                    $j('.features-li').hide();
                    $j('.whats-box-li').hide();
                    $j('.downloads-li').hide();
                    $j('.reviews-li').hide();

                    //empty product sections links
                    $j('.spares-link').attr('href', '');
                    $j('.downloads-link').attr('href', '');
                    $j('.features-link').attr('href', '');
                    $j('.reviews-link').attr('href', '');
                    $j('.whats-box-link').attr('href', '');
                    $j('.questions-link').attr('href', '');
                    $j('.specs-link').attr('href', '');

                    //==========================================

                    //set mini view data
                    $j('.product-url').show();

                    $j('.product-sku').html(response.productSku);
                    $j('.product-image').attr('src', response.productImage);
                    $j('.product-url').attr('href', response.productUrl);
                    $j('.product-sku').html(' ' + response.productSku + ' ');

                    //set "Search results ..." title
                    $j('.product-name').html(response.productName);

                    //show product sections if they appear on that product's page
                    if(response.hasSpares)    { $j('.spares-li').css('display', 'inline-block'); }
                    if(response.hasDownloads) { $j('.downloads-li').css('display', 'inline-block'); }
                    if(response.hasFeatures)  { $j('.features-li').css('display', 'inline-block'); }
                    if(response.hasReviews)   { $j('.reviews-li').css('display', 'inline-block'); }
                    if(response.hasWhatsBox)  { $j('.whats-box-li').css('display', 'inline-block'); }

                    //add links to the product sections
                    $j('.spares-link').attr('href', response.productUrl    + '#spares-and-accessories');
                    $j('.downloads-link').attr('href', response.productUrl + '#downloads');
                    $j('.features-link').attr('href', response.productUrl  + '#features');
                    $j('.reviews-link').attr('href', response.productUrl   + '#reviews');
                    $j('.whats-box-link').attr('href', response.productUrl + '#whats-in-the-box');
                    $j('.questions-link').attr('href', response.productUrl + '#faqs');
                    $j('.specs-link').attr('href', response.productUrl     + '#specifications');

                } else {
                    $j('.search-box-error').html('<p class="note-msg error"> No Product found!</p>');
                }
            }
        });
    }

    var selectProductName = $j('.before-main .spares-search-controls #model-dropdown');

    if(selectProductName != null) {
        jQuery('body').on('change', selectProductName, function(e) {
            var value = e.target.value;
            if(value != '-' &&  $j('.before-main-container #search-reg').length) {
                $j('.before-main-container [name="search"]').attr('value', value);
            }
            $j('.before-main-container #spares-model-number-search').submit();
        });
    }




    // Do product code search on submit
    $j('#spares-model-number-search').on('submit', function (event) {

        var uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("?"));
            window.history.replaceState({}, document.title, clean_uri);
        }

        var input = $j('.before-main-container [name="search"]');
        input.val($j.trim(input.val()));

        if($j.trim(input.val()) === '') {
            return false;
        }
        //sync dropdown with input box
        var element = $j('.before-main-container #model-dropdown');
        element.value = input.val();

        loadProduct(input.val());

        return false;
    });



});
