<?php
class Vax_Prodfaqs_Helper_Data extends Mage_Core_Helper_Data
{
	protected function _getPrivateIps()
	{
		$textIp = str_replace(' ', ',', Mage::getStoreConfig('prodfaqs/access_control/ip_addresses'));
		$ips = explode(',',$textIp);
		$ips = array_unique($ips);
		$pos = array_search(' ', $ips);
		if ($pos !== false) {
			unset($ips[$pos]);
		}
		return $ips;
	}
	
	public function isPrivateMode()
	{
		return in_array(Mage::app()->getFrontController()->getRequest()->getClientIp(), $this->_getPrivateIps());
	}
}