<?php
    function getDir() {
        return  "./../../var/site-dashboard/";
    }

    function checkDirExists() {
        if (!file_exists(getDir())) {
            mkdir(getDir(), 0777, true);
        }
    }

    function generateLastDeployed() {
        $filename="./../../index.php";
        $filenameJson=getDir() . "last-deployed.json";

        $stats = @stat($filename);
        $ownerArray = \posix_getpwuid($stats['uid']);
        $groupArray = \posix_getgrgid($stats['gid']);

        /* file owners */
        $owner = $ownerArray['name'];
        $group = $groupArray['name'];

        /* file dates */
        $lasModifiedTime= $stats['mtime']; //Time of last modification
        $lastAccessedTime= $stats['atime']; //Time of last access.
        $lastChangeTime = $stats['ctime']; //Time of last status change(linux) /created(onwindows)
        $lastModifiedTimeFormated = date('Y-m-d H:i:s',$stats['mtime']);
        $lastAccessedTimeFormated = date('Y-m-d H:i:s',$stats['atime']);
        $lastChangeTimeFormated = date('Y-m-d H:i:s',$stats['ctime']); 

        /* file owners array */
        $owner = array('owner'=> $owner,
                       'group'=> $group
                      );

        /* file dates array */
        $date = array('accessed'=> $lastAccessedTimeFormated,
                      'modified'=> $lastModifiedTimeFormated, 
                      'changed'=> $lastChangeTimeFormated);

        $jsonArray = array('owner'=>$owner,
                           'date'=>$date, 
                           'dateCreated'=>date('Y-m-d H:i:s'));

        /** Encode the array with JSON encoder **/
        $jsonOutput = \json_encode($jsonArray);
        \file_put_contents($filenameJson,$jsonOutput);
    }
    
    function generateSystemInfo() {
        
        $systemRoot='/';
        $filename=getDir() . 'system-info.json';

        /** CPU **/

        $array1Min5min15min = sys_getloadavg(); //average number of processes working in that duration
        $cpuUsage1min = $array1Min5min15min[0];
        $cpuUsage5min = $array1Min5min15min[1];
        $cpuUsage15min = $array1Min5min15min[2];
        $cpuinfo = file_get_contents('/proc/cpuinfo');

        preg_match_all('/^processor/m', $cpuinfo, $matches);
        $cpuCores = count($matches[0]);


        /** DiskSpace **/

        // Bytes 
        $diskSpaceFreeBytes = \disk_free_space($systemRoot);
        $diskSpaceTotalBytes = \disk_total_space($systemRoot);
        $diskSpaceUsedBytes = $diskSpaceTotalBytes - $diskSpaceFreeBytes;

        // KiloBytes
        $diskSpaceFreeKiloBytes = $diskSpaceFreeBytes/1024;
        $diskSpaceTotalKiloBytes = $diskSpaceTotalBytes/1024;
        $diskSpaceUsedKiloBytes = $diskSpaceUsedBytes/1024;

        // MegaBytes
        $diskSpaceFreeMegaBytes = $diskSpaceFreeBytes/1024/1024;
        $diskSpaceTotalMegaBytes = $diskSpaceTotalBytes/1024/1024;
        $diskSpaceUsedMegaBytes = $diskSpaceUsedBytes/1024/1024;

        // GigaBytes
        $diskSpaceFreeGigaBytes = $diskSpaceFreeBytes/1024/1024/1024;
        $diskSpaceTotalGigaBytes = $diskSpaceTotalBytes/1024/1024/1024;
        $diskSpaceUsedGigaBytes = $diskSpaceUsedBytes/1024/1024/1024;


        /** Memory **/

        //bytes
        $memoryUsedBytes = \memory_get_usage();             // only the used memory used by emalloc() is reported. 
        $memoryAllocatedBytes = \memory_get_usage(TRUE);    //total memory allocated from system, including unused pages.
        $memoryFreeBytes = $memoryAllocatedBytes - $memoryUsedBytes;

    //    $memoryUsedBytesPeak = \memory_get_peak_usage ();
    //    $memoryAllocatedBytesPeak = \memory_get_peak_usage (true);
    //    echo '<br/> $memoryUsedBytesPeak: '.$memoryUsedBytesPeak;
    //    echo '<br/> $memoryAllocatedBytesPeak: ' .$memoryAllocatedBytesPeak;
    //    echo '<br/> $memoryUsedBytes: '.$memoryUsedBytes;
    //    echo '<br/> $memoryAllocatedBytes: ' .$memoryAllocatedBytes;


        //Kilobytes
        $memoryUsedKiloBytes = $memoryUsedBytes/1024;
        $memoryAllocatedKiloBytes = $memoryAllocatedBytes/1024;
        $memoryFreeKiloBytes= $memoryFreeBytes/1024;

        //Megabytes
        $memoryUsedMegaBytes = $memoryUsedBytes/1024/1024;
        $memoryAllocatedMegaBytes = $memoryAllocatedBytes/1024/1024;
        $memoryFreeMegaBytes= $memoryFreeBytes/1024/1024;

        //Gigabytes
        $memoryUsedGigaBytes = $memoryUsedBytes/1024/1024/1024;
        $memoryAllocatedGigaBytes = $memoryAllocatedBytes/1024/1024/1024;
        $memoryFreeGigaBytes= $memoryFreeBytes/1024/1024/1024;

        /** Create array for JSON **/

        $cpuCores = array('total'=>$cpuCores); 

        $cpuUsage = array('1min'=>$cpuUsage1min,
                          '5min'=>$cpuUsage5min,
                          '15min'=>$cpuUsage15min
                         );

        $memoryUsed = array('bytes'=>$memoryUsedBytes, 
                            'kilobytes'=>$memoryUsedKiloBytes,
                            'megabytes'=>$memoryUsedMegaBytes,
                            'gigabytes'=>$memoryUsedGigaBytes,
                            );
        $memoryAllocated = array('bytes'=>$memoryAllocatedBytes, 
                            'kilobytes'=>$memoryAllocatedKiloBytes,
                            'megabytes'=>$memoryAllocatedMegaBytes,
                            'gigabytes'=>$memoryAllocatedGigaBytes);

        $memoryFree = array('bytes'=>$memoryFreeBytes, 
                            'kilobytes'=>$memoryFreeKiloBytes,
                            'megabytes'=>$memoryFreeMegaBytes,
                            'gigabytes'=>$memoryFreeGigaBytes);

        $diskSpaceFree = array('bytes'=>$diskSpaceFreeBytes, 
                            'kilobytes'=>$diskSpaceFreeKiloBytes,
                            'megabytes'=>$diskSpaceFreeMegaBytes,
                            'gigabytes'=>$diskSpaceFreeGigaBytes);

        $diskSpaceTotal = array('bytes'=>$diskSpaceTotalBytes, 
                            'kilobytes'=>$diskSpaceTotalKiloBytes,
                            'megabytes'=>$diskSpaceTotalMegaBytes,
                            'gigabytes'=>$diskSpaceTotalGigaBytes);

        $diskSpaceUsed = array('bytes'=>$diskSpaceUsedBytes, 
                            'kilobytes'=>$diskSpaceUsedKiloBytes,
                            'megabytes'=>$diskSpaceUsedMegaBytes,
                            'gigabytes'=>$diskSpaceUsedGigaBytes);

        $jsonArray = array( 'creator'=>'shabnam bibi',
                            'cpuCores'=>$cpuCores,
                            'cpuUsage'=>$cpuUsage,
                            'memoryFree'=>$memoryFree,
                            'memoryUsed'=>$memoryUsed, 
                            'memoryAllocated'=>$memoryAllocated, 
                            'diskSpaceFree'=>$diskSpaceFree,
                            'diskSpaceUsed'=>$diskSpaceUsed,
                            'diskSpaceTotal'=>$diskSpaceTotal, 
                            'dateCreated'=>date('Y-m-d H:i:s'));

        /** Encode the array with JSON encoder **/
        $jsonOutput = \json_encode($jsonArray);
        \file_put_contents($filename,$jsonOutput);
    }

    checkDirExists();

    echo "Generating System info" . PHP_EOL;
    generateSystemInfo();
    
    echo "Generating Last Deployed" . PHP_EOL;
    generateLastDeployed();
    
?>
