<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevilUk_Pdfinvoiceplus_Model_Entity_Itemsorder extends Magestore_Pdfinvoiceplus_Model_Entity_Itemsorder
{
    public function getPdf($html = '') {
        $isMassPDF = $this->isMassPDF();
        $mailPdf = new Varien_Object;
        if($isMassPDF){
            $templateBody = $this->getTheProcessedTemplate();
            $mailPdf->setData('htmltemplate', $templateBody);
            $mailPdf->setData('filename', $this->getFileName());
        }
        else{
            $pdf = $this->loadPdf();
            $templateBody = $this->getTheProcessedTemplate();
            $pdf->WriteHTML($this->getCss($templateBody), 1);
            $pdf->WriteHTML($templateBody);

            $mailPdf->setData('htmltemplate', $templateBody);
            $output = $pdf->Output($this->getFileName(), 'S');
            $mailPdf->setData('pdfbody', $output);
            $mailPdf->setData('filename', $this->getFileName());
        }
        return $mailPdf;
    }
}