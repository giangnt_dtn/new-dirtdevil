<?php
class DirtDevil_DisableCheckout_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_IS_CHECKOUT_DISABLED = 'disable_checkout/general/enabled';
    const XML_PATH_IS_BETA_CHECKOUT_DISABLED = 'disable_checkout/general/enabled_beta_site';
    const XML_PATH_BETA_SITE_URL = 'disable_checkout/general/beta_site_url';
    
    /**
     * Checks if the checkout should be disabled for current store view
     * 
     * @return boolean 
     */
    public function isCheckoutDisabled($product = null)
    {
        // disable checkout unless on beta site
        if($_SERVER['HTTP_HOST'] && $_SERVER['HTTP_HOST'] === Mage::getStoreConfig(self::XML_PATH_BETA_SITE_URL)) {
            return Mage::getStoreConfig(self::XML_PATH_IS_BETA_CHECKOUT_DISABLED);
        }

        if($product && $product->isSaleDisabled()) {
            return true;
        }

        return Mage::getStoreConfig(self::XML_PATH_IS_CHECKOUT_DISABLED);
    }
}
