<?php

class CodeSocial_RedisClearCache_Model_Observer {

    private function getCacheInstances() {
        
        $cache = array();
        $cache = unserialize(Mage::getStoreConfig('redisclearcache/servers/ip_port_array', Mage::app()->getStore()->getId()));
        
        return $cache;
    }
    
    public function cleanRedisCache(Varien_Event_Observer $observer) {
               
        $type = $observer->getEvent()->getType();
        
        if ($type && $type !== 'codesocial_redis_cache') { 
            return;
        }
  
        if(!Mage::app()->useCache('codesocial_redis_cache')) {
            $this->_getSession()->addError(Mage::helper('adminhtml')->__("Redis Cache Clear is disabled"));
            return;
        }
        
        try {
            $cache = $this->getCacheInstances();
            
            if(!$cache || ($cache && !is_array($cache))) {
                $this->_getSession()->addError(Mage::helper('adminhtml')->__("No Redis Cache Servers specified"));
                return false;
            }
            
            foreach($cache as $options) {
                $this->clearBackendCache($options);
            } 
            
        } catch (Exception $e) {
            $this->_getSession()->addError(Mage::helper('adminhtml')->__($e->getMessage()));
        }
       
        return $this;
    }

    private function clearBackendCache($options) {
        
        $cmCache = new Cm_Cache_Backend_Redis($options);
        $cmCache->clean();
        $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__("The Redis cache for " . $options["server"] . " has been flushed."));
    }
    
    protected function _getSession() {
        return Mage::getSingleton('adminhtml/session');
    }
}