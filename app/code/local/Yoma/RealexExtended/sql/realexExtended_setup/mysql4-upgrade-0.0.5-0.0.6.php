<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `{$this->getTable('realexExtended/chanel')}`
    ADD COLUMN `payment_method` varchar(255) AFTER `tss_rule_specific`;
");
$installer->endSetup();