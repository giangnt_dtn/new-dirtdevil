jQuery(document).ready(function($j) {

    $j(".support-product .tabs .faqs a").on('click', function(e) {
        e.preventDefault();

        activateTabMenu(".support-product .tabs .faqs a");
        activateTabContent(".support-product .main-container .faqs");

        return false;
    });

    $j(".support-product .tabs .manuals a").on('click', function(e) {
        e.preventDefault();

        activateTabMenu(".support-product .tabs .manuals a");
        activateTabContent(".support-product .main-container .manuals");

        return false;
    });

    $j(".support-product .tabs .main a").on('click', function(e) {
        e.preventDefault();

        activateTabMenu(".support-product .tabs .main a");
        activateTabContent(".support-product .main-container .main");

        return false;
    });

    function activateTabMenu(tab) {
        $j(tab).addClass("active");
        $j(".support-product .tabs > div a").not($j(tab)).removeClass("active");
        $j(".support-product .tabs > span a").not($j(tab)).removeClass("active");
    }

    function activateTabContent(tab) {

        $j(tab).css("display", "block");
        $j(".support-product .main-container > div").not($j(tab)).css("display", "none");
    }

    $j(".faqs .top5 h3 a").on('click', function(e) {
        e.preventDefault();

        $j(".faqs .top5 h3").toggleClass("active");
        $j(".top5 ul").toggleClass("active");

        //deactivate all other accordion elements
        $j('.topics li > a').each(function(){
            $j(this).closest('li').removeClass('active');
        });
        return false;
    });

    $j(".faqs .topic > a").on('click', function(e) {
        e.preventDefault();

        $j(this).closest('li').toggleClass("active");

        //deactivate all other accordion elements
        $j('.topics li > a').not($j(this)).each(function(){
            $j(this).closest('li').removeClass('active');
        });

        //deactivate top5 accordion
        $j('.top5 h3').removeClass('active');
        $j('.top5 ul').removeClass('active');
        return false;
    });

    $j(".faqs .top5 .faq-single .question").on('click', function(e) {
        e.preventDefault();
        $j(this).closest('.faq-single').find('.answer').toggleClass("active");
        return false;
    });

    $j(".faq-single .answer h4").on('click', function(e) {
        e.preventDefault();
        $j(this).closest('.faq-single').find('.answer').toggleClass("active");
        return false;
    });

    $j('#prodfaqs-search .input-text').keypress(function(e) {
        if(e.which == 13) {
            $j('form#prodfaqs-search').submit();
            return false;
        }
    });

    $j('#prodfaqs-search').bind('submit', function(event) {

        if($j('#prodfaqs-search .input-text').val() == '') {
            return false;
        }

        var link = $j('form#prodfaqs-search').attr('action') +
            "?term=" + $j('#prodfaqs-search .input-text').val() +
            "&product_id=" + $j('#prodfaqs-search #product_id').val();

        $j.ajax({
            url: link,
            type: "GET",
            context: document.body,
            beforeSend: function(){
                $j('#search-results-container .loading').show();
                $j('#search-results-container .results').empty();
            },
            success: function(data, status) {
                var response = $j.parseJSON(data);

                if (response.faqs.length > 0) {
                    $j.each(response.faqs, function(index, faq) {
                        var item = $j(response.template);
                        item.find('.aq').text(faq.title);
                        item.find('.description').html(faq.answer);
                        item.appendTo('#search-results-container .results');
                    });
                    $j('#search-results-container .loading').hide();
                    $j('#search-results-container .results').show();
                }
                else {
                }
            }
        });

        return false;
    });
});

function openContactForm() {
    if ($j('input#product_id') && $j('input#product_id').val()) {
        window.location.href = "/contacts?product=" +  $j('input#product_id').val();
    } else {
        window.location.href = "/contacts";
    }
}

var $j = jQuery.noConflict();

$j(document).ready(function() {
    $j('.before-main-container h1').click(function(evt) {
        if ($j(window).width() < 770) {
            $j(this).toggleClass('active');
            $j(this).parent().find('.menu-mobile').toggle();
        }
    });
});
