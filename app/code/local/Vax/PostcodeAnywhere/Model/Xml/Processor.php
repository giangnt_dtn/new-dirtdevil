<?php

/**
 * Vax Postcode Anywhere XML Processor
 *
 * @copyright Vax Ltd 2013
 */
class Vax_PostcodeAnywhere_Model_Xml_Processor extends Varien_Object
{
    /**
     * Processes a post code response.
     *
     * @param string $xml
     *
     * @return boolean
     */
    public function parsePostcodeResponse($xml)
    {
        try
        {
            $parser = new SimpleXMLElement($xml);
        }
        catch (Exception $e) {
            Mage::logException($e);

            return false;
        }

        $options = array();

        foreach ($parser->Data->Item as $item) {
            if ( ! empty($item['description']) && ! empty($item['id'])) {
                $options[] = array(
                    'label' => (string) $item['description'],
                    'value' => (string) $item['id']
                );
            }
        }

        return $options;
    }

    /**
     * Parses an address response.
     *
     * @param string $xml
     *
     * @return boolean
     */
    public function parseAddressResponse($xml)
    {
        try
        {
            $parser = new SimpleXMLElement($xml);
        }
        catch (Exception $e)
        {
            Mage::logException($e);

            return false;
        }

        $address = array();

        $item = $parser->Data->Item;

        if ($item['id'])
        {
            $address = $this->_parseAddress($item);
        }

        return $address;
    }

    /**
     * Parses an address.
     *
     * @param array $item
     *
     * @return array
     */
    protected function _parseAddress($item)
    {
        $data             = array();
        $data['street_1'] = ($item['organisation_name']) ? (string) $item['organisation_name'] . ", " . (string) $item['line1'] : (string) $item['line1'];
        $data['street_2'] = (string) $item['line2'];
        $data['city']     = (string) $item['post_town'];
        $data['region']   = (string) $item['county'];
        $data['postcode'] = (string) $item['postcode'];

        return $data;
    }
}