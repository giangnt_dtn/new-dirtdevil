<?php
class Yoma_RealexExtended_Adminhtml_Model_System_Config_Source_Avs {

    protected $_options = array(

        array('value'=>'M','label'=>'Matched'),
        array('value'=>'N','label'=>'Not Matched'),
        array('value'=>'U','label'=>'Not Supported'),
        array('value'=>'I','label'=>'Failed'),
        array('value'=>'P','label'=>'Not Processed'),
    );

    public function toOptionArray($isMultiselect){

        $options = $this->_options;

        return $options;
    }

}
