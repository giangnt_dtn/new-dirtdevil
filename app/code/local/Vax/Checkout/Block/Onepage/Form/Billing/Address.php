<?php

/**
 * Vax Checkout Onepage Form Billing Address.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Form_Billing_Address extends Vax_Checkout_Block_Onepage_Form_Address
{
    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->_getQuote()->getBillingAddress();
    }
}