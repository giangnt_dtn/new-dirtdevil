<?php
/**
 * Vax Registration (Retailer) Grid inner class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      C Carnell
 */
class Vax_Registration_Block_Adminhtml_Registration_Retailer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize grid settings
     *
     */
    protected function _construct() {
        parent::_construct();
 
        $this->setId('vax_registration_retailer_grid');
        $this->setDefaultSort('retailer_id');
 
        /*
         * Override method getGridUrl() in this
         * class to provide URL for ajax
         */
        $this->setUseAjax(true);
    }
 
    /**
     * Prepare registration collection
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('vax_registration/retailer_collection');
        $this->setCollection($collection);
 
        return parent::_prepareCollection();
    }
 
    /**
     * Prepare grid columns to those in the database
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Grid
     */
    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => $this->__('Retailer Id'),
            'sortable' => true,
            'width' => '60px',
            'index' => 'retailer_id'
        ));
 
        $this->addColumn('title', array(
            'header' => $this->__('Name'),
            'index' => 'title'
        ));
 
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('cms')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }
        
        $this->addColumn('position', array(
            'header' => $this->__('Position'),
            'index' => 'position'
        ));
        
        $this->addColumn('enabled', array(
            'header' => $this->__('Enabled'),
            'index' => 'is_enabled',
            'type'      => 'options',
            'options'   => array(
              1 => 'Yes',
              0 => 'No')
        ));
 
        // If they have permission to edit the guarantee
        if(Mage::getSingleton('admin/session')->isAllowed('registration/edit')) {
            $this->addColumn('action', array(
                'header' => $this->__('Action'),
                'width' => '100px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => $this->__('Edit Details'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'retailer_id',
                    ),
                ),
                'filter' => false,
                'sortable' => false,
            ));
        }
 
        return parent::_prepareColumns();
    }
    
    /**
     * Return URL where to send the user when he clicks on a row
     */
    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('retailer_id' => $row->getId()));
    }
 
    /**
     * Return Grid URL for AJAX query
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
    
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }
}