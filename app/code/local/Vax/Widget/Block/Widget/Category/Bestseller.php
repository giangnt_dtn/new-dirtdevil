<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Category_Bestseller
    extends Vax_Widget_Block_Product_List implements Mage_Widget_Block_Interface
{
    protected $_productBlocks = array();

    /**
     * Initializes bestsellers block.
     */
    protected function _construct()
    {
        $this->setData('current_category', Mage::getModel('catalog/category')->load($this->_getCategoryId()));
        $this->setTemplate('catalog/widget/category/bestsellers/' . $this->getStyle() . '.phtml');

        parent::_construct();
    }

    /**
     * Returns ID of the category selected in a widget.
     *
     * @return int
     */
    protected function _getCategoryId()
    {
        $categoryData = explode('/', $this->getCategoryId());
        if (sizeof($categoryData > 0)) {
            return (int)$categoryData[1];
        }
    }

    /**
     * Returns list of bestsellers for the current category.
     *
     * @return Vax_Catalog_Model_Resource_Product_Related_Collection
     */
    public function getProducts()
    {
        return $this->getCurrentCategory()->getRelatedProducts();
    }

    /**
     * Returns list of bestsellers for the current category.
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        return $this->getCurrentCategory()->getRelatedProductCollection();
    }

    /**
     * Generates block for single item.
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Block_Product_View
     */
    public function getProductBlock($product)
    {
        if(!$product instanceof Mage_Catalog_Model_Product) {
            // This is a instance of related product
            $product = Mage::getModel('catalog/product')->load($product->getProductId());
        }

        if (!isset($this->_productBlocks[$product->getId()])) {
            // resets the default block
            Mage::unregister('product');

            /** @var Mage_Catalog_Block_Product_View $block */
            $block = $this->getLayout()
                ->createBlock('catalog/product_view', 'bestseller_' . $product->getId(), array(
                    'product_id' => $product->getId()
                ));

            $block->addPriceBlockType($product->getTypeId(), 'catalog/product_price', 'catalog/widget/product/price.phtml');
            $block->setNameInLayout('vax_catalog_widget_bestseller_item');
            $block->setTemplate('catalog/widget/category/bestsellers/' . $this->getStyle() . '/item.phtml');
            $this->_productBlocks[$product->getId()] = $block;
        }

        return $this->_productBlocks[$product->getId()];
    }

    /**
     * Returns url link defined in widget.
     *
     * @return string
     */
    public function getLinkUrl()
    {
        return Mage::getUrl($this->getData('url'));
    }

    public function getBvRatingsHtml()
    {
        $bvRatings = $this->getLayout()->createBlock('vax_bazaarvoice/inline_ratings', 'inline_ratings');
        $bvRatings->setTemplate('bazaarvoice/inline/ratings.phtml');
        $bvRatings->setProductCollection($this->getProductCollection());
        $bvRatings->setProdids($bvRatings->getBvJsForCollection());
        return $bvRatings->toHtml();
    }
}