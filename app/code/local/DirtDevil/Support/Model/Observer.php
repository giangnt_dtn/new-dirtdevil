<?php
class DirtDevil_Support_Model_Observer
{
    /**
     * Applies the support handle
     * @param   Varien_Event_Observer $observer
     * @return  DirtDevil_Support_Model_Observer
     */
    public function addSupportHandle($observer)
    {
        $event = $observer->getEvent();
        $category = $event->getCategory();

        if(!$category) {
            return false;
        }

        if($this->isChildOfSupport($category)) {
            $update = $observer->getEvent()->getLayout();

            if($category->hasChildren()) {
                $update->addHandle('support_category_layout_update');
            } else {
                $update->addHandle('support_product_layout_update');
            }
        }

        return $this;
    }

    /**
     * Checks if a category is in the support categories
     *
     * @return bool
     */
    private function isChildOfSupport($category) {

        // Get UK support root category
        $support = Mage::getModel('catalog/category')->load(Mage::helper('dirtdevil_spares')->getSupportRootCategory());

        // Load Id's of the root categories children from UK store
        $all_child_categories_of_support = $support->getAllChildren(true);

        if(is_array($all_child_categories_of_support) && in_array($category->getId(), $all_child_categories_of_support)) {
            // Must be in 'support' section of website, so return true!
            return true;
        }

        return false;
    }
}