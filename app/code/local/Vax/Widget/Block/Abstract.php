<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Abstract extends Mage_Core_Block_Template
{
    /**
     * Returns URL of the given parameter. If not specified as valid http(s) protocol uses link to media folder.
     * @param $url
     * @return string
     */
    protected function _getMediaUrl($url) {
        $url = trim($url);

        if (strpos($url, 'http') === 0) {
            return $url;
        }

        return Mage::getUrl('media') . $url;
    }

    /**
     * Returns store URL for the given path.
     *
     * @param $url
     * @return string
     */
    public function getStoreUrl($url)
    {
        return Mage::getUrl($url);
    }

    public function getText()
    {
        return base64_decode(parent::getText());
    }
}
