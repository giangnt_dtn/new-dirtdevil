<?php

class Yoma_RealexExtended_System_ConfigController  extends Mage_Adminhtml_Controller_Action
{

    public function exportmatrixAction()
    {
        $website    = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getId();
        $fileInfo = Mage::getModel('realexExtended/export_csv')->createCSV($website);
        if (!strpos($fileInfo[0], '.csv')) {
            $fileInfo[0] = 'blank.csv';
        }
        $this->_prepareDownloadResponse($fileInfo[0], $fileInfo[1]);
    }

}