jQuery(document).ready(function($j){

    var category_url = "/category/index/index/";
    var product_url = "/product/index/userguides/";
    var question_url = "/product/index/question/";
    var related_url = "/product/index/related/";
    var productcode_url = "/product/index/code/";
    var download_url = "/product/index/download/";
    var subcategory_count = 0;
    var bodylocation = null;
    var activeStep = 1;

    // Store all ajax requests in a pool
    $j.xhrPool = [];
    $j.ajaxSetup({
        beforeSend: function(jqXHR) {
            $j.xhrPool.push(jqXHR);
        },
        complete: function(jqXHR) {
            var index = $j.xhrPool.indexOf(jqXHR);
            if (index > -1) {
                $j.xhrPool.splice(index, 1);
            }
        }
    });

    // Abort all outstanding ajax requests
    $j.xhrPool.abortAll = function() {
        $j(this).each(function(index, jqXHR) {
            jqXHR.abort();
            $j.xhrPool.splice(index, 1);
        });
    };

    get_location();

    function get_location() {
        var bodyclass = $j('body').attr('class');
        if(bodyclass.indexOf('registration')>=0) {
            bodylocation = "registration";
        }
        else {
            bodylocation = "support";
        }
    }
    $j('.select_category_link').click(function(e) {
        if ($j(this).parent().hasClass('inactive')) {
            return false;
        }
        activeStep = Math.max(activeStep, 2);
        setActiveStep();
        var category = $j(this).data('category');
        var clicked = $j(this);
        $j("#ajax-loading").show();
        var url = category_url + "id/" + category;
        $j.ajax({
            url: url,
            context: document.body,
            success: function(data, status) {
                var categories = $j.parseJSON(data);
                subcategory_count = categories.length;

                if ( ! subcategory_count) {
                    //move_step_2_to_3();
                    load_products(category);
                    return false;
                }

                var li_template = $j('#fault_subcategory_option li:first').clone(false);
                var ul_contents = '';

                $j.each(categories, function(index, category) {
                    li_template.find('.category_name').text(category.name);
                    li_template.find('a').removeClass('product-types-item').addClass('model-range-item').removeAttr('data-category').attr('data-category', category.id);
                    ul_contents += '<li>'+li_template.html()+'</li>';
                });
                $j('#fault_subcategory_option').html(ul_contents);
                $j('#fault_subcategory_option').show();

                $j('.select_model_link').each(function(){
                    if($j(this).hasClass('active')) $j(this).removeClass('active');
                    if($j(this).hasClass('selected')) $j(this).removeClass('selected');

                });
                $j('.select_category_link').each(function(){
                    if($j(this).hasClass('active')) $j(this).removeClass('active');
                    if($j(this).hasClass('selected')) $j(this).removeClass('selected');

                });
                clicked.parents('ul').children('li').each(function(){
                    $j(this).removeClass('active');
                    $j(this).removeClass('selected');
                });
                clicked.addClass('selected').parent().removeClass('inactive');

                $j('#categories-select .fault-breadcrumb li:eq(1)').removeClass('active');
                $j('#categories-select .fault-breadcrumb li:eq(3)').addClass('active');

                // setup click events on new links
                $j('.select_model_link').click(function(e) {
                    $j('.select_model_link').each(function(){
                        if($j(this).hasClass('active')) $j(this).removeClass('active');
                    });

                    load_products($j(this).attr('data-category'));
                    $j(this).addClass('selected');
                    activeStep = Math.max(activeStep, 3);
                    setActiveStep();
                });

                $j("#ajax-loading").hide();
            }
        });

        return false;
    });

    function setActiveStep()
    {
        for (i=1; i<= activeStep; i++) {
            $j('.step-' + i).addClass('menu-active');
        }
    }

    function load_products(range) {
        $j("#ajax-loading").show();
        var url = product_url + "id/" + range + "?location=" + bodylocation;

        $j.ajax({
            url: url,
            context: document.body,
            success: function(data, status) {
                var product_response = $j.parseJSON(data);

                //var output = get_product_list_html(product_response.products);
                var output = get_userguides(product_response);

                if(output != '') {
                    //$j('#categories_select').hide();
                    $j('#product_select_inner').html(output);
                    $j('#product_select').show();
                    $j("#ajax-loading").hide();

                } else {
                    $j("#ajax-loading").hide();
                    alert('Sorry, no products found.');
                }
            }
        });
    }

    $j('#product_select').on('click', 'ul li', function() {

        var id = $j(this).attr('data-id');
        if (id > 0) {

            $j('#fault_product_name').val($j(this).attr('data-name'));
            $j('#fault_product_code').val($j(this).attr('data-model-no'));
            $j('#product_select').hide();
            $j('#fault_entry').show();
        }
    });

    $j(window).on('delayed-resize', function (e, resizeEvent) {
        setActiveStep();
    });
});
