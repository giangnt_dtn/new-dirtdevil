<?php
class Vax_SupportPortal_Block_Html_Accordion extends Mage_Core_Block_Template
{
    protected $_steps = array();

    protected function _construct()
    {
        $this->setTemplate('contacts/html/accordion.phtml');
        return parent::_construct();
    }

    public function getId()
    {
        if (!$this->getData('id')) {
            $id = preg_replace('/[^a-z0-9_]/', '_', strtolower($this->getNameInLayout()));
            $this->setData('id', $id);
        }
        return $this->getData('id');
    }

    public function getSteps()
    {
        if (empty($this->_steps)) {
            foreach ($this->getSortedChildren() as $childBlock) {
                $block = $this->getChild($childBlock);
                if ($block instanceof Vax_SupportPortal_Block_Html_Accordion_Step) {
                    $this->_steps[$block->getNameInLayout()] = $block;
                }
            }
        }
        return $this->_steps;
    }

    public function getStep($key)
    {
        if (array_key_exists($key, $this->_steps)) {
            return $this->_steps[$key];
        }

        $block = $this->getChild($key);
        if (!$block || !$block instanceof Vax_SupportPortal_Block_Html_Accordion_Step) {
            return false;
        }
        return $block;
    }

    public function addStep($name, $title, $attributes = array())
    {
        $attributes['title'] = $title;
        $stepBlock = $this->getLayout()->createBlock('vax_supportportal/html_accordion_step', $name, $attributes);
        $siblingName = !empty($attributes['sibling_name']) ? $attributes['sibling_name'] : '';
        $after = !empty($attributes['after']) ? $attributes['after'] : '';
        $this->insert($stepBlock, $siblingName, $after);
        return $this;
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }
}