<?php
class Vax_LiveChat_Block_Support extends Vax_LiveChat_Block_General
{
    /*
     * Returns the helper in use
     * @return Mage_Core_Helper_Abstract
     */
    public function getTheHelper() {
        return Mage::helper('vax_livechat/support');
    }

    /**
     * Returns the product associated with the product support page
     *
     * @return Product
     */
    public function getProduct()
    {
        $category = null;
        $product = null;

        if(Mage::registry("current_category")) {
            $category = Mage::registry("current_category");
        }

        if($category && $category->getCategoryProductId()) {
            $product = Mage::getModel('catalog/product')->load($category->getCategoryProductId());

            return $product;
        }

        return false;
    }
}
