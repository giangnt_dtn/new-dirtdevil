<?php

/**
 * Store Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_Rest_Model_Core_Api2_Store_Rest_Admin_V1 extends Mage_Api2_Model_Resource
{
    /**
     * Retrieves all stores.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $storeCollection = Mage::getResourceModel('core/store_collection');
        $storeCollection->addRootCategoryIdAttribute();
        $stores          = $storeCollection->load();

        foreach ($stores as $store) {
            Mage::app()->setCurrentStore($store);
            $categories = Mage::helper('catalog/category')->getStoreCategories('name', true, true)->toArray();

            $activeCategories = array();

            foreach ($categories as $category) {
                if($category['is_active']) {
                    $activeCategories[] = array(
                        'entity_id' => $category['entity_id'],
                        'name'      => $category['name'],
                    );
                }
            }

            $store->setActiveCategories($activeCategories);
        }

        $stores = $stores->toArray();

        return $stores['items'];
    }
}