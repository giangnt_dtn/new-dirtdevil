/***********************************
 * PRODUCT PAGE UK
 /***********************************/
jQuery(document).ready(function () {
    //calculate how many thumbnails fit below the product image
    var galleryItems = galleryNumber();

    moreViewsThumbnails(galleryItems, 'ul.product-image-thumbs', 1);
    mobilePanels();
    youTube();

    var sparesList = '.nav-spares-and-accessories ul.mini-products-list';

    jQuery('.nav-spares-and-accessories .header-wrapper').on('click', function(){
        setTimeout(function(){
            jQuery('.nav-spares-and-accessories ul.mini-products-list li .product-details').setAllToMaxHeight();
        }, 500);
    });

    enquire.register('screen and (max-width: ' + 480 + 'px)', {
        match: function () {
            moreViewsThumbnails(2, sparesList, 1);
        }, unmatch: function () {
        }
    });
    enquire.register('screen and (max-width: ' + 769 + 'px)', {
        match: function () {
            jQuery('.product-view .short-description .product-shop').appendTo('.product-view .product-essential');

            jQuery('.short-description .text-wrapper').each(function() {
                if (jQuery(this).innerHeight() > 91) {
                    jQuery(this).find('.std').css('height', '86px');
                    jQuery(this).find('.more').show();
                }

                jQuery('.short-description .more').click(function () {
                    jQuery(this).parent('.text-wrapper').addClass('open');
                    jQuery('.short-description .text-wrapper .std').css('height', 'auto');
                });

                if (jQuery(this).hasClass('open')) {
                    jQuery('.short-description .more').hide();
                    jQuery('.short-description .text-wrapper .std').css('height', 'auto');
                }
            });
        },
        unmatch: function () {
            jQuery('.product-view .product-essential .product-shop').appendTo('.product-view .product-shop-wrapper');
            jQuery('.short-description .more').hide();
            jQuery('.short-description .text-wrapper .std').css('height', 'auto');
        }
    });
    enquire.register('screen and (min-width: ' + 481 + 'px) and (max-width: ' + 770 + 'px)', {
        match: function () {
            moreViewsThumbnails(4, sparesList, 1);
        }, unmatch: function () {
        }
    });
    enquire.register('screen and (min-width: ' + 771 + 'px) and (max-width: ' + 1000 + 'px)', {
        match: function () {
            //sticky nav functions
            stickyNav('.product-feature-nav', 130);
            colorSectionHeaders(172);
            autoScroll(130);

            //spares carousel, 3 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(3, sparesList, 1);
        }, unmatch: function () {
        }
    });
    enquire.register('screen and (min-width: ' + 1001 + 'px)', {
        match: function () {
            //sticky nav functions
            stickyNav('.product-feature-nav', 130);
            colorSectionHeaders(172);
            autoScroll(130);

            //spares carousel, 4 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(4, sparesList, 1);
            jQuery(sparesList).setAllToMaxHeight();
        }, unmatch: function () {
        }
    });
    var firstItemhHeight =  jQuery('.whats-in-the-box li:nth-child(2)').outerHeight() + jQuery('.whats-in-the-box li:nth-child(4)').outerHeight();
    jQuery('.whats-in-the-box li:first-child').css('height', firstItemhHeight);

});

jQuery(window).resize(function () {
    var firstItemhHeight =  jQuery('.whats-in-the-box li:nth-child(2)').outerHeight() + jQuery('.whats-in-the-box li:nth-child(4)').outerHeight();
    jQuery('.whats-in-the-box li:first-child').css('height', firstItemhHeight);
});

jQuery(window).load(function () {
    swipeImages('.product-image-gallery .gallery-image');
});

/* Colored section headers and sticky links - color once the user scrolls to them
 *
 * header offset is the size of the sticky nav*/
function colorSectionHeaders(headerOffset) {
    //'.nav-features .header-wrapper': 'a.features',
    //is the Features header -it is removed from here to make it colored by default. add it if you want Features to be initially grey
    var links = {
        '.nav-features .header-wrapper': 'a.features',
        '.nav-whats-in-the-box .header-wrapper': 'a.box',
        '.nav-specifications .header-wrapper': 'a.spec',
        '.nav-spares-and-accessories .header-wrapper': 'a.spares',
        '.nav-reviews .header-wrapper': 'a.reviews'
    };

    var firstHeader = Object.keys(links);
    var firstLink;
    var firstpanel;
    //the first features link should be active
    for (var i = 0; i < firstHeader.length; i++) {
        if (jQuery(firstHeader[i]).length) {

            firstpanel = firstHeader[i];
            jQuery(firstpanel).addClass('color');

            firstLink = links[firstHeader[i]];
            jQuery(firstLink).addClass('active');
            break;
        }
    }

    for (var section in links) {
        if (jQuery(window).width() > 770 && section != firstpanel) {
            colorThis(section, links[section],headerOffset);
        }
    }
    jQuery(window).scroll(function () {
        if (jQuery(window).width() > 770 && !jQuery(firstLink).hasClass('active') &&
            (jQuery(window).scrollTop() < (jQuery(window).height() + jQuery(firstpanel).outerHeight()))) {

            jQuery(firstLink).addClass('active');
        }

        var notFirst = '.product-feature-nav a:not(' + firstLink + ')';
        if (jQuery(notFirst).hasClass('active')) {
            jQuery(firstLink).removeClass('active');
        }

    });
}
