<?php

/**
 * Instant Cart extension, extended by Dirt Devil to edit the minicart items count
 * when the iCart adds a product to the basket and the user goes on shopping
 *
 */

class DirtDevil_MageWorx_InstantCart_Block_Wrapper extends MageWorx_InstantCart_Block_Wrapper {
    public function getTopLinkCart() {
        // compatibility with customer credit
        if (Mage::getStoreConfig('checkout/cart_link/use_qty')) {
            $count = Mage::getSingleton('checkout/cart')->getItemsQty();
        } else {
            $count = Mage::getSingleton('checkout/cart')->getItemsCount();
        }

        $translate = $this->__('Cart');
        if( $count > 0  ) {
            $text = Mage::helper('checkout')->__('
              <span class="icon"></span>
              <span class="label">'.$translate.'</span>
              <span class="count">%s</span>
            ', $count);
        } else {
            $text = Mage::helper('checkout')->__('
              <span class="icon"></span>
              <span class="label">'.$translate.'</span>
            ');
        }
        return $text;
    }
}
