<?php
class Yoma_RealexExtended_Helper_Data extends Mage_Core_Helper_Abstract {

    public static function updateStatus($session,$numRows) {
        if ($numRows<1) {
            $session->addError(Mage::helper('adminhtml')->__($numRows.' rows have been imported.'))  ;
        } else {
            $session->addSuccess(Mage::helper('adminhtml')->__($numRows.' rows have been imported.'));
        }
    }

    public function saveCSV($file, $fileName, $websiteId=NULL,$code=NULL) {

        $dir = Mage::getBaseDir('var'). DS . 'export' . DS;

        $fileName = 'REALEX_'.$code.'_' . $fileName;

        if (strpos($fileName, '.csv')) {
            $timestamp = md5(microtime());
            if (!is_null($websiteId)) {
                $fileName = str_replace('.csv', '', $fileName) . 'Id=' . $websiteId . '_' . $timestamp . '.csv';
            } else {
                $fileName = str_replace('.csv', '', $fileName) . $timestamp . '.csv';
            }
        }

        try {
            if(!is_dir($dir)) {
                if(!mkdir($dir)){
                }
            }
            if (!ctype_digit(file_put_contents($dir.$fileName, $file))) {
            }
        } catch (Exception $e) {

        }

    }

    public function encodeTssRuleSpecific($rules){

        $results = array();
        $rules =  explode(",", trim($rules));
        foreach($rules as $rule){
            array_push($results,explode(":", preg_replace('/\s+/', '',$rule)));
        }
        return json_encode($results);
    }

    public function encodeThreeDsecureResult($result){
        $results = array();
        $rules =  explode(",", trim($result));
        foreach($rules as $rule){
            array_push($results,explode(":", trim($rule)));
        }
        return json_encode($results);
    }

    public function unSerializeTssRuleSpecific($rules){

    }

    public function getCardType($payment){
        if($payment->getRealexTokenCcId()){
            $token = mage::getModel('realex/tokencard')->load($payment->getRealexTokenCcId());
            if($token->getId()){
                return $token->getCardType();
            }
        }

        if($payment->getCcType()) {
            return Mage::getModel('realex/realex_source_cards')->getGatewayCardType($payment->getCcType());
        }

        return '*';
    }

}