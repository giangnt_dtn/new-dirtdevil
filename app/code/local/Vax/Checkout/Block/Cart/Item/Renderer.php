<?php
/**
 * Shopping cart item render block
 *
 * @category    Vax
 * @package     Vax_Checkout
 * @author      Team Vax
 */
class Vax_Checkout_Block_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{
	protected $_crossSellProducts = false;

    /**
     * Function getCrossSellCollection
     *
     * returns a collection of cross sell products
     *
     * @return mixed
     */
    public function getCrossSellCollection()
    {
		if (!is_null($this->_crossSellProducts)) {
			return $this->_crossSellProducts;
		}

        $_product = Mage::getModel('catalog/product')->load($this->_item->getProductId());
        $this->_crossSellProducts = $_product->getCrossSellProducts();

		return $this->_crossSellProducts;
    }

    /*
     * Returns custom product attribute value to cart item renderer
     * where it's not available to quote collection via definition in config.xml
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeCode
     *
     * @return string
     *
     */
    public function getCustomAttributeValue($product, $attributeCode) {

        $attr = $product->getResource()->getAttribute($attributeCode);

        if ($attr && $attr->usesSource()) {
            $optionId = $product->getResource()
                ->getAttributeRawValue($product->getId(), $attributeCode, Mage::app()->getStore());

            return $attr->getSource()->getOptionText($optionId);
        }

        return $product->getResource()->getAttributeRawValue($product->getId(), $attributeCode, Mage::app()->getStore());
    }

    /**
     * Fix for nginx on port 80
     * @return mixed|string
     *
     * Get item delete url
     *
     * @return string
     */
    public function getDeleteUrl()
    {
        if ($this->hasDeleteUrl()) {
            return $this->getData('delete_url');
        }

        return $this->getUrl(
            'checkout/cart/delete',
            array(
                'id'=>$this->getItem()->getId(),
                'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->helper('core/url')->getEncodedUrl(Mage::getUrl('*/*'))
            )
        );
    }

}
