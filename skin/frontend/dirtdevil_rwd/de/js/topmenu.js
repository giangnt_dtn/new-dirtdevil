$j(document).ready(function () {
    navDropdown();
    mobileLanguageDropdown();
    footerDate();
});

function navDropdown() {
    enquire.register('screen and (min-width: ' + 771 + 'px)', {
        match: function () {
            navDropdownCall();
        },
        unmatch: function () {
            $j('.nav-spares').css('display', '');
            $j('.nav-spares div').css('display', '');

            $j('.nav-primary').css('display', '');
            $j('.nav-primary div').css('display', '');

            $j('.nav-newsletter').css('display', '');
            $j('.nav-newsletter div').css('display', '');

            $j('.nav-registration').css('display', '');
            $j('.nav-registration div').css('display', '');

            $j('.nav-contact-us').css('display', '');
            $j('.nav-contact-us div').css('display', '');

            $j('.nav-support').css('display', '');
            $j('.nav-support div').css('display', '');
        }
    });
}

function navDropdownCall() {
    $j(".nav-top li.products").on('mouseenter touchend click', function () {
        $j('.nav-products').show();
        $j('.nav-spares').hide();
        $j('.nav-registration').hide();
        $j('.nav-support').hide();
        $j('.nav-contact-us').hide();
        $j('.nav-newsletter').hide();

        // to do link to unveil enabled in admin
        //$j('img').unveil();
    });

    $j(".nav-top li.spares").on('mouseenter touchend click', function () {
        $j('.nav-products').hide();
        $j('.nav-spares').show();
        $j('.nav-registration').hide();
        $j('.nav-support').hide();
        $j('.nav-contact-us').hide();
        $j('.nav-newsletter').hide();

        // to do link to unveil enabled in admin
        //$j('img').unveil();
    });

    $j(".nav-top li.contact-us").on('mouseenter touchend click', function () {
        $j('.nav-contact-us').show();
        $j('.nav-spares').hide();
        $j('.nav-support').hide();
        $j('.nav-products').hide();
        $j('.nav-newsletter').hide();
        $j('.nav-registration').hide();
    });

    $j(".nav-top li.newsletter").on('mouseenter touchend click', function () {
        $j('.nav-newsletter').show();
        $j('.nav-spares').hide();
        $j('.nav-support').hide();
        $j('.nav-products').hide();
        $j('.nav-registration').hide();
        $j('.nav-contact-us').hide();

    });

    $j(".nav-top li.registration").on('mouseenter touchend click', function () {
        $j('.nav-registration').show();
        $j('.nav-support').hide();
        $j('.nav-products').hide();
        $j('.nav-contact-us').hide();
        $j('.nav-newsletter').hide();

    });

    $j(".nav-top li.support").on('mouseenter touchend click', function () {
        $j('.nav-support').show();
        $j('.nav-spares').hide();
        $j('.nav-registration').hide();
        $j('.nav-products').hide();
        $j('.nav-contact-us').hide();
        $j('.nav-newsletter').hide();

    });


    $j(".nav-support select").on("hover click mouseenter", function(){
        $j(".nav-support").addClass('selection-active');
    });

    $j(".nav-spares select").on("hover click mouseenter", function(){
        console.log('111');
        $j(".nav-spares").addClass('selection-active');
    });

    //desktop behavior
    if(! $j('.touch').length) {
        $j(".nav-top li.products").on('hover click mouseenter', function (e) {
            if($j( window ).width() > 770)
                $j('.nav-group-dropdown').hide();
                $j('.nav-primary').slideDown('fast');
        });
        $j(".nav-top li.spares").on('hover click mouseenter', function (e) {
            if($j( window ).width() > 770)
                $j('.nav-group-dropdown').hide();
                $j('.nav-primary').hide();
                $j('.nav-spares').slideDown('fast');
        });
        $j(".nav-top li.contact-us").on('hover click mouseenter', function (e) {
            if($j( window ).width() > 770)
                $j('.nav-group-dropdown').hide();
                $j('.nav-primary').hide();
                $j('.nav-contact-us').slideDown('fast');
        });
        $j(".nav-top li.newsletter").on('hover click mouseenter', function (e) {
            if($j( window ).width() > 770)
                $j('.nav-group-dropdown').hide();
                $j('.nav-primary').hide();
                $j('.nav-newsletter').slideDown('fast');
        });
        $j(".nav-top li.registration").on('hover click mouseenter', function (e) {
            if($j( window ).width() > 770)
                $j('.nav-group-dropdown').hide();
                $j('.nav-primary').hide();
                $j('.nav-registration').slideDown('fast');
        });
        $j(".nav-top li.support").on('hover click mouseenter', function (e) {
            if($j( window ).width() > 770)
                $j('.nav-group-dropdown').hide();
                $j('.nav-primary').hide();
                $j('.nav-support').slideDown('fast');
        });
        $j("#header-nav").mouseleave(function () {
            if($j( window ).width() > 770)
                $j('.nav-primary').slideUp('fast');
                $j('.nav-spares').slideUp('fast');
                $j('.nav-contact-us').slideUp('fast');
                $j('.nav-newsletter').slideUp('fast');
                $j('.nav-registration').slideUp('fast');
                if(!$j('.nav-support').hasClass('selection-active')) {
                    $j('.nav-support').slideUp('fast');
                }
                if(!$j('.nav-spares').hasClass('selection-active')) {
                    $j('.nav-spares').slideUp('fast');
                }
        });
    }

    //touchscreen behavior
    if($j('.touch').length && $j( window ).width() > 770) {
        // if the user taps on a link, the menu opens;
        // if the user taps again on that link, the menu closes
        // if the user taps on another link, the other link is open and the menu remains open
        var origin = null;

        $j(".nav-top li.products").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-primary').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-primary').slideUp('fast', function() {
                    $j('.nav-primary').css('display', '');
                });
                origin = null;
            }
        });

        $j(".nav-top li.spares").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-spares').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-spares').slideUp('fast', function() {
                    $j('.nav-spares').css('display', '');
                });
                origin = null;
            }
        });

        $j(".nav-top li.contact-us").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-contact-us').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-contact-us').slideUp('fast', function() {
                    $j('.nav-contact-us').css('display', '');
                });
                origin = null;
            }
        });

        $j(".nav-top li.newsletter").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-newsletter').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-newsletter').slideUp('fast', function() {
                    $j('.nav-newsletter').css('display', '');
                });
                origin = null;
            }
        });

        $j(".nav-top li.registration").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-registration').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-registration').slideUp('fast', function() {
                    $j('.nav-registration').css('display', '');
                });
                origin = null;
            }
        });

        $j(".nav-top li.support").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-support').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-support').slideUp('fast', function() {
                    $j('.nav-support').css('display', '');
                });
                origin = null;
            }
        });
    }
}

function mobileLanguageDropdown() {
    jQuery('body').on('click', 'a.change-language', function(e) {
        e.preventDefault();
        jQuery('.language-box').toggleClass('active');
    });
}


function footerDate() {
    var d = new Date();
    jQuery('.footer address.copyright').text('© ' + d.getFullYear() + ' Dirt Devil. All Rights Reserved.');
}