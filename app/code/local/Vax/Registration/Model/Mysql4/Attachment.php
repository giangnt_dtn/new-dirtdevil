<?php

/**
 * Attachment Mysql Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Mysql4_Attachment extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vax_registration/attachment', 'attachment_id');
    }

    /**
     * Load attachment by registration_id.
     *
     * @param mixed   $attachment
     * @param integer $registrationId
     *
     * @return \Vax_Registration_Model_Mysql4_Attachment
     */
    public function loadByRegistrationId($attachment, $registrationId)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $this->_getLoadSelect('registration_id', $registrationId, $attachment)->limit(1);
        $data    = $adapter->fetchRow($select);

        if ($data) {
            $attachment->setData($data);
        }

        $this->_afterLoad($attachment);

        return $this;
    }
}