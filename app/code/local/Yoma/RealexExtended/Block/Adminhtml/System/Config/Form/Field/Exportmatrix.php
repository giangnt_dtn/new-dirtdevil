<?php
class Yoma_RealexExtended_Block_Adminhtml_System_Config_Form_Field_Exportmatrix extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');
        $params = array(
            'website' => $buttonBlock->getRequest()->getParam('website'),
            'code' => 'chanel'
        );
        $url = Mage::getModel('adminhtml/url')->getUrl("*/*/exportmatrix", $params);
        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Export CSV')
            ->setOnClick("setLocation('$url')")
            ->toHtml();

        return $html;
    }

}