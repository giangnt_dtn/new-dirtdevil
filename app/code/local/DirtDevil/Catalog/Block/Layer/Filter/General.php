<?php
class DirtDevil_Catalog_Block_Layer_Filter_General extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('catalog/layer/filter.phtml');
    }

    public function setCategoryFilter()
    {
        $this->setRequestVar('cat');
        $this->setItems($this->getCategoryItems());
    }

    private function getCategoryItems()
    {
        $categoty   = $this->getLayer()->getCurrentCategory();
        /** @var $categoty Mage_Catalog_Model_Categeory */
        $categories = $categoty->getChildrenCategories();

        $this->getLayer()->getProductCollection()
            ->addCountToCategories($categories);

        $data = array();
        foreach ($categories as $category) {
            if ($category->getIsActive() && $category->getProductCount()) {
                $data[] = array(
                    'label' => Mage::helper('core')->htmlEscape($category->getName()),
                    'value' => $category->getId(),
                    'count' => $category->getProductCount(),
                );
            }
        }

        return $data;
    }
}
