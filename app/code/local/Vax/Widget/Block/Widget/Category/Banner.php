<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Category_Banner
    extends Vax_Widget_Block_Widget_Abstract implements Mage_Widget_Block_Interface
{
    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        $this->setTemplate('catalog/widget/category/banner.phtml');
        parent::_construct();
    }

    /**
     * Get URL of small image.
     * @return mixed
     */
    public function getSmallImageUrl()
    {
        return $this->_getMediaUrl($this->getSmallImage());
    }

    /**
     * Gets URL of large image.
     * @return mixed
     */
    public function getLargeImageUrl()
    {
        return $this->_getMediaUrl($this->getLargeImage());
    }
}
