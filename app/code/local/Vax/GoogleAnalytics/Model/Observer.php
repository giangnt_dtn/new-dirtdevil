<?php
class Vax_GoogleAnalytics_Model_Observer
{
    /**
     * Add order information into GA block to render on checkout success pages
     *
     * @param Varien_Event_Observer $observer
     */
    public function setGoogleAnalyticsOnOrderSuccessPageView(Varien_Event_Observer $observer)
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();

        if (empty($orderId)) {
            return;
        }

        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google.tag.manager');
        Mage::log(get_class($block));

        if ($block) {
            $block->setOrderId($orderId);
        }
    }
}
