/*
 * Update product stock using Ajax
 * Used on category pages only
 */
function updateStock(url, instockPattern, oosPattern) {
    if(isBot()) {
        return false;
    }
    var products = [];
    jQuery('.item').each(function() {
        var product = {};
        product.id = $j(this).data('product-id');
        product.stock = $j(this).data('product-stock');
        products.push(product);
    });
    jQuery.post(url, { products : products }, function(result) {
        if (result !== null) {
            var r = jQuery.parseJSON(result);
            if (r !== null) {
                jQuery.each(r, function(key, value) {
                    if(typeof value === "number" && value <= 0) {
                        buttonHtml = oosPattern.replace(/{{key}}/g, key).replace(/{{value}}/g, value);
                        jQuery('.item[data-product-id="'+key+'"]').find('.btn-cart').replaceWith(buttonHtml);
                    }
                    else if(typeof value === "string") {
                        buttonHtml = instockPattern.replace(/{{key}}/g, key).replace(/{{value}}/g, value);
                        jQuery('.item[data-product-id="'+key+'"]').find('.out-of-stock').replaceWith(buttonHtml);
                    };
                });
            }
        }
    });
}

/*
 * At a certain date/time swap over the special price
 */
function setFuturePrice(productId, fromHourMinute) {
    var date = new Date();
    var currentHour = date.getHours();
    var currentMinute = date.getMinutes();
    if (currentMinute < 10) {
        currentMinute = "0" + currentMinute;
    }
    var hourMinute = currentHour + ":" + currentMinute;
    var currentPrice = document.querySelector("#product-price-" + productId + "> .price");
    var currentPriceFuture = document.querySelector(".future-special-price-p-" + productId);
    var specialPrice = document.querySelector(".special-price-" + productId);
    var futurePrice = document.querySelector(".future-special-price-" + productId);

    if (hourMinute >= fromHourMinute && currentPriceFuture !== null && currentPrice !== null) {
        currentPriceFuture.style.display = "inline-block";
        currentPrice.innerHTML = 'Was: ' + currentPrice.innerHTML;
        currentPrice.style.textDecoration = "line-through";
    }

    else if (hourMinute >= fromHourMinute && specialPrice !== null && futurePrice !== null) {
        futurePrice.style.display = "inline";

        if(specialPrice) {
            specialPrice.style.display = "none";
        }
    };
}

/* Detect if the user-agent is a bot */
function isBot()
{
    var userAgent = navigator.userAgent.toLowerCase();

    var Browser = {
        Version: (userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
        Googlebot: /googlebot/.test(userAgent),
        Bing: /bingbot/.test(userAgent),
        Check: function() { alert(userAgent); }
    };

    if(Browser.Googlebot || Browser.Bing) {
        return true;
    }

    return false;
}
