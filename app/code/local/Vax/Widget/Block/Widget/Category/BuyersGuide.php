<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Category_BuyersGuide
    extends Vax_Widget_Block_Widget_Abstract implements Mage_Widget_Block_Interface {

    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        $this->setTemplate('catalog/widget/category/buyers_guide.phtml');
        parent::_construct();
    }

    /**
     * Gets image URL.
     * @return string
     */
    public function getImageUrl() {
        return $this->_getMediaUrl($this->getMainImage());
    }
}