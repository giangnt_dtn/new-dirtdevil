<?php
/**
 * Vax Registration Edit Tabs definition class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */

class Vax_Registration_Block_Adminhtml_Registration_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    protected function _construct() {
        parent::_construct();
        $this->setId('switch_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Registration Information'));
    }
 
    /**
     * Add tabs content
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Edit_Tabs
     */
    protected function _beforeToHtml() {
        $this->addTab('customer_section', array(
            'label' => $this->__('Customer'),
            'title' => $this->__('Customer'),
            'content' => $this->getLayout()
                    ->createBlock('vax_registration/adminhtml_registration_edit_tab_customer')
                    ->toHtml(),
        ));

        $this->addTab('product_section', array(
            'label' => $this->__('Product'),
            'title' => $this->__('Product'),
            'content' => $this->getLayout()
                    ->createBlock('vax_registration/adminhtml_registration_edit_tab_product')
                    ->toHtml(),
        ));
 
        return parent::_beforeToHtml();
    }
}