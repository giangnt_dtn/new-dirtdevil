<?php
class Yoma_RealexExtended_Adminhtml_Model_System_Config_Source_Profiles {

    protected $_options = array(

        array('value'=>1,'label'=>'Cardholder Not Enrolled'),
        array('value'=>2,'label'=>'Unable to Verify Enrolment'),
        array('value'=>3,'label'=>'Invalid response from Enrolment Server'),
        array('value'=>4,'label'=>'Enrolled but invalid response from ACS'),
        array('value'=>5,'label'=>'Successful authentication'),
        array('value'=>6,'label'=>'Issuing bank with Attempt server'),
        array('value'=>7,'label'=>'Incorrect password entered'),
        array('value'=>8,'label'=>'Authentication Unavailable'),
        array('value'=>9,'label'=>'Invalid response from ACS'),
        array('value'=>10,'label'=>'Timeout (no response sent)'),
    );

    public function toOptionArray($isMultiselect){

        $options = $this->_options;

        return $options;
    }


}
