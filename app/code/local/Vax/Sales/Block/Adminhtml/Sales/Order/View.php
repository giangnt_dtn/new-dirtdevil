<?php

/**
 * Sales Order View Block.
 */
class Vax_Sales_Block_Adminhtml_Sales_Order_View extends Magestore_Pdfinvoiceplus_Block_Adminhtml_Sales_Order_View
{
    /**
     * Added export button.
     *
     * @return void
     */
    public function  __construct()
    {
        parent::__construct();

        if ($this->getOrder()->getStatus() == 'pending') {
            return;
        }

        if($this->getOrder()->getExportedToGp() && !$this->getOrder()->getExtOrderId()) {
            $onclick = "confirmSetLocation('This order has already been exported to GP but no GP number has been assigned. Are you sure you wish to export this order to GP?.','{$this->getUrl('*/*/integrate')}')";
        }
        else if($this->getOrder()->getExtOrderId()) {
            $onclick = "confirmSetLocation('This order has already been exported to GP. Are you sure you wish to export this order again?','{$this->getUrl('*/*/integrate')}')";
        } else {
            $onclick = "confirmSetLocation('Are you sure you wish to export this order to GP?.','{$this->getUrl('*/*/integrate')}')";
        }

        $this->_addButton(
            'integrate',
            array(
                'label'   => Mage::helper('core')->__('Export To GP'),
                'onclick' => $onclick,
                'class'   => 'save'
            ),
            0,
            100,
            'header',
            'header'
        );
    }
}