<?php

/**
 * Attachment Mysql Collection Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Mysql4_Attachment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init('vax_registration/attachment');
    }
}