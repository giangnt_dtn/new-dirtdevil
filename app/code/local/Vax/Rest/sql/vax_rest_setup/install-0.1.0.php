<?php

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('sales_flat_order'), 'case_id', 'varchar(255)');
$installer->getConnection()->addColumn($installer->getTable('sales_flat_order_grid'), 'case_id', 'varchar(255)');

$installer->endSetup();