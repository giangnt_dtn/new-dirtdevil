<?php

/**
 * Layer Filter Attribute Model.
 *
 * @copyright Vax Ltd 2013
 */
class DirtDevil_Catalog_Model_Layer_Filter_Attribute extends Mage_Catalog_Model_Layer_Filter_Attribute
{
    /* denote what delimeter we are using for multiple filters */
    const MULTIPLE_FILTERS_DELIMITER = '_';

    protected $_itemsCount = null;

   /**
     * Apply attribute option filter to product collection
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Varien_Object $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Attribute
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock, $prepareCountQuery = false)
    {

        $filter = $request->getParam($this->_requestVar);
        if (is_array($filter)) {
            return $this;
        }

        if (empty($filter)) {
            return $this;
        }

        $multipleFilterOptions = explode(self::MULTIPLE_FILTERS_DELIMITER, $filter);
        $values = array();
        $text = '';
        foreach ($multipleFilterOptions as $filterOption) {
            if (!$filterOption) {
                continue;
            }
            $values[] = $filterOption;
            if (!$text) {
                $text = $this->_getOptionText($filterOption);
            }
        }
        if ($text && count($values)) {
            $this->_getResource()->applyFilterToCollection($this, $values);
            $this->getLayer()->getState()->addFilter($this->_createItem($text, $values));
            // don't reset items if an ajax request
            if(!Mage::app()->getRequest()->isAjax()) {
                $this->_items = array();
            }

        }

        return $this;
    }

    /**
     * Get data array for building attribute filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        $attribute = $this->getAttributeModel();
        $this->_requestVar = $attribute->getAttributeCode();

        $key = $this->getLayer()->getStateKey().'_'.$this->_requestVar;
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        if ($data === null) {
            $options = $attribute->getFrontend()->getSelectOptions();
            $optionsCount = $this->_getResource()->getCount($this);
            $optionsOrigCount = $this->_getResource()->getOrigCount($this);
            $data = array();
            foreach ($options as $option) {
                if (is_array($option['value'])) {
                    continue;
                }
                $origCount = isset($optionsOrigCount[$option['value']]) ? $optionsOrigCount[$option['value']] : 0;
                if (Mage::helper('core/string')->strlen($option['value'])) {
                    // Check filter type
                    if  ( $this->_getIsFilterableAttribute($attribute) == self::OPTIONS_ONLY_WITH_RESULTS ||
                        // for ajax response
                        ( $this->_getIsFilterableAttribute($attribute) == self::OPTIONS_ONLY_WITH_RESULTS && Mage::app()->getRequest()->isAjax() && !empty($optionsCount[$option['value']])) ) {
                        if (!empty($optionsCount[$option['value']])) {
                            $data[] = array(
                                'label' => $option['label'],
                                'value' => $option['value'],
                                'count' => $optionsCount[$option['value']],
                                'orig_count' => $origCount,
                            );
                        } elseif (Mage::app()->getRequest()->isAjax()) {
                            $data[] = array(
                                'label' => $option['label'],
                                'value' => $option['value'],
                                'count' => 0,
                                'orig_count' => $origCount
                            );
                        }
                    }
                    else {
                        $data[] = array(
                            'label' => $option['label'],
                            'value' => $option['value'],
                            'count' => isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0,
                            'orig_count' => $origCount
                        );
                    }
                }
            }

            // Count total number of items for entire filter
            $totalCount = 0;
            foreach ($data as $dataItem) {
                $totalCount += $dataItem['count'];
            }

            $this->_itemsCount = $totalCount;

            $tags = array(
                Mage_Eav_Model_Entity_Attribute::CACHE_TAG.':'.$attribute->getId()
            );

            $tags = $this->getLayer()->getStateTags($tags);
            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
        }
        return $data;
    }

    protected function _initItems()
    {
        $data = $this->_getItemsData();
        $items=array();
        foreach ($data as $itemData) {
            $_item = $this->_createItem(
                $itemData['label'],
                $itemData['value'],
                $itemData['count']);
            $_item->setOrigCount($itemData['orig_count']);
            $items[] = $_item;
        }
        $this->_items = $items;
        return $this;
    }

    /**
     * Overwrites parent class to return calculated items of selected fileter.
     *
     * @return int|null
     */
    public function getItemsCount() {
        if (null !== $this->_itemsCount && is_numeric($this->_itemsCount)) {
            return $this->_itemsCount;
        }

        return parent::getItemsCount();
    }
}