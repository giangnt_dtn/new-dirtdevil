<?php
class Vax_Prodfaqs_Block_TopQueries extends Mage_Core_Block_Template
{
	/**
	 * Either currently viewed product or category
	 * @var Mage_Catalog_Model_Abstract
	 */
	protected $_currentObject;
	
	protected function _construct()
	{
		$this->_initObject();
		return parent::_construct();
	}
	
	protected function _initObject()
	{
		$product = Mage::registry('current_product');
		if ($product && is_object($product) && $product instanceof Mage_Catalog_Model_Product) {
			$this->_currentObject = $product;
			return;
		}
		$category = Mage::registry('current_category');
		if ($category && is_object($category) && $category instanceof Mage_Catalog_Model_Category) {
			$this->_currentObject = $category;
			return;
		}
	}
	
	public function getCurrentObject()
	{
		return $this->_currentObject;
	}
	
	public function getCurrentObjectType()
	{
		if (!$this->getCurrentObject()) {
			return false;
		}
		
		if ($this->getCurrentObject() instanceof Mage_Catalog_Model_Product) {
			return 'product';
		}
		
		if ($this->getCurrentObject() instanceof Mage_Catalog_Model_Category) {
			return 'category';
		}
	}
	
	public function getTopQueries()
	{
        switch($this->getCurrentObjectType()) {
            case 'product':
                $data = Mage::getModel('prodfaqs/prodfaqs')->getTopicsByProduct($this->getCurrentObject()->getId());
                $faqIds = array();
                foreach ($data as $faq) {
                    $faqIds[] = $faq['faqs_id'];
                }
                $collection = Mage::getResourceModel('prodfaqs/prodfaqs_collection')->addFieldToFilter('main_table.faqs_id', $faqIds);
                $collection->addStatusFilter(1)
                    ->addFieldToFilter('visibility', 'public')
                    ->addFieldToFilter('question_type', 'product_question');
                $collection->join(array('topic' => 'prodfaqs/topic'), 'main_table.topic_id=topic.topic_id', array('topic_title' => 'topic.title'));

                $collection->setOrder('topic.topic_order', 'DESC');
                $collection->setOrder('main_table.faq_order', 'DESC');
                break;
            case 'category':
                $collection = Mage::getModel('prodfaqs/prodfaqs')->getCategoryFaqs($this->getCurrentObject());
                $collection->addStatusFilter(1)
                    ->addFieldToFilter('visibility', 'public')
                    ->addFieldToFilter('question_type', 'product_question');
                break;
            default:
                $collection = Mage::getResourceModel('prodfaqs/prodfaqs_collection');
                $collection->addStatusFilter(1)
                    ->addFieldToFilter('visibility', 'public')
                    ->addFieldToFilter('question_type', 'product_question');
                $collection->join(array('topic' => 'prodfaqs/topic'), 'main_table.topic_id=topic.topic_id', array('topic_title' => 'topic.title'));

                $collection->setOrder('topic.topic_order', 'DESC');
                $collection->setOrder('main_table.faq_order', 'DESC');
                break;
        }
        if ($this->getLimit()) {
            $collection->setPageSize((int)$this->getLimit());
        }
		return $collection;
	}

    public function getTopQueryTopics()
    {
        return $this->getTopQueries()->group('topic.topic_order');
    }

    public function getQueriesByTopic($limit = null)
    {
        $topics = array();
        $counter = 0;
        foreach ($this->getTopQueries() as $_query) {
            if (!array_key_exists($_query->getTopicTitle(), $topics)) {
                $topics[$_query->getTopicTitle()] = array();
                $counter++;
            }
            $topics[$_query->getTopicTitle()][] = $_query;
            if ($limit && $counter == $limit) {
                break;
            }
        }
        return $topics;
    }
}