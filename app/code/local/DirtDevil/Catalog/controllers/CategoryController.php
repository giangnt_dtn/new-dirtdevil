<?php
/**
 * Category controller
 *
 * @category   Vax
 * @package    Vax_Catalog
 * @author     Vax
 */
require_once 'Mage/Catalog/controllers/CategoryController.php';

class DirtDevil_Catalog_CategoryController extends Mage_Catalog_CategoryController
{
    /*
     * Check for ajax on param as we need to stop turpentine caching the normal non-ajax response
     */
    protected function isAjax() {

        if($this->getRequest()->isAjax() && $this->getRequest()->getParam('isAjax') == 1) {
            return true;
        }

        return false;
    }

    /**
     * Category view action - extend the default functionality to add Ajax option as a response
     */
    public function viewAction()
    {
        if ($category = $this->_initCatagory()) {

            $design = Mage::getSingleton('catalog/design');
            $settings = $design->getDesignSettings($category);

            // apply custom design
            if ($settings->getCustomDesign()) {
                $design->applyCustomDesign($settings->getCustomDesign());
            }

            Mage::getSingleton('catalog/session')->setLastViewedCategoryId($category->getId());

            $update = $this->getLayout()->getUpdate();
            $update->addHandle('default');

            if (!$category->hasChildren()) {
                $update->addHandle('catalog_category_layered_nochildren');
            }

            $this->addActionLayoutHandles();
            $update->addHandle($category->getLayoutUpdateHandle());
            $update->addHandle('CATEGORY_' . $category->getId());

            // apply custom ajax layout
            if ($this->isAjax() && Mage::getStoreConfig('catalog/ajax_category/enabled')) {
                $update->addHandle('catalog_category_ajax_view');
            }

            try {
                Mage::dispatchEvent(
                    'catalog_controller_category_view_after',
                    array(
                        'category' => $category,
                        'layout' => $update,
                        'controller_action' => $this
                    )
                );
            } catch (Mage_Core_Exception $e) {
                Mage::logException($e);
                return false;
            }

            $this->loadLayoutUpdates();

            // apply custom layout update once layout is loaded
            if ($layoutUpdates = $settings->getLayoutUpdates()) {
                if (is_array($layoutUpdates)) {
                    foreach($layoutUpdates as $layoutUpdate) {
                        $update->addUpdate($layoutUpdate);
                    }
                }
            }

            $this->generateLayoutXml()->generateLayoutBlocks();

            // apply custom layout (page) template once the blocks are generated
            if ($settings->getPageLayout()) {
                $this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
            }

            if ($root = $this->getLayout()->getBlock('root')) {
                $root->addBodyClass('categorypath-' . $category->getUrlPath())
                    ->addBodyClass('category-' . $category->getUrlKey());
            }

            $this->_initLayoutMessages('catalog/session');
            $this->_initLayoutMessages('checkout/session');

             // return json formatted response for ajax
            if ($this->isAjax() && Mage::getStoreConfig('catalog/ajax_category/enabled')) {

                // rebuild product listing
                $productList = $this->getLayout()->getBlock('product_list')->toHtml();

                // rebuild the layered nav as the options may have changed
                $layeredNav = null;

                if($this->getLayout()->getBlock('catalog.layerednav')) {
                    $layeredNav = $this->getLayout()->getBlock('catalog.layerednav')->toHtml();
                }

                // in rwd layered nav is now
                if($this->getLayout()->getBlock('catalog.leftnav')) {
                    $layeredNav = $this->getLayout()->getBlock('catalog.leftnav')->toHtml();
                }

                // Fix urls that contain '___SID=U'
                $urlModel = Mage::getSingleton('core/url');
                $productList = $urlModel->sessionUrlVar($productList);
                
                if($this->getLayout()->getBlock('catalog.layerednav') || $this->getLayout()->getBlock('catalog.leftnav')) {
                    $layeredNav = $urlModel->sessionUrlVar($layeredNav);
                }

                $productList = str_replace('{{form_key_esi_placeholder}}', '{{dd_form_key_placeholder}}', $productList);

                $response = array(
                    'productlist' => $productList,
                    'layerednav' => $layeredNav
                );

                $this->getResponse()->setHeader('Content-Type', 'application/json', true);
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            } else {
                $this->renderLayout();
            }
        }
        elseif (!$this->getResponse()->isRedirect()) {
            $this->_forward('noRoute');
        }
    }
}