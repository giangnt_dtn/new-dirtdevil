/***********************************
 * PRODUCT PAGE DE
 /***********************************/
jQuery(document).ready(function () {
    //calculate how many thumbnails fit below the product image
    var galleryItems = galleryNumber();

    moreViewsThumbnails(galleryItems, 'ul.product-image-thumbs', 1);
    mobilePanels();
    mobileReviewPanel();
    youTube();
    showSuitableFor();

    if (jQuery(window).outerWidth() > 770) {
        jQuery('.block-related .product-name').setAllToMaxHeight();
    }

    var sparesList = '.nav-spares-and-accessories ul.mini-products-list';

    jQuery('.nav-spares-and-accessories .header-wrapper').on('click', function(){
        setTimeout(function(){
            alignProductGridActions('.mini-products-list', 'li.product', '.block-related .product-details' );
            jQuery('.block-related .product-name').setAllToMaxHeight();
            jQuery('.view-all-link').css({'margin-top': 0});
        }, 500);
    });

    enquire.register('screen and (max-width: ' + 480 + 'px)', {
        match: function () {
            moreViewsThumbnails(2, sparesList, 1);
            alignProductGridActions('.mini-products-list', 'li.product', '.block-related .product-details' );

        }, unmatch: function () {
        }
    });
    enquire.register('screen and (min-width: ' + 481 + 'px) and (max-width: ' + 599 + 'px)', {
        match: function () {
            moreViewsThumbnails(3, sparesList, 1);
            alignProductGridActions('.mini-products-list', 'li.product', '.block-related .product-details' );

        }, unmatch: function () {
        }
    });
    enquire.register('screen and (min-width: ' + 600 + 'px) and (max-width: ' + 770 + 'px)', {
        match: function () {
            moreViewsThumbnails(4, sparesList, 1);
            alignProductGridActions('.mini-products-list', 'li.product', '.block-related .product-details' );

        }, unmatch: function () {
        }
    });
    enquire.register('screen and (min-width: ' + 771 + 'px) and (max-width: ' + 1000 + 'px)', {
        match: function () {
            //sticky nav functions
            stickyNav('.product-feature-nav', 150);
            colorSectionHeaders(240);
            autoScroll(-1);

            //spares carousel, 3 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(3, sparesList, 1);
            alignProductGridActions('.mini-products-list', 'li.product', '.block-related .product-details' );

        }, unmatch: function () {
        }
    });
    enquire.register('screen and (min-width: ' + 1001 + 'px)', {
        match: function () {
            //sticky nav functions
            stickyNav('.product-feature-nav',170);
            colorSectionHeaders(240);
            autoScroll(-1);

            //spares carousel, 4 items per page, 1st page visible initially amd on resize
            moreViewsThumbnails(4, sparesList, 1);
            alignProductGridActions('.mini-products-list', 'li.product', '.block-related .product-details' );

            jQuery(sparesList).setAllToMaxHeight();
        }, unmatch: function () {
        }
    });
});

jQuery(window).load(function () {
    swipeImages('.product-image-gallery .gallery-image');
});

/* Colored section headers and sticky links - color once the user scrolls to them
 *
 * header offset is the size of the sticky nav*/
function colorSectionHeaders(headerOffset) {
    //'.nav-features .header-wrapper': 'a.features',
    //is the Features header -it is removed from here to make it colored by default. add it if you want Features to be initially grey
    var links = {
        '.nav-spares-and-accessories .header-wrapper': 'a.spares',
        '.nav-features .header-wrapper': 'a.features',
        '.nav-whats-in-the-box .header-wrapper': 'a.box',
        '.nav-specifications .header-wrapper': 'a.spec',
        '.nav-downloads .header-wrapper': 'a.downloads',
        '.nav-reviews .header-wrapper': 'a.reviews',
        '.nav-faqs .header-wrapper': 'a.faqs'
    };

    var firstHeader = Object.keys(links);
    var firstLink;
    var firstpanel;
    //the first features link should be active
    for (var i = 0; i < firstHeader.length; i++) {
        if (jQuery(firstHeader[i]).length) {

            firstpanel = firstHeader[i];
            jQuery(firstpanel).addClass('color');

            firstLink = links[firstHeader[i]];
            jQuery(firstLink).addClass('active');
            break;
        }
    }

    for (var section in links) {
        if (jQuery(window).width() > 770 && section != firstpanel) {
            colorThis(section, links[section],headerOffset);
        }
    }
    jQuery(window).scroll(function () {
        if (jQuery(window).width() > 770 && !jQuery(firstLink).hasClass('active') &&
            (jQuery(window).scrollTop() < (jQuery(window).height() + jQuery(firstpanel).outerHeight()))) {

            jQuery(firstLink).addClass('active');
        }

        var notFirst = '.product-feature-nav a:not(' + firstLink + ')';
        if (jQuery(notFirst).hasClass('active')) {
            jQuery(firstLink).removeClass('active');
        }
    });
}

/**
 * Open Reviews section on mobile when the user clicks on the summary that links to in
 *
 */


//Features Sections accordion on mobile
function mobileReviewPanel() {
    var reviewContent = $j('.nav-menu .nav-reviews .content-wrapper');
    var reviewHeader = $j('.nav-menu .nav-reviews .header-wrapper');

    enquire.register('screen and (min-width: ' + 771 + 'px)', {
        match: function () {
            //re-enable the panels for desktop
            reviewHeader.show();
        },
        unmatch: function () {
            //remove "Features" header red color on mobile
            reviewHeader.removeClass('color');
            //collapse the panels after resize to mobile
            reviewContent.hide();
        }
    });

    $j('.rating-links').on('click touchend', function () {
        var panel = reviewHeader;
        var content = reviewContent;
        if ($j(window).width() < 771) {

            //color the section header when active
            $j(this).toggleClass('color');

            //expand the section when the user clicks on its header
            content.slideToggle('fast');
        }
        //remove the color if the user resizes the window
        enquire.register('screen and (min-width: ' + 771 + 'px)', {
            match: function () {
                $j(this).removeClass('color');
            },
            unmatch: function () {
            }
        });
    });
}

$j(document).ready(function() {
    if ($j(window).width() < 771 &&
        (document.location.href.indexOf('#reviews') != -1 || document.location.href.indexOf('#new-review') != -1)) {
        $j($j('.rating-links')[0]).trigger('click');
    }
});
