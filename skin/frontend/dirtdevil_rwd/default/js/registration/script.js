$j = jQuery.noConflict();

$j(function () {
    $j('#btn-submit-registration').click(function (event) {
        event.preventDefault();

        if (isValid()) {
            $j('#form-registration').submit();
        }
    });

    $j('.btn-search-address').on('click', function (event) {
        event.preventDefault();

        var postcode = $j('#postcode').val();

        if (_.isEmpty(postcode)) {
            return;
        }

        $j.ajax({
            url: '/postcode-lookup/index?postcode=' + postcode,
            success: function (data) {
                if (!_.isEmpty(data)) {
                    $j('.address-line-1').hide();
                    $j('.address-line-2').hide();
                    $j('.address-search').html(data).show();
                }
            }
        });
    });

    $j(document).on('click', '.btn-select-address', function (event) {
        event.preventDefault();

        var id = $j('#address_id').val();

        if (_.isEmpty(id)) {
            return;
        }

        $j.ajax({
            url: '/postcode-lookup/index/fetchAddress?address_id=' + id,
            success: function (data) {
                if (!_.isEmpty(data)) {
                    $j('#address').val(data.street_1);
                    $j('#address2').val(data.street_2);
                    $j('#town').val(data.city);
                    $j('#county').val(data.region);
                    $j('#postcode').val(data.postcode);

                    $j('.address-line-1').show();
                    $j('.address-line-2').show();
                    $j('.address-search').html('').hide();
                    isValid();
                }
            },
            dataType: 'json'
        });
    });

    var verimail = new Comfirm.AlphaMail.Verimail();
    var latestEmailSuggestion = '';
    var latestEmailStatus = 0;

    jQuery("input#email").verimail({
        messageElement: "p#status-message"
    });

    jQuery.validator.addMethod(
        'emailaddress',
        function (value, element, params) {
            verimail.verify(value, function (status, message, suggestion) {
                latestEmailSuggestion = message;
                latestEmailStatus = status;
            });

            return latestEmailStatus >= 0;
        },
        jQuery.validator.format(latestEmailSuggestion)
    );

    jQuery.validator.addMethod(
        'price',
        function (value, element, params) {
            return value.match(/^£?[\d.]+$/);
        },
        jQuery.validator.format('Please enter a correct price. e.g. 10.00')
    );


    jQuery.validator.addMethod(
        'ukphone',
        function(value, element, params) {
            var phoneNumber = jQuery.trim(value);
            return _.isObject(phoneNumber.match(/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/g));
        },
        jQuery.validator.format('Please enter a telephone number')
    );

    function isValid() {
        var validator = $j('#form-registration').validate({
            errorClass: 'help-inline',
            errorElement: 'span',
            ignore: ".ignore",
            rules: {
                email: {
                    emailaddress: true
                },
                pur_price: {
                    price: true
                },
            },
            showErrors: function (errorMap, errorList) {
                this.defaultShowErrors();

                _.each(errorList, function (error) {
                    $j(error.element).parents('.control-group').addClass('error');
                });
            },
            unhighlight: function (element, successClass) {
                $j(element).parents('.control-group').removeClass('error');
            }
        });

        validator.form();

        return validator.numberOfInvalids() === 0;
    }
});
