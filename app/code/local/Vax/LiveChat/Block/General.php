<?php
class Vax_LiveChat_Block_General extends Mage_Core_Block_Template
{
    /*
     * Returns the helper in use
     * @return Mage_Core_Helper_Abstract
     */
    public function getTheHelper() {
        return Mage::helper('vax_livechat/data');
    }

    /**
     * Returns the deployment ID.
     *
     * @return string
     */
    public function getDeploymentID()
    {
        return $this->getTheHelper()->getDeploymentID();
    }

    /**
     * Returns live chat button id
     *
     * @return string
     */
    public function getButtonId() {
        return "liveagent_button_online_" . $this->getTheHelper()->getDeploymentID();
    }
}
