<?php

class Yoma_RealexExtended_Model_Export_Csv extends Mage_Core_Model_Abstract
{
    const ENCLOSURE = '"';
    const DELIMITER = ',';
    protected $_fileDates = array();
    protected $_dateFileArray = array();
    protected $_theDataArray = array();

    /**
     * Create CSV file
     *
     * @param int $website
     * @return array
     */
    public function createCSV($website)
    {
        if (is_dir(Mage::getBaseDir('var') . DS . 'export' . DS)) {
            $directory = Mage::getBaseDir('var') . DS . 'export' . DS;
            $csvFiles = glob($directory . 'REALEX_*.csv');

            if (!$this->_findCsvFiles($csvFiles,$website)) {

            } else {
                $this->_findNoWebsiteCsvFiles($csvFiles);
            }

            if (!isset($this->_fileDates) || empty($this->_fileDates)) {
                return $this->_getNoCsvFilePresentArr();
            }

            $this->_findMostRecentCSV();

        } else {
            return $this->_getNoCsvFilePresentArr();
        }
        return $this->_theDataArray;
    }

    /**
     * Find saved csv files
     *
     * @param array $csvFiles
     * @param int $website
     * @return bool
     */
    protected function _findCsvFiles($csvFiles,$website) {
        if (empty($csvFiles)) {
            return false;
        }
        return $this->_findCsvFile($csvFiles,$website);
    }

    /**
     * Find single csv files
     *
     * @param $csvFiles
     * @param $website
     * @return bool
     */
    protected function _findCsvFile($csvFiles,$website) {

        $found = false;
        foreach ($csvFiles as $file) {
            $file = basename($file);
            $posOfId = strpos($file, 'Id=');
            $websiteId = substr($file, $posOfId+3, 1);

            // Get files for the current website config scope
            if ($website == $websiteId) {
                $this->timeSortSetup($file);
                $found = true;
            }

        }

        return $found;
    }


    /**
     * Loops through csv files and sets up $_dateFileArray & $_theDataArray
     *
     * @param $csvFiles Array of csv files in var/export
     */
    protected function _findNoWebsiteCsvFiles($csvFiles)
    {
        foreach ($csvFiles as $file) {
            $file = basename($file);
            $this->timeSortSetup($file);
        }
    }

    /**
     * Get most recent csv, read data and assign to $this->$theData
     *
     * @param $csvFiles Array of csv files in var/export.
     */
    protected function _findMostRecentCSV()
    {
        // Get file with the most recent timestamp
        array_multisort($this->_fileDates, SORT_DESC);
        $mostRecent = $this->_fileDates[0];
        $mostRecentCSV = $this->_dateFileArray[$mostRecent];
        $dir = Mage::getBaseDir('var') . DS . 'export' . DS . $mostRecentCSV;

        if (is_file($dir)) {
            $fp = fopen($dir, 'r');
            $theData = fread($fp, filesize($dir));
            fclose($fp);
        } else {
            $theData = $this->_noCSVPresent($dir);
            return $theData;
        }

        $this->_theDataArray = array($mostRecentCSV, $theData);
    }

    /**
     * Sets up $this->_fileDates & $this->_dateFileArray to be sorted
     *
     * @param $csvFiles Array of csv files in var/export.
     */
    protected function timeSortSetup($file)
    {
        $currentModified = filectime(Mage::getBaseDir('var') . DS . 'export' . DS. $file);
        $this->_fileDates[] = $currentModified;
        $this->_dateFileArray[$currentModified] = $file;
    }

    /**
     * Assigns blank CSV file to $this->_theDataArray and posts a log
     *
     * @param $dir Location of var/export.
     */
    protected function _getNoCsvFilePresentArr()
    {
        $dir = Mage::getBaseDir('var') . DS . 'export' . DS;
        Mage::helper('wsacommon/log')->postMajor('WSA Helper','No file found in var/export with the name:', $dir);
        return array('', 'blank');
    }

}