<?php
class Vax_Prodfaqs_Block_Adminhtml_Prodfaqs_Edit_Tab_Form extends FME_Prodfaqs_Block_Adminhtml_Prodfaqs_Edit_Tab_Form
{
	protected function _prepareForm() {
		parent::_prepareForm();
		$this->getForm()->getElement('question_type')->setValues(
			array(
			       array(
				     'value'	=> 'general_question',
				     'label'	=> Mage::helper('prodfaqs')->__('General Question'),
				     ),
			       array(
				    'value'	=> 'product_question',
				    'label'	=> Mage::helper('prodfaqs')->__('Product Question'),
				   ),
				   array(
				   	'value'	=> 'maintenance_item',
				   	'label'	=> Mage::helper('prodfaqs')->__('Maintenance Question'),
				   )
			)		
		);
		
        if ( Mage::getSingleton('adminhtml/session')->getFaqsData() )
        {
            $this->getForm()->setValues(Mage::getSingleton('adminhtml/session')->getFaqsData());
            Mage::getSingleton('adminhtml/session')->setFaqsData(null);
        } elseif ( Mage::registry('faqs_data') ) {
            $this->getForm()->setValues(Mage::registry('faqs_data')->getData());
        }
		
		return $this;
	}
}