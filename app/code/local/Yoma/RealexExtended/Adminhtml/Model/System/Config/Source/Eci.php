<?php
class Yoma_RealexExtended_Adminhtml_Model_System_Config_Source_Eci {

    protected $_options = array(

        array('value'=>'0','label'=>'0 Not Authenticated / Unavailable'),
        array('value'=>'1','label'=>'1 Not Enrolled / Attempt Acknowledged'),
        array('value'=>'2','label'=>'2 Fully Authenticated'),
        array('value'=>'5','label'=>'5 Fully Authenticated'),
        array('value'=>'6','label'=>'6 Not Enrolled / Attempt Acknowledged'),
        array('value'=>'7','label'=>'7 Not Authenticated / Unavailable'),
    );

    public function toOptionArray($isMultiselect){

        $options = $this->_options;

        return $options;
    }

}
