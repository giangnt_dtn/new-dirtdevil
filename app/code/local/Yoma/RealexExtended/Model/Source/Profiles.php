<?php

class Yoma_RealexExtended_Model_Source_Profiles {

    public function toOptionArray()
    {

        $website = Mage::app()->getRequest()->getParam('website');
        $websiteId = Mage::getModel('core/website')->load($website)->getId();

        return $this->_getProfiles($websiteId);
    }

    protected function _getProfiles($websiteId){

        $options = array();

        $profiles = mage::getModel('realexExtended/chanel')
            ->getCollection()
            ->addWebsiteFilter($websiteId)
            ->addDistinctProfileFilter()
        ->load();

        foreach($profiles as $profile){
            $options[] = array(
                'value' => $profile->getProfileName(),
                'label' => ucwords(str_replace('_',' ',$profile->getProfileName()))
            );
        }
        return $options;
    }

} 