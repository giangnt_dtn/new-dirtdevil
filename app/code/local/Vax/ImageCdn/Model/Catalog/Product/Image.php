<?php
/**
 * Extends various methods to use ImageCDN
 */
class Vax_ImageCdn_Model_Catalog_Product_Image extends OnePica_ImageCdn_Model_Catalog_Product_Image
{
    /**
    * Override default functionality to stop clearing the CDN cache on 'Flush Catalog Images Cache'
    *
    * @return string
    */
    public function clearCache()
    {
        $directory = Mage::getBaseDir('media') . DS.'catalog'.DS.'product'.DS.'cache'.DS;
        $io = new Varien_Io_File();
        $io->rmdir($directory, true);

        Mage::helper('core/file_storage_database')->deleteFolder($directory);
    }
}
