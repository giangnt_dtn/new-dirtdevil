<?php
class Vax_Prodfaqs_Block_Troubleshooting extends Vax_Prodfaqs_Block_Abstract
{
    protected $_currentStep;
    protected $_currentTopic;
    protected $_currentQuestion;

    protected function _construct() {
        $this->setTemplate('prodfaqs/troubleshooting.phtml');
        $this->_initFaqs();
        return parent::_construct();
    }

    protected function _initFaqs()
    {
        $this->_currentQuestion = (int)Mage::app()->getFrontController()->getRequest()->getParam('check', 0);
        $this->_currentTopic = (int)Mage::app()->getFrontController()->getRequest()->getParam('issue', 0);
        $this->_currentStep = 'issue';
        if ($this->_currentQuestion) {
            $this->_currentStep = 'action';
            $this->_currentTopic = Mage::getModel('prodfaqs/prodfaqs')->load($this->_currentQuestion)->getTopicId();
        } elseif ($this->_currentTopic) {
            $this->_currentStep = 'check';
        }
        return $this;
    }

    public function getProgressStep() {
        return $this->_currentStep;
    }

    public function getCurrentTopicId() {
        return $this->_currentTopic;
    }

    public function getCurrentQuestionId() {
        return $this->_currentQuestion;
    }

    public function getQuestions($publicOnly = true, $questionType = null)
    {
        return Mage::getModel('prodfaqs/prodfaqs')->getFaqsByTopic($this->getCurrentTopicId(), $publicOnly, $questionType, $this->getProduct()->getId());
    }

    public function getFaqAnswer($publicOnly, $questionType) {
        
        return $this->getQuestions($publicOnly, $questionType)->addFieldToFilter('main_table.faqs_id', $this->getCurrentQuestionId())->getFirstItem();
    }
}