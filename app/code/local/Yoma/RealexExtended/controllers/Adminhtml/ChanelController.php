<?php

class Yoma_RealexExtended_Adminhtml_ChanelController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("realexExtended/chanel")
            ->_addBreadcrumb(Mage::helper("adminhtml")->__("Transaction Rules Manager"), Mage::helper("adminhtml")->__("Transaction Rules Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("RealexExtended"));
        $this->_title($this->__("Manage Transaction Rules"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("RealexExtended"));
        $this->_title($this->__("Manage Transaction Rules"));
        $this->_title($this->__("Edit Rule"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("realexExtended/chanel")->load($id);
        if ($model->getId()) {
            Mage::register("chanel_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("realexExtended/chanel");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Manage Transaction Rules"), Mage::helper("adminhtml")->__("Manage Transaction Rules"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Rule Description"), Mage::helper("adminhtml")->__("Rule Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("realexExtended/adminhtml_chanel_edit"))->_addLeft($this->getLayout()->createBlock("realexExtended/adminhtml_chanel_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("realexExtended")->__("Rule does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("RealexExtended"));
        $this->_title($this->__("Chanel"));
        $this->_title($this->__("New Rule"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("realexExtended/chanel")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("chanel_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("realexExtended/chanel");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Chanel Manager"), Mage::helper("adminhtml")->__("Chanel Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Chanel Description"), Mage::helper("adminhtml")->__("Chanel Description"));


        $this->_addContent($this->getLayout()->createBlock("realexExtended/adminhtml_chanel_edit"))->_addLeft($this->getLayout()->createBlock("realexExtended/adminhtml_chanel_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {

            try {

                $post_data = $this->_prepareData($post_data);

                $model = Mage::getModel("realexExtended/chanel")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Rule was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setChanelData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setChanelData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("realexExtended/chanel");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("realexExtended/chanel");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'chanel.csv';
        $grid = $this->getLayout()->createBlock('realexextended/adminhtml_chanel_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'chanel.xml';
        $grid = $this->getLayout()->createBlock('realexextended/adminhtml_chanel_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    protected function _prepareData($data){

        $result = array();

        foreach($data as $key=>$value){
            switch($key){
                case 'three_d_secure_profile':
                    $result[$key] = Mage::getModel('realexExtended/secureProfile')->getSelected($value);
                    break;
                case 'avs_result':
                    $result[$key] = Mage::getModel('realexExtended/avs')->getSelected($value);
                    break;
                case 'cvn_result':
                    $result[$key] = Mage::getModel('realexExtended/cvn')->getSelected($value);
                    break;
                case 'tss_rule_specific':
                    $result[$key] = Mage::helper('realexExtended')->encodeTssRuleSpecific($value);
                    break;
                case 'three_d_secure_result':
                    $result[$key] = Mage::getModel('realexExtended/secure')->getSelected($value);
                    break;
                case 'profile_name':
                    $result[$key] = str_replace(' ','_',strtoupper($value));
                    break;
                default:
                    $result[$key] = $value;
            }
        }

        return $result;
    }
}
