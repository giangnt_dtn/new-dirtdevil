<?php

/**
 * Cron Observer.
 *
 * @copyright Vax Ltd
 */
class DirtDevilUk_Emails_Model_Observer_Cron
{
    /**
     * Executes the Order Email sending.
     *
     * @param mixed $event
     *
     * @return void
     */
    public function executeOrderEmailSending($event = null)
    {
        $storeId = Mage::app()->getStore()->getId();

        if (Mage::helper('dirtdeviluk_emails')->isLogging()) {
            Mage::log("executeOrderEmailSending()", null, "order_emails.log");
        }

        /* don't run the cron if sending order emails is enabled at checkout */
        if (Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            if (Mage::helper('dirtdeviluk_emails')->isLogging()) {
                Mage::log("executeOrderEmailSending() - skipping as order emails are being sent after placed", null, "order_emails.log");
            }

            return $this;
        }

        /* check if sending emails via cron is enabled */
        if (!Mage::helper('dirtdeviluk_emails')->isEnabled($storeId)) {
            return $this;
        }

        $forceEmails = false;

        if (!Mage::helper('dirtdeviluk_emails')->isForcingEmails($storeId)) {
            $forceEmails = true;
        }

        $orders = Mage::getModel('sales/order')->getCollection()
            ->addAttributeToFilter('email_sent', array('null' => true))
            ->addAttributeToFilter('state', array('eq' => Mage_Sales_Model_Order::STATE_PROCESSING));

        if(!$orders) {
            if (Mage::helper('dirtdeviluk_emails')->isLogging($storeId)) {
                Mage::log("executeOrderEmailSending() - no orders found", null, "order_emails.log");
            }

            return $this;
        }

        foreach($orders as $order) {
            if (Mage::helper('dirtdeviluk_emails')->isLogging($storeId)) {
                Mage::log("executeOrderEmailSending() - order Id " . $order->getId(), null, "order_emails.log");
            }

            $emails  = Mage::getModel('dirtdeviluk_emails/order_email')->load($order->getId());

            if($emails->getCanSendNewEmailFlag() != true) continue;

            $emails->queueNewOrderEmail($forceEmails);
        }

    }
}
