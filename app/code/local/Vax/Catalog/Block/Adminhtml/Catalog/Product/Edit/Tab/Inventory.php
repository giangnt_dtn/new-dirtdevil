<?php

/**
 * Product Inventory Tab Block.
 */
class Vax_Catalog_Block_Adminhtml_Catalog_Product_Edit_Tab_Inventory extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Inventory
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('vax/catalog/product/tab/inventory.phtml');
    }
}
