<?php

/**
 * Registration Index Controller.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Index Action.
     *
     * Displays the registration index.
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}
