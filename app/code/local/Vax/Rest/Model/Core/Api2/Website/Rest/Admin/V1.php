<?php

/**
 * Website Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_Rest_Model_Core_Api2_Website_Rest_Admin_V1 extends Mage_Api2_Model_Resource
{
    /**
     * Retrieves all websites.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $websiteCollection = Mage::getResourceModel('core/website_collection');
        $websites          = $websiteCollection->load();

        foreach ($websites as $website) {
            $websiteStores = array();

            foreach ($website->getStores() as $websiteStore) {
                if($websiteStore['is_active']) {
                    $websiteStores[] = array(
                        'entity_id' => $websiteStore['store_id'],
                        'code'      => $websiteStore['code'],
                        'name'      => $websiteStore['name'],
                    );
                }
            }

            $website->setActiveStores($websiteStores);
        }

        $websitesData = $websites->toArray();

        return $websitesData['items'];
    }
}