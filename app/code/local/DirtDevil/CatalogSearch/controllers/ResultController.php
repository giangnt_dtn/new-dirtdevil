<?php
/**
 * Vax Limited
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
 **/
require_once 'Mage/CatalogSearch/controllers/ResultController.php';
class DirtDevil_CatalogSearch_ResultController extends Mage_CatalogSearch_ResultController
{
    /*
    * Check for ajax on param as we need to stop turpentine caching the normal non-ajax response
    */
    protected function isAjax() {
        if($this->getRequest()->isAjax() && $this->getRequest()->getParam('isAjax') == 1) {
            return true;
        }
        return false;
    }
    /**
     * Display search result
     */
    public function indexAction()
    {
        $query = Mage::helper('catalogsearch')->getQuery();
        /* @var $query Mage_CatalogSearch_Model_Query */
        $query->setStoreId(Mage::app()->getStore()->getId());
        if ($query->getQueryText() != '') {
            if (Mage::helper('catalogsearch')->isMinQueryLength()) {
                $query->setId(0)
                    ->setIsActive(1)
                    ->setIsProcessed(1);
            }
            else {
                if ($query->getId()) {
                    $query->setPopularity($query->getPopularity()+1);
                }
                else {
                    $query->setPopularity(1);
                }
                if ($query->getRedirect()){
                    $query->save();
                    $this->getResponse()->setRedirect($query->getRedirect());
                    return;
                }
                else {
                    $query->prepare();
                }
            }
            Mage::helper('catalogsearch')->checkNotes();
            $this->loadLayout();
            $this->_initLayoutMessages('catalog/session');
            $this->_initLayoutMessages('checkout/session');
            // return json formatted response for ajax
            if ($this->isAjax() && Mage::getStoreConfig('catalog/ajax_category/enabled')) {
                // rebuild product listing
                $productList = $this->getLayout()->getBlock('search_result_list')->toHtml();
                // rebuild the layered nav as the options may have changed
                $layeredNav = null;
                if($this->getLayout()->getBlock('catalogsearch.layerednav')) {
                    $layeredNav = $this->getLayout()->getBlock('catalogsearch.layerednav');
                }
                // in rwd layered nav is now
                if($this->getLayout()->getBlock('catalogsearch.leftnav')) {
                    $layeredNav = $this->getLayout()->getBlock('catalogsearch.leftnav');
                }

                $layeredNav = $layeredNav->toHtml();

                // Fix urls that contain '___SID=U'
                $urlModel = Mage::getSingleton('core/url');
                $productList = $urlModel->sessionUrlVar($productList);
                if($this->getLayout()->getBlock('catalogsearch.layerednav') || $this->getLayout()->getBlock('catalogsearch.leftnav')) {
                    $layeredNav = $urlModel->sessionUrlVar($layeredNav);
                }
                $productList = str_replace('{{form_key_esi_placeholder}}', '{{dd_form_key_placeholder}}', $productList);
                $response = array(
                    'productlist' => $productList,
                    'layerednav' => $layeredNav
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json', true);
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            } else {
                $this->renderLayout();
            }
            if (!Mage::helper('catalogsearch')->isMinQueryLength()) {
                $query->save();
            }
        }
        else {
            $this->_redirectReferer();
        }
    }
}
