<?php

/**
 * Checkout Service Interface.
 *
 * @copyright Vax Ltd
 */
interface Vax_Checkout_Model_Interface_Service_Checkout
{
    /**
     * Resets the current checkout.
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @return Vax_Checkout_Model_Response_Opc
     */
    public function submit(Mage_Sales_Model_Order $order);

    /**
     * Checks whether a successful order exists.
     *
     * @return boolean
     */
    public function hasProcessedOrder();

    /**
     * Checks whether a failed order exists.
     *
     * @return boolean
     */
    public function hasFailedOrder();

    /**
     * Resets the current checkout.
     *
     * @return Vax_Checkout_Model_Interface_Service_Checkout
     */
    public function reset();
}