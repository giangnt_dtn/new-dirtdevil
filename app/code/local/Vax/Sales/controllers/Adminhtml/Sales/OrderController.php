<?php

include_once Mage::getModuleDir('controllers', 'Mage_Adminhtml').DS.'Sales'.DS.'OrderController.php';

/**
 * Sales Order View Controller.
 */
class Vax_Sales_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    /**
     * Exports an order to GP.
     *
     * @return void
     */
    public function integrateAction()
    {
        try
        {
            $order = Mage::getModel('sales/order')->load($this->getRequest()->getParam('order_id'));

            /*if ($order->getStatus() == 'pending') {
                return;
            }*/

            $adminUser = $this->getAdminUsername();
            $message = "Exported To GP";

            if($adminUser) {
                $message = 'Manually Exporting order to GP by ' . $adminUser;
            }

            $order->setState(
                'new',
                'pending',
                $message,
                false
            );

            $order->save();

            $this->_getSession()->addSuccess(
                $this->__('Exported order to GP')
            );
        }
        catch (Exception $ex)
        {
            $this->_getSession()->addError(
                $this->__('Error exporting order to GP: ').$ex->getMessage()
            );
        }

        Mage::app()->getResponse()->setRedirect(
            Mage::helper('adminhtml')->getUrl(
                "adminhtml/sales_order/view",
                array(
                    'order_id' => $order->getId()
                )
            )
        );
    }

    /**
     * Return admin user name.
     *
     * @return string
     */
    public function getAdminUsername()
    {
        if(!Mage::getSingleton('admin/session')->getUser()) {
            return false;
        }

        if (Mage::getSingleton('admin/session')->getUser()->getId()) {
            return Mage::getSingleton('admin/session')->getUser()->getFirstname()
            . " "
            . Mage::getSingleton('admin/session')->getUser()->getLastname();
        }
    }
}
