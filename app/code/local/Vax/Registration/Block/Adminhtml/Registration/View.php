<?php

/**
 * Registration View Adminhtml Block.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Block_Adminhtml_Registration_View extends Mage_Core_Block_Template
{
    /**
     * Constructor.
     *
     * @copyright Vax Ltd
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('vax/registration/view.phtml');
    }

    /**
     * Returns the registration.
     *
     * @return mixed
     */
    public function getRegistration()
    {
        return Mage::registry('current_registration');
    }

    /**
     * Returns the registration product.
     *
     * @return Mage_Catalog_Product|null
     */
    public function getRegistrationProduct()
    {
        $sku = $this->getRegistration()->getModel();

        return Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaderText()
    {
        return $this->__('Guarantee Registration (%d)', $this->htmlEscape($this->getRegistration()->getId()));
    }
}
