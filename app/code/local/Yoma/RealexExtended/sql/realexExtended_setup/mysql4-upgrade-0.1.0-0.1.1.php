<?php
$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('realexExtended/chanel')}` ADD COLUMN `order_review` int(11) AFTER `three_d_secure_result`;
");
$installer->endSetup();