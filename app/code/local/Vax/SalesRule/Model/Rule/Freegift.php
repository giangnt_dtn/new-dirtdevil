<?php

class Vax_SalesRule_Model_Rule_Freegift extends Mage_SalesRule_Model_Rule
{
    public function getActionsInstance()
    {
        return Mage::getModel('vax_salesrule/rule_condition_freegift_combine');
    }

    public function loadPost(array $data)
    {
        parent::loadPost($data);

        $this->setData('is_free_gift_promotion', 1);
        return $this;
    }

    protected function _afterSave()
    {
        parent::_afterSave();

        if ($this->getActions()) {
            Mage::getResourceModel('vax_salesrule/freegift')->saveFreeGifts($this->getId(), $this->getActions()->asArray());
        }

        return $this;
    }
}