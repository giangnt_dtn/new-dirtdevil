// used to calculate how many subpages are needed for the spares & accessories slider,
// given the name of the slider's outer box, and the width of a single list element in pixels
function pageWidth(outerBox, liWidth) {
    outerBox = jQuery(outerBox);
    if (outerBox.length) {
        var pageNumber = outerBox.width() / liWidth;
        return Math.ceil(pageNumber);
    } else {
        return 1;
    }
}

//video overlay script
function youTube() {
    var overlayImg, youtubeBox;
    jQuery(".youtube").each(function () {
        youtubeBox = jQuery(this);
        var iframe = youtubeBox.find('iframe');
        iframe.hide();

        if (jQuery(window).width() < 768) {
            overlayImg = youtubeBox.find("img.mobile");
        } else {
            overlayImg = youtubeBox.find("img.desktop");
        }

        youtubeBox.append('<div class="play"></div>');

        //get the embedded video's default source and edit it with the needed parameters
        var youtubeSrc = iframe.attr('src');

        var customLink = youtubeSrc.slice(0, youtubeSrc.indexOf("?"));
        customLink += '?';
        //play on click
        customLink += '&autoplay=1';
        //show heading
        customLink += '&showinfo=1';
        //autohide
        customLink += '&autohide=0';
        //remove border
        customLink += '&border=1';
        //hide related videos
        customLink += '&rel=0';
        //fix flash error
        customLink += '&html5=1';

        //when you click on the overlay image, it gets hidden and the youtube iframe gets shown
        youtubeBox.click(function () {
            overlayImg.hide();
            jQuery(this).find('.play').hide();

            iframe.show();
            iframe.attr('src', customLink);
        });
    });
}

// equalize spares boxes height - works only on boxes that are already shown when it is called
// used for spares slider on desktop
jQuery.fn.setAllToMaxHeight = function () {
    return this.height(Math.max.apply(this, jQuery.map(this, function (e) {
        return jQuery(e).height()
    })));
};

// carousel for displaying a number of images from a list
// used for product image gallery and spares slider
// takes the number of items per page, a reference to the list that contains the items and a way to reset to page 1 on resize
function moreViewsThumbnails(thumbsPerPage, list, currentPage) {
    if (thumbsPerPage === undefined) {
        thumbsPerPage = 5;
    }

    var listli = list + ' li';

    if (jQuery(list).length) {
        list = jQuery(list);
    } else {
        return false;
    }

    //round up the number of thumbnail pages in groups of 5
    // 1.6 pages is rounded to 2 pages
    var thumbs = jQuery(listli).length;
    var pages = parseInt(Math.ceil(thumbs / thumbsPerPage));
    if (currentPage == undefined) currentPage = 1;
    list.css("margin-left", 0);

    list.prev().addClass('nomore');
    list.next().addClass('nomore');
    if (thumbs > thumbsPerPage) {
        //if there are further pages the arrows are active
        list.prev().addClass('nomore');
        if (list.next().hasClass('nomore'))jQuery(list).next().removeClass('nomore');

        var margin = 0;

        list.on('movestart', function(e) {
            // If the movestart is heading off in an upwards or downwards
            // direction, prevent it so that the browser scrolls normally.
            if ((e.distX > e.distY && e.distX < -e.distY) ||
                (e.distX < e.distY && e.distX > -e.distY)) {
                e.preventDefault();
            }
        });

        list.parent().on('swipeleft', function (e) {
            GoRight();
        });
        list.parent().on('swiperight', function (e) {
            GoLeft();
        });

        list.next().click(function () {
            GoRight();
        });
        list.prev().click(function () {
            GoLeft();
        });


        function GoRight() {
            if (currentPage < pages) {
                list.animate({"margin-left": (margin - 100) + '%'}, 200);
                margin -= 100;

                currentPage++;
                if (list.prev().hasClass('nomore'))list.prev().removeClass('nomore');
                if (currentPage == pages) {
                    list.next().addClass('nomore');
                }
            }
        }

        function GoLeft() {
            if (currentPage > 1) {
                list.animate({"margin-left": (margin + 100) + '%'}, 200);
                margin += 100;

                currentPage--;
                if (list.next().hasClass('nomore'))list.next().removeClass('nomore');
                if (currentPage == 1) {
                    list.prev().addClass('nomore');
                }
            }
        }
    }
}

// move between gallery images using swipes
function swipeImages(image) {
    image = jQuery(image);
    if (image.length) {
        var imageNumber = -1;
        image.on('movestart', function(e) {
            // If the movestart is heading off in an upwards or downwards
            // direction, prevent it so that the browser scrolls normally.
            if ((e.distX > e.distY && e.distX < -e.distY) ||
                (e.distX < e.distY && e.distX > -e.distY)) {
                e.preventDefault();
            }

            imageNumber = $j(this).attr('id');

            // "image-main" is the first image and it is -1 because the rest of the images start from 0
            if (imageNumber == "image-main") imageNumber = -1;

            // the image id is like "image-2" - we need the number to see which image it is
            else imageNumber = parseInt(imageNumber.slice(-1));

            //just in case an image doesn't have an id with a number
            if (imageNumber == undefined) imageNumber = -1;
        });

        image.on('swipeleft', function (e) {
            GoRight(imageNumber);
        });

        image.on('swiperight', function (e) {
            GoLeft(imageNumber);
        });
    }

    var idString = "#image-";

    function GoRight(current) {
        // find the current image id and find the image object with it
        var currentRef = idString + current;
        if (current == -1) {
            currentRef = '#image-main';
        }

        var currentImg = $j(currentRef);

        // hide it on swipe if it is not the last image
        if (currentImg.hasClass('visible') && $j("#image-" + (current + 1)).length) {

            currentImg.removeClass('visible');
            //keep the int
            var right = current + 1;
            //show the next image if it exists
            var nextRef = idString + right;
            if ($j(nextRef).length) {
                $j(nextRef).addClass('visible');
                $j(nextRef).unveil();
            }
        }
    }

    function GoLeft(current) {
        // find the current image id and find the image object with it
        var currentRef = idString + current;
        if (current == -1) {
            currentRef = '#image-main';
        }

        var currentImg = $j(currentRef);

        //hide it on swipe if it is not the last one
        if (currentImg.hasClass('visible') && current != -1) {
            currentImg.removeClass('visible');

            // keep the int
            var left = current - 1;
            // check for "#image-main"
            if (current == 0) {
                left = "main";
            }

            //show the previous image if it exists
            var prevRef = idString + left;
            if ($j(prevRef).length) {
                $j(prevRef).addClass('visible');
                $j(prevRef).unveil();
            }
        }
    }
}

//number of visible gallery images for the current product
function galleryNumber() {
    if (jQuery('.product-img-box').length && jQuery('.more-views').length) {
        var containerWidth = jQuery('.product-img-box').width();
        var arrowContainer = jQuery('.more-views');

        var arrowWidth = arrowContainer.css('padding-left');
        arrowWidth = parseInt(arrowWidth.replace("px", ""));

        var thumbWidth = 60;

        //number of visible thumbnails depending on the screen width;
        var number = (containerWidth - (2 * arrowWidth)) / thumbWidth;
        number = Math.floor(number);
        return number;
    } else {
        return 1;
    }
}

/* Sticky Features Sections Nav
 * uses the selector of the sticky element and the height of the sticky nav above it */
function stickyNav(stickyName, headerOffset) {
    var StickyName = jQuery(stickyName);
    if (StickyName.length) {
        var stickyOffset = StickyName.offset().top - headerOffset;

        jQuery(window).scroll(function () {
            var sticky = StickyName,
                scroll = jQuery(window).scrollTop();

            if (scroll >= stickyOffset) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    }
}

/* colors the active Section header and the nav link to it when the user scrolls
*
* header offset is the size of the sticky nav*/
function colorThis(header, link, headerOffset) {
    var colorMe = jQuery(header);
    var colorLink = jQuery(link);

    if (colorMe.length && link.length && jQuery(window).width() > 771) {
        jQuery(window).scroll(function () {
            var colorOffset = colorMe.offset().top;
            if (jQuery(window).width() > 771) {
                var toBeColored = colorMe;
                var scroll = jQuery(window).scrollTop();

                //activate the color a little before the scroller reaches it - account for menu height
                scroll += headerOffset;

                if (scroll >= colorOffset) {
                    //remove other active links
                    jQuery('#nav-wrap a.active').removeClass('active');

                    //add header color
                    toBeColored.addClass('color');

                    //add link color
                    colorLink.addClass('active');
                } else {
                    toBeColored.removeClass('color');
                    colorLink.removeClass('active');
                }
            }
        });
    } else {
        return;
    }
}

//autoscrolling to internal page links
function autoScroll(headerOffset) {
    jQuery('a.top').click(function () {
        jQuery('html,body').animate({scrollTop: 0}, '500', 'swing');
    });
    jQuery(' a[href^="#"]').on('click', function (e) {
        if (!jQuery('a.skip-link-close').is(e.target)) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name="' + this.hash.slice(1) + '"]');

            if (target.length) {
                jQuery('html,body').animate({
                    scrollTop: (target.offset().top - headerOffset)
                }, 200);
                return false;
            }
        }
    });
}

//Features Sections accordion on mobile
function mobilePanels() {
    var allContent = $j('.nav-menu .panel .content-wrapper');
    var allHeaders = $j('.nav-menu .panel .header-wrapper');

    enquire.register('screen and (min-width: ' + 771 + 'px)', {
        match: function () {
            //re-enable the panels for desktop
            allContent.show();
        },
        unmatch: function () {
            //remove "Features" header red color on mobile
            allHeaders.removeClass('color');
            //collapse the panels after resize to mobile
            allContent.hide();
        }
    });

    $j('.nav-menu .panel .header-wrapper').click(function () {
        var panel = $j(this).parent();
        var content = panel.find('.content-wrapper');
        if ($j(window).width() < 771) {

            //color the section header when active
            $j(this).toggleClass('color');

            //expand the section when the user clicks on its header
            content.slideToggle('fast');
            var firstItemhHeight_mob =  jQuery('.whats-in-the-box li:nth-child(2)').outerHeight() + jQuery('.whats-in-the-box li:nth-child(3)').outerHeight() - 2;
            jQuery('.whats-in-the-box li:first-child').css('height', firstItemhHeight_mob);
        }
        //remove the color if the user resizes the window
        enquire.register('screen and (min-width: ' + 771 + 'px)', {
            match: function () {
                $j(this).removeClass('color');
            },
            unmatch: function () {
            }
        });
    });
}
