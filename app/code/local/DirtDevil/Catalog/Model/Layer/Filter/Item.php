<?php
/**
 * Filter item model
 *
 * @category    Vax
 * @package     Vax_Catalog
 * @author      Vax
 */
class DirtDevil_Catalog_Model_Layer_Filter_Item extends Mage_Catalog_Model_Layer_Filter_Item
{
    /**
     * Check if current filter is selected (for checkboxes)
     *
     * @return boolean
     */
    public function isSelected()
    {
        $selected = explode("_", Mage::getSingleton('core/app')->getRequest()->getParam($this->getFilter()->getRequestVar()));
        if(in_array($this->getValue(), $selected))
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get filter item url
     *
     * @return string
     */
    public function getUrl()
    {
        if (!Mage::helper('catalog/category')->useAjaxCheckboxes() && !Mage::helper('catalog/category')->useAjaxPriceSlider()) {
            return parent::getUrl();
        }

        /* current filter that are we looping through  */
        /* $this->getFilter()->getRequestVar() = name of filter e.g. cat */
        /* $param = filter value, e.g. category 20 */
        $exploded_filter_values = null;

        if(Mage::getSingleton('core/app')->getRequest()->getParam($this->getFilter()->getRequestVar())) {
            $exploded_filter_values = explode("_", Mage::getSingleton('core/app')->getRequest()->getParam($this->getFilter()->getRequestVar()));
        }

        if($exploded_filter_values && count($exploded_filter_values) > 0) {
            $tmp = array_merge($exploded_filter_values, array($this->getValue()));

            foreach ($tmp as &$val) {
                if (!$val) {
                    unset($val);
                }
            }

        } else {
            $tmp = $this->getValue();
        }

        if(count($tmp) > 1) {
            $values = implode('_', $tmp);
        } else {
            $values = $tmp;
        }

        $query = array(
            $this->getFilter()->getRequestVar()=>$values,
            Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null // exclude current page from urls
        );

        return Mage::getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true, '_query'=>$query));
    }

    /**
     * Removes filter value from URL (in case of filters supporting multiple values)
     *
     * @return string URI
     */
    public function getRemoveUrl()
    {
        if (!Mage::helper('catalog/category')->useAjaxCheckboxes() && !Mage::helper('catalog/category')->useAjaxPriceSlider()) {
            return parent::getRemoveUrl();
        }
        $selected = explode("_", Mage::getSingleton('core/app')->getRequest()->getParam($this->getFilter()->getRequestVar()));
        if (count($selected) <= 1) {
            return parent::getRemoveUrl(); // clear all filters if only one value is selected
        }
        $tmp = array_diff($selected, (array)$this->getValue());
        foreach ($tmp as &$val) {
            if (!$val) {
                unset($val);
            }
        }
        $values = implode('_', $tmp);

        $query = array(
            $this->getFilter()->getRequestVar()=>$values,
            Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null // exclude current page from urls
        );
        return Mage::getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true, '_query'=>$query));

    }
}
