<?php

/**
 * Quote Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_Rest_Model_Sales_Api2_Quote_Rest_Admin_V1 extends Mage_Api2_Model_Resource
{
    /**
     * The Current Quote.
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote;

    /**
     * {@inheritdoc}
     */
    protected function _create(array $request)
    {
        try
        {
            $quote       = Mage::getModel('sales/quote')->setStore($this->_getStore());
            $mappedQuote = Mage::helper('rest/request_quote')->mapQuote($quote, $request);
        }
        catch (\Exception $ex)
        {
            $this->_critical('Unable to create quote - '.$ex->getMessage(), '409');
            return;
        }

        return $this->_getLocation($mappedQuote);
    }

    /**
     * {@inheritdoc}
     */
    protected function _update(array $request)
    {
        $this->_quote = Mage::helper('rest/model_quote')->getQuote(
            $this->getRequest()->getParam('id'),
            $this->_getStore()->getId()
        );

        if ($this->_quote->getIsActive() == false) {
            return;
        }

        $this->_quote = Mage::helper('rest/request_quote')->mapQuote(
            $this->_quote,
            $request
        );

        Mage::app()->setCurrentStore($this->_quote->getStoreId());

        if (isset($request['submit']) &&  $request['submit'] == true) {
            $response = $this->_submitOrder();
            if ($response->isFailure()) {
                $this->_critical($response->getOpcMessage(), '409');
                return;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function _retrieve()
    {
        return Mage::helper('rest/response_quote')->buildResponse(
            Mage::helper('rest/model_quote')->getQuote(
                $this->getRequest()->getParam('id'), $this->_getStore()->getId()
            )
        );
    }

    /**
     * Submits the current order.
     *
     * @return Vax_Checkout_Model_Response_Opc
     */
    protected function _submitOrder()
    {
        return $this->_getCheckout()->submit($this->_buildOrder());
    }

    /**
     * Builds an order.
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _buildOrder()
    {
        $builder = Mage::getModel(
            'checkout/builder_order',
            Mage::getModel('sales/convert_quote')
        );

        return $builder->build($this->_quote);
    }

    /**
     * Returns the realex checkout.
     *
     * @return Vax_Realex_Model_Service_Checkout
     */
    protected function _getCheckout()
    {
        $factory = Mage::getModel(
            'checkout/factory_checkout',
            Mage::helper('realex/config')
        );

        return $factory->get($this->_quote->getPayment()->getMethod());
    }
}