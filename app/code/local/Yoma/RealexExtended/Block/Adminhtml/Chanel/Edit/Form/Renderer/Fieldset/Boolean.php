<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Form_Renderer_Fieldset_Boolean
    extends Varien_Data_Form_Element_Multiselect
{

    public function getValue(){

        $name = $this->getName();
        switch($this->getName()){
            case 'three_d_secure_profile[]':
                $value = implode(',',mage::getModel('realexExtended/secureProfile')->getProfileOptions($this->getData('value'),true));
                $this->setValue($value);
                break;
            case 'avs_result[]':
                $value = implode(',',mage::getModel('realexExtended/Avs')->getProfileOptions($this->getData('value')));
                $this->setValue($value);
                break;
            case 'cvn_result[]':
                $value = implode(',',mage::getModel('realexExtended/Cvn')->getProfileOptions($this->getData('value')));
                $this->setValue($value);
                break;
            case 'three_d_secure_result[]':
                $value = implode(',',mage::getModel('realexExtended/secure')->getProfileOptions($this->getData('value')));
                $this->setValue($value);
                break;
        }
        return $this->getData('value');
    }

 }