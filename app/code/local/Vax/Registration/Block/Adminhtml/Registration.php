<?php

/**
 * Registration Adminhtml Block.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Block_Adminhtml_Registration extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'vax_registration';
        $this->_controller = 'adminhtml_registration';
        $this->_headerText = $this->__('List Guarantees');

        parent::_construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if( ! Mage::getSingleton('admin/session')->isAllowed('registration/new')) {
            $this->_removeButton('add');
        }
    }
}