<?php

/**
 * Vax Checkout Onepage Order Delivery Address.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Order_Address_Delivery extends Vax_Checkout_Block_Onepage_Order_Address
{
    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->_getQuote()->getShippingAddress();
    }
}