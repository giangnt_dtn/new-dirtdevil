<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `{$this->getTable('realexExtended/chanel')}`
    ADD COLUMN `tss_result` int(11) AFTER `website_id`,
    ADD COLUMN `avs_result` int(11) AFTER `tss_result`,
    ADD COLUMN `cvn_result` int(11) AFTER `avs_result`;
");
$installer->endSetup();