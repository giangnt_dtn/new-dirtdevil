<?php

/**
 * Vax Checkout Onepage Order Address.
 *
 * @copyright Vax Ltd 2013
 */
abstract class Vax_Checkout_Block_Onepage_Order_Address extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the correct address.
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public abstract function getAddress();

    /**
     * Formats an address line.
     *
     * @param string $line
     *
     * @return string
     */
    protected function _formatLine($line)
    {
        if (strlen($line) > 20) {
            $line = '<abbr title="'.$line.'">'.substr(trim($line), 0, 20).'</abbr>';
        }

        return $line;
    }

    /**
     * Returns the current line 1.
     *
     * @return string
     */
    public function getLine1()
    {
        return $this->_formatLine(
            $this->getAddress()->getStreet1()
        );
    }

    /**
     * Returns the current line 2.
     *
     * @return string
     */
    public function getLine2()
    {
        return $this->_formatLine(
            $this->getAddress()->getStreet2()
        );
    }

    /**
     * Returns the current city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->_formatLine(
            $this->getAddress()->getCity()
        );
    }

    /**
     * Returns the current region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->_formatLine(
            $this->getAddress()->getRegion()
        );
    }

    /**
     * Returns the current postcode.
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->_formatLine(
            $this->getAddress()->getPostcode()
        );
    }
}