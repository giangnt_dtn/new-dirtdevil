var controller = "/spares/index/sku/";

function loadRelatedProducts(sku, errorHandler)
{
    $j('#product-selector-container').show();
    $j('.category-products').empty();

    var url = controller + "?sku=" + sku + "&category=" + $j('#category').val();

    $j.ajax({
        url: url,
        context: document.body,
        success: function(data, status) {
            var response = $j.parseJSON(data);
            if (response.productlist !== null) {
                $j('.category-products').html(response.productlist);
                $j('.category-products img').unveil();
                categoryLayout();
            }
            else {
                $j('.category-products').html('<p class="note-msg error">' + response.error  + '</p>');
            }
            try {
                attachAjaxCategoryListener();
            } catch(e) {

            }
        }
    });
}

jQuery(document).ready(function($j){
    $j('.before-main-container #model-dropdown').change(function(){
        if($j('.before-main-container #model-dropdown option:selected').val() === '-') {
            return false;
        }

        $j('.before-main-container #search-reg').val($j('.before-main-container #model-dropdown option:selected').val());

        loadRelatedProducts($j('.before-main-container #search-reg').val(), '.search-dropdown-error');
    });

    $j('#model-dropdown-top').change(function(){
        if($j('#model-dropdown-top option:selected').val() === '-') {
            return false;
        }

        $j('.before-main-container #search-reg').val($j('#model-dropdown-top option:selected').val());

        loadRelatedProducts($j('#search-reg').val(), '.search-dropdown-error');
    });

    // Do product code search on submit
    $j('body').on('submit', '#spares-model-number-search, #spares-model-number-search-top', function(event) {
        var input = $j('.before-main-container #search-reg');
        input.val($j.trim(input.val()));

        if($j.trim(input.val()) === '') {
            return false;
        }
        //sync dropdown with input box
        var element = document.getElementById('model-dropdown');
        element.value = input.val();

        loadRelatedProducts(input.val(), '.search-box-error');

        return false;
    });

    // load the related products called from "VIEW ALL" on the product page
    // don't accept query strings longer than 10 and less than 3 characters
    if(getQueryVariable('sku') && (getQueryVariable('sku').length > 2 && getQueryVariable('sku').length < 10 )) {
        var sku = getQueryVariable('sku');

        //sync dropdown value with input box value to be the sku we need
        $j('.before-main-container #search-reg').val(sku);
        var element = $j('.before-main-container #model-dropdown');
        element.value = sku;


        loadRelatedProducts(sku, '.search-box-error');
        return false;
    } else {
        return false;
    }

});
//get query string variable from the URL
// @var: String (required variable)
// @return: String (variable value)
function getQueryVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}
