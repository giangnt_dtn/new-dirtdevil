<?php

/**
 * Product Index Controller.
 *
 * @copyright Vax Ltd
 */
class DirtDevil_Spares_Product_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Template to Render
     *
     * @var string.
     */
    protected $template;

    /**
     * Base url of the resource library
     *
     * @var string
     */
    protected $libraryBaseUrl = "http://library.vax.co.uk/";

    /**
     * Index action - returns an array of a products found with a certain code     *  subcategories
     *
     * Responds to AJAX (XmlHttpRequest)
     * @todo If not AJAX then redirect to applicable category
     * @todo Redirect if category does not exist, or is not active
     */
    public function codeAction() {

        // Get the category ID from request, ensure integer
        $code     = $this->getRequest()->getParam('pcode');
        $location = $this->getRequest()->getParam('location');

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter(
                array(
                    array("attribute" => 'sku', 'like' => "%$code%"),
                    array("attribute" => 'name', 'like' => "%$code%"),
                    array("attribute" => 'uk_product_no', 'like' => "%$code%"),
                ))
            ->addStoreFilter(Mage::app()->getStore()->getId());
        $productCollection->joinField('category_table',
            'catalog/category_product',
            'category_id',
            'product_id=entity_id',
            null);
        // Not RTB
        $productCollection->addAttributeToFilter('inventory_rtb_only', 0);
        $productCollection->addAttributeToFilter('status', 1);
        // Only machines
        $productCollection->addAttributeToFilter('attribute_set_id', array('neq' => 26));
        // Group product by entity id, this stops duplicates!
        $productCollection->getSelect()->group('entity_id');
        $productCollection->load();

        if($productCollection->count() >0) {
            $data = $this->loadProductData($productCollection, $location);

            // Set partial template to render
            $this->template = 'productselector/_partial/product.phtml';
        }
        else {
            $this->template = 'productselector/_partial/no_results.phtml';
            $data = array();
        }

        $data = array(
            'template' => $this->renderTemplate(),
            'products' => $data,
        );

        // Return JSON encoded data
        $this->getResponse()->setBody(json_encode($data));
    }

    /**
     * Category Action.
     *
     * @return void
     */
    public function categoryAction()
    {
        $categoryId     = (int) $this->getRequest()->getParam('id');
        $storeId        = (int) $this->getRequest()->getParam('store_id');
        $location       = $this->getRequest()->getParam('location');
        $this->template = 'productselector/_partial/product.phtml';

        $data = array(
            'template' => $this->renderTemplate(),
            'products' => $this->getCategoryProducts($categoryId, $location),
        );

        $this->getResponse()->setBody(json_encode($data));
    }

    /**
     * User Guides action - returns an array of a products user guides
     *
     * Responds to AJAX (XmlHttpRequest)
     * @todo If not AJAX then redirect to applicable product
     * @todo Redirect if product does not exist, or is not visible
     */
    public function userguidesAction() {
        $categoryId = $this->getRequest()->getParam('id');

        if (!$categoryId) {
            return false;
        }

        $category = Mage::getModel('catalog/category')->load($categoryId);

        $associatedProductId = $category->getCategoryProductId();
        $associatedProduct = false;

        if($associatedProductId) {
            $associatedProduct = Mage::getModel('catalog/product')->load($associatedProductId);
        }

        if (!$associatedProduct) {
            return false;
        }

        $userGuides = Mage::helper('dirtdevil_support')->getUserguidesForProduct($associatedProduct);

        $this->getResponse()->setBody(Zend_Json::encode($userGuides));
    }

    public function productUserguidesAction()
    {
        $sku = $this->getRequest()->getParam('id');
        $lang = substr(Mage::app()->getLocale()->getLocaleCode(), 0, 2);

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
        if ( ! $product ) {
            // 404
        }

        // Set partial template to render
        $this->template = 'catalog/product/view/_partial/userguide.phtml';

        $data = array(
            'base_url' => $this->libraryBaseUrl,
            'template' => $this->renderTemplate(),
            'userguides' => $this->getProductUserGuides($sku, $lang),
        );

        // Return JSON encoded data
        $this->getResponse()->setBody(json_encode($data));
    }

    /**
     * Load Product Data.
     *
     * @param array  $products
     * @param string $location
     * @param mixed  $category
     *
     * @return array
     */
    protected function loadProductData($products, $location, $category = false)
    {
        $data = array();

        foreach ($products as $product) {
            $product->load($product->getId());

            if ($product->getStatus()) {
                $resizedImageUrl = (string) Mage::helper('catalog/image')
                    ->init($product, 'image')
                    ->keepAspectRatio(true)
                    ->keepFrame(true)
                    ->resize(160,160);

                if ($location == "registration") {
                    $url = DIRECTORY_SEPARATOR . "registration" . DIRECTORY_SEPARATOR . "form" .
                        DIRECTORY_SEPARATOR . "get" . DIRECTORY_SEPARATOR . "sku" . DIRECTORY_SEPARATOR . $product->getSku();
                }
                else if ($category && $category->getParentCategory()->getParentCategory()->isUKSupportCategory()) {
                    $url = $product->getUrlKey();
                }
                else if ($category)  {
                    $url = DIRECTORY_SEPARATOR.$category->getUrlPath().DIRECTORY_SEPARATOR.$product->getUrlKey();
                    $url = str_replace('//', '/', $url);
                }
                else {
                    $url = $product->getUrlKey();
                }

                $data[] = array(
                    'id'      => $product->getId(),
                    'name'    => $product->getName(),
                    'url'     => $url,
                    'image'   => $resizedImageUrl,
                    'modelNo' => $product->getUkProductNo(),
                );
            }
        }

        return $data;
    }

    /**
     * Get a list of a category's products along with public data
     *
     * @param integer categoryId The Id of the category
     * @return array
     */
    protected function getCategoryProducts($categoryId, $location) {

        $currentStoreId = Mage::app()->getStore()->getId();
        $supportStore = Mage::helper('vax_core')->getSparesStoreView();

        if ($currentStoreId != $supportStore->getId()) {
            Mage::app()->getStore()->load($supportStore->getId());
        }

        // Load product collection for specified category id
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addCategoryFilter($category)
            ->setOrder('name', 'ASC');
        Mage::getModel('catalog/product_visibility')->addVisibleInSiteFilterToCollection($products);

        if ($supportStore->getId()) {
            $products->addStoreFilter($supportStore);
        }

        $data = $this->loadProductData($products, $location, $category);

        if ($currentStoreId != $supportStore->getId()) {
            Mage::app()->getStore()->load($currentStoreId);
        }

        return $data;
    }

    protected function _getCategoryProductCollection($categoryId, $location)
    {
        // Load product collection for specified category id
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $products = Mage::getResourceModel('catalog/product_collection')
            ->addCategoryFilter($category)
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->setOrder('name', 'ASC');
        Mage::getModel('catalog/product_visibility')->addVisibleInSiteFilterToCollection($products);

        return $products;
    }

    /**
     * Returns the userguides for a product.
     *
     * @param string $sku  Product SKU
     * @param string $lang Userguide Language
     *
     * @return string
     */
    protected function getProductUserGuides($sku, $lang="English")
    {
        return Mage::helper('vax_document')->getUserGuide($sku, $lang);
    }

    /**
     * Renders the Template.
     *
     * @return string
     */
    protected function renderTemplate()
    {
        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'Vax_Core',
            array('template' => $this->template)
        );

        return $block->toHtml();
    }
}
