<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId("chanelGrid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("realexExtended/chanel")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn("profile_name", array(
            "header" => Mage::helper("realexExtended")->__("Profile"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "profile_name",
        ));

        $this->addColumn("website_id", array(
            "header" => Mage::helper("realexExtended")->__("Website"),
            "align" => "left",
            "width" => "10px",
            "type" => "int",
            "index" => "website_id",
        ));

        $this->addColumn("billing_country_id", array(
            "header" => Mage::helper("realexExtended")->__("Billing Country"),
            "align" => "left",
            "width" => "50px",
            "type" => "varchar",
            "index" => "billing_country_id",
        ));

        $this->addColumn("shipping_country_id", array(
            "header" => Mage::helper("realexExtended")->__("Shipping Country"),
            "align" => "left",
            "width" => "50px",
            "type" => "varchar",
            "index" => "shipping_country_id",
        ));

        $this->addColumn("currency", array(
            "header" => Mage::helper("realexExtended")->__("Currency Code"),
            "align" => "left",
            "width" => "50px",
            "type" => "varchar",
            "index" => "currency",
        ));

        $this->addColumn("card_type", array(
            "header" => Mage::helper("realexExtended")->__("Card Type"),
            "align" => "left",
            "width" => "50px",
            "type" => "varchar",
            "index" => "card_type",
        ));

        $this->addColumn("ticket_size_from", array(
            "header" => Mage::helper("realexExtended")->__("Amount From"),
            "align" => "left",
            "width" => "50px",
            "type" => "float",
            "index" => "ticket_size_from",
        ));

        $this->addColumn("ticket_size_to", array(
            "header" => Mage::helper("realexExtended")->__("Amount To"),
            "align" => "left",
            "width" => "50px",
            "type" => "float",
            "index" => "ticket_size_to",
        ));

        $this->addColumn("three_d_secure", array(
            "header" => Mage::helper("realexExtended")->__("Use 3DSecure"),
            "align" => "center",
            "width" => "10px",
            'type'      => 'options',
            'options'   => Mage::getSingleton('realexExtended_adminhtml/system_config_source_yesno')->toArray(),
            "index" => "three_d_secure",
        ));


        $this->addColumn("three_d_secure_profile", array(
            "header" => Mage::helper("realexExtended")->__("3DSecure Profile"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "three_d_secure_profile",
            "renderer" => "realexExtended/adminhtml_chanel_grid_renderer_boolean",
        ));

        $this->addColumn("sub_account", array(
            "header" => Mage::helper("realexExtended")->__("Sub Account"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "sub_account",
        ));

        $this->addColumn("rv_sub_account", array(
            "header" => Mage::helper("realexExtended")->__("RealVault Sub Account"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "rv_sub_account",
        ));

        $this->addColumn("settle_type", array(
            "header" => Mage::helper("realexExtended")->__("Settle Method"),
            "align" => "left",
            "width" => "100px",
            'type'      => 'options',
            'options'   => Mage::getSingleton('realex/realex_source_paymentAction')->toArray(),
            "index" => "settle_type",
        ));

        $this->addColumn("tss_result", array(
            "header" => Mage::helper("realexExtended")->__("TSS Result"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "tss_result",
        ));

        $this->addColumn("avs_result", array(
            "header" => Mage::helper("realexExtended")->__("AVS Score"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "avs_result",
            "renderer" => "realexExtended/adminhtml_chanel_grid_renderer_boolean",

        ));

        $this->addColumn("cvn_result", array(
            "header" => Mage::helper("realexExtended")->__("CVN Score"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "cvn_result",
            "renderer" => "realexExtended/adminhtml_chanel_grid_renderer_boolean",
        ));

        $this->addColumn("tss_rule_specific", array(
            "header" => Mage::helper("realexExtended")->__("TSS Rules Score"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "tss_rule_specific",
            "renderer" => "realexExtended/adminhtml_chanel_grid_renderer_tss",
        ));

        $this->addColumn("tss_rule_specific", array(
            "header" => Mage::helper("realexExtended")->__("TSS Rules Score"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "tss_rule_specific",
            "renderer" => "realexExtended/adminhtml_chanel_grid_renderer_tss",
        ));

        $this->addColumn("three_d_secure_result", array(
            "header" => Mage::helper("realexExtended")->__("ECI Result"),
            "align" => "left",
            "width" => "100px",
            "type" => "varchar",
            "index" => "three_d_secure_result",
            "renderer" => "realexExtended/adminhtml_chanel_grid_renderer_boolean",
        ));

        $this->addColumn("auto_settle", array(
            "header" => Mage::helper("realexExtended")->__("Auto Settle"),
            "align" => "center",
            "width" => "10px",
            'type'      => 'options',
            'options'   => Mage::getSingleton('realexExtended_adminhtml/system_config_source_yesno')->toArray(),
            "index" => "auto_settle",
        ));

        $this->addColumn("order_review", array(
            "header" => Mage::helper("realexExtended")->__("Mark as Order Review"),
            "align" => "center",
            "width" => "10px",
            'type'      => 'options',
            'options'   => Mage::getSingleton('realexExtended_adminhtml/system_config_source_yesno')->toArray(),
            "index" => "order_review",
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));

    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_chanel', array(
            'label' => Mage::helper('realexExtended')->__('Remove Rule'),
            'url' => $this->getUrl('*/adminhtml_chanel/massRemove'),
            'confirm' => Mage::helper('realexExtended')->__('Are you sure?')
        ));
        return $this;
    }

}