<?php

/**
 * Vax Checkout Onepage Success Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Success extends Vax_Checkout_Block_Onepage
{
    protected $_socialMessage = '';
    protected $_maxItem;
    /**
     * Returns the most expensive item from the latest order.
     *
     * @return Varien_Object
     */
    public function getMostExpensiveItem()
    {
        if (!$this->_maxItem) {
            $maxItem = new Varien_Object();
            foreach ($this->getOrder()->getAllVisibleItems() as $item) {
                if ($item->getPrice() >= $maxItem->getDataSetDefault('price', 0)) {
                    $maxItem = $item;
                }
            }

            $this->_maxItem = $maxItem;
        }

        return $this->_maxItem;
    }

    /**
     * Prepares social message for sharing.
     *
     * @return string
     */
    public function getSocialMessage()
    {
        if (!$this->getMostExpensiveItem() instanceof Mage_Sales_Model_Order_Item) {
            return '';
        }

        return $this->__('I just bought %s from @VaxUK!', $this->getMostExpensiveItem()->getName());
    }

    /**
     * Provides URL for the item advertised via social media.
     *
     * @return string
     */
    public function getSocialUrl()
    {
        if (!($this->getMostExpensiveItem() instanceof Mage_Sales_Model_Order_Item)) {
            return Mage::getBaseUrl();
        }

        return $this->getMostExpensiveItem()->getProduct()->getProductUrl();
    }

    /**
     * @see Yoma_Realex_Block_Onepage_Success
     */
    public function getRememberToken($orderId){

        $tokenSaved = false;

        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        if (!$order->getId()) {
            return false;
        }

        $payment = $order->getPayment();
        if (!$payment->getId()) {
            return false;
        }

        if (!in_array($payment->getMethod(),array('realexdirect','realexredirect'))) {
            return false;
        }

        // if token param then redirect else check session
        if($this->getRequest()->getParam('token')){
            $tokenSaved = $this->getRequest()->getParam( 'token' );
        }else{
            $tokenSaved = Mage::getSingleton('customer/session')->getTokenSaved();
            Mage::getSingleton('customer/session')->unsTokenSaved();
            Mage::getSingleton('realex/session')->unsTokenSaved();
        }

        if($tokenSaved){
            return true;
        }
        return false;
    }
}
