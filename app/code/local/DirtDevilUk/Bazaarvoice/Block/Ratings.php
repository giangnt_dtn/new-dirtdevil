<?php
class DirtDevilUk_Bazaarvoice_Block_Ratings extends Bazaarvoice_Connector_Block_Ratings
{
    const ATTR_SET_ACCESSORY         = 26;

    protected $_productCollection;

    /**
     * Product collection setter.
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    public function setProductCollection(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $this->_productCollection = $collection;
    }

    /* remove accessories and sku from collection */
    public function getLoadedProductCollection()
    {
        if($this->_productCollection) {

            //if($this->_productCollection->getResource()->getAttribute('attribute_set_id')) {
            //    return $this->_productCollection->addAttributeToFilter('attribute_set_id', array('neq' => self::ATTR_SET_ACCESSORY))->addAttributeToSelect('sku');
            //}

            return $this->_productCollection;
        }

        // Get reference to parent block - product list
        $productListBlock = $this->getParentBlock();
        // Verify the parent is really a product list
        if(!($productListBlock instanceof Mage_Catalog_Block_Product_List)) {
            // Return empty array to keep template code happy
            return array();
        }

        if(!$productListBlock->getLoadedProductCollection()) {
            return array();
        }

        // Get and return ref to loaded prod collection
        $this->_productCollection = $productListBlock->getLoadedProductCollection();

        //if($this->_productCollection->getResource()->getAttribute('attribute_set_id')) {
        //    $this->_productCollection->addAttributeToFilter('attribute_set_id', array('neq' => self::ATTR_SET_ACCESSORY));
        //}

        return $this->_productCollection;
    }
}
