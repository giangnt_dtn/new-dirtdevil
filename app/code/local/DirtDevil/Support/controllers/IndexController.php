<?php

/**
 * Support Index Controller.
 *
 * @copyright Vax Ltd
 */
class DirtDevil_Support_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Index Action.
     *
     * Displays the support index.
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}
