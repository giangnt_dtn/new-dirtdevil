function addShareButtons(product_id, url) {
    
    if(product_id != null && url != null) {
        
        stWidget.addEntry({
            "service":"sharethis",
            "element":document.getElementById('share_this_button_' + product_id),
            "url": url,
            "title":"sharethis",
            "type":"hcount",
            "text":"sharethis" ,
            //"image":"http://www.softicons.com/download/internet-icons/social-superheros-icons-by-iconshock/png/256/sharethis_hulk.png",
            "summary":"sharethis"
            });

        stWidget.addEntry({
            "service":"fblike",
            "element":document.getElementById('fb_like_button_' + product_id),
            "url": url,
            "title":"Facebook Like",
            "type":"hcount",
            "text":"Facebook Like" ,
            //"image":"http://www.softicons.com/download/internet-icons/social-superheros-icons-by-iconshock/png/256/sharethis_hulk.png",
            "summary":"Facebook Like"
            });

        stWidget.addEntry({
            "service":"plusone",
            "element":document.getElementById('gg_like_button_' + product_id),
            "url": url,
            "title":"Google+ Like",
            "type":"hcount",
            "text":"Google+ Like" ,
            //"image":"http://www.softicons.com/download/internet-icons/social-superheros-icons-by-iconshock/png/256/sharethis_hulk.png",
            "summary":"Google+ Like"
            });
    }
}