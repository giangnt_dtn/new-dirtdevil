<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn(
    $installer->getTable('vax_registration/registration'),
    'terms_agreed',
    array(
        'type'     => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable' => true,
        'comment'  => 'Terms Agreed Flag'
    )
);

$retailersTable = $installer->getConnection()
    ->newTable($installer->getTable('vax_registration/retailer'))
    ->addColumn(
        'retailer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Retailer ID'
    )
    ->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        50,
        array(
            'nullable' => false,
        ),
        'Title'
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
        ),
        'Store Id'
    )
    ->addColumn(
        'position',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned' => true,
            'default'  => 0
        ),
        'Position'
    )
    ->addColumn(
        'is_enabled',
        Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        null,
        array(
            'default'  => 0,
            'nullable' => false,
        ),
        'Store Id'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        array(
    'nullable'  => false,
    ))
    ->addColumn(
        'modified_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE
        ),
        'Update Time'
    )
    ->addIndex(
        $installer->getIdxName('vax_registration/retailer', array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(
            'vax_registration/retailer',
            'store_id',
            'core/store',
            'store_id'
        ),
        'store_id',
        $installer->getTable('core/store'),
        'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Vax Registration Retailer Table');

$installer->getConnection()->createTable($retailersTable);

$attachmentsTable = $installer->getConnection()
    ->newTable($installer->getTable('vax_registration/attachment'))
    ->addColumn(
        'attachment_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true,
        ),
        'Attachment ID'
    )
    ->addColumn(
        'attachment_type',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        20,
        array(
            'nullable' => true,
            'default'  => 'Warranty'
        ),
        'Attachment Type'
    )
    ->addColumn(
        'file_name_original',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false,
        ),
        'File Name - Unsafe'
    )
    ->addColumn(
        'file_name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false,
        ),
        'File Name'
    )
    ->addColumn(
        'completed_file_name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false,
        ),
        'Completed File Name'
    )
    ->addColumn(
        'file_size',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned' => true,
            'nullable' => true,
            'default'  => 0,
        ),
        'File Size'
    )
    ->addColumn(
        'completed_thumbnail',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => true,
            'default'  => null,
        ),
        'Completed Thumbnail'
    )
    ->addColumn(
        'registration_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned' => true,
            'nullable' => false,
        ),
        'Registration Id'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null,
        array(
            'nullable'  => false,
        )
    )
    ->addColumn(
        'modified_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE
        ),
        'Update Time'
    )
    ->addIndex(
        $installer->getIdxName('vax_registration/attachment', array('registration_id')),
        array('registration_id')
    )
    ->addForeignKey(
        $installer->getFkName(
            'vax_registration/attachment',
            'registration_id',
            'vax_registration/registration',
            'entity_id'
        ),
        'registration_id',
        $installer->getTable('vax_registration/registration'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Vax Registration Attachment Table');

$installer->getConnection()->createTable($attachmentsTable);

$resource        = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$tableN          = $resource->getTableName('vax_registration/retailer');

//$writeConnection->query("INSERT INTO {$tableN} SET title = 'EP Electronic Partner', store_id = 6, position = 1, is_enabled = 1");
//$writeConnection->query("INSERT INTO {$tableN} SET title = 'Euronics', store_id = 6, position = 2, is_enabled = 1");
//$writeConnection->query("INSERT INTO {$tableN} SET title = 'Expert', store_id = 6, position = 3, is_enabled = 1");
//$writeConnection->query("INSERT INTO {$tableN} SET title = 'MediMax', store_id = 6, position = 4, is_enabled = 1");

$installer->endSetup();