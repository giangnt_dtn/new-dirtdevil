<?php
class Vax_Prodfaqs_Block_Faq_CommonProblems extends Vax_Prodfaqs_Block_Faq_Abstract
{
    public function getCommonProblems()
    {
        return $this->getRelatedFaqsByProduct()
            ->addStatusFilter(1)
            ->addFieldToFilter('visibility', 'public')
            ->addFieldToFilter('question_type', 'product_question');
    }
}