<?php
/**
 * ImageCdn data helper
 *
 * @category    vax_uk
 * @package     Vax_ImageCdn
 * @author      Craig Carnell
 */
class Vax_ImageCdn_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Magento admin settings
     */
    const XML_PATH_IMAGECDN_RESTRICTION_ENABLED = 'vax_imagecdn/general/enabled';
    const XML_PATH_IMAGECDN_DEBUGGING_ENABLED = 'vax_imagecdn/general/debugging';
    
    /**
     * Server variable we should use
     */
    //const XML_PATH_IMAGECDN_SERVER_VAR = 'vax_imagecdn/general/server_var';
    
    /**
     * Config paths for using throughout the code
     */
    //const XML_PATH_IMAGECDN_ALLOWED_IPS = 'vax_imagecdn/general/allowed_ips';

    /**
     * Cached values
     */
    //protected $allowed_ips = null;
    //protected $server_var = null;
    protected $restriction_enabled = null;
    protected $debug_cdn = null;
    
    /**
     * Restriction enabled?
     */
    public function getRestrictionEnabled() {
        
        if($this->restriction_enabled == null) {
            $this->restriction_enabled = (bool)Mage::getStoreConfig(self::XML_PATH_IMAGECDN_RESTRICTION_ENABLED);
        }
        
        return $this->restriction_enabled;
    }
    
    /**
     * Debug CDN?
     */
    public function isDebugging() {
        
        if($this->debug_cdn == null) {
            $this->debug_cdn = (bool)Mage::getStoreConfig(self::XML_PATH_IMAGECDN_DEBUGGING_ENABLED);
        }
        
        return $this->debug_cdn;
    }
    
    /**
     * Is the current installation allowed to empty the whole CDN cache?
     */
    public function IsUploadAllowed() {

        if($this->isDebugging()) {
            Mage::log('IsUploadAllowed()', null, 'cdn.log');
        }
        
        $allowed_upload = (bool)Mage::getConfig()->getNode('global/resources/image_cdn/enable_cdn_updates') ;
            
        if($allowed_upload) {
            
            if($this->isDebugging()) {
                Mage::log('IsUploadAllowed() - true', null, 'cdn.log');
            }
            
            return true;
        }

        if($this->isDebugging()) {
            Mage::log('IsUploadAllowed() - false', null, 'cdn.log');
        }
            
        return false;
    }

    /*
     * Is the CDN extension enabled?
     */
    public function useCdn() {
	return Mage::getStoreConfig('imagecdn/general/status') ? true : false;
    }
    
    /*
     * Get the URL for the CDN depending on if we are secure or not
     */
    public function getCdnUrl($ssl = false) {
        if(Mage::app()->getStore()->isCurrentlySecure() || $ssl) {
            return Mage::getStoreConfig('imagecdn/rackspace/secure_base_url');
        }
        
	return Mage::getStoreConfig('imagecdn/rackspace/base_url');
    }
}
