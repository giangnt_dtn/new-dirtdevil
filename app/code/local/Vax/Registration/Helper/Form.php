<?php
/**
 * Vax Registration - Helper
 *
 * @category    code
 * @package     vax
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      Vax
 */
class Vax_Registration_Helper_Form extends Mage_Core_Helper_Abstract {
    const XML_CONFIG_REGISTRATION_COUNTY = 'vax_registration/form_options/county';
    const XML_CONFIG_REGISTRATION_STORE_SELECT = 'vax_registration/form_options/retailerselect';
    const XML_CONFIG_REGISTRATION_FILE_UPLOADER = 'vax_registration/form_options/fileuploader';
    const XML_CONFIG_REGISTRATION_TERMS = 'vax_registration/form_options/terms';
    const XML_CONFIG_REGISTRATION_TERMS_CMS_PAGE = 'vax_registration/form_options/terms_cms_page';
    const XML_CONFIG_REGISTRATION_POSTCODE_VALIDATION_CLASS = 'vax_registration/form_options/postcode_validation_class';
        
    /*
     * Returns whether the county field is required in registration form
     * 
     * @return bool
     */
    public function isRegistrationCountyEnabled() {
        return Mage::getStoreConfig(self::XML_CONFIG_REGISTRATION_COUNTY);
    }
    
    /*
     * Returns a list of physical/online shops in registration form
     * 
     * @return bool
     */
    public function getRetailersForStore() {
        
        $store_id = Mage::app()->getStore()->getId();
        
        //$array_return = array();
        $collection = Mage::getModel('vax_registration/retailer')->getCollection()
                ->addStoreFilter($store_id)
                ->addEnabledFilter(1)
                ->addAttributeToSort('position', 'ASC');

        //foreach ($collection as $shop_model) {
        //    array_push($array_return, array("label" => $shop_model->getTitle(), "value" => $shop_model->getId()));
        //}

        //array_unshift($array_return, array("label" => "[ Please select ]", "value" => 0));
        //return $array_return;
        return $collection;
    }
    
    public function extendedWarrantyAvailable($product = null) {
        
        if($product) {
            
            $extendedWarranty = false;
            $attributeValue = Mage::getModel('catalog/product')->load($product->getId())->getExtendedWarrantyOffer();
                        
            if($attributeValue) {
                $extendedWarranty = (bool)$attributeValue;
            }
            
            return $extendedWarranty;
        }
        
        return false;
    }
    
    public function extendedWarrantyLength($product = null) {
        
        if($product) {
            
            $extendedWarrantyLength = false;
            $attributeValue = Mage::getModel('catalog/product')->load($product->getId())->getExtendedWarrantyLength();
            
            return $attributeValue;
        }
        
        return false;
    }
    
    
    /*
     * Should we display the store drop down?
     * 
     * @return bool
     */
    public function shouldShowStoreSelect($product = null) {
     
        return $this->extendedWarrantyAvailable($product) && Mage::getStoreConfig(self::XML_CONFIG_REGISTRATION_STORE_SELECT);
    }
    
    /*
     * Should we display the terms agreed checkbox?
     * 
     * @return bool
     */
    public function shouldShowTermsCheckBox($product = null) {
     
        return $this->extendedWarrantyAvailable($product) && Mage::getStoreConfig(self::XML_CONFIG_REGISTRATION_TERMS);
    }
    
    /*
     * Should we display the store drop down?
     * 
     * @return bool
     */
    public function shouldShowFileUploader($product = null) {
     
        return $this->extendedWarrantyAvailable($product) && Mage::getStoreConfig(self::XML_CONFIG_REGISTRATION_FILE_UPLOADER);
    }
    
    /*
     * Get CMS Page for Terms & Conditions
     * 
     * @return string/url
     */
    public function getTermsPage() {
     
        return Mage::getStoreConfig(self::XML_CONFIG_REGISTRATION_TERMS_CMS_PAGE);
    }
        
    /*
     * Get Image HTML for admin
     * 
     * @return string/url
     */
    public function getImageHTML($_attachment) {
        
        if(!$_attachment) return '';
        
        $image = $_attachment->getCompletedFileName();
        $image_url = "";
        $thumbnail = $_attachment->getCompletedThumbnail();
        $thumbnail_url = "";
        
        $upload_handler = new Vax_Upload_Model_Upload();

        if(!empty($image)) {
            $image_url = $upload_handler->getCompletedDownloadUrl($image);           
        } else {
            return "No image found";
        }
        
        if(!empty($thumbnail)) {
            $thumbnail_url = $upload_handler->getCompletedDownloadUrl($thumbnail, 'thumbnail');
        } else {
            $thumbnail_url = $image_url;
            $thumbnail = $image;
        }
        
        $ext = pathinfo($image_url, PATHINFO_EXTENSION);
        
        switch($ext) {
            case 'pdf':
                // PLEASE FIX ME!!
                $thumbnail_url = '/skin/frontend/vax/de/images/registration/pdf-icon.png'; //Mage::getSkinUrl('images/registration/pdf-icon.png', array('_secure'=>true));
                break;
            default:
        }
        
        return '<a href="#" onclick="popWin(\'' . $image_url . '\',\'' . $image . '\',\'top:0,left:0,width=820,height=600,resizable=yes,scrollbars=yes\')"><img src="' . $thumbnail_url . '" alt="' . $thumbnail . '" style="max-width:100px; max-height: 100px;" /></a>';
    }
    
    /*
     * Additional date output for German etc.
     * The default 'date' does not output locale based month names (e.g. ocktober)
     * The default strftime does not support 'th', 'st' etc.
     * 
     * @params - string (see Zend_Date constants), unix timestamp
     * @return string/url
     */
    public function getLocaleDate($format, $timestamp) {
        
        // a more complete example
        $datetime = new Zend_Date($timestamp);
        // admin controls this output through configuration
        $datetime->setLocale(Mage::getStoreConfig(
                    Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE))
                ->setTimezone(Mage::getStoreConfig(
                    Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE));

        return $datetime->get($format);

        // a php way of doing things.. (but locale has not been set?)
        //$format = str_replace('%O', date('S', $timestamp), $format);   
        
        // make sure the locale is set
        //setlocale(LC_ALL, Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE));
        
        //return strftime($format, $timestamp);
    }
    
    /**
     * Get post code validation class for store
     * 
     * @return Mage_Core_Model_Store
     */
    public function getPostCodeValidationClass() {
        return Mage::getStoreConfig(self::XML_CONFIG_REGISTRATION_POSTCODE_VALIDATION_CLASS);
    }

    /**
     * Returns a list of title options.
     *
     * @return array
     */
    public function getTitleOptions()
    {
        return array(
            ''     => '[ Select Title ]',
            'Mrs'  => 'Mrs',
            'Mr'   => 'Mr',
            'Ms'   => 'Ms',
            'Miss' => 'Miss',
            'Dr'   => 'Dr',
            'Rev'  => 'Rev',
            'Prof' => 'Prof'
        );
    }
}