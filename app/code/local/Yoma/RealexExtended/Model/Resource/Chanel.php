<?php
class Yoma_RealexExtended_Model_Resource_Chanel extends Mage_Core_Model_Mysql4_Abstract {

    const ROW_BILLING = 0;
    const ROW_SHIPPING = 1;
    const ROW_CURRENCY = 2;
    const ROW_CARD_TYPE = 3;
    const ROW_TICKET_FROM = 4;
    const ROW_TICKET_TO = 5;
    const ROW_3D_SECURE = 6;
    const ROW_3D_SECURE_PROFILE = 7;
    const ROW_SUB_ACCOUNT = 8;
    const ROW_RV_SUB_ACCOUNT = 9;
    const ROW_SETTLE_TYPE = 10;
    const ROW_PROFILE_NAME = 11;
    const ROW_CVN_PROFILE = 12;
    const ROW_AVS_PROFILE = 13;
    const ROW_TSS_RESULT = 14;
    const ROW_TSS_RESULT_SPECIFIC = 15;
    const ROW_3D_SECURE_RESULT = 16;
    const ROW_AUTO_SETTLE= 17;
    const ROW_ORDER_REVIEW= 18;
    const ROW_COUNT = 18;

    protected $table = null;

    protected function _construct() {
    
        $this->_init('realexExtended/chanel', 'id');
    }


    public function matchRule($payment, $order){

        $row = array();

        $read = $this->_getReadAdapter();
        $this->_table = Mage::getSingleton('core/resource')->getTableName('realexExtended/chanel');
        $website_id = Mage::getModel('core/store')->load($order->getStoreId())->getWebsiteId();

        for($i=0;$i<16;$i++) {
            $select = $this->getSelect($read, $payment, $order, $i);

            $select->where('ticket_size_from <=?', $payment->getPaymentAmount());
            $select->where('ticket_size_to >=?', $payment->getPaymentAmount());
            $select->where('website_id =?', $website_id);

            $row = $read->fetchAll($select);
            if(isset($row[0])){
                break;
            }
        }
        return $row;

    }

    public function getSelect($read,$payment,$order, $i){

        $select = $read->select()->from($this->_table);

        $billing = $order->getBillingAddress();
        if($order->getIsVirtual()){
            $shipping = $billing;
        }else{
            $shipping = $order->getShippingAddress();
        }

        $cardType = mage::helper('realexExtended')->getCardType($payment);
        $paymentMethod = $payment->getMethod();

        switch($i) {
            case 0:
                $select->where(
                    $read->quoteInto(" (billing_country_id=? ", $billing->getCountryId()) .
                    $read->quoteInto(" AND shipping_country_id=? ", $shipping->getCountryId()) .
                    $read->quoteInto(" AND currency=? ", $order->getOrderCurrencyCode()) .
                    $read->quoteInto(" AND card_type=?) ", $cardType)
                );
                break;
            case 1:
                $select->where(
                    $read->quoteInto(" (billing_country_id=? ", $billing->getCountryId()) .
                    $read->quoteInto(" AND shipping_country_id=? ", $shipping->getCountryId()) .
                    $read->quoteInto(" AND currency=? AND card_type= '*') ", $order->getOrderCurrencyCode())
                );
                break;
            case 2:
                $select->where(
                    $read->quoteInto(" (billing_country_id=? ", $billing->getCountryId()) .
                    $read->quoteInto(" AND shipping_country_id=? ", $shipping->getCountryId()) .
                    $read->quoteInto(" AND currency= '*' AND card_type=?) ", $cardType)
                );
                break;
            case 3:
                $select->where(
                    $read->quoteInto(" (billing_country_id=? ", $billing->getCountryId()) .
                    $read->quoteInto(" AND shipping_country_id=?  AND currency= '*' AND card_type= '*') ", $shipping->getCountryId())
                );
                break;

            case 4:
                $select->where(
                    $read->quoteInto(" (billing_country_id=?  AND shipping_country_id= '*' ", $billing->getCountryId()) .
                    $read->quoteInto(" AND currency=? ", $order->getOrderCurrencyCode()) .
                    $read->quoteInto(" AND card_type=?) ", $cardType)
                );
                break;
            case 5:
                $select->where(
                    $read->quoteInto(" (billing_country_id=?  AND shipping_country_id='*' ", $billing->getCountryId()) .
                    $read->quoteInto(" AND currency=? AND card_type= '*') ", $order->getOrderCurrencyCode())
                );
                break;
            case 6:
                $select->where(
                    $read->quoteInto(" (billing_country_id=?  AND shipping_country_id='*' ", $billing->getCountryId()) .
                    $read->quoteInto(" AND currency= '*' AND card_type=?) ", $cardType)
                );
                break;
            case 7:
                $select->where(
                    $read->quoteInto(" (billing_country_id=?  AND shipping_country_id='*'  AND currency= '*' AND card_type= '*') ", $billing->getCountryId())
                );
                break;

            case 8:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id=? ", $shipping->getCountryId()) .
                    $read->quoteInto(" AND currency=? ", $order->getOrderCurrencyCode()) .
                    $read->quoteInto(" AND card_type=?) ", $cardType)
                );
                break;
            case 9:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id=? ", $shipping->getCountryId()) .
                    $read->quoteInto(" AND currency=? AND card_type= '*') ", $order->getOrderCurrencyCode())
                );
                break;
            case 10:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id=? ", $shipping->getCountryId()) .
                    $read->quoteInto(" AND currency= '*' AND card_type=?) ", $cardType)
                );
                break;
            case 11:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id=? AND currency= '*' AND card_type= '*') ", $shipping->getCountryId())
                );
                break;

            case 12:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id='*' AND currency=? ", $order->getOrderCurrencyCode()) .
                    $read->quoteInto(" AND card_type=?) ", $cardType)
                );
                break;
            case 13:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id='*' AND currency=? AND card_type= '*') ", $order->getOrderCurrencyCode())
                );
                break;
            case 14:
                $select->where(
                    $read->quoteInto(" (billing_country_id='*'  AND shipping_country_id='*' AND currency= '*' AND card_type=?) ", $cardType)
                );
                break;
            case 15:
                $select->where(" (billing_country_id='*'  AND shipping_country_id='*' AND currency= '*' AND card_type='*') " );
                break;
        }

        return $select;
    }

    public function uploadAndImport(Varien_Object $object){

        $csvFile = $_FILES["groups"]["tmp_name"]["realex_transaction_profiles"]["fields"]["import_chanel"]["value"];
        $csvName = $_FILES["groups"]["name"]["realex_transaction_profiles"]["fields"]["import_chanel"]["value"];

        $session = Mage::getSingleton('adminhtml/session');
        $dataStored = false;

        if (!empty($csvFile)) {

            $csv = trim(file_get_contents($csvFile));

            $table = Mage::getSingleton('core/resource')->getTableName('realexExtended/chanel');

            $websiteId = $object->getScopeId();
            $websiteModel = Mage::app()->getWebsite($websiteId);

            Mage::helper('realexExtended')->saveCSV($csv, $csvName, $websiteId, 'chanel');

            if (!empty($csv)) {
                $exceptions = array();
                $csvLines = explode("\n", $csv);
                $csvLine = array_shift($csvLines);
                $csvLine = $this->_getCsvValues($csvLine);
                if (count($csvLine) < self::ROW_COUNT) {
                    $exceptions[0] = Mage::helper('realexExtended')->__('Invalid TSS Chanelling File Format');
                }

                $countryCodes = array();
                $currencyCodes = array();
                $cardTypes = array();
                $profiles = array();

                foreach ($csvLines as $k => $csvLine) {
                    $csvLine = $this->_getCsvValues($csvLine);
                    if (count($csvLine) > 0 && count($csvLine) < self::ROW_COUNT) {
                        $exceptions[0] = Mage::helper('realexExtended')->__('Invalid TSS Chanelling File Format');
                    } else {
                        $profile_name = str_replace(' ','_',strtoupper(trim($csvLine[self::ROW_PROFILE_NAME])));
                        if (!in_array($profile_name, $profiles)) {
                            $profiles[] = trim($profile_name);
                        }

                        $splitBilling = explode(",", trim($csvLine[self::ROW_BILLING]));
                        $splitShipping = explode(",", trim($csvLine[self::ROW_SHIPPING]));
                        $splitCurrency = explode(",", trim($csvLine[self::ROW_CURRENCY]));
                        $splitCardTypes = explode(",", trim($csvLine[self::ROW_CARD_TYPE]));
                        foreach ($splitBilling as $country) {
                            if (!in_array($country, $countryCodes)) {
                                $countryCodes[] = trim($country);
                            }
                        }
                        foreach ($splitShipping as $country) {
                            if (!in_array($country, $countryCodes)) {
                                $countryCodes[] = trim($country);
                            }
                        }
                        foreach ($splitCurrency as $currency) {
                            if (!in_array($currency, $currencyCodes)) {
                                $currencyCodes[] = trim($currency);
                            }
                        }
                        foreach ($splitCardTypes as $cardType) {
                            if (!in_array($cardType, $cardTypes)) {
                                $cardTypes[] = trim($cardType);
                            }
                        }
                    }
                }
                if (empty($exceptions)) {
                    $connection = $this->_getWriteAdapter();
                    foreach($profiles as $profile){
                        $condition = array(
                            $connection->quoteInto('website_id = ?', $websiteId),
                            $connection->quoteInto('profile_name = ?', $profile),
                        );
                        $connection->delete($table, $condition);
                    }
                }

                if (!empty($exceptions)) {
                    throw new Exception("\n" . implode("\n", $exceptions));
                }


                if (empty($exceptions)) {
                    $data = array();
                    $countryCodesToIds = array();
                    $counter = 0;
                    $countryCollection = Mage::getResourceModel('directory/country_collection')->addCountryCodeFilter($countryCodes)->load();
                    foreach ($countryCollection->getItems() as $country) {
                        $countryCodesToIds[$country->getData('iso3_code')] = $country->getData('country_id');
                        $countryCodesToIds[$country->getData('iso2_code')] = $country->getData('country_id');
                    }
                    $codes = Mage::app()->getStore()->getAvailableCurrencyCodes(true);
                    foreach($currencyCodes as $key=>$currency){
                        if(!in_array($currency, $codes)) {
                            unset($currencyCodes[$key]);
                        }
                    }
                    $cards = mage::getModel('realex/realex_source_cards')->getSupportedCardTypes();
                    foreach($cardTypes as $key=>$cardType){
                        if(!in_array($cardType, $cards)) {
                            unset($cardTypes[$key]);
                        }
                    }
                    foreach ($csvLines as $k=>$csvLine) {

                        $csvLine = $this->_getCsvValues($csvLine);
                        $splitBilling = explode(",", trim($csvLine[self::ROW_BILLING]));
                        $splitShipping = explode(",", trim($csvLine[self::ROW_SHIPPING]));
                        $splitCurrency = explode(",",strtoupper(trim($csvLine[self::ROW_CURRENCY])));
                        $splitCardTypes = explode(",",strtoupper(trim($csvLine[self::ROW_CARD_TYPE])));

                        if ( $csvLine[self::ROW_TICKET_FROM] == '*' || $csvLine[self::ROW_TICKET_FROM] == '') {
                            $ticketFrom = 0;
                        } else if (!$this->_isPositiveDecimalNumber($csvLine[self::ROW_TICKET_FROM]) ) {
                            $exceptions[] = Mage::helper('realexExtended')->__('Invalid Ticket Size From "%s" in the Row #%s',  $csvLine[self::ROW_TICKET_FROM], ($k+1));
                        } else {
                            $ticketFrom = (float)$csvLine[self::ROW_TICKET_FROM];
                        }

                        if ( $csvLine[self::ROW_TICKET_TO] == '*' || $csvLine[self::ROW_TICKET_TO] == '') {
                            $ticketTo = 10000000;
                        } else if (!$this->_isPositiveDecimalNumber($csvLine[self::ROW_TICKET_TO]) ) {
                            $exceptions[] = Mage::helper('realexExtended')->__('Invalid Ticket Size To "%s" in the Row #%s',  $csvLine[self::ROW_TICKET_TO], ($k+1));
                        } else {
                            $ticketTo = (float)$csvLine[self::ROW_TICKET_TO];
                        }

                        if ($csvLine[self::ROW_3D_SECURE] == '*' || $csvLine[self::ROW_3D_SECURE] == '' || $csvLine[self::ROW_3D_SECURE] == 0) {
                            $threedSecure = 0;
                        } else {
                            $threedSecure = 1;
                        }

                        if ($csvLine[self::ROW_3D_SECURE_PROFILE] == '*' || $csvLine[self::ROW_3D_SECURE_PROFILE] == '') {
                            $threedSecureProfile = '';
                        } else {
                            $threedSecureProfile = Mage::getModel('realexExtended/secureProfile')
                                ->getSelected(
                                    explode(",", trim($csvLine[self::ROW_3D_SECURE_PROFILE]))
                                );
                        }

                        if ($csvLine[self::ROW_SUB_ACCOUNT] == '*' || $csvLine[self::ROW_SUB_ACCOUNT] == '') {
                            $subAccount = '';
                        } else {
                            $subAccount = $csvLine[self::ROW_SUB_ACCOUNT];
                        }

                        if ($csvLine[self::ROW_RV_SUB_ACCOUNT] == '*' || $csvLine[self::ROW_RV_SUB_ACCOUNT] == '') {
                            $rvSubAccount = '';
                        } else {
                            $rvSubAccount = $csvLine[self::ROW_RV_SUB_ACCOUNT];
                        }

                        if ($csvLine[self::ROW_SETTLE_TYPE] == '' || $csvLine[self::ROW_SETTLE_TYPE] == '') {
                            $settleType = '';
                        } else {
                            if(strtolower(trim($csvLine[self::ROW_SETTLE_TYPE])) == 'capture'){
                                $settleType = Yoma_Realex_Model_Realex_Source_PaymentAction::ACTION_AUTHORIZE_CAPTURE;
                            }
                            if(strtolower(trim($csvLine[self::ROW_SETTLE_TYPE])) == 'authorize'){
                                $settleType = Yoma_Realex_Model_Realex_Source_PaymentAction::ACTION_AUTHORIZE;
                            }
                        }

                        $profileName = str_replace(' ','_',strtoupper(trim($csvLine[self::ROW_PROFILE_NAME])));

                        if ($csvLine[self::ROW_CVN_PROFILE] == '*' || $csvLine[self::ROW_CVN_PROFILE] == '') {
                            $cvnProfile = '';
                        } else {
                            $cvnProfile = Mage::getModel('realexExtended/cvn')
                                ->getSelected(
                                    explode(",", trim($csvLine[self::ROW_CVN_PROFILE]))
                                );
                        }

                        if ($csvLine[self::ROW_AVS_PROFILE] == '*' || $csvLine[self::ROW_AVS_PROFILE] == '') {
                            $avsProfile = '';
                        } else {
                            $avsProfile = Mage::getModel('realexExtended/avs')
                                ->getSelected(
                                    explode(",", trim($csvLine[self::ROW_AVS_PROFILE]))
                                );
                        }

                        if ( $csvLine[self::ROW_TSS_RESULT] == '*' || $csvLine[self::ROW_TSS_RESULT] == '') {
                            $tssResult = 0;
                        }else {
                            $tssResult = preg_replace('/\s+/', '',$csvLine[self::ROW_TSS_RESULT]);
                        }
                        /* remove check for decimal and conversion to float to allow for greater than and less than
                        } else if (!$this->_isPositiveDecimalNumber($csvLine[self::ROW_TSS_RESULT]) ) {
                            $exceptions[] = Mage::helper('realexExtended')->__('Invalid Overall Tss Result "%s" in the Row #%s',  $csvLine[self::ROW_TSS_RESULT], ($k+1));
                        } else {
                            $tssResult = (float)$csvLine[self::ROW_TSS_RESULT];
                        }
                        */
                        if ( $csvLine[self::ROW_TSS_RESULT_SPECIFIC] == '*' || $csvLine[self::ROW_TSS_RESULT_SPECIFIC] == '') {
                            $tssResultSpecific = 0;
                        } else {
                            $tssResultSpecific = Mage::helper('realexExtended')->encodeTssRuleSpecific($csvLine[self::ROW_TSS_RESULT_SPECIFIC]);
                        }

                        if ( $csvLine[self::ROW_3D_SECURE_RESULT] == '*' || $csvLine[self::ROW_3D_SECURE_RESULT] == '') {
                            $threeDSecureResult = '';
                        } else {
                            $threeDSecureResult = Mage::getModel('realexExtended/secure')
                                ->getSelected(
                                    explode(",", trim($csvLine[self::ROW_3D_SECURE_RESULT])   )
                                );
                        }

                        if ($csvLine[self::ROW_AUTO_SETTLE] !== '1') {
                            $settle = '0';
                        } else {
                            $settle = '1';
                        }
                        if ($csvLine[self::ROW_ORDER_REVIEW] !== '1') {
                            $review = '0';
                        } else {
                            $review = '1';
                        }

                        foreach ($splitBilling as $billing) {

                            $billing = trim($billing);
                            if (empty($countryCodesToIds) || !array_key_exists($billing, $countryCodesToIds)) {
                                $billingId = '*';
                                if ($billing != '*' && $billing != '') {
                                    $exceptions[] = Mage::helper('shipping')->__('Invalid Billing Country "%s" in the Row #%s', $billing, ($k + 1));
                                }
                            } else {
                                $billingId = $countryCodesToIds[$billing];
                            }

                            foreach ($splitShipping as $shipping) {

                                $shipping = trim($shipping);
                                if (empty($countryCodesToIds) || !array_key_exists($shipping, $countryCodesToIds)) {
                                    $shippingId = '*';
                                    if ($shipping != '*' && $shipping != '') {
                                        $exceptions[] = Mage::helper('shipping')->__('Invalid Shipping Country "%s" in the Row #%s', $shipping, ($k + 1));
                                    }
                                } else {
                                    $shippingId = $countryCodesToIds[$shipping];
                                }

                                foreach ($splitCurrency as $currency) {
                                    if(empty($currencyCodes)){
                                        $currencyId = '*';
                                    }else{
                                        $currencyId = $currency;
                                    }
                                    foreach($splitCardTypes as $cardType){
                                        if(empty($cardTypes)){
                                            $cardTypeId = '*';
                                        }else{
                                            $cardTypeId = $cardType;
                                        }
                                        $data[] = array(
                                            'website_id'=>$websiteId,
                                            'billing_country_id'=>$billingId,
                                            'shipping_country_id'=>$shippingId,
                                            'currency'=>$currencyId,
                                            'card_type'=>$cardTypeId,
                                            'ticket_size_from'=>$ticketFrom,
                                            'ticket_size_to'=>$ticketTo,
                                            'three_d_secure'=>$threedSecure,
                                            'three_d_secure_profile'=>$threedSecureProfile,
                                            'sub_account'=>$subAccount,
                                            'rv_sub_account'=>$rvSubAccount,
                                            'settle_type'=>$settleType,
                                            'profile_name'=>$profileName,
                                            'cvn_result'=>$cvnProfile,
                                            'avs_result'=>$avsProfile,
                                            'tss_result'=>$tssResult,
                                            'tss_rule_specific'=>$tssResultSpecific,
                                            'three_d_secure_result'=>$threeDSecureResult,
                                            'auto_settle'=>$settle,
                                            'order_review'=>$review,
                                        );

                                        $counter++;
                                    }//end card types
                                }//end currencey
                            }//end shipping
                            $dataStored = false;
                            if (!empty($exceptions)) {
                                break;
                            }
                            if($counter>1000) {
                                foreach($data as $k=>$dataLine) {
                                    try {
                                        $connection->insert($table, $dataLine);
                                    } catch (Exception $e) {
                                        $messageStr = Mage::helper('realexExtended')->__('Error# 302 - Duplicate Row #%s',
                                            ($k+1));

                                    }
                                }
                                Mage::helper('realexExtended')->updateStatus($session,count($data));
                                $counter = 0;
                                unset($data);
                                unset($dataDetails);
                                $dataStored = true;
                            }
                        }

                    }
                    if(empty($exceptions) && !$dataStored) {
                        foreach($data as $k=>$dataLine) {
                            try {
                                $connection->insert($table, $dataLine);
                            } catch (Exception $e) {
                                $messageStr = Mage::helper('realexExtended')->__('Error# 302 - Duplicate Row #%s',
                                    ($k+1));
                                $exceptions[] = $messageStr;


                            }
                        }
                        Mage::helper('realexExtended')->updateStatus($session,count($data));

                    }
                    if (!empty($exceptions)) {
                        throw new Exception( "\n" . implode("\n", $exceptions) );
                    }

                }
            }
        }
    }

    private function _getCsvValues($string, $separator=",")
    {
        $elements = explode($separator, trim($string));
        for ($i = 0; $i < count($elements); $i++) {
            $nquotes = substr_count($elements[$i], '"');
            if ($nquotes %2 == 1) {
                for ($j = $i+1; $j < count($elements); $j++) {
                    if (substr_count($elements[$j], '"') > 0) {
                        // Put the quoted string's pieces back together again
                        array_splice($elements, $i, $j-$i+1, implode($separator, array_slice($elements, $i, $j-$i+1)));
                        break;
                    }
                }
            }
            if ($nquotes > 0) {
                // Remove first and last quotes, then merge pairs of quotes
                $qstr =& $elements[$i];
                $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
                $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
                $qstr = str_replace('""', '"', $qstr);
            }
            $elements[$i] = trim($elements[$i]);
        }
        return $elements;
    }

    private function _isPositiveDecimalNumber($n)
    {
        return preg_match ("/^[0-9]+(\.[0-9]*)?$/", $n);
    }
}