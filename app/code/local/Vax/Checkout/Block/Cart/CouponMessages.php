<?php

/**
 * Vax Coupon Session Messages
 *
 * @copyright Vax Ltd 2014
 */
class Vax_Checkout_Block_Cart_CouponMessages extends Mage_Core_Block_Messages
{
    /**
     * Ignore Messages.
     *
     * @var boolean
     */
    protected $_ignoreMessages = false;

    /**
     * Overwrites the default method. Allows to ignore default messages.
     *
     * @param Mage_Core_Model_Message_Collection $messages
     * @param boolean                            $ignore
     *
     * @return Vax_Checkout_Block_Cart_CouponMessages|Mage_Core_Block_Messages
     */
    public function addMessages(Mage_Core_Model_Message_Collection $messages, $ignore = false)
    {
        if ($ignore || $this->_ignoreMessages) {
            return $this;
        }

        foreach ($messages->getItems() as $message) {
            $this->getMessageCollection()->add($message);
        }
        return $this;
    }

    /**
     * Overrides parent's method. Igonres default messages.
     *
     * @return void
     */
    public function _prepareLayout()
    {
        $this->_ignoreMessages = true;
        parent::_prepareLayout();
        $this->_ignoreMessages = false;

        foreach (Mage::getSingleton('vax_checkout/couponSession')->getMessages() as $message) {
            $this->addMessage($message);
        }
    }

    /**
     * Adds coupon messages.
     *
     * @return Vax_Checkout_Block_Cart_CouponMessages
     */
    public function initCouponMessage()
    {
        $storage = Mage::getSingleton('vax_checkout/couponSession');
        $this->addMessages($storage->getMessages(true));
        $this->setEscapeMessageFlag($storage->getEscapeMessages(true));
        $this->addStorageType('vax_checkout/couponSession');

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHtml($type = null)
    {
        $this->_prepareLayout();
        $html = '';

        foreach (Mage::getSingleton('vax_checkout/couponSession')->getMessages()->getItems() as $type => $message) {
                $html= '<div class="coupon-msg '.$message->getType().'-msg">'
                . $message->getText()
                    . '</div>';
        }

        Mage::getSingleton('vax_checkout/couponSession')->getMessages()->clear();

        return $html;
    }
}