<?php
class DirtDevil_Adminhtml_Block_Newsletter_Subscriber_Grid extends Mage_Adminhtml_Block_Newsletter_Subscriber_Grid
{

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        //subscribers names from the newsletter box in the footer and Newsletter page
        $this->addColumn('subscriber_firstname', array(
            'header'    => Mage::helper('newsletter')->__('Subscriber First Name'),
            'index'     => 'subscriber_firstname',
            'default'   =>    '----'
        ));

        $this->addColumn('subscriber_lastname', array(
            'header'    => Mage::helper('newsletter')->__('Subscriber Last Name'),
            'index'     => 'subscriber_lastname',
            'default'   =>    '----'
        ));
    }

}
