<?php

/**
 * Vax Checkout Onepage Form Delivery Address.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Form_Delivery_Address extends Vax_Checkout_Block_Onepage_Form_Address
{
    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->_getQuote()->getShippingAddress();
    }
}