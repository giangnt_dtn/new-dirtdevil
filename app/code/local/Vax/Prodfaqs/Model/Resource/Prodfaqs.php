<?php
class Vax_Prodfaqs_Model_Resource_Prodfaqs extends FME_Prodfaqs_Model_Mysql4_Prodfaqs
{
    public function loadFaqsByProduct($product_id, $publicOnly = true, $questionType = null){
	
		//Sorting condition for Product faqs
		$sort_by = Mage::helper('prodfaqs')->getProductFaqSorting();

		if($sort_by == 'helpful'):
		
			$condition = 'rating_stars';
			$cond_value = ' DESC';
		
		elseif($sort_by == 'order'):
			
			$condition = 'faq_order';
			$cond_value = ' ASC';
		
		else :	
			$condition = 'created_time';
			$cond_value = ' DESC';
		endif;
				
		$p_faqsTable = Mage::getSingleton('core/resource')->getTableName('prodfaqs_products');
	    $read_connection = Mage::getSingleton('core/resource')->getConnection('core_read');
	        
		if(!$publicOnly && Mage::helper('vaxfaqs')->isPrivateMode()):
		
			$select = $read_connection->select()->from(array('p_faq' => $p_faqsTable), array('*'))
		                                    ->joinLeft(array('faq' => $this->getTable('prodfaqs')), 'p_faq.faqs_id = faq.faqs_id')
		                                    ->joinLeft(array('topic' => $this->getTable('prodfaqs/topic')), 'faq.topic_id=topic.topic_id', array('topic_title' => 'topic.title'))
						    ->where('product_id =(?)', $product_id)
						    ->where('faq.status =(?)', 1)
						    ->where('parent_faq_id =(?)', 0)
						    ->order('topic_order ASC')
						    ->order(array($condition.$cond_value));
	        else:
			$select = $read_connection->select()->from(array('p_faq' => $p_faqsTable), array('*'))
		                                    ->joinLeft(array('faq' => $this->getTable('prodfaqs')), 'p_faq.faqs_id = faq.faqs_id')
		                                    ->joinLeft(array('topic' => $this->getTable('prodfaqs/topic')), 'faq.topic_id=topic.topic_id', array('topic_title' => 'topic.title'))
						    ->where('product_id =(?)', $product_id)
						    ->where('faq.status =(?)', 1)
						    ->where('parent_faq_id =(?)', 0)
						    ->where('visibility =(?)', 'public')
						    ->order('topic_order ASC')
						    ->order(array($condition.$cond_value));
		
		
		endif;
		
		// Filtering by question type
		if ($questionType) {
			$questionType = (array)$questionType;
			$select->where('question_type IN(?)', $questionType);
		}
	    $result = $read_connection->fetchAll($select);
		
		return $result;	
    }	
}