<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevilUk_Realex_Model_Transaction extends Yoma_Realex_Model_Transaction
{
    /**
     * Get transaction raw data
     *
     * @param string $key
     * @return mixed|null
     */
    public function getAdditionalInformation($key = null)
    {
        $transactionData = $this->getData('additional_information');

        if (is_string($transactionData)) {
            try {
                $transData = unserialize($transactionData);
                $transactionData = $transData;
            } catch (Exception $e) {
                // ignore unserialize notice
                Mage::logException($e);
            }
        }

        if (is_null($key)) {
            return $transactionData;
        }

        return array_key_exists($key, $transactionData) ? $transactionData[$key] : null;
    }
}
