<?php
/**
 * DirtDevil Catalog Product View.
 *
 * @copyright Vax Ltd 2015
 */
class DirtDevil_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_View {
	/**
	 * Checks whether the product is a spare.
     * Returns true if the product has an attribute set that says "spare" (UK),
     * or an attribute that flags it as a spare (DE)
	 *
	 * @return bool
	 */
	public function isSpare()
	{
		$attributeSetName = strtolower($this->getAttrSetName());

        $productType = '';
        if($this->getProduct()->hasAttribute('product_type')) {
            $productType = $this->getProduct()->getAttributeText('product_type');
        }
        if(( strpos($attributeSetName, 'spare') !== false) || ($productType == 'Spare') ) {
            return true;
        }
		return false;
	}

	/**
	 * Returns the name of the attribute set that product resides in.
	 *
	 * @return string
	 */
	public function getAttrSetName()
	{
		$attributeSetModel = Mage::getModel('eav/entity_attribute_set');
		$attributeSetModel->load($this->getProduct()->getAttributeSetId());

		$attributeSetName = $attributeSetModel->getAttributeSetName();
		return $attributeSetName;
	}
    /**
     * Returns whether the product's Specification section is empty - for UK
     *
     * @return boolean
     */
    public function emptySpec() {
        $_product   = $this->getProduct();
        $emptyLeft  = true;
        $emptyMid   = true;
        $emptyRight = true;

            //left column
            if( $_product->getData( 'weight' )
                || $_product->getData( 'suction' )
                || $_product->getData( 'uk_guarantee' )) {
                $emptyLeft = false;
            }

            //middle column
            if ( $_additional = $this->getSpecificationsMiddleTable() ) {
                foreach ($_additional as $_data) {
                    if ($_data['value']    != 'Nein' //German
                        && $_data['value'] != 'No'   //English
                        && $_data['value'] != 'Non'  //French
                        && $_data['value'] != 'no_selection'
                        && $_data['value'] != 'N/A' ){
                        $emptyMid = false;
                    }
                }
            }

            //right column (specs image)
            $dimensionImage = $_product->getData( 'dimensions_image' );
            if (! empty( $dimensionImage )
                && $dimensionImage !== 'no_selection' ){
                $emptyRight = false;
            }

        return ($emptyLeft && $emptyMid && $emptyRight);
    }

    /**
     * Retrieve product tax percentage
     *
     * @return double
     */
    public function getTaxPercentage(){
        $store = Mage::app()->getStore('default');
        $taxCalculation = Mage::getModel('tax/calculation');
        $request = $taxCalculation->getRateRequest(null, null, null, $store);
        $taxClassId = $this->getProduct()->getTaxClassId();
        $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));
        return $percent;
    }

    /**
     * Retrieve product formatted price according to German/English store view
     *
     * @return String
     */
    public function getFormattedPrice(){
        $_product   = $this->getProduct();
        $price = $this->__('Save %s', Mage::app()->getStore()->formatPrice(
            $_product->getPrice() - $_product->getFinalPrice(),
            false
        ));

        return $price;
    }
}
