<?php
include_once('Mage/Contacts/controllers/IndexController.php');

class Vax_SupportPortal_IndexController extends Mage_Contacts_IndexController
{
    public function indexAction() {
        $this->loadLayout();

        //set the form action to Mage::getUrl('*/*/send') if you want to send an email:
        if(Mage::getStoreConfig('contacts/contacts/supportportal_sendmail')) {
            $this->getLayout()->getBlock('contacts.contactForm')
                 ->setFormAction(Mage::getUrl('*/*/send'));
        }

        $this->getLayout()->getBlock('contacts.contactForm')
             ->setFormAction( Mage::getUrl('*/*/send') );

        $productId = (int)$this->getRequest()->getParam('product', 0);
        if ($productId) {
            $product = Mage::getModel('catalog/product')->load($productId);
            $this->getLayout()->getBlock('contacts.wrapper')->setProduct($product);
            $this->getLayout()->getBlock('contacts.contactForm')->setProduct($product);
        }

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function postAction()
    {
        //error/success messages are given by the validation functions
        try {
            $formType = $this->_validateForm();

            if($formType != null) {
                //      Record Contact details and query to database if all details are correct
                $this->_recordContactToDatabase($formType);
            }
            Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your email was successfully sent. Our team will contact you as soon as possible.'));
            $this->_redirect('*/*/');
            return;
        }
        catch (Exception $e) {
            Mage::logException($e);
            $this->_redirect('*/*/');
            return;
        }
    }

    public function sendAction()
    {
        $formType = $this->_validateForm();

        $this->_sendemail($formType);
        $this->_subscribeNewsletter();

        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your email was successfully sent. Our team will contact you as soon as possible.'));
        $this->_redirect('*/*/');
        return;
    }

    /**
     * Record a Newsletter subscription if the box is ticked
     */
    private function _subscribeNewsletter() {
        $keepInTouch = $this->getRequest()->getParam('keep_in_touch');
        $email = $this->getRequest()->getParam('email');
        $firstName = $this->getRequest()->getParam('first_name');
        $lastName = $this->getRequest()->getParam('surname');

        if(isset($keepInTouch)) {
            //create a new subscription with the email and names of the user
            $status = Mage::getModel( 'dirtdevil_newsletter/subscriber' )->subscribe( $email, $firstName, $lastName );

            //success messages
            if ( $status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE ) {
                Mage::getSingleton( 'customer/session' )->addSuccess( $this->__( 'A confirmation request has been sent to your email.' ) );
            } else {
                Mage::getSingleton( 'customer/session' )->addSuccess( $this->__( 'You have successfully subscribed to our newsletter.' ) );
            }
        }
    }


    /**
     * Validates both types of form
     * @return null or type of the form (owner or general)
     */
    protected function _validateForm()
    {
        //      Validate Customer Details
        //      Owner is a customer who checked the checkbox:
        //      default form type is without machine details
        $formType = 'general';
        if ($this->getRequest()->getParam('existing_customer')) {
            $formType = 'owner';
        } else {
            $formType = 'general';
        }

        $result = false;
        switch ($formType) {
            case 'general':
                $result = $this->_validateGeneralForm();
                break;

            case 'owner':
                $result = $this->_validateOwnerForm();
                break;
        }
        if (!$result) {
            return null;
        }
        //      if there are no errors the form type is retuned
        //      else the form type is null
        return $formType;
    }

    /**
     * Validates general form
     * @return bool
     */
    protected function _validateGeneralForm()
    {
        $requiredKeys = array(
            'first_name' => Mage::helper('contacts')->__('First Name'),
            'surname' => Mage::helper('contacts')->__('Surname'),
            'email' => Mage::helper('contacts')->__('Email'),
            'telephone' => Mage::helper('contacts')->__('Telephone'),
            'comment' => Mage::helper('contacts')->__('Message')
        );

        $notEmpty = new Zend_Validate_NotEmpty();

        foreach ($requiredKeys as $key => $label) {
            if (!$notEmpty->isValid($this->getRequest()->getParam($key))) {
                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('%s is required field.', $label));
                return false;
            }
        }

        $email = new Zend_Validate_EmailAddress();
        if (!$email->isValid($this->getRequest()->getParam('email'))) {
            Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('%s is not valid email address.', $this->getRequest()->getParam('email')));
            return false;
        }
        return true;
    }

    /**
     * Validates owner form
     * @return bool
     */
    protected function _validateOwnerForm()
    {
        $requiredKeys = array(
            'model_no' => Mage::helper('contacts')->__('Model Number'),
            'serial_no' => Mage::helper('contacts')->__('Serial Number'),
            'notf' => Mage::helper('contacts')->__('Subject / Nature of the fault'),
        );

        $notEmpty = new Zend_Validate_NotEmpty();

        foreach ($requiredKeys as $key => $label) {
            if (!$notEmpty->isValid($this->getRequest()->getParam($key))) {
                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('%s is required field.', $label));
                return false;
            }
        }
        return true;
    }

    /**
     * Sends email to general contact
     *
     * @param string $type
     * @return bool
     */
    protected function _sendemail($type)
    {
        $fieldList = array(
            'first_name' => Mage::helper('contacts')->__('First Name'),
            'surname' => Mage::helper('contacts')->__('Surname'),
            'email' => Mage::helper('contacts')->__('Email'),
            'telephone' => Mage::helper('contacts')->__('Telephone'),
            'comment' => Mage::helper('contacts')->__('Message'),
            'model_no' => Mage::helper('contacts')->__('Model Number'),
            'date_purchased' => Mage::helper('contacts')->__('Date Purchased'),
            'serial_no' => Mage::helper('contacts')->__('Serial Number'),
            'warranty_no' => Mage::helper('contacts')->__('Warranty Number'),
            'notf' => Mage::helper('contacts')->__('Subject / Nature of the fault'),
        );

        $name = $subject = '';
        $email = $this->getRequest()->getParam('email');

        switch ($type) {
            case 'general':
                $subject = Mage::helper('contacts')->__('New general enquiry');
                $name = $this->getRequest()->getParam('first_name') . ' ' . $this->getRequest()->getParam('surname');
                break;

            case 'owner':
                $subject = Mage::helper('contacts')->__('New enquiry from machine owner');
                $name = $this->getRequest()->getParam('first_name') . ' ' . $this->getRequest()->getParam('surname');
                break;
        }

        $salesData = new Varien_Object(array('name' => $name, 'email' => $email));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('contacts_email_email_template');
        $emailTemplate->setTemplateSubject($subject);

        $mailTo = Mage::getStoreConfig('trans_email/ident_general/email');
        $nameTo = Mage::getStoreConfig('trans_email/ident_general/name');

        $emailTemplate->setSenderName($name);
        $emailTemplate->setSenderEmail($email);

        $comment = '';

        foreach ($fieldList as $field => $label) {
            if ($this->getRequest()->getParam($field)) {
                $comment .= $label . ' :: ' . $this->getRequest()->getParam($field) . PHP_EOL;
            }
        }

        $salesData->setComment($comment);

        try {
            $emailTemplate->send($mailTo, $nameTo, array('data' => $salesData));
            return true;
        } catch (Exception $ex) {
            Mage::logException($ex);
        }

        return false;
    }

    protected function _recordContactToDatabase($type)
    {

        $post = $this->getRequest()->getPost();
        if ($post) {
            // Set keep in touch if not set to checked to be 0
            $post['keep_in_touch'] = isset($_POST["keep_in_touch"]) ? trim($_POST["keep_in_touch"]) : 0;



            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                // Set subject depending on lookup of posted subject from form
                $str_subject = "";
                switch ($post['subject']) {
                    case 1:
                        $str_subject = Mage::helper('contacts')->__("Website Sales Enquiry");
                        break;
                    case 2:
                        $str_subject = Mage::helper('contacts')->__("Website Support Enquiry");
                        break;
                    default:
                        $str_subject = Mage::helper('contacts')->__("Website General Enquiry");
                        break;
                }

                $postObject->setData("subject", $str_subject);

                $translate->setTranslateInline(true);

                // Store contact record in database! - bring on the custom module
                $contactmodel = Mage::getModel('vax_supportportal/contact');
                $contactmodel->setFirstName($post['first_name']);
                $contactmodel->setSurname($post['surname']);
                $contactmodel->setEmail($post['email']);
                if(isset($post['keep_in_touch'])) {
                    $contactmodel->setMailOpt(true);
                } else {
                    $contactmodel->setMailOpt(false);
                }
                $contactmodel->setFormData(serialize($post));
                $contactmodel->setStoreId(Mage::app()->getStore()->getStoreId());
                $contactmodel->save();

                $params = array();
                foreach ($this->getRequest()->getParams() as $key => $value) {
                    if (is_array($value)) {
                        $params[$key] = $value;
                    } else {
                        $params[$key] = urlencode($value);
                    }
                }
                $success = true;
                return;
            } catch (Exception $e) {
                $success = false;
                Mage::log($e);
            }

            if (!$success) {
                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Sorry. We were unable to submit your request. Please contact the Dirt Devil Careline on %s if you continue to encounter this error.', Mage::getStoreConfig('general/store_information/phone')));
                $params = array();

                foreach ($this->getRequest()->getParams() as $key => $value) {
                    if (is_array($value)) {
                        $params[$key] = $value;
                    } else {
                        $params[$key] = urlencode($value);
                    }
                }
                return;
            }
        } else {
            $params = array();
            foreach ($this->getRequest()->getParams() as $key => $value) {
                $params[$key] = urlencode($value);
            }
            $params = array();
            foreach ($this->getRequest()->getParams() as $key => $value) {
                $params[$key] = urlencode($value);
            }
        }
    }
}
