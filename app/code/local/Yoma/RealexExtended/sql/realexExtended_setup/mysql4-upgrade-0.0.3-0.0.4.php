<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `{$this->getTable('realexExtended/chanel')}`
    ADD COLUMN `three_d_secure_result` varchar(255) AFTER `tss_rule_specific`;
");
$installer->endSetup();