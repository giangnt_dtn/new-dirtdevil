<?php

/**
 * Vax Rest Quote Request Helper.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Rest_Helper_Request_Quote extends Mage_Core_Helper_Abstract
{
    /**
     * Mapes a quote from the request.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param mixed                  $request
     *
     * @return Mage_Sales_Model_Quote
     */
    public function mapQuote($quote, $request)
    {
        $this->_mapLines($quote, $request);

        $totalsAddress = Mage::helper('rest/model_quote')
                ->getTotalsAddress($quote)
                ->setCollectShippingRates(true)
                ->setCountryId('GB');

        $quote->setCase(isset($request['case']) ? (string) $request['case'] : null);
        $quote->setAccountId(isset($request['account_id']) ? (string) $request['account_id'] : null);
        $quote->setIsFOC(isset($request['is_foc']) ? (boolean) $request['is_foc'] : false);
        $quote->collectTotals();
        $quote->save();

        if (isset($request['billing_address'])) {
            $this->_mapAddress($quote->getBillingAddress(), $request['billing_address']);
        }

        if (isset($request['shipping_address'])) {
            $this->_mapAddress($quote->getShippingAddress(), $request['shipping_address']);
        }

        $this->_mapCustomer($quote, $totalsAddress);

        if ( ! $quote->isVirtual()) {
            $this->_mapShippingMethod($quote, $request);
        }

        if (isset($request['payment_details'])) {
            $this->_mapPaymentDetails($quote, $request);
        }

        if (isset($request['parent_order_id'])) {
            $quote->setParentOrderIncrementId($request['parent_order_id']);
        }

        return $this->_prepare($quote);
    }

    /**
     * Maps the quotes line from a request.
     *
     * @param mixed $quote
     * @param array $request
     *
     * @return void
     */
    protected function _mapLines($quote, $request)
    {
        $quote->removeAllItems();

        foreach ($request['lines'] as $quoteLine) {
            $product = Mage::helper('rest/model_product')->getProduct($quoteLine['product_id'], $quote->getStoreId());
            $request = new Varien_Object();
            $request->setQty($quoteLine['quantity']);
            $quote->addProduct($product, $request);
        }
    }

    /**
     * Maps an address from an array.
     *
     * @param mixed $address
     * @param array $request
     *
     * @return void
     */
    protected function _mapAddress($address, $request)
    {
        $address->setPrefix($request['title']);
        $address->setFirstname($request['first_name']);
        $address->setLastname($request['last_name']);

        $line1 = $request['address_line1'];
        $line2 = isset($request['address_line2']) ? $request['address_line2'] : null;

        $address->setStreet(array(
            $line1,
            $line2,
        ));

        $address->setCity($request['city']);
        $address->setRegion($request['county']);
        $address->setPostcode($request['post_code']);
        $address->setCountryId($request['country_code']);
        $address->setEmail($request['email_address']);
        $address->setTelephone($request['telephone_number']);
    }

    /**
     * Maps the quote's shipping method.
     *
     * @param mixed $quote
     * @param mixed $request
     *
     * @return void
     */
    protected function _mapShippingMethod($quote, $request)
    {
        $shippingAddress = $quote->getShippingAddress();

        if (isset($request['shipping_method'])) {
            $shippingAddress->setShippingMethod($request['shipping_method']);
        } else {
            $rate = $shippingAddress->getShippingRatesCollection()->getFirstItem();

            if ($rate) {
                $shippingAddress->setShippingMethod($rate['code']);
            }
        }

        $shippingAddress->collectTotals()->save();

        $quote->setIsFOC(isset($request['is_foc']) ? (boolean) $request['is_foc'] : false)
              ->setTotalsCollectedFlag(false)
              ->collectTotals()
              ->save();
    }

    /**
     * Maps the quote's payment details.
     *
     * @param mixed $quote
     * @param mixed $request
     *
     * @return void
     */
    protected function _mapPaymentDetails($quote, $request)
    {
        if ($quote->getGrandTotal() == 0) {
            $this->_mapFreePaymentDetails($quote);
        } else {
            $this->_mapChargedPaymentDetails($quote, $request['payment_details']);
        }
    }

    /**
     * Maps a the quote's payment details from an array.
     *
     * @param $quote
     *
     * @return void
     */
    protected function _mapFreePaymentDetails($quote)
    {
        $payment = $quote->getPayment();
        $payment->importData(array(
            'method' => 'free',
        ));

        Mage::helper('rest/model_quote')
            ->getTotalsAddress($quote)
            ->setPaymentMethod('free')
            ->setCollectShippingRates(true);
    }

    /**
     * Maps a the quote's payment details from an array.
     *
     * @param mixed $quote
     * @param mixed $request
     *
     * @return void
     */
    protected function _mapChargedPaymentDetails($quote, $request = array())
    {
        $payment = $quote->getPayment();
        $payment->importData(array(
            'method'       => 'realex',
            'cc_type'      => (string) $request['card_type'],
            'cc_number'    => (string) $request['card_number'],
            'cc_owner'     => (string) $request['card_holder_name'],
            'cc_cid'       => (string) $request['cvv'],
            'cc_exp_month' => (string) $request['expiry_month'],
            'cc_exp_year'  => (string) $request['expiry_year'],
        ));

        Mage::helper('rest/model_quote')
            ->getTotalsAddress($quote)
            ->setPaymentMethod('realex')
            ->setCollectShippingRates(true);
    }

    /**
     * Maps the customer attached to the quote
     *
     * @param mixed $quote
     * @param mixed $costsAddress
     *
     * @return void
     */
    protected function _mapCustomer($quote, $totalsAddress)
    {
        $quote->setCustomerPrefix($totalsAddress->getPrefix());
        $quote->setCustomerFirstname($totalsAddress->getFirstname());
        $quote->setCustomerLastname($totalsAddress->getLastname());
        $quote->setCustomerEmail($totalsAddress->getEmail());
    }

    /**
     * Prepares the quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _prepare(Mage_Sales_Model_Quote $quote)
    {
        $quote->setReservedOrderId(null);

        return $quote->deleteNominalItems()->reserveOrderId();
    }
}