<?php

/**
 * Vax Checkout Onepage Sidebar Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Sidebar extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the classes for a step.
     *
     * @param integer $index
     *
     * @return string
     */
    public function getStepClasses($index)
    {
        $classes  = 'step';
        $classes .= ' step-'.$index;

        if ($index == $this->getStepIndex()) {
            $classes .= ' step-active';
        }

        if ($index < $this->getStepIndex()) {
            $classes .= ' step-visited';
        }

        if ($index == ($this->getStepIndex() - 1)) {
            $classes .= ' step-prev';
        }

        if ($index == $this->getNumberOfSteps()) {
            $classes .= ' step-last';
        }

        if ($index == $this->getNumberOfSteps() && $this->hasOrderFailed()) {
            $classes .= ' step-error';
        }

        return $classes;
    }
}