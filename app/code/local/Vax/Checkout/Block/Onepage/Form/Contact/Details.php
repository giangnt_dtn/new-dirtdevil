<?php

/**
 * Vax Checkout Onepage Form Contact Details.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Form_Contact_Details extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns a list of title options.
     *
     * @return array
     */
    public function getTitleOptions()
    {
        return array(
            ''     => '[ Select Title ]',
            'Mrs'  => 'Mrs',
            'Mr'   => 'Mr',
            'Ms'   => 'Ms',
            'Miss' => 'Miss',
            'Dr'   => 'Dr',
            'Rev'  => 'Rev',
            'Prof' => 'Prof'
        );
    }

    /**
     * Returns the current customer title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getQuote()
                    ->getCustomerPrefix();
    }

    /**
     * Returns the current customer first name.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->getQuote()
                    ->getCustomerFirstname();
    }

    /**
     * Returns the current customer last name.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->getQuote()
                    ->getCustomerLastname();
    }

    /**
     * Returns the current customer email address.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->getQuote()
                    ->getCustomerEmail();
    }

    /**
     * Returns the current customer phone number.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->getQuote()
                    ->getShippingAddress()
                    ->getTelephone();
    }

    /**
     * Returns whether the newsletter opt in is required.
     *
     * @return string
     */
    public function isNewsletterRequired()
    {
        return $this->getQuote()
                    ->requiresNewsletter();
    }
}