<?php
class DirtDevil_DisableCheckout_Model_Observer
{
    /**
     * Sets redirection for the given controller to the previous action
     * 
     * @param Mage_Core_Controller_Varien_Action $controller
     * @return Mage_Core_Controller_Varien_Action 
     */
    public function goBack(Mage_Core_Controller_Varien_Action $controller)
    {
        $returnUrl = $controller->getRequest()->getParam('return_url');
        if ($returnUrl) {
            // clear layout messages in case of external url redirect
            if ($this->_isUrlInternal($returnUrl)) {
                $this->_getSession()->getMessages(true);
            }
            $controller->getResponse()->setRedirect($returnUrl);
        } else {
            $backUrl = $this->_getRefererUrl($controller);
            $controller->getResponse()->setRedirect($backUrl);
        }
        return $controller;
    }
    
    /**
     * Sets the product as not salable if the checkout is disabled in the 
     * current scope
     * 
     * @param Varien_Event_Observer $observer
     */
    public function disableProduct(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('dirtdevil_disable_checkout')->isCheckoutDisabled()) {
            return;
        }
        $observer->getProduct()->setIsSalable(false);
        $observer->getSalable()->getProduct()->setIsSalable(false);
        $observer->getSalable()->setIsSalable(false);
    }
    
    public function disableController(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('dirtdevil_disable_checkout')->isCheckoutDisabled()) {
            return;
        }
        
        $this->goBack($observer->getControllerAction());
    }
    
    /**
     * Identify referer url via all accepted methods (HTTP_REFERER, regular or base64-encoded request param)
     *
     * @return string
     */    
    protected function _getRefererUrl(Mage_Core_Controller_Varien_Action $controller)
    {
        $refererUrl = $controller->getRequest()->getServer('HTTP_REFERER');
        if ($url = $controller->getRequest()->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $controller->getRequest()->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }
        if ($url = $controller->getRequest()->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_URL_ENCODED)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }

        if (!$this->_isUrlInternal($refererUrl)) {
            $refererUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $refererUrl;
    }
    
    
    /**
     * Check url to be used as internal
     *
     * @param   string $url
     * @return  bool
     */    
    protected function _isUrlInternal($url)
    {
        if (strpos($url, 'http') !== false) {
            /**
             * Url must start from base secure or base unsecure url
             */
            if ((strpos($url, Mage::app()->getStore()->getBaseUrl()) === 0)
                || (strpos($url, Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, true)) === 0)
            ) {
                return true;
            }
        }
        return false;
    }    
}