<?php

/**
 * Catalog Price Rule Observer.
 */
class Vax_Rest_Model_CatalogRule_Observer extends Mage_CatalogRule_Model_Observer
{
    /**
     * {@inheritdoc}
     */
    public function processAdminFinalPrice($observer)
    {
        if (Mage::registry('is_api')) {
            return parent::processFrontFinalPrice($observer);
        }

        return parent::processAdminFinalPrice($observer);
    }
}