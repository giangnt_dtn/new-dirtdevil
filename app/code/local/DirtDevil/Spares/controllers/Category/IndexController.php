<?php
class DirtDevil_Spares_Category_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Index action - returns an array of a category's subcategories
     *
     * Responds to AJAX (XmlHttpRequest)
     * @todo If not AJAX then redirect to applicable category
     * @todo Redirect if category does not exist, or is not active
     */
    public function indexAction() {

        // Get the category ID from request, ensure integer
        $categoryId = (int) $this->getRequest()->getParam('id');

        // Get the location - registration/support/spares
        $location = $this->getRequest()->getParam('location');

        $storeId = (int) $this->getRequest()->getParam('store_id', Mage::app()->getStore()->getId());

        // Return early if parameter is invalid
        if ($categoryId === null || $categoryId == '') {

        }

        $category = null;

        if($storeId !== null && is_numeric($storeId)) {
            $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($categoryId);
        } else {
            $category = Mage::getModel('catalog/category')->load($categoryId);
        }

        $category->load($categoryId);

        // @todo Redirect if not AJAX
        if ( ! $this->getRequest()->isXmlHttpRequest()) {

        }

        // Return JSON encoded data
        $this->getResponse()->setBody(json_encode($this->getSubCategoryInfo($categoryId, $location)));
    }

    /**
     * Get a list of a category's subcategories along with public data
     *
     * @param integer categoryId The Id of the parent category
     * @param string $location
     * @return array
     */
    protected function getSubCategoryInfo($categoryId, $location) {

        $data = array();

        // Get the child categories
        $subCategories = Mage::getModel('catalog/category')->load($categoryId)->getChildrenCategories();

        // Build array of data for subcategories
        foreach ($subCategories as $subCategory) {
            $subCategory->load($subCategory->getId());

            $url = "#";

            // if this is a "category product" as opposed to a sub-category containing products
            if ($subCategory->getCategoryProductId()) {
                $product = Mage::getModel('catalog/product')->load($subCategory->getCategoryProductId());

                if($location == "registration") {
                    if ($product && $product->getSku()) {
                        $url = DIRECTORY_SEPARATOR . "registration" . DIRECTORY_SEPARATOR . "form" .
                            DIRECTORY_SEPARATOR . "get" . DIRECTORY_SEPARATOR . "sku" . DIRECTORY_SEPARATOR . $product->getSku();
                    }
                }
            }

            // fall back if no children and url hasn't changed
            if(!$subCategory->hasChildren() && $url === "#") {
                $url = $subCategory->getUrl();
            }

            $data[] = array(
                'id' => $subCategory->getId(),
                'name' => $subCategory->getName(),
                'url' => $url,
                'image' => Mage::helper('dirtdevil_spares')->getThumbnailImage($subCategory),
            );
        }

        return $data;
    }
}
