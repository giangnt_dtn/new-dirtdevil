<?php
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('newsletter/subscriber'),
        'subscriber_firstname',
        array(
            'type'     =>  Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => true,
            'default'  => null,
            'comment'  =>  'Subscriber First Name'
        )
    );
$installer->getConnection()
    ->addColumn(
        $installer->getTable('newsletter/subscriber'),
        'subscriber_lastname',
        array(
            'type'     =>  Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => true,
            'default'  => null,
            'comment'  =>  'Subscriber Last Name'
        )
    );
$installer->endSetup();
