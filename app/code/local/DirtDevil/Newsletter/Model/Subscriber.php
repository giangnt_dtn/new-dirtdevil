<?php

/**
 * Class DirtDevil_Newsletter_Model_Subscriber
 *
 * @method DirtDevil_Newsletter_Model_Subscriber setSubscriberFirstname(string $value)
 * @method DirtDevil_Newsletter_Model_Subscriber setSubscriberLastname(string $value)
 * @method DirtDevil_Newsletter_Model_Subscriber setSubscriberSource(string $value)
 * @method string getSubscriberFirstname()
 * @method string getSubscriberLastname()
 * @method string getSubscriberSource()
 * @method DirtDevil_Newsletter_Model_Subscriber setSubscriberExported(int $value)
 * @method int getSubscriberExported()
 * @method DirtDevil_Newsletter_Model_Subscriber setSubscriberAttributesChanged(int $value)
 * @method int getSubscriberAttributesChanged()
 *
 */
class DirtDevil_Newsletter_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{
    /**
     * Subscribes by email
     *
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param string $source
     * @throws Exception
     * @return int
     * Added Firstname and Lastname to subscription details
     */
    public function subscribe($email, $firstName = null, $lastName = null, $source = null)
    {

        $this->loadByEmail($email);
        $customerSession = Mage::getSingleton('customer/session');

        if (!$this->getId()) {
            $this->setSubscriberConfirmCode($this->randomSequence());
        }

        $isConfirmNeed       = (Mage::getStoreConfig(self::XML_PATH_CONFIRMATION_FLAG) == 1) ? true : false;
        $isOwnSubscribes     = false;
        $ownerId             = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();
        $isSubscribeOwnEmail = $customerSession->isLoggedIn() && $ownerId == $customerSession->getId();

        if (!$this->getId() || $this->getStatus() == self::STATUS_UNSUBSCRIBED
            || $this->getStatus() == self::STATUS_NOT_ACTIVE
        ) {
            if ($isConfirmNeed === true) {
                // if user subscribes own login email - confirmation is not needed
                $isOwnSubscribes = $isSubscribeOwnEmail;
                if ($isOwnSubscribes == true) {
                    $this->setStatus(self::STATUS_SUBSCRIBED);
                } else {
                    $this->setStatus(self::STATUS_NOT_ACTIVE);
                }
            } else {
                $this->setStatus(self::STATUS_SUBSCRIBED);
            }
            $this->setSubscriberEmail($email);

            if ($firstName != null && $lastName != null) {
                /*added subscriber names*/
                $this->setSubscriberFirstname($firstName);
                $this->setSubscriberLastname($lastName);
            }

            if ($source !== null) {
                $this->setSubscriberSource($source);
            }
        }

        // if subscriber information is changed, mark it to be updated in ET
        if ($this->getId()) {
            if ($firstName && $this->getSubscriberFirstname() != $firstName) {
                $this->setSubscriberFirstname($firstName);
                $this->setSubscriberAttributesChanged(1);
            }
            if ($lastName && $this->getSubscriberLastname() != $lastName) {
                $this->setSubscriberLastname($lastName);
                $this->setSubscriberAttributesChanged(1);
            }
            if ($source && $this->getSubscriberSource() != $source) {
                $this->setSubscriberSource($source);
                $this->setSubscriberAttributesChanged(1);
            }
        }

        if ($isSubscribeOwnEmail) {
            $this->setStoreId($customerSession->getCustomer()->getStoreId());
            $this->setCustomerId($customerSession->getCustomerId());
        } else {
            $this->setStoreId(Mage::app()->getStore()->getId());
            $this->setCustomerId(0);
        }

        $this->setIsStatusChanged(true);
        $success = false;

        try {
            $this->save();
            if ($isConfirmNeed === true
                && $isOwnSubscribes === false
            ) {
                $this->sendConfirmationRequestEmail();
            } else {
                $this->sendConfirmationSuccessEmail();
            }

            $success = true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        // send to exact target only once
        if ($success && Mage::helper('core')->isModuleEnabled('Vax_ExactTarget')) {
            if (Mage::helper('vax_exacttarget/subscriber')->isNewsletterRealTimeExportEnabled()) {
                $sent_subscriber_id = Mage::getSingleton('core/session')->getSentVaxSubscriberId();
                if ($sent_subscriber_id && !empty($sent_subscriber_id) && $sent_subscriber_id === $this->getId()) {
                    // already sent to exact target, skip
                } else {
                    if ($this->getSubscriberAttributesChanged()) {
                        Mage::helper('vax_exacttarget/subscriber')->updateSingleSubscriber($this->getId());
                    } else {
                        Mage::helper('vax_exacttarget/subscriber')->subscribeSingleNewsletter($this->getId());
                    }
                    Mage::getSingleton('core/session')->setSentVaxSubscriberId($this->getId());
                }
            }
        }

        if ($success) {
            return $this->getStatus();
        }
    }
}
