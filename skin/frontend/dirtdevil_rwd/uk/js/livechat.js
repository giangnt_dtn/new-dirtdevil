//this file contains all functions called from vax_rwd/default/template/livechat/commonScripts.phtml
function bindStartChat(button, deploymentId){
    if(jQuery(button).length) {
        jQuery(button).click(function () {
            liveagent.startChat(deploymentId)
        });
    }
}

function bindRejectChat(button, deploymentId){
    if(jQuery(button).length) {
        jQuery(button).click(function () {
            jQuery('.chatContainer').hide();
            liveagent.rejectChat(deploymentId)
        });
    }
}

//for everywhere
function datalayerButtonTracker(button, identifier) {
    if(jQuery(button).length) {
        jQuery(button).click(function(){
            if (typeof dataLayer == 'undefined') {
                return;
            }
            var chatEvent = {
                "event"    : "chatID",
                "Category" : "chatID",
                "Action"   : "" + window.location.href,
                "Label"    : "" + identifier
            };
            dataLayer.push(chatEvent);
        });
    }
}

//used in product.phtml and livechat.phtml
// hides the button when there's nobody to answer
function showWhenOnline(productDeploymentId, button) {
    if ( ! window._laq) {
        window._laq = [];
    }

    window._laq.push( function() {
        liveagent.showWhenOnline(
            productDeploymentId,
            document.getElementById(button)
        );
    });
}

function buttonCallback(e) {
    var button = document.getElementsByClassName('liveagent')[0];

    if (e == liveagent.BUTTON_EVENT.BUTTON_AVAILABLE) {
        button.style.display = '';
        button.style.left = '';
        button.style.top = '';
    }
    if (e == liveagent.BUTTON_EVENT.BUTTON_UNAVAILABLE ||
        e == liveagent.BUTTON_EVENT.BUTTON_ACCEPTED ||
        e == liveagent.BUTTON_EVENT.BUTTON_REJECTED) {
        button.style.display = 'none';
    }
}

//for everywhere
function chatWindow(endPoint, username, password, windowHeight, windowWidth, chatDelay, deploymentID, addCallBackFunction) {
    if(chatDelay > 0) {
        window.setTimeout(
            function () {
                doChatWindow(endPoint, username, password, windowHeight, windowWidth, chatDelay, deploymentID, addCallBackFunction)
            },
            chatDelay
        );
    } else {
        doChatWindow(endPoint, username, password, windowHeight, windowWidth, chatDelay, deploymentID, addCallBackFunction)
    }
}

function doChatWindow(endPoint, username, password, windowHeight, windowWidth, chatDelay, deploymentID, addCallBackFunction) {
    var identifier = new Date().getTime() + '.' + Math.random().toString(36).substring(5);

    if (typeof(_gaq) !== 'undefined') {
        _gaq.push(['_trackEvent', 'Salesforce Popup Engagement', 'Window Popup', window.location.toString()]);
    }

    if(addCallBackFunction) {
        liveagent.addButtonEventHandler(deploymentID, buttonCallback);
    }

    liveagent.addCustomDetail('Chat page origin', window.location.href);
    liveagent.addCustomDetail('identifier', identifier);

    liveagent.init(
        endPoint,
        username,
        password
    );
    liveagent.setChatWindowHeight(windowHeight);
    liveagent.setChatWindowWidth(windowWidth);
}
