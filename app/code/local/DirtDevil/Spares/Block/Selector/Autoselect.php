<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevil_Spares_Block_Selector_Autoselect extends Mage_Core_Block_Template
{
    /**
     * Returns search phrase from request.
     *
     * @return string
     */
    public function getSearchPhrase()
    {
        if (!$this->getRequest()->getParam('top-menu')) {
            return false;
        }

        $requestPhrase = $this->getSearchModel();
        if (!$requestPhrase || $requestPhrase == '-') {
            $requestPhrase = $this->getRequest()->getParam('search');
        }
        return $requestPhrase;
    }

    /**
     * Gets the selected spare model from request.
     *
     * @return string
     */
    public function getSearchModel()
    {
        return $this->getRequest()->getParam('model-dropdown');
    }
}
