<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class Vax_Checkout_Model_Sales_Quote extends Mage_Sales_Model_Quote
{
    /**
     * Newsletter Option Flag
     *
     * @var boolean
     */
    protected $_requiresNewsletter = false;

    /**
     * Sets the requires newsletter flag.
     *
     * @param boolean $requiresNewsletter
     *
     * @return Vax_Checkout_Model_Sales_Quote
     */
    public function setRequiresNewsletter($requiresNewsletter = false)
    {
        $this->_requiresNewsletter = (boolean) $requiresNewsletter;

        return $this;
    }

    /**
     * Returns the requires newsletter flag
     *
     * @return boolean
     */
    public function requiresNewsletter()
    {
        return $this->_requiresNewsletter;
    }

    /**
     * Returns the totals address.
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getTotalsAddress()
    {
        if ($this->isVirtual()) {
            return $this->getBillingAddress();
        }

        return $this->getShippingAddress();
    }

    /**
     * Returns total by total code.
     *
     * @param $totalCode
     * @return mixed
     */
    public function getTotal($totalCode)
    {
        foreach ($this->getTotals() as $total) {
            if ($total->getCode() == $totalCode) {
                return $total;
            }
        }
    }

    /**
     * Returns discount of the order (not applied to cart items).
     *
     * @return number
     */
    public function getOrderDiscount()
    {
        $totalDiscount = $this->getTotalDiscount(false, false);

        $itemDiscount = 0;
        foreach ($this->getAllVisibleItems() as $item) {
            $itemDiscount += $item->getItemDiscount();
        }

        $discount = $totalDiscount - $itemDiscount;

        return ($discount > 0.009) ? $discount : 0;
    }

    /**
     * Checks whether a customer is in an outlying region.
     *
     * @return boolean
     */
    public function isCustomerInOutlyingRegion($postcode = null)
    {
        if ( ! $postcode) {
            $postcode = $this->getShippingAddress()->getPostcode();
        }

        // Aberdeen, Falkirk, Belfast
        if (preg_match('/^AB|^FK|^BT/', $postcode)) {
            return true;
        }

        // Scottish Highlands and Islands
        if (preg_match('/^PA2[0-9]|^KA2[7-8]|^PH19|^PH2[0-9]|^IV1|^IV2[0-8]?/', $postcode)) {
            return true;
        }

        // Isle of Man, Isle of Wight
        if (preg_match('/^IM|^PO3[1-9]|^PO4[0-1]/', $postcode)) {
            return true;
        }

        // Channel Islands
        if (preg_match('/^GY|^JE/', $postcode)) {
            return true;
        }

        false;
    }

    /**
     * Returns the total quote discount.
     *
     * @return float
     */
    public function getTotalDiscount($includeSpecialPrice = true)
    {
        $discountTotal     = $this->getBaseSubtotal() - $this->getBaseSubtotalWithDiscount();
        $specialPriceTotal = 0;
        $regularPriceTotal = 0;

        foreach ($this->getAllItems() as $item) {

            if ($item->getParentItemId()) {
                continue;
            }

            $product           = Mage::getModel('catalog/product')->load($item->getProductId());
            $specialPriceTotal += ($product->getFinalPrice() * $item->getQty());
            $regularPriceTotal += ($product->getPrice() * $item->getQty());
        }

        if ($includeSpecialPrice) {
            $discountTotal += $regularPriceTotal - $specialPriceTotal;
        }

        return $discountTotal;
    }

    /**
     * Deletes the nominal items from the quote.
     *
     * @return Vax_Realex_Model_Quote
     */
    public function deleteNominalItems()
    {
        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->isNominal()) {
                $item->isDeleted(true);
            }
        }

        return $this;
    }

    /**
     * Checks whether the quote contains accessories only.
     *
     * @return boolean
     */
    public function containsOnlyAccessories()
    {
        $containsAccesoriesOnly = true;

        foreach ($this->getAllItems() as $item) {
            if ( ! $item->getProduct()->isAccessory()) {
                $containsAccesoriesOnly = false;
                break;
            }
        }

        return $containsAccesoriesOnly;
    }

    public function isCollectingFromWarehouse() {
        return false;
    }
}
