<?php

/**
 * Sales Force Config Helper.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Helper_Salesforce_Config
{
    /**
     * Returns the host connection param.
     *
     * @return string
     */
    public function getHost()
    {
        return (string) Mage::getStoreConfig('vax_registration/salesforce_config/host');
    }
}