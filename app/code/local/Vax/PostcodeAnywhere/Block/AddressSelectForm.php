<?php

/**
 * Vax Postcode Anywhere Address Select Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_PostcodeAnywhere_Block_AddressSelectForm extends Mage_Core_Block_Template
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->setData('template', 'postcodeanywhere/addressSelectForm.phtml');
        parent::_construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        if ( ! $this->hasSelectElement() ||! $this->getSelectElement() instanceof Mage_Core_Block_Html_Select) {
            return '';
        }

        return parent::_toHtml();
    }
}