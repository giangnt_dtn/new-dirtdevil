<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Form_Renderer_Fieldset_Tss
    extends Varien_Data_Form_Element_Text
{

    public function getValue(){

        $name = $this->getName();
        $values = json_decode($this->getData('value'));

        $temp =array();

        foreach($values as $rule){
            $temp[] = implode(':',$rule);
        }


        $this->setValue(implode(',',$temp));

        return $this->getData('value');
    }

 }