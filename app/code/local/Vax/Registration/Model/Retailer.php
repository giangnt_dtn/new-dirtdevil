<?php

/**
 * Retailer Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Retailer extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vax_registration/retailer');
    }
}