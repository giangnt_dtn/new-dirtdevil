<?php

/**
 * Checkout Checkout Factory.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Factory_Checkout
{
    /**
     * Returns the correct checkout
     *
     * @param string $method Payment Method
     *
     * @return Vax_Checkout_Model_Interface_Service_Checkout
     */
    public function get($method = null)
    {
        return new Vax_Checkout_Model_Service_Checkout;
    }
}