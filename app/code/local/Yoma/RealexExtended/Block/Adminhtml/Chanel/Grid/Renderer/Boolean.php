<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Grid_Renderer_Boolean extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $columnName = $this->getColumn()->getIndex();
        $value =  $row->getData($this->getColumn()->getIndex());

        switch($columnName){
            case 'avs_result':
                $value = implode(',',mage::getModel('realexExtended/avs')->getProfileOptions($value));
                break;
            case 'cvn_result':
                $value = implode(',',mage::getModel('realexExtended/avs')->getProfileOptions($value));
                break;
            case 'three_d_secure_profile':
                $value = implode(',',mage::getModel('realexExtended/secureProfile')->getProfileOptions($value,true));
                break;
            case 'three_d_secure_result':
                $value = implode(',',mage::getModel('realexExtended/secure')->getProfileOptions($value));
                break;
        }

        return $value;

       }

}