<?php

/**
 * OPC Order Success Response.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Response_Opc_Order_Failure extends Vax_Checkout_Model_Response_Opc_Order
{
    /**
     * {@inheritdoc}
     */
    public function getOpcResponse()
    {
        return array(
            'failure'   => array(
                'order' => array('id' => $this->_order->getIncrementId()),
                'msg'   => $this->_message,
            )
        );
    }
}