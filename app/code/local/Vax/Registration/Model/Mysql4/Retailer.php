<?php

/**
 * Retailer Mysql Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Mysql4_Retailer extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vax_registration/retailer', 'retailer_id');
    }
}