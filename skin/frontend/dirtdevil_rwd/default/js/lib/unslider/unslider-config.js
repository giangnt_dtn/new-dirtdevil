var unslider;
jQuery(document).ready(function($) {
    unslider = $('.banner').unslider({
        keys: true,
        dots: true,
        speed: 800,
        delay: 5000,
        fluid: true,
        autoplay: true,
    });
//was speed 500, delay 6000
    $('.unslider-arrow').click(function() {
        var fn = this.className.split(' ')[1];

        unslider.data('unslider')[fn]();
    });

    var $sliderctrl = $('.banner-controls').show();

    $sliderctrl.show();
    function is_touch_device() {
        return (('ontouchstart' in window)
        || (navigator.MaxTouchPoints > 0)
        || (navigator.msMaxTouchPoints > 0)
        || (window.DocumentTouch && document instanceof DocumentTouch));
    }

    if (is_touch_device()) {
        $sliderctrl.show();
    } else {

       /* $('.banner').mouseenter(function() {
            $sliderctrl.show();
        });

        $('.banner').mouseleave(function() {
            $sliderctrl.hide();
        });

        $('.banner-controls').mouseenter(function() {
            $sliderctrl.show();
        });
        */
        $sliderctrl.show();
    }
});