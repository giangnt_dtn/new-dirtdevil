(function($) {
    $.fn.AjaxLogin = function(options) {

        var opts = $.extend({}, $.fn.AjaxLogin.defaults, options);

        return start();

        /**
         * Init.
         */
        function start() {
            // Ajax calls
            sendEvents();
        }

        /**
         * Registration or login request by user.
         */
        function sendEvents() {
            // Click on login in Login window
            $('.login-window button').on('click', function() {
                setDatas('login');
                validateDatas('login');
                if (opts.errors != '') {
                    setError(opts.errors, 'login');
                    $('.ajaxlogin-temp-error').show();
                } else {
                    callAjaxControllerLogin();
                }
                return false;
            });
        }


        /**
         * Show or hide the Loader with effects.
         * @param string windowName
         * @param int step
         */
        function animateLoader(windowName, step) {
            // Start
            if (step == 'start') {
                $('.ajaxlogin-loader').fadeIn();
            } else {
                $('.ajaxlogin-loader').fadeOut();
            }
        }


        /**
         * Validate user inputs.
         * @param string windowName
         */
        function validateDatas(windowName) {
            opts.errors = '';

            if (windowName == 'login') {
                // There is no email address
                if (opts.email.length < 1) {
                    opts.errors = opts.errors + 'noemail,'
                    // It is not email address
                } else if (validateEmail(opts.email) != true) {
                    opts.errors = opts.errors + 'wrongemail,'
                }

                // There is no password
                if (opts.password.length < 1) {
                    opts.errors = opts.errors + 'nopassword,'
                    // Too long password
                } else if (opts.password.length > 16) {
                    opts.errors = opts.errors + 'wronglogin,'
                }
            }
        }

        /**
         * Email validator. Retrieve TRUE if it is an email address.
         * @param string emailAddress
         * @returns {boolean}
         */
        function validateEmail(emailAddress) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (filter.test(emailAddress)) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Save user input data to property for ajax call.
         * @param string windowName
         */
        function setDatas(windowName) {
            if (windowName == 'login') {
                opts.email = $('#email').val();
                opts.password = $('#password').val();
            }
        }

        /**
         * Load error messages into windows and show them.
         * @param string errors Comma separated.
         * @param string windowName
         */
        function setError(errors, windowName) {
            $('.ajaxlogin-error').text('');

            $('.ajaxlogin-error').hide();

            var errorArr = new Array();
            errorArr = errors.split(',');

            var length = errorArr.length - 1;

            for (var i = 0; i < length; i++) {
                var errorText = $('.ytmpa-' + errorArr[i]).text();

                $('.err-' + errorArr[i]).text(errorText);
            }

            $('.ajaxlogin-error').fadeIn();
        }


        /**
         * Ajax call for login.
         */
        function callAjaxControllerLogin() {
            // If there is no another ajax calling
            if (opts.stop != true){

                opts.stop = true;

                // Load the Loader
                animateLoader('login', 'start');

                // Send data
                var ajaxRegistration = jQuery.ajax({
                    url: opts.controllerUrl,
                    type: 'POST',
                    data: {
                        ajax : 'login',
                        email : opts.email,
                        password : opts.password
                    },
                    dataType: "html"
                });
                // Get data
                ajaxRegistration.done(function(msg) {
                    // If there is error
                    if (msg != 'success'){
                        setError(msg, 'login');
                        // If everything are OK
                    } else {
                        opts.stop = false;
                        // Redirect
                        if (opts.redirection == '1') {
                            window.location = opts.profileUrl;
                        } else {
                            window.location.reload();
                        }
                    }
                    animateLoader('login', 'stop');
                    opts.stop = false;
                });
                // Error on ajax call
                ajaxRegistration.fail(function(jqXHR, textStatus, errorThrown) {
                    opts.stop = false;
                    animateLoader('login', 'stop');
                });
            }
        }




    };

    $.fn.AjaxLogin.defaults = {
        redirection : '0',
        windowSize : '',
        stop : false,
        controllerUrl : '',
        profileUrl : '',
        autoShowUp : '',
        errors : '',
        firstname : '',
        lastname : '',
        newsletter : 'no',
        email : '',
        password : '',
        passwordsecond : '',
        licence : 'no'
    };

})(jQuery);