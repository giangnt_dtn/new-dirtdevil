// Product registration form
// add the model number automatically when the user selects a model name
$j(document).ready(function(){
    var selectProductName = $j('.main-container select#model-dropdown');
    if(selectProductName != null) {
        jQuery(selectProductName).on('change', function(e) {
            var value = e.target.value;
            if(value != '-' &&  $j('#search-reg').length) {
                $j('[name="sku"]').val(value);
           }
        });
    }

    name = Cookies.get('regName');
    email =Cookies.get('regEmail');
    from = Cookies.get('regFrom');
    sku = Cookies.get('regSku');



    if(name === 'undefined') {
    } else {
        if (name) {
            $j('[name="first_name"]').val(name);
            Cookies.remove('regName');
        }
        if (email) {
            $j('[name="email"]').val(email);
            Cookies.remove('regEmail');
        }
        if (from) {
            $j('[name="pur_from"]').val(from);
            Cookies.remove('regFrom');
        }
        if (sku) {
            $j('[name="sku"]').val(sku);
            Cookies.remove('regSku');
        }
    }

});
