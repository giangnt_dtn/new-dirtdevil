<?php

class Vax_SalesRule_Model_Resource_Freegift extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('vax_salesrule/free_gift', '');
    }

    public function saveFreeGifts($ruleId, array $actions)
    {
        $data = [];
        foreach ($actions['conditions'] as $condition) {
            if ($condition['value'] && $condition['free_gifts']) {
                $productId = Mage::getResourceSingleton('catalog/product')->getIdBySku($condition['value']);
                if ($productId) {
                    $key = $ruleId . '_' . $productId;
                    if (!isset($data[$key])) {
                        $data[$key] = [
                            'rule_id'    => $ruleId,
                            'product_id' => $productId,
                        ];

                        $freeGiftSkus = explode(',', $condition['free_gifts']);
                        $freeGiftIds  = [];
                        foreach ($freeGiftSkus as $sku) {
                            $freeGiftIds[] = Mage::getResourceSingleton('catalog/product')->getIdBySku(trim($sku));
                        }
                        $data[$key]['gift_product_ids'] = implode(',', $freeGiftIds);
                    }
                }
            }
        }

        $this->_getWriteAdapter()->delete($this->getMainTable(), 'rule_id = ' . $ruleId);
        if (count($data)) {
            $this->_getWriteAdapter()->insertMultiple($this->getMainTable(), $data);
        }
    }
}