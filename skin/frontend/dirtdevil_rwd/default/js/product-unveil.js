//only unveils product main images once thumbnail has been clicked
jQuery(document).ready(function(){
    jQuery('.more-views .product-image-thumbs li a').click(function(){
        var index = jQuery(this).parents("li").index();
        var elementSelector = ".product-img-box .product-image .product-image-gallery img#image-" + index;
        jQuery(elementSelector).unveil();
    });
});