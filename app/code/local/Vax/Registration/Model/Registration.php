<?php

/**
 * Vax Registration Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Registration extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_init('vax_registration/registration');
    }

    /**
     * Checks whether the customer has opted in for contact.
     *
     * @return boolean
     */
    public function hasOptedIn()
    {
        return (boolean) $this->getMailOpt();
    }

    /**
     * Returns the registered product.
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getRegisteredProduct()
    {
        if ( ! $this->_registeredProduct) {
            $this->_registeredProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->getModel());
        }

        return $this->_registeredProduct;
    }

    /**
     * Returns whether the registered product is saleable on the web.
     *
     * @return boolean
     */
    public function isRegisteredProductSaleableOnWeb()
    {
        return $this->getRegisteredProduct()->getInventoryDiscontinued() == 0;
    }

    /**
     * Returns the full name of the customer.
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getFirstName().' '.$this->getSurname();
    }
}