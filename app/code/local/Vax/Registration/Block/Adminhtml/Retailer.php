<?php

/**
 * Retailer Adminhtml Block.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Block_Adminhtml_Retailer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'vax_registration';
        $this->_controller = 'adminhtml_registration_retailer';
        $this->_headerText = $this->__('List Retailers');

        parent::_construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if( ! Mage::getSingleton('admin/session')->isAllowed('registration/retailer/new')) {
            $this->_removeButton('add');
        }
    }
}