<?php
/**
 * Dirt Devil Product View Specification Block.
 *
 * @copyright Vax Ltd 2015
 */
class DirtDevil_Catalog_Block_Product_View_Tab_Specification extends Mage_Catalog_Block_Product_View_Attributes {

	/*  The following specification Attributes are disabled here because there are shown someplace else:
							 - features and what's in the box are in other sections
							 - weight, suction power and warranty are in the left table
							 - width, height and depth are in the right table with the dimensions image
						*/
	private static $_unusableHere = array(
		"subtitle",
		"product_features",
        "display_dimensions ",
        "product_fiche",
		"whats_in_the_box",
        "whats_in_the_box_image",
        "whats_in_the_box_numbered",
		"weight",
		"suction",
		"warranty",
		"width",
		"height",
		"depth",
		"eco_hard_floor_rating",
		"eco_overall_rating",
		"eco_carpet_rating",
		"eco_filtration_rating",
		"youtube_video_1",
		"youtube_thumb_1",
		"youtube_video_2",
		"youtube_thumb_2",
		"youtube_video_3",
		"youtube_thumb_3",
	);

	/**
	 * Checks whether attributes exist.
	 *
	 * @return boolean
	 */
	public function hasAttributes() {
		return $this->getNumberOfAttributes() > 0;
	}

	/**
	 * Returns the number of attributes.
	 *
	 * @return integer
	 */
	public function getNumberOfAttributes() {
		try {
			$additionalData = $this->getAdditionalData();
			return count( $additionalData );
		} catch (Exception $e) {
			Mage::logException($e);
			return 0;
		}
	}

	/**
	 * Returns an array of the Attribute code, attribute label and attribute value
	 * for the Specifications Attributes in the middle Specifications table on the Product Page
	 *
	 * @return Array
	 */
	public function getSpecificationsMiddleTable() {
		return $this->getAdditionalData(self::$_unusableHere);
	}
}
