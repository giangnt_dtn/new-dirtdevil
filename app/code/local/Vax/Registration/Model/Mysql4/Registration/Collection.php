<?php

/**
 * Registration Mysql Collection Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Mysql4_Registration_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init('vax_registration/registration');
    }

    /**
     * Adds a store filter to the collection.
     *
     * @param integer|null $storeId
     *
     * @return \Vax_Registration_Model_Mysql4_Registration_Collection
     */
    public function addStoreFilter($storeId = null)
    {
        if (Mage::app()->isSingleStoreMode()) {
            return $this;
        }

        if (null === $storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        $this->getSelect()->where('store_id=' . $storeId);

        return $this;
    }
}