$j(document).ready(function () {
    var image =  $j('.product-image-gallery .gallery-image.visible');
    image.after( "<div class='close'>X</div>");
    $j('.product-image-gallery .close').hide();
    $j(document).click(function (e) {
        if ($j('.zoom').length) {

            var zoomicon = $j('.zoom');

            var lightbox = $j('.zoomed');

            //click on the zoom icon to toggle the lightbox
            if (zoomicon.is(e.target)) {
                minizoomtoggle();
            }
            //click outside the lightbox to close it
            else if (!lightbox.is(e.target) // if the target of the click isn't the container...
                && lightbox.has(e.target).length === 0) // ... nor a descendant of the container
            {
                minizoomff();
            }
        }
    });
});
//closing the lightbox
function minizoomtoggle(image) {
    var image =  $j('.product-image-gallery .gallery-image.visible');
    //check if a video is being shown instead of an image and do nothing if so
    if( ! $j('.visible.video').length ) {
        image.toggleClass('zoomed');
        //close button
        $j('.product-image-gallery .close').toggle();
        //return the image to its original position
        image.css('left', 0);
        //center the image if it has been zoomed
        $j('.zoomed').center();
    }
}
//opening/closing the lightbox
function minizoomff() {
    if ($j('.zoomed').length ) {
        var image =  $j('.product-image-gallery .gallery-image.visible');
        image.removeClass('zoomed');
        //close button
        $j('.product-image-gallery .close').hide();
        //return the image to its original position
        image.css('left', 0);
    }
}
//center the lightbox on browsers that don't support css: transform
jQuery.fn.center = function () {
    this.css("left", Math.max(0, (($j(window).width() - $j(this).outerWidth()) / 2) +
    $j(window).scrollLeft()) + "px");
    return this;
};
