 <?php

/**
 * Product Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_Rest_Model_Catalog_Api2_Product_Rest_Admin_V1 extends Mage_Catalog_Model_Api2_Product
{
    /**
     * Cached Attribute Sets.
     *
     * @var array
     */
    protected $attributeSets = array();

    /**
     * Retrieves all products.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $productData = array();
        $products    = $this->_loadCollection();

        foreach ($products as $product) {
            $productData[] = $this->_buildProductResponse($product);
        }

        return $productData ;
    }

    /**
     * Retrieves the product collection.
     *
     * @return array
     */
    protected function _loadCollection()
    {
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addStoreFilter($this->_getStore());
        $collection->addAttributeToSelect('*');
        $collection->setPage($this->getRequest()->getParam('page', 1), 100);

        return $collection->load();
    }

    /**
     * Builds a single product response.
     *
     * @param mixed $product
     *
     * @return array
     */
    protected function _buildProductResponse($product)
    {
        return array(
            'id'                => $product->getId(),
            'base_price'        => $product->getPrice(),
            'current_price'     => $this->_getCurrentProductPrice($product),
            'description'       => $product->getDescription(),
            'type'              => $product->getTypeId(),
            'is_rtb_only'       => (boolean) $product->isRTBOnly(),
            'is_sale_disabled'  => $product->isSaleDisabled(),
            'is_machine'        => $product->isUkMachine(),
            'is_fee'            => $product->isFee(),
            'is_instalment'     => false,
            'is_discontinued'   => $product->isDiscontinued(),
            'is_virtual'        => $product->isVirtual(),
            'name'              => $product->getName(),
            'min_quantity'      => max(array(1, Mage::getModel("cataloginventory/stock_item")->loadByProduct($product->getId())->getMinSaleQty())),
            'quantity'          => $product->getQuantity(),
            'short_description' => $product->getShortDescription(),
            'sku'               => $product->getSku(),
            'small_image_url'   => $product->getSmallImageUrl(178, 178),
            'url_key'           => $product->getUrlKey(),
            'status'            => $product->getStatus(),
            'uk_product_no'     => $product->getUkProductNo(),
            'visibility'        => $product->getVisibility(),
            'attributes'        => $this->_buildAttributes($product),
            'related_products'  => $product->getRelatedProductIds(),
            'categories'        => $this->_buildCategories($product),
        );
    }

    /**
     * Builds the products attributes.
     *
     * @param mixed $product
     *
     * @return array
     */
    protected function _buildAttributes($product)
    {
        $attributes = array();

        foreach ($this->_getAttributeGroups($product->getAttributeSetId()) as $groupID => $name) {
            $attributes[$name] = $this->_formatAttributes($product, $product->getAttributes($groupID));
        }

        return $attributes;
    }

    /**
     * Retrieves the attribute groups for an attribute set.
     *
     * @param mixed $attributeSetID
     *
     * @return array
     */
    protected function _getAttributeGroups($attributeSetID)
    {
        if ( ! isset($this->attributeSets[$attributeSetID])) {
            $attributeGroupData = array();
            $attributeGroups          = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($attributeSetID)
                ->setSortOrder('sort_order')
                ->load();

            foreach ($attributeGroups as $attributeGroup) {
                $attributeGroupData[$attributeGroup['attribute_group_id']] = $attributeGroup['attribute_group_name'];
            }

            $this->attributeSets[$attributeSetID] = $attributeGroupData;
        }

        return $this->attributeSets[$attributeSetID];
    }

    /**
     * Formats a set of attributes for the response.
     *
     * @param mixed $product    Product
     * @param array $attributes Attributes
     *
     * @return array
     */
    protected function _formatAttributes($product, array $attributes = array())
    {
        $formattedAttributes = array();

        foreach ($attributes as  $attribute) {
            if ($attribute->getIsVisibleOnFront()) {
                $formattedAttributes[$attribute->getAttributeCode()] = array(
                    'label' => $attribute->getFrontendLabel(),
                    'value' => $product->getResource()->getAttribute($attribute->getAttributeCode())->getFrontend()->getValue($product),
                    'type'  => $attribute->getBackendType(),
                );
            }
        }

        return $formattedAttributes;
    }

    /**
     * Builds a response from the products categories.
     *
     * @param $product mixed
     *
     * @return array
     */
    protected function _buildCategories($product)
    {
        $categories = array();
        $collection = $product->getCategoryCollection();
        $collection->setStoreId($this->_getStore()->getId());
        $collection->addAttributeToSelect('name');

        foreach ($collection->load() as $category) {
            $categories[] = $category->getName();
        }

        return $categories;
    }

    /**
     * Returns the current price of the product factoring in special price.
     *
     * @param mixed $product
     *
     * @return float
     */
    protected function _getCurrentProductPrice($product)
    {
        $today    = new \DateTime(date('Y-m-d H:i:s', mktime(0, 0, 0)));
        $fromDate = new \DateTime($product->getSpecialFromDate());
        $toDate   = new \DateTime($product->getSpecialToDate());

        if ($product->getSpecialPrice()) {
            if ($fromDate <= $today && $today <= $toDate) {
                return $product->getSpecialPrice();
            }
        }

        return $product->getPrice();
    }
}