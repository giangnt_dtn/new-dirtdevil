<?php
class Vax_Prodfaqs_Block_Faq_RelatedTopics extends Vax_Prodfaqs_Block_Faq_Abstract
{
    public function getRelatedTopics()
    {
        return $this->getRelatedFaqsByProduct()
                    ->addStatusFilter(1)
                    ->addFieldToFilter('visibility', 'public')
                    ->addFieldToFilter('question_type', 'maintenance_item');
    }
}