<?php
$this->startSetup();

$this->run("
    CREATE TABLE `{$this->getTable('realexExtended/chanel')}` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `billing_country_id` varchar(2),
        `shipping_country_id` varchar(2),
        `currency` varchar(3),
        `card_type` varchar(10),
        `ticket_size_from` decimal(12),
        `ticket_size_to` decimal(12),
        `three_d_secure` int(11),
        `three_d_secure_profile` int(11),
        `sub_account` varchar(50),
        `profile_name` varchar(50),
         `website_id` int(11),
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$this->run("
  ALTER TABLE `{$this->getTable('realexExtended/chanel')}`
    ADD UNIQUE (`billing_country_id`, `shipping_country_id`, `currency`, `card_type`, `ticket_size_from`, `ticket_size_to`, `website_id`);
");

$this->endSetup();

