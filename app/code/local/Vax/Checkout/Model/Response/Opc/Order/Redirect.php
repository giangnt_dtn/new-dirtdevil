<?php

/**
 * OPC Order Success Response.
 *
 * @copyright Vax Ltd
 */
class Vax_Checkout_Model_Response_Opc_Order_Redirect extends Vax_Checkout_Model_Response_Opc_Order
{
    /**
     * {@inheritdoc}
     */
    public function getOpcResponse()
    {
        return array(
            'redirect'   => array(
                'acsURL' => $this->_url ? $this->_url : $this->_order->getQuote()->getPayment()->getOrderPlaceRedirectUrl(),
                'order'  => array('id' => $this->_order->getIncrementId()),
                'msg'    => $this->_message,
            )
        );
    }
}