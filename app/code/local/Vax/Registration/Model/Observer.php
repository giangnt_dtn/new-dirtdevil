<?php

/**
 * Vax Registration Observer.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Registration_Model_Observer
{
    /**
     * Adds The UK Guarantee Attribute.
     *
     * @param type $observer
     */
    public function addFlatProductAttributes($observer)
    {
        $indexer   = $observer->getIndexer();
        $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'uk_guarantee');

        $indexer->addAttributeCode($attribute);
    }
}