// Calculate header height and sticky container height when having header info banner
function calHeaderHeight() {
    var headerHeight = jQuery('.page-header-container').height() + jQuery('header .widget').height();
    jQuery('body:not(.checkout-onepage-index) header').css('height', headerHeight);
    jQuery('body:not(.checkout-onepage-index) header .sticky-container').css('height', headerHeight);
    jQuery('body:not(.checkout-onepage-index) .breadcrumbs .sticky-container').css('top', headerHeight);
}

jQuery(document).ready(function () {
    if (jQuery('.header-info-banner').length) {
        calHeaderHeight();
    }
});

jQuery(window).resize(function () {
    if (jQuery('.header-info-banner').length) {
        calHeaderHeight();
    }
});
