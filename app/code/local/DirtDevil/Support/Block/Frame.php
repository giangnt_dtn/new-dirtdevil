<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevil_Support_Block_Frame extends Mage_Core_Block_Template
{
    protected $_contentHtml;

    /**
     * Returns block content.
     *
     * @return string
     */
    public function getContentHtml()
    {
        if (!$this->_contentHtml) {
            if ($this->hasContentTemplate()) {
                try {
                    $this->_contentHtml = $this->getLayout()->createBlock('core/template')->setTemplate($this->getContentTemplate())->toHtml();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

            if ($this->hasCmsBlock()) {
                try {
                    $_block = Mage::getModel('cms/block')->load($this->getCmsBlock());
                    $this->setCmsBlockTitle($_block->getTitle());
                    $_filter = Mage::getModel('core/email_template_filter');
                    $this->_contentHtml = $_filter->filter($_block->getContent());
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }

        return $this->_contentHtml;
    }

    /**
     * Returns section title.
     *
     * @return mixed
     */
    public function getTitle()
    {
        // Init content html where title can be loaded from CMS block
        $this->getContentHtml();

        if ($this->_getData('title')) {
            return $this->_getData('title');
        } elseif ($this->_getData('cms_block_title')) {
            return $this->_getData('cms_block_title');
        }
    }


}