<?php
$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('realexExtended/chanel')}` CHANGE COLUMN `payment_method` `auto_settle` int(11) DEFAULT NULL;
");
$installer->endSetup();