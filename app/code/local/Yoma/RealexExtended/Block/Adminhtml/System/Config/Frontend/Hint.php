<?php
class Yoma_RealexExtended_Block_Adminhtml_System_Config_Frontend_Hint
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Location of our hint template file.
     *
     * @var string $_template
     */
    protected $_template = 'realexExtended/system/config/frontend/hint.phtml';

    /**
     * Render fieldset html.
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->toHtml();
    }
}