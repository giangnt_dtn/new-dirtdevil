<?php
$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('realexExtended/chanel')}` ADD COLUMN `rv_sub_account` varchar(50) AFTER `sub_account`, CHANGE COLUMN `profile_name` `profile_name` varchar(50) DEFAULT NULL AFTER `rv_sub_account`, CHANGE COLUMN `website_id` `website_id` int(11) DEFAULT NULL AFTER `profile_name`, CHANGE COLUMN `tss_result` `tss_result` varchar(255) DEFAULT NULL AFTER `website_id`, CHANGE COLUMN `avs_result` `avs_result` varchar(255) DEFAULT NULL AFTER `tss_result`, CHANGE COLUMN `cvn_result` `cvn_result` int(11) DEFAULT NULL AFTER `avs_result`, CHANGE COLUMN `tss_rule_specific` `tss_rule_specific` varchar(255) DEFAULT NULL AFTER `cvn_result`, CHANGE COLUMN `payment_method` `payment_method` varchar(255) DEFAULT NULL AFTER `tss_rule_specific`, CHANGE COLUMN `three_d_secure_result` `three_d_secure_result` varchar(255) DEFAULT NULL AFTER `payment_method`;
");
$installer->endSetup();