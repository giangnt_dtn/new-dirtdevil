<?php
    $installer = $this;
    $connection = $installer->getConnection();
    $installer->startSetup();
    $data = array(
        array('order_review', 'Order Review'),
    );
    $connection = $installer->getConnection()->insertArray(
        $installer->getTable('sales/order_status'),
        array('status', 'label'),
        $data
        );
    $installer->endSetup();