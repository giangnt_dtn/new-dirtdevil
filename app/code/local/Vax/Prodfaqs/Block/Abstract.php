<?php
abstract class Vax_Prodfaqs_Block_Abstract extends Mage_Core_Block_Template
{
    public function getProduct() {
        if ($product = Mage::registry('current_product')) {
            return $product;
        }
    }

    public function getTopics($publicOnly = true, $questionType = null) {
        return Mage::getModel('prodfaqs/prodfaqs')->getTopicsByProduct($this->getProduct()->getId(), $publicOnly, $questionType);
    }

    public function getProductRelatedFaqs($publicOnly = true, $questionType = null){
        $product_id = $this->getProduct()->getId();
        $product_faqs = Mage::getModel('prodfaqs/prodfaqs')->getFaqsOfProduct($product_id, $publicOnly, $questionType);
        return $product_faqs;
    }

}