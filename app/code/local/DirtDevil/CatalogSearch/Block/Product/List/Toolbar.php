<?php

class DirtDevil_CatalogSearch_Block_Product_List_Toolbar extends Vax_Turpentine_Block_Catalog_Product_List_Toolbar
{
    /**
     * Init Toolbar
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_availableMode = array('grid' => $this->__('Grid'), 'list' =>  $this->__('List'));
    }
}