<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Tab_Post extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("realexExtended_post", array("legend" => Mage::helper("realexExtended")->__("Rule")));

        $fieldset->addType('boolean', 'Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Form_Renderer_Fieldset_Boolean');

        $fieldset->addType('tss', 'Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit_Form_Renderer_Fieldset_Tss');


        $fieldset->addField('tss_result', 'text', array(
            'name'  => 'tss_result',
            'label'     => 'Overall Transaction Score',
        ));

        $fieldset->addField('avs_result', 'boolean', array(
            'name'  => 'avs_result',
            'label'     => 'AVS Result',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_avs')->toOptionArray(),
        ));

        $fieldset->addField('cvn_result', 'boolean', array(
            'name'  => 'cvn_result',
            'label'     => 'CVN Result',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_cvn')->toOptionArray(),
        ));

        $fieldset->addField('tss_rule_specific', 'tss', array(
            'name'  => 'tss_rule_specific',
            'label'     => 'Result Score Specific',
        ));

        $fieldset->addField('three_d_secure_result', 'boolean', array(
            'name'  => 'three_d_secure_result',
            'label'     => 'ECI Result',
            'values'    => Mage::getModel('realexExtended_adminhtml/system_config_source_eci')->toOptionArray(),
        ));

        $fieldset->addField('auto_settle', 'select', array(
            'name'  => 'auto_settle',
            'label'     => 'Auto Settle',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
        ));

        $fieldset->addField('order_review', 'select', array(
            'name'  => 'order_review',
            'label'     => 'Mark as Order Review',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
        ));

        if (Mage::getSingleton("adminhtml/session")->getChanelData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getChanelData());
            Mage::getSingleton("adminhtml/session")->setChanelData(null);
        } elseif (Mage::registry("chanel_data")) {
            $form->setValues(Mage::registry("chanel_data")->getData());
        }

        return parent::_prepareForm();
    }
}