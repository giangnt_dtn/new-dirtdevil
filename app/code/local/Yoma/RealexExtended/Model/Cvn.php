<?php

class Yoma_RealexExtended_Model_Cvn extends Yoma_RealexExtended_Model_Result {

    const RESULT_M = 0x02;
    const RESULT_N = 0x04;
    const RESULT_U = 0x08;
    const RESULT_I = 0x10;
    const RESULT_P = 0x20;

    public function getSelected($values = array()){
        $result = 0;

        $constants = $this->_getValuesAsArray();

        foreach($values as $selection){
            $result = $result | $constants[strtoupper($selection)];
        }

        return $result;

    }

    protected function _getValuesAsArray(){

        return array(
            'M' => self::RESULT_M,
            'N' => self::RESULT_N,
            'U' => self::RESULT_U,
            'I' => self::RESULT_I,
            'P' => self::RESULT_P,
        );
    }

    public function getProfileOptions($profile){
        $options = array();

        foreach($this->_getValuesAsArray() as $key=>$option){
            if($option & $profile){
                $options[] = $key;
            }
        }

        return $options;

    }
}