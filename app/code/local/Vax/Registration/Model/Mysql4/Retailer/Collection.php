<?php

/**
 * Retailer Mysql Collection Model.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Model_Mysql4_Retailer_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_init('vax_registration/retailer');
    }

    /**
     * Adds a store filter to the collection.
     *
     * @param integer|null $store_id
     *
     * @return \Vax_Registration_Model_Mysql4_Retailer_Collection
     */
    public function addStoreFilter($storeId = null)
    {
        if (Mage::app()->isSingleStoreMode()) {
            return $this;
        }

        if (null === $storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        $this->getSelect()->where('store_id=' . $storeId);

        return $this;
    }

    /**
     * Adds an enabled filter.
     *
     * @param mixed $enabled
     *
     * @return \Vax_Registration_Model_Mysql4_Retailer_Collection
     */
    public function addEnabledFilter($enabled)
    {
        if (is_string($enabled) || is_numeric($enabled)) {
            $enabled = (array) $enabled;
        }

        $this->getSelect()->where('is_enabled IN (?)', $enabled);

        return $this;
    }

    /**
     * Sets the attribute to sort the collection on.
     *
     * @param string $attribute
     * @param string $direction
     *
     * @return \Vax_Registration_Model_Mysql4_Retailer_Collection
     */
    public function addAttributeToSort($attribute, $direction)
    {
        $this->getSelect()->order($attribute, $direction);

        return $this;
    }
}