<?php

/**
 * Layer Filter Price Model.
 *
 * @copyright Vax Ltd 2013
 */
class DirtDevil_Catalog_Model_Layer_Filter_Price extends Mage_Catalog_Model_Layer_Filter_Price
{
    /**
     * {@inheritdoc}
     */
    protected function _renderItemLabel($range, $value)
    {
        $store     = Mage::app()->getStore();
        $fromPrice = preg_replace( '/\.00/', '', $store->formatPrice(($value-1) * $range));
        $toPrice   = preg_replace( '/\.00/', '', $store->formatPrice($value * $range));

        return Mage::helper('catalog')->__('%s - %s', $fromPrice, $toPrice);
    }
}