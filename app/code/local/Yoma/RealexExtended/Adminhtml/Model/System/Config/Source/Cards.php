<?php
class Yoma_RealexExtended_Adminhtml_Model_System_Config_Source_Cards {

    protected $_options;

    public function toOptionArray($isMultiselect=false){
        if (!$this->_options) {

            $cards = Mage::getModel('realex/realex_source_cards');
            $supportedTypes  = $cards->toOptionArray(false);

            $realexTypes = array();

            foreach($supportedTypes as $type){
                $type['value'] = $cards->getGatewayCardType($type['value'],false);
                $realexTypes[] = $type;
            }
            $this->_options = array_merge(array(array('value'=>'*','label'=>'Any')),$realexTypes);
        }

        $options = $this->_options;

        if(!$isMultiselect){
            array_unshift($options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
        }

        return $options;
    }
}
