<?php

/**
 * Vax Postcode Anywhere Helper.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_PostcodeAnywhere_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_POSTCODEANYWHERE_ENABLED      = 'vax_postcodeanywhere/general/enabled';
    const XML_PATH_POSTCODEANYWHERE_ACCOUNT_CODE = 'vax_postcodeanywhere/general/accountcode';
    const XML_PATH_POSTCODEANYWHERE_LICENSE_KEY  = 'vax_postcodeanywhere/general/licensekey';

    /**
     * Checks whether the module is enabled.
     *
     * @return boolen
     */
    public function isEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_POSTCODEANYWHERE_ENABLED);
    }

    /**
     * Returns an address via post code lookup
     *
     * @param string $postCode
     *
     * @return string
     */
    public function getAddressesByPostcode($postCode)
    {
        $client   = new Varien_Http_Client($this->_getPostcodeQueryUrl($postCode));
        $response = $client->request('POST');

        return Mage::getModel('vax_postcodeanywhere/xml_processor')->parsePostcodeResponse($response->getBody());
    }

    /**
     * Returns the post code query url.
     *
     * @param integer $id
     *
     * @return string
     */
    public function getAddressByID($id)
    {
        $client   = new Varien_Http_Client($this->_getAddressQueryUrl($id));
        $response = $client->request('POST');

        return Mage::getModel('vax_postcodeanywhere/xml_processor')->parseAddressResponse($response->getBody());
    }

    /**
     * Returns the post code query url.
     *
     * @param string $postCode
     *
     * @return boolean|string
     */
    protected function _getPostcodeQueryUrl($postCode)
    {
        $sURL  = "http://services.postcodeanywhere.co.uk/xml.aspx?";
        $sURL .= "account_code=".urlencode(Mage::getStoreConfig(self::XML_PATH_POSTCODEANYWHERE_ACCOUNT_CODE));
        $sURL .= "&license_code=".urlencode(Mage::getStoreConfig(self::XML_PATH_POSTCODEANYWHERE_LICENSE_KEY));
        $sURL .= "&action=lookup";
        $sURL .= "&type=by_postcode";
        $sURL .= "&postcode=".urlencode($postCode);

        return $sURL;
    }

    /**
     * Returns the address query url.
     *
     * @param integer $id
     *
     * @return boolean|string
     */
    public function _getAddressQueryUrl($id)
    {
        $sURL  = "http://services.postcodeanywhere.co.uk/xml.aspx?";
        $sURL .= "account_code=".urlencode(Mage::getStoreConfig(self::XML_PATH_POSTCODEANYWHERE_ACCOUNT_CODE));
        $sURL .= "&license_code=".urlencode(Mage::getStoreConfig(self::XML_PATH_POSTCODEANYWHERE_LICENSE_KEY));
        $sURL .= "&action=fetch";
        $sURL .= "&style=simple";
        $sURL .= "&id=" . $id;

        return $sURL;
    }
}