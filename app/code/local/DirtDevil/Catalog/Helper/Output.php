<?php
class DirtDevil_Catalog_Helper_Output extends Mage_Catalog_Helper_Output
{
    /**
     * Does a product have free delivery?
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  boolean
     */
    public function hasFreeDelivery($product) {
        if(!$product) {
            return false;
        }

        if(!Mage::helper('core')->isModuleEnabled('Vax_Delivery')) {
            return false;
        }

        return true;
    }

    /**
     * Does a product have free return?
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  boolean
     */
    public function hasFreeReturn($product) {
        if(!$product) {
            return false;
        }

        if(!Mage::helper('core')->isModuleEnabled('Vax_Delivery')) {
            return false;
        }

        return true;
    }

    /**
     * Is a product new?
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  boolean
     */
    public function isNewProduct($product)
    {
        if ($product->getData('news_from_date') == null && $product->getData('news_to_date') == null) {
            return false;
        }

        if ($product->getData('news_from_date') !== null) {
            if (date('Y-m-d', strtotime($product->getData('news_from_date'))) > date('Y-m-d', time())) {
                return false;
            }
        }

        if ($product->getData('news_to_date') !== null) {
            if (date('Y-m-d', strtotime($product->getData('news_to_date'))) < date('Y-m-d', time())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the category title
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return  string
     */
    public function getCategoryTitle($category) {
        $categoryTitle = $this->categoryAttribute($category, $category->getName(), 'name');

        if($this->categoryAttribute($category, $category->getCategoryTitle(), 'category_title')):
            $categoryTitle = $this->categoryAttribute($category, $category->getCategoryTitle(), 'category_title');
        endif;

        return $categoryTitle;
    }

    /**
     * Returns the category background banner
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return  string
     */
    public function getCategoryBanner($category) {
        $categoryBanner = $this->categoryAttribute($category, $category->getCategoryBanner(), 'category_banner');

        if($categoryBanner) {
            $categoryBanner = '/media/catalog/category/' . $categoryBanner;
        }

        return $categoryBanner;
    }

    /**
     * Returns the small category background banner
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return  string
     */
    public function getSmallCategoryBanner($category) {
        $smallCategoryBanner = $this->categoryAttribute($category, $category->getCategoryBannerSmall(), 'category_banner_small');

        if($smallCategoryBanner) {
            $smallCategoryBanner = '/media/catalog/category/' . $smallCategoryBanner;
        }

        return $smallCategoryBanner;
    }

    /**
     * Returns the category image
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return  string
     */
    public function getImageUrl($category) {
        $_imgHtml   = '';
        if ($_imgUrl = $category->getImageUrl()) {
            $_imgUrl = Mage::helper('catalog/image')->getSquareTransparentPlaceholderImage($_imgUrl);
            $_imgHtml = '<p class="category-image"><img src="'.$_imgUrl.'" alt="'.$this->escapeHtml($category->getName()).'" title="'.$this->escapeHtml($category->getName()).'" /></p>';
            $_imgHtml = $this->categoryAttribute($category, $_imgHtml, 'image');
        }

        return $_imgHtml;
    }

    /**
     * Get Associated Product ID
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return  Mage_Catalog_Model_Product
     */
    public function getAssociatedProduct($_category) {
        $associatedProductId = $_category->getCategoryProductId();
        $associatedProduct = false;

        if($associatedProductId) {
            $associatedProduct = Mage::getModel('catalog/product')->load($associatedProductId);
        }

        return $associatedProduct;
    }

    public function getProductHover($product) {
        $featureText = $product->getName();

        if($product->getShortDescription()) {
            $featureText .= ' - ' . $product->getShortDescription();
        }

        if($product->getData('category_key_features')) {
            $features = $product->getAttributeText('category_key_features');
            $featureText .= "<ul class=\"features\">";

            if(is_array($features)) {
                foreach ($features as $feature):
                    $featureText .= "<li>" . $feature . "</li>";
                endforeach;
            } else {
                $featureText .= "<li>" . $features . "</li>";
            }

            $featureText .= '</ul>';
        }

        return $featureText;
    }
}
