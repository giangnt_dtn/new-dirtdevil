<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
require_once 'Mage/Widget/controllers/Adminhtml/WidgetController.php';

class Vax_Widget_Adminhtml_WidgetController extends Mage_Widget_Adminhtml_WidgetController
{
    /**
     * Overrides parent method to allow for HTML content saving.
     */
    public function buildWidgetAction()
    {
        $type = $this->getRequest()->getPost('widget_type');
        $params = $this->getRequest()->getPost('parameters', array());

        if ('editor' == $type) {
            $params['text'] = base64_encode($params['text']);
        }

        $asIs = $this->getRequest()->getPost('as_is');
        $html = Mage::getSingleton('widget/widget')->getWidgetDeclaration($type, $params, $asIs);
        $this->getResponse()->setBody($html);
    }
}