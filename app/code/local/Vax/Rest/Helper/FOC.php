<?php

/**
 * Vax Rest FOC Helper.
 *
 * @copyright Vax Ltd 2012
 */
class Vax_Rest_Helper_FOC extends Mage_Core_Helper_Abstract
{
    /**
     * FOCs a quote if needed.
     *
     * @param mixed $observer
     *
     * @return void
     */
    public function focQuote($observer)
    {
        // Grab quote and shipping address.
        $quote       = $observer->getEvent()->getQuote();
        $costAddress = Mage::helper('rest/model_quote')->getTotalsAddress($quote);

        // Check for FOC.
        if ( ! $quote->getIsFOC()) {
            return;
        }

        // FOC each quote item.
        foreach($quote->getAllItems() as $item) {
            $item->setAppliedRuleIds(null);
            $item->setDiscountAmount($item->getPriceInclTax() * $item->getQty());
            $item->setBaseDiscountAmount($item->getPriceInclTax() * $item->getQty());
            $item->setDiscountPercent(100);
            $item->setHiddenTaxAmount($item->getRowTotalInclTax() - $item->getRowTotal());
            $item->setBaseHiddenTaxAmount($item->getBaseRowTotalInclTax() - $item->getBaseRowTotal());
            $item->setTaxAmount(0);
            $item->setBaseTaxAmount(0);
        }

        $costAddress
            ->setSubtotalWithDiscount(0)
            ->setBaseSubtotalWithDiscount(0)
            ->setBaseGrandTotal(0)
            ->setGrandTotal(0)
            ->setDiscountDescription('Free of Charge Order')
            ->setDiscountAmount(-1 * $costAddress->getSubtotalInclTax())
            ->setBaseDiscountAmount(-1 * $costAddress->getSubtotalInclTax())
            ->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingAmount(0)
            ->setBaseShippingAmount(0)
            ->setShippingTaxAmount(0)
            ->setBaseShippingTaxAmount(0)
            ->setShippingInclTax(0)
            ->setBaseShippingInclTax(0)
            ->setHiddenTaxAmount($costAddress->getSubtotalInclTax() - $costAddress->getSubtotal())
            ->setBaseHiddenTaxAmount($costAddress->getBaseSubtotalInclTax() - $costAddress->getBaseSubtotal())
            ->setTaxAmount(0)
            ->setBaseTaxAmount(0);

        // Update the quote totals.
        $quote
            ->setAppliedRuleIds(null)
            ->setSubtotalWithDiscount(0)
            ->setBaseSubtotalWithDiscount(0)
            ->setGrandTotal(0)
            ->setBaseGrandTotal(0);
    }
}