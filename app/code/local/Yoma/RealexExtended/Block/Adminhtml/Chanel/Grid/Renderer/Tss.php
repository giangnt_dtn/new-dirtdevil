<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Grid_Renderer_Tss extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $values =  json_decode($row->getData($this->getColumn()->getIndex()));
        $temp =array();

        foreach($values as $rule){
            $temp[] = implode(':',$rule);
        }

        return implode(',',$temp);

       }

}