<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Product_PromoBanner
    extends Vax_Widget_Block_Widget_Abstract implements Mage_Widget_Block_Interface
{
    /** @var  Mage_Catalog_Model_Product */
    protected $_product;
    /**
     * Widget initialization.
     */
    protected function _construct()
    {
        $this->setTemplate('catalog/widget/product/promo_banner.phtml');
        parent::_construct();
    }

    /**
     * Returns widget product.
     *
     * @return Mage_Catalog_Model_Product|Mage_Core_Model_Abstract
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::getModel('catalog/product')->load($this->_getProductId());
        }

        return $this->_product;
    }

    /**
     * Returns URL to product logo.
     * @return string
     */
    public function getProductLogoUrl()
    {
        if(!$this->getProductLogo()) {
            return false;
        }

        return $this->_getMediaUrl($this->getProductLogo());
    }

    /**
     * Returns URL to main product image.
     * @return string
     */
    public function getProductImageUrl()
    {
        return $this->_getMediaUrl($this->getMainImage());
    }

    public function getProductImageSmallUrl()
    {
        return $this->_getMediaUrl($this->getMainImageSmall());
    }

    /**
     * Returns ID of the product selected in a widget.
     *
     * @return int
     */
    protected function _getProductId()
    {
        $productData = explode('/', $this->getProductId());
        if (sizeof($productData > 0)) {
            return (int)$productData[1];
        }
    }
}