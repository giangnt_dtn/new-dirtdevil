<?php

class Yoma_RealexExtended_Block_Adminhtml_Chanel_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "realexExtended";
        $this->_controller = "adminhtml_chanel";
        $this->_updateButton("save", "label", Mage::helper("realexExtended")->__("Save Rule"));
        $this->_updateButton("delete", "label", Mage::helper("realexExtended")->__("Delete Rule"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("realexExtended")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
                function saveAndContinueEdit(){
                    editForm.submit($('edit_form').action+'back/edit/');
                }
            ";
    }

    public function getHeaderText()
    {

        if (Mage::registry("chanel_data") && Mage::registry("chanel_data")->getId()) {
            return Mage::helper("realexExtended")->__("Edit Rule '%s'", $this->htmlEscape(Mage::registry("chanel_data")->getId()));
        } else {
            return Mage::helper("realexExtended")->__("Add Rule");
        }
    }
}