<?php 
/**
 * Category Related Product Setup
 *
 * @category    code
 * @package     dirtdevil
 * @copyright   Copyright (c) 2015 Vax Ltd.
 * @author      Vax
 */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'category_banner', array(
    'type'          => 'varchar',
    'label'         => 'Category Banner',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'General Information'
));

$installer->addAttribute('catalog_category', 'category_banner_small', array(
    'type'          => 'varchar',
    'label'         => 'Category Banner Small',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'General Information'
));


$installer->endSetup();
