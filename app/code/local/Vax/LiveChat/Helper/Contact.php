<?php

/**
 * Contact Us Live Chat Helper.
 *
 * @copyright Vax Ltd
 */
class Vax_LiveChat_Helper_Contact extends Vax_LiveChat_Helper_Data
{
    /**
     * Checks whether chat is enabled.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        if ( ! $this->isModuleEnabled()) {
            return false;
        }

        return Mage::getStoreConfigFlag('vax_livechat/contact_us/is_enabled');
    }

    /**
     * Returns the chat endpoint url.
     *
     * @return string
     */
    public function getEndpoint()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/endpoint');
    }

    /**
     * Returns the script url.
     *
     * @return string
     */
    public function getScriptUrl()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/js_script');
    }

    /**
     * Returns the deployment ID.
     *
     * @return string
     */
    public function getDeploymentID()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/deployment_id');
    }

    /**
     * Returns the API username.
     *
     * @return string
     */
    public function getUsername()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/username');
    }

    /**
     * Returns the API password.
     *
     * @return string
     */
    public function getPassword()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/password');
    }

    /**
     * Returns the chat window height.
     *
     * @return string
     */
    public function getWindowHeight()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/window_height');
    }

    /**
     * Returns the chat window width.
     *
     * @return string
     */
    public function getWindowWidth()
    {
        return Mage::getStoreConfig('vax_livechat/contact_us/window_width');
    }

    /**
     * Returns whether the callbackHandler is required
     *
     * @return boolean
     */
    public function requiresCallback() {
        return false;
    }
}
