<?php

class Yoma_RealexExtended_Model_SecureProfile extends Yoma_RealexExtended_Model_Profile {

    const NOT_ENROLLED = 0x02;
    const UNABLE_VERIFY_ENROLLMENT = 0x04;
    const INVALID_RESPONSE = 0x08;
    const ENROLLED_ACS_INVALID_RESPONSE = 0x10;
    const SUCCESSFUL_AUTH = 0x20;
    const SERVER_ATTEMPT = 0x40;
    const INCORRECT_PASSWORD = 0x80;
    const AUTH_UNAVAILABLE = 0x100;
    const ACS_INVALID_RESPONSE = 0x200;
    const NO_RESPONSE = 0x400;

    public function getSelected($values = array()){
        $result = 0;

        $constants = $this->_getValuesAsArray();

        foreach($values as $selection){
            $result = $result | $constants[(int)($selection - 1)];
        }

        return $result;

    }

    protected function _getValuesAsArray(){

        return array(
            self::NOT_ENROLLED,
            self::UNABLE_VERIFY_ENROLLMENT,
            self::INVALID_RESPONSE,
            self::ENROLLED_ACS_INVALID_RESPONSE,
            self::SUCCESSFUL_AUTH,
            self::SERVER_ATTEMPT,
            self::INCORRECT_PASSWORD,
            self::AUTH_UNAVAILABLE,
            self::ACS_INVALID_RESPONSE,
            self::NO_RESPONSE
        );
    }

    public function getProfileOptions($profile,$useValue = false){
        $options = array();

        foreach($this->_getValuesAsArray() as $key=>$option){
            if($option & $profile){
                if($useValue){
                    $options[] = $key + 1;
                }else {
                    $options[$key] = 1;
                }
            }
        }

        return $options;

    }

}