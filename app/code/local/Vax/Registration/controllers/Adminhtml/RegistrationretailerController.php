<?php
/**
 * Vax Registration Admin controller
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */


class Vax_Registration_Adminhtml_RegistrationretailerController extends Mage_Adminhtml_Controller_Action {
    public function indexAction() {
        /*
         * Redirect user via 302 http redirect (the browser url changes)
         */
        $this->_redirect('*/*/list');
    }
 
    /**
     * Display grid
     */
    public function listAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/retailer/list')) {
            $this->_redirect('/');
        }
        
        // null out the form data first
        $this->_getSession()->setFormData(array());
 
        // Title
        $this->_title($this->__('Registration - Retailers'));
 
        $this->loadLayout();
        $this->_setActiveMenu('retailer');
        $this->renderLayout();
    }
 
    /**
     * Check ACL permissions
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('registration/retailer');
    }
 
    /**
     * Grid action for ajax requests
     */
    public function gridAction() {       
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function viewAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/retailer/view')) {
            $this->_redirect('*/*/list');
        }
        
        $this->_forward('edit');
    }
    
    /*
     * New registration
     */
    public function newAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/retailer/new')) {
            $this->_redirect('*/*/list');
        }
        /*
         * Redirect the user via a magento internal redirect
         */
        $this->_forward('edit');
    }
    
    /**
     * Edit or Create
     */
    public function editAction() {
        if(!Mage::getSingleton('admin/session')->isAllowed('registration/retailer/edit')) {
            $this->_redirect('*/*/list');
        }
        
        $model = Mage::getModel('vax_registration/retailer');
        Mage::register('current_retailer', $model);
        $id = $this->getRequest()->getParam('retailer_id');
 
        try {
            if ($id) {
                if (!$model->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }
 
            if ($model->getId()) {
                $pageTitle = $this->__('Edit Retailer %s (%s)', $model->getID(), $model->getTitle());
            } else {
                $pageTitle = $this->__('New Retailer');
            }
            $this->_title($this->__('Retailer'))->_title($pageTitle);
 
            $this->loadLayout();
 
            $this->_setActiveMenu('Retailer');
            $this->renderLayout();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/list');
        }
    }
    
    /**
     * Process form post
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            // Filter post data - date validation
            $data = $this->_filterPostData($data);
            $this->_getSession()->setFormData($data);
            $model = Mage::getModel('vax_registration/retailer');
            $id = $this->getRequest()->getParam('retailer_id');
            
            try {
                if ($id)
                    $model->load($id);
                
                $model->addData($data);
                
                $stores = $this->getRequest()->getParam('stores');
                
                if(is_array($stores)) {
                    $store = array_pop($stores);
                }
                
                $model->setStoreId($store);
                $model->save();
 
                if (!$model->getId()) {
                    Mage::throwException($this->__('Error saving retailer'));
                }
 
                if ($id) {
                    $this->_getSession()->addSuccess($this->__('Retailer was successfully edited'));
                    $this->_logAdminAction($id, "update");
                }
                else {
                    $this->_getSession()->addSuccess($this->__('Retailer was successfully created'));
                    $this->_logAdminAction($model->getId(), "create");
                }
                
                $this->_getSession()->setFormData(false);
 
                if ($this->getRequest()->getParam('back')) {
                    $params = array('retailer_id' => $model->getId());
                    $this->_redirect('*/*/edit', $params);
                } else {
                    $this->_redirect('*/*/list');
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                if ($model && $model->getId()) {
                    $this->_redirect('*/*/edit', array('retailer_id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/new');
                }
            }
 
            return;
        }
 
        $this->_getSession()->addError($this->__('No data found to save'));
        $this->_redirect('*/*');
    }
    
    /*
     * Filter posted data - in this case we only need to filter dates!
     * 
     * return array data
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('pur_date'));
        return $data;
    }
    
    protected function _logAdminAction ($retailer_id, $action) { 
        $eventcode = 'registration_retailer_edit'; 
        
        $userid = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $username = Mage::getSingleton('admin/session')->getUser()->getUsername();

        return Mage::getSingleton('enterprise_logging/event')->setData(array( 
        'ip' => Mage::helper('core/http')->getRemoteAddr(), 
        'user' => $username, 
        'user_id' => $userid, 
        'is_success' => true, 
        'fullaction' => "adminhtml_registration_retailer_edit", 
        'event_code' => $eventcode,
        'action' => $action, 
        'info' => $retailer_id, 
        ))->save(); 
    } 
}