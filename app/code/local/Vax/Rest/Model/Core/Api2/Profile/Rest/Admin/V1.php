<?php

/**
 * User Profile Resource.
 *
 * @copyright Vax Ltd
 */
class Vax_Rest_Model_Core_Api2_Profile_Rest_Admin_V1 extends Mage_Api2_Model_Resource
{
    /**
     * Retrieves the currently authenticated user's profile.
     *
     * @return array
     */
    protected function _retrieve()
    {
        $currentUserID = $this->getApiUser()->getUserId();
        $currentUser   = Mage::getModel('admin/user')->load($currentUserID);

        return array(
            'identifier' => 'username',
            'username'   => $currentUser->getUsername(),
            'email'      => $currentUser->getEmail(),
            'roles'      => array(str_replace(' ', '_', $currentUser->getRole()->getRoleName())),
        );
    }
}