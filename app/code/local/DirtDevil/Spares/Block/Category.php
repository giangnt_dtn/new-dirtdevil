<?php
/**
 * Spares category block
 *
 * @category   DirtDevil
 * @package    DirtDevil_Page
 * @author     Vax
 */
class DirtDevil_Spares_Block_Category extends Mage_Core_Block_Template
{
    protected $rootCategoryId;
    protected $categories;

    public function getCategories() {

        if($this->getOverrideCategoryId()) {
            $this->rootCategoryId = $this->getOverrideCategoryId();
        } else {
            if(Mage::registry("current_category") && Mage::registry("current_category")->getId()) {
                $this->rootCategoryId = Mage::registry("current_category")->getId();
            } else {
                $this->rootCategoryId = Mage::helper('dirtdevil_spares')->getSparesRootCategory();
            }
        }

        $supportRootCategory = Mage::getModel('catalog/category')->load($this->rootCategoryId);

        $this->categories = $supportRootCategory->getChildrenCategories($supportRootCategory);

        return $this->categories;
    }

    public function getThumbnailImage($category) {
        return Mage::helper('dirtdevil_spares')->getThumbnailImage($category);
    }
}
