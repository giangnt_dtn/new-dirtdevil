<?php
class Vax_LiveChat_Block_Product extends Vax_LiveChat_Block_General
{
    /*
     * Returns the helper in use
     * @return Mage_Core_Helper_Abstract
     */
    public function getTheHelper() {
        return Mage::helper('vax_livechat/product');
    }

    /**
     * Returns the deployment ID.
     *
     * @return string
     */
    public function getDeploymentID()
    {
        /* allow a custom deployment ID if set per product */
        if($this->getProduct() && $this->getProduct()->getSalesChatDeploymentId()) {
            return $this->getProduct()->getSalesChatDeploymentId();
        }

        return $this->getTheHelper()->getDeploymentID();
    }

    /**
     * Returns live chat button id
     *
     * @return string
     */
    public function getButtonId() {
        return "liveagent_button_online_" . $this->getDeploymentID();
    }

    /**
     * Returns the product
     *
     * @return string
     */
    public function getProduct() {

        // support products on CMS landing page
        if($this->getProductId()) {
            return Mage::getModel('catalog/product')->load($this->getProductId());
        }

        if(Mage::registry('current_product')) {
            return Mage::registry('current_product');
        }

        return null;
    }
}
