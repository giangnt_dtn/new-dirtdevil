<?php

/**
 * Vax Checkout Onepage Actions Block.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Steps_Actions extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the previous button title.
     *
     * @return string
     */
    public function getPrevBtnTitle()
    {
        if ($this->isFirstStep()) {
            return $this->__('Back to Basket');
        }

        return $this->__('Previous');
    }

    /**
     * Returns the next button title.
     *
     * @return string
     */
    public function getNextBtnTitle()
    {
        if ($this->hasOrderProcessed()) {
            return $this->__('Return to Shop');
        }

        if ($this->hasOrderFailed()) {
            return $this->__('Try Again');
        }

        return $this->__('Continue');
    }

    /**
     * Returns the current submit data.
     *
     * @return string
     */
    public function getHTMLClass()
    {
        $steps = $this->getSteps();

        if ($this->isLastStep() && $this->hasOrderFailed()) {
            return 'order-retry';
        }

        return $steps[$this->getStepIndex() - 1]['htmlClass'];
    }
}