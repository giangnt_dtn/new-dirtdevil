<?php

/**
 * Vax Postcode Anywhere Address
 *
 * @copyright Vax Ltd 2013
 */
class Vax_PostcodeAnywhere_Model_Source_Address
{
    /**
     * Options Array.
     *
     * @var array
     */
    protected $_options = array();

    /**
     * Sets the options array.
     *
     * @param array $options
     *
     * @return \Vax_PostcodeAnywhere_Model_Source_Address
     */
    public function setOptions(array $options)
    {
        $this->_options = $options;

        return $this;
    }

    /**
     * Returns the Options Array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_options;
    }
}