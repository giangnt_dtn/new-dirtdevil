<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Widget_Block_Widget_Instance extends Mage_Core_Block_Template
{
    protected $_widget;

    /**
     * Sets the widget ID.
     *
     * @param int $id
     * @return $this
     */
    public function setWidgetId($id)
    {
        try {
            $widget = Mage::getModel('widget/widget_instance')->load($id);

            if (!$widget->getId()) {
                return;
            }

            $this->_widget = $widget;

        } catch (Exception $e) {
            Mage::logException($e);
        }

        return $this;
    }

    /**
     * Widget getter.
     *
     * @return mixed
     */
    public function getWidget()
    {
        return $this->_widget;
    }

    /**
     * Renders the widget.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getWidget() || !$this->getWidget()->getInstanceType()) {
            return '';
        }

        $declaration = Mage::getModel('widget/widget')->getWidgetDeclaration(
            $this->getWidget()->getType(),
            $this->getWidget()->getWidgetParameters()
        );

        if (!$declaration) {
            return '';
        }

        return Mage::getModel('widget/template_filter')->filter($declaration);
    }
}