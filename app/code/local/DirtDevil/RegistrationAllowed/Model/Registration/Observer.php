<?php

/**
 * Customer Registration Observer.
 *
 * @copyright Vax Ltd 2015
 */
class DirtDevil_RegistrationAllowed_Model_Registration_Observer
{
    /**
     * Sets the disable registration flag.
     *
     * @param Varien_Event_Observer $observer
     *
     * @return void
     */
    public function setIsRegistrationFormAllowed(Varien_Event_Observer $observer)
    {
        if (!$observer->getResult()->getIsAllowed()) {
            $observer->getResult()->setIsAllowed(false);
        }

        if (!Mage::getStoreConfig('customer/create_account/is_allowed')) {
            $observer->getResult()->setIsAllowed(false);
        }

        $observer->getResult()->setIsAllowed(true);
    }


	/**
	 * Disables the registration controller.
	 *
	 * @param Varien_Event_Observer $observer
	 *
	 * @return void
	 */
	public function disableController(Varien_Event_Observer $observer)
	{
		if (Mage::helper('dirtdevil_registrationallowed')->isRegistrationAllowed()) {
			return;
		}

		Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
	}
}
