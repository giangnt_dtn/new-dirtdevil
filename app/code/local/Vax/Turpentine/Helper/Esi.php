<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2014 Vax Ltd.
**/
class Vax_Turpentine_Helper_Esi extends Nexcessnet_Turpentine_Helper_Esi
{
    /**
     * {@inheritdoc}
     */
    public function getDummyRequest( $url=null ) {
        if( $url === null ) {
            $url = $this->getDummyUrl();
        }

        $request = Mage::getModel('turpentine/dummy_request', $url);
        $request->fakeRouterDispatch();
        return $request;
    }
}