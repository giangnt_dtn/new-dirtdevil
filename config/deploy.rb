# Internal - 10.182.6.134
# External - 162.13.64.12
set :stages,              %w[staging production]
set :default_stage,       "staging"

require 'capistrano/ext/multistage'

set :application,         'Magento Community - Dirt Devil UK'
set :app_path,            'app'

role :web,        		  "10.182.6.134"
role :app,       		  "10.182.6.134"   # This may be the same as your `Web` server or a separate administration server

set :deploy_via,          :rsync_with_remote_cache
set :user,                'deploy'

set  :scm,                :git
set  :repository,         "git@github.com:vaxltd/dirtdevil-uk.git"

set :use_sudo,            false
set :keep_releases,       3

set :app_symlinks, ["/media", "/var", "/sitemaps", "/staging"]
set :app_shared_dirs, ["/app/etc", "/sitemaps", "/media", "/var", "/staging"]
set :app_shared_files, ["/app/etc/local.xml", "/app/etc/config.xml"]

default_run_options[:pty] = true
ssh_options[:keys]        = %w(/var/lib/jenkins/.ssh/id_rsa_project)

namespace :magento do
  namespace :redis do
    desc "Clears Redis Cache"
    task :set, :roles => :web do
      run "sh -c 'cd #{latest_release} && php -f site/redis/redis_clear.php'"
    end
  end
end
