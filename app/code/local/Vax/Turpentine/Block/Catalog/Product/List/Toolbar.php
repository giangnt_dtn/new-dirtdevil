<?php
/**
 * Extend Vax Toolbar block to stop caching of toolbar with options plus with the grid option
 *
 * @category    code
 * @package     vax_uk
 * @copyright   Copyright (c) 2013 Vax Ltd.
 * @author      C Carnell
 */
class Vax_Turpentine_Block_Catalog_Product_List_Toolbar extends
    Nexcessnet_Turpentine_Block_Catalog_Product_List_Toolbar {

    /**
    * Sets the current View mode (grid, list, etc.)
    *
    * @param string $mode
    */
    public function setCurrentMode($mode)
    {
        $this->setData('_current_grid_mode', $mode);
    }

    /**
     * Checks whether "Show All" link should be displayed
     *
     * @return bool
     */
    public function canShowAllLink()
    {
        if (!Mage::getStoreConfig('catalog/frontend/list_allow_all')) {
            return false;
        }

        if ($this->isLimitCurrent('all')) {
            return false;
        }

        $lastPageNum = (int)$this->getLastPageNum();

        if (1 == $lastPageNum || $lastPageNum <= (int)$this->getRequest()->getParam($this->getPageVarName())) {
            return false;
        }

        return $this->getDataSetDefault('show_all_link', true);
    }

    /**
     * Checks whether "View Less" link should be displayed
     *
     * @return bool
     */
    public function canShowViewLessLink()
    {
        return $this->isLimitCurrent('all');
    }

    /**
     * Decides which View Less link to use(main , or category search function)
     *
     * @return string
     */
    public function whichViewLess()
    {
        if (!Mage::registry('current_category')) {
            $mainSearch = '?q=' . $this->getRequest()->getParam('q');

            return $mainSearch;
        }

            return Mage::registry('current_category')->getUrl();

    }
}