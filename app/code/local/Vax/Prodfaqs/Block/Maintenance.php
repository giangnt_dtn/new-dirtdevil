<?php
class Vax_Prodfaqs_Block_Maintenance extends Vax_Prodfaqs_Block_Abstract
{
    public function getFaqHtml($faq)
    {
        if (!$faq instanceof Vax_Prodfaqs_Model_Prodfaqs) {
            $faq = Mage::getModel('prodfaqs/prodfaqs')->load((int)$faq);
        }
        $topic = Mage::getModel('prodfaqs/topic')->load($faq->getTopicId());
        $faq->setTopic($topic);
        if (!$faq->getFaqsId()) {
            return '';
        }
        $block = $this->getLayout()->createBlock('vaxfaqs/faq', 'faq_block_' . $faq->getFaqsId());
        $block->setTemplate('prodfaqs/singlefaq.phtml')
              ->setFaq($faq);
        return $block->toHtml();
    }
}