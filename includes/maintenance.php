<?php
$maintenanceFile = 'maintenance.flag';
$maintenanceDevFile = 'maintenance-dev.flag';

$clientIP = null;
if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $clientIP = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
} else {
    if(isset($_SERVER['REMOTE_ADDR'])) {
        $clientIP = $_SERVER['REMOTE_ADDR'];
    }
}

// these are the IP's that are  allowed to view the site in maintenance mode:
$allowedIPs = array('127.0.0.1',      // local developer
    '195.26.57.129',  // uk office
    '195.26.58.28',   // german office
    '81.190.62.182',  // konrad
    '173.0.82.75',    // batch.sandbox.paypal.com
    '173.0.82.91',    // business.sandbox.paypal.com
    '173.0.89.210',   // developer.paypal.com
    '173.0.82.126',   // ipn.sandbox.paypal.com
    '173.0.82.101',   // mobileclient.sandbox.paypal.com
    '173.0.82.75',    // reports.sandbox.paypal.com
    '173.0.82.77',    // www.sandbox.paypal.com
    '173.0.82.78',    // api.sandbox.paypal.com
    '173.0.82.66',    // ipnpb.sandbox.paypal.com
    '173.0.82.89',    // svcs.sandbox.paypal.com
    '173.0.82.83',    // api-3t.sandbox.paypal.com
);

if (file_exists($maintenanceFile)) {
    include_once dirname('../') . '/errors/503.php';
    exit;
}

// disallow all users except those in the allowed ips
$maintenance = false;

if (file_exists($maintenanceDevFile) && $clientIP !== null) {
    // if we are behind a load balancer, ip contains multiple values
    if(is_array($clientIP)) {
        $maintenance = true;

        foreach($clientIP as $ip) {
            if(in_array($ip, $allowedIPs)) {
                $maintenance = false;
            }
        }
    } else {
        if(!in_array($clientIP, $allowedIPs)) {
            $maintenance = true;
        }
    }
}

if($maintenance) {
    header('Location: http://www.dirtdevil.co.uk/maintenance.php') ;
    exit;
}
