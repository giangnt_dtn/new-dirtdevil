<?php

/**
 * Vax Checkout Onepage Order Confirmation.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Order_Confirmation extends Vax_Checkout_Block_Onepage
{
    /**
     * Returns the current response message.
     *
     * @return string
     */
    public function getResponseMessage()
    {
        if ( ! $this->getLastResponse()) {
            return $this->__('Unfortunately we cannot place your order at this time. Please try again later.');
        }

        $response = $this->getLastResponse()
                         ->getOpcResponse();

        if ( ! $response || ! isset($response['failure']['msg'])) {
            return $this->__('Unfortunately we cannot place your order at this time. Please try again later.');
        }

        return $this->__($response['failure']['msg']);
    }
}