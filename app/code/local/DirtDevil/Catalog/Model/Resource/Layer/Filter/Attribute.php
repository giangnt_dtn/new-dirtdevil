<?php
/**
 * Catalog Layer Attribute Filter Resource Model
 *
 * @category    Vax
 * @package     Vax_Catalog
 * @author      Vax
 */
class DirtDevil_Catalog_Model_Resource_Layer_Filter_Attribute extends Mage_Catalog_Model_Resource_Layer_Filter_Attribute
{
    protected $_useCurrentFilter = true;
    /**
     * Apply attribute filter to product collection
     * 
     * Modify the function to return a unique idx
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @param int $value
     * @return Mage_Catalog_Model_Resource_Layer_Filter_Attribute
     */
    public function applyFilterToCollection($filter, $value)
    {
        $filterSingleton = FilterSingleton::singleton($filter);
        
        if (!isset($filterSingleton->return)) {
            $collection = $filter->getLayer()->getProductCollection();

            if (!is_array($value)) {
                $value = explode('_', $value);
            }
            foreach ($value as $key => $val) {
                if (!$val) {
                    unset($value[$key]);
                }
            }
            if (!count($value)) {
                return $collection;
            }

            //$collection->getSelect()->distinct(true);
            $attribute  = $filter->getAttributeModel();
            $connection = $this->_getReadAdapter();

            foreach ($value as $val) {
                $tableAlias = $attribute->getAttributeCode() . '_idx_' . $val;
                $conditions = array(
                    "{$tableAlias}.entity_id = e.entity_id",
                    $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
                    $connection->quoteInto("{$tableAlias}.store_id = ?", $collection->getStoreId()),
                    $connection->quoteInto("{$tableAlias}.value = ?", $val)
                );
                $collection->getSelect()->join(
                    array($tableAlias => $this->getMainTable()),
                    implode(' AND ', $conditions),
                    array()
                );
            }

            $filterSingleton->return = $collection;
            return $collection;
            
        } else {
            return $filterSingleton->return;
        }
    }

    public function applyCountFilter($filter, $value, $collection) {
        if (!is_array($value)) {
            $value = explode('_', $value);
        }
        foreach ($value as $key => $val) {
            if (!$val) {
                unset($value[$key]);
            }
        }
        if (!count($value)) {
            return $collection;
        }

        $attribute  = $filter->getAttributeModel();
        $connection = $this->_getReadAdapter();
        $tableAlias = $attribute->getAttributeCode() . '_idx_' . implode('_', (array)$value);
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $collection->getStoreId()),
        );

        foreach ($value as $val) {
            $conditions[] = $connection->quoteInto("{$tableAlias}.value LIKE(?)", '%' . $val . '%');
        }

        $collection->getSelect()->join(
            array($tableAlias => $this->getMainTable()),
            implode(' AND ', $conditions),
            array()
        );
        return $collection;
    }

    protected function _getFilterOptions($filter)
    {
        $connection = $this->_getReadAdapter();
        $select = $connection->select();
        $select->from($this->getMainTable(), array('value'))
               ->where('attribute_id = ?', $filter->getAttributeModel()->getAttributeId())
               ->where('store_id = ?', $filter->getStoreId());
        $select->distinct(true);
        return $connection->fetchCol($select);
    }

    public function getCount($filter)
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return parent::getCount($filter);
        }
        $connection = $this->_getReadAdapter();
        // we need to get all possible values for given filter
        $values = $this->_getFilterOptions($filter);
        $collection = $filter->getLayer()->getProductCollection();

        $collectionFilters = $collection->getLimitationFilters();

        if (isset($collectionFilters['category_id'])) {
            $category = Mage::getModel('catalog/category')->load($collectionFilters['category_id']);
            $category->setIsAnchor(true);
            $collection->addCategoryFilter($category);
        }

        // we need to run the count in the loop
        foreach ($values as $value) {
            // reset columns, order and limitation conditions
            $select = clone $collection->getSelect();
            $select->reset(Zend_Db_Select::COLUMNS);
            $select->reset(Zend_Db_Select::ORDER);
            $select->reset(Zend_Db_Select::LIMIT_COUNT);
            $select->reset(Zend_Db_Select::LIMIT_OFFSET);

            $attribute  = $filter->getAttributeModel();
            $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
            $conditions = array(
                "{$tableAlias}.entity_id = e.entity_id",
                $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
                $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
                $connection->quoteInto("{$tableAlias}.value = ?", $value),
            );

            $select
                ->join(
                    array($tableAlias => $this->getMainTable()),
                    join(' AND ', $conditions),
                    array('count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")))
                ->group("{$tableAlias}.value");
            $values[$value] = $connection->fetchOne($select);
            unset($select);
        }
        return $values;
    }

    /**
     * Gets the original count of items before filters were applied
     *
     * @param $filter
     */
    public function getOrigCount($filter)
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return parent::getCount($filter);
        }
        $connection = $this->_getReadAdapter();
        $newFilter = clone $filter;
        $collection = $newFilter->getLayer()->getCurrentCategory()->getProductCollection();

        $select = $collection->getSelect();
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $attribute  = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
        );

        $select
            ->join(
                array($tableAlias => $this->getMainTable()),
                join(' AND ', $conditions),
                array('value', 'count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")))
            ->group("{$tableAlias}.value");

        return $connection->fetchPairs($select);
    }

}

/*
 * Fix for ajax based filter application
 */
class FilterSingleton {
    
    static private $instance = array();
    
    public $return = null;
      
    private function __construct() {
        
    }

    static public function singleton($filter) {
         if (!isset(self::$instance[$filter->getAttributeModel()->getAttributeId()])) {
            $c = __CLASS__;
            self::$instance[$filter->getAttributeModel()->getAttributeId()] = new $c;
        }

        return self::$instance[$filter->getAttributeModel()->getAttributeId()];
    }
}

