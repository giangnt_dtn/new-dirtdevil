<?php
/**
 * Spares selector block
 *
 * @category   DirtDevil
 * @package    DirtDevil_Page
 * @author     Vax
 */
class DirtDevil_Spares_Block_Selector extends Mage_Core_Block_Template
{
    // DE Value..
    const PRODUCT_TYPE_MACHINE = 69;

    /*
     * Return active categories in root store
     * @returns array
     */
    public function getCategories() {

        $storeId = Mage::app()->getStore()->getId();
        $rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->setStoreId($storeId)
            ->addFieldToFilter('is_active', 1)
            ->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"))
            ->addAttributeToSelect('*');
        return $categories;
    }

    /*
     * Get products in a certain category
     * @returns array
     */
    public function getProductCollection($category) {

        $storeId = Mage::app()->getStore()->getId();

        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addStoreFilter($storeId)
            ->addCategoryFilter($category)
            ->setOrder('sku', 'ASC');


        //alternative: $productCollection->addAttributeToFilter('product_type_value', 'Machine');
        $products->addAttributeToFilter('product_type', self::PRODUCT_TYPE_MACHINE);
        $products->addAttributeToFilter('status', 1);
        $products->getSelect()->group('entity_id');

        return $products;
    }

    /*
     * Get products in categories
     * @return array
     */
    public function getProductsInCategory() {

        $categories = $this->getCategories();

        $prodCats = null;

        foreach ($categories as $category) {

            $products = $this->getProductCollection($category);

            // no products found and no children categories
            if(!$products || ($products && $products->count() === 0)) {
                continue;
            }

            foreach ($products as $product) {
                $prodCats[$category->getName()][] = $product->getSku();
            }
        }

        return $prodCats;
    }

    /*
     * Output products in categories in html select
     * @return string
     */
    public function getProductsInCategoryBlockHTML() {

        $prodCats = $this->getProductsInCategory();
        $html = '';

        foreach ($prodCats as $category_name => $product_range) {

            $html .= '<optgroup label="' . $category_name . '">';

            sort($product_range);

            foreach ($product_range as $sku) {
                $html .= '<option value="' . $sku . '">' . $sku . '</option>';
            }
        }

        return $html;
    }
}
