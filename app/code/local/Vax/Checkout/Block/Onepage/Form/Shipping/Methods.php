<?php

/**
 * Vax Checkout Onepage Shipping Methods.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Block_Onepage_Form_Shipping_Methods extends Mage_Checkout_Block_Onepage_Abstract
{
    /**
     * Shipping Methods.
     *
     * @var array
     */
    protected $_shippingMethods;

    /**
     * Checks whether the shipping methods block is visible.
     *
     * @return boolean
     */
    public function isVisible()
    {
        return ! $this->getQuote()->isVirtual();
    }

    /**
     * Returns the available shipping methods.
     *
     * @return array
     */
    public function getMethods()
    {
        if ( ! $this->_shippingMethods) {
            $this->getShippingAddress()->collectShippingRates()->save();
            $this->_shippingMethods = $this->getShippingAddress()->getGroupedAllShippingRates();
        }

        return $this->_shippingMethods;
    }

    /**
     * Returns the number of available shipping methods.
     *
     * @return integer
     */
    public function getNumberOfMethods()
    {
        return count($this->getMethods());
    }

    /**
     * Formats the a shipping price.
     *
     * @param float $price
     *
     * @return string
     */
    public function formatShippingPrice($price)
    {
        return $this->getQuote()
                ->getStore()
                ->convertPrice(
                    Mage::helper('tax')->getShippingPrice(
                        $price,
                        $this->isShippingPriceTaxInclusive(),
                        $this->getShippingAddress()
                    ),
                    true
                );
    }

    /**
     * Checks whether the shipping price is tax inclusive.
     *
     * @return boolean
     */
    public function isShippingPriceTaxInclusive()
    {
        return $this->helper('tax')->displayShippingPriceIncludingTax();
    }

    /**
     * Returns the current shipping code.
     *
     * @return float
     */
    public function getShippingCode()
    {
        return $this->getShippingAddress()->getShippingMethod();
    }

    /**
     * Returns the current shipping address.
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getShippingAddress()
    {
        return $this->getQuote()->getShippingAddress();
    }
}