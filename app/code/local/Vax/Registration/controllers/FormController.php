<?php
/**
 * Registration Form Controller.
 *
 * @copyright Vax Ltd
 */

class Vax_Registration_FormController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get Action.
     *
     * Retrieves a product to be guaranteed.
     *
     * @return void
     */
    public function getAction()
    {
        $sku     = $this->getRequest()->getParam('sku');
        $product = Mage::getModel('catalog/product');
        $id = $product->getResource()->getIdBySku($sku);
        if ($id) {
            $product->load($id);
        }

        if ( ! $product) {
            $this->_redirect('*/');
            return;
        }

        Mage::getSingleton('core/session')->setVaxRegistrationSku($this->getRequest()->getParam('sku'));
        $this->_redirect('*/*/');
    }

    /**
     * Index Action.
     *
     * Displays the registration form.
     *
     * @return void
     */
    public function indexAction()
    {
        if ( ! Mage::getSingleton('core/session')->getVaxRegistrationSku()) {
            $this->_redirect('*/');
            return;
        }

        $sku     = Mage::getSingleton('core/session')->getVaxRegistrationSku();
        $product = null;
        
        if($sku) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        }
        
        if ($this->getRequest()->isPost()) {
            try
            {
                Mage::helper('vax_registration')->saveRegistration(
                    $product,
                    $this->getRequest()
                );

                $this->_redirect('*/*/success');
            }
            catch (\Exception $ex)
            {
                Mage::getSingleton('customer/session')->addError($this->__('Sorry. We were unable to submit your request. Please contact the Dirt Devil Careline on 0844 412 8455 if you continue to encounter this error'));
                Mage::log($ex->__toString(), null, 'registration.log');
            }
        }

        $this->loadLayout();
        $this->getLayout()
             ->getBlock('registration.form')
             ->setFormAction( Mage::getUrl('*/*') )
             ->setRegistrationProduct($product);

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Upload Action.
     *
     * Handles a document upload.
     *
     * @return void
     */
    public function uploadAction()
    {
        $upload_handler = new VaxUploadHandler();
        $upload_handler->initialize();
    }

    /**
     * Deletion Action.
     *
     * Deletes a document upload.
     *
     * @return void
     */
    public function deleteAction()
    {
        $upload_handler = new VaxUploadHandler();
        $upload_handler->initialize();
    }

    /*
     * Upload Registration completed file function
     */
    private function moveToCompletedDirectory($file_name, $version = null, $registration_id) {

        if(!$file_name) return false;

        $upload_handler = new Vax_Upload_Model_Upload();

        $file_path = $upload_handler->getUploadPath($file_name, $version);
        $new_file_name = false;

        try {
            if(is_file($file_path)) {
                $new_file_name = $registration_id . '.' . pathinfo($file_name, PATHINFO_EXTENSION);
                $new_path = $upload_handler->getCompletedDownloadPath($new_file_name, $version);

                if (copy($file_path, $new_path)) {
                    unlink($file_path);
                }
            }

        } catch (Exception $ex) {
            Mage::log($ex->__toString(), null, 'registration.log');
        }

        return $new_file_name;
    }

    /* handle file uploads */
    private function uploadProofOfPurchase() {
        
        try {
            $regfile = Mage::getModel('vax_registration/attachment')->loadByRegistrationId(Mage::getSingleton('core/session')->getVaxRegistrationId());

            if ($regfile && $regfile->getId()) {
                $new_path       = $this->moveToCompletedDirectory($regfile->getFileName(), null, Mage::getSingleton('core/session')->getVaxRegistrationId());
                $new_thumb_path = $this->moveToCompletedDirectory($regfile->getFileName(), 'thumbnail', Mage::getSingleton('core/session')->getVaxRegistrationId());

                if ($new_path) {
                    $regfile->setCompletedFileName($new_path);
                }

                if ($new_thumb_path) {
                    $regfile->setCompletedThumbnail($new_thumb_path);
                }

                $regfile->save();
            }
        
        } catch (Exception $ex) {
            Mage::log($ex->__toString(), null, 'registration.log');
        }
    }
    
    /**
     * Success Action.
     *
     * Displays the registration success page.
     *
     * @return void
     */
    public function successAction()
    {
        /* redirect if no registration id */
        if(!Mage::getSingleton('core/session')->getVaxRegistrationId()) {
            $this->_redirect('*/');
            return;
        }
        
        $registration = Mage::getModel('vax_registration/registration')->load(Mage::getSingleton('core/session')->getVaxRegistrationId());

        /* redirect if no registration in session */
        if (!$registration || !$registration->getId()) {
            $this->_redirect('*/');
            return;
        }

        /* handle registration uploads */
        $this->uploadProofOfPurchase();

        $product = $registration->getRegisteredProduct();
        
        if($product) {
            Mage::getSingleton('checkout/session')->setRegisteredProductId($product->getId());
        }
        
        $emailSentForRegistrationId = Mage::getSingleton('core/session')->getVaxRegistrationEmailSentForId();
        
        // only send the email once if a customer refreshes the page
        if($emailSentForRegistrationId !== $registration->getId()) {
            $this->sendRegistrationEmail($registration, $product);
            Mage::getSingleton('core/session')->setVaxRegistrationEmailSentForId($registration->getId());
        }
 
        $this->loadLayout();

        $this->getLayout()
             ->getBlock('registration.summary')
             ->setRegistrationDetails($registration)
             ->setRegistrationProduct($product);

        $this->renderLayout();
    }
    
    /**
     * sendRegistrationEmail.
     *
     * Send an email to the customer with their registration confirmation
     *
     * @return void
     */
    private function sendRegistrationEmail($registration, $product) {
        
        $emailTemplate = null;
        
        try {
            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('product_registration_tpl');
        } catch (Exception $ex) {
            Mage::log($ex->__toString(), null, 'registration.log');
        }
        
        // at the moment php-fpm is 50% segfaulting at loading the email template, so return false if it hasn't loaded
        if(!$emailTemplate) {
            return false;
        }
        
        try {
            $emailTemplate->setTemplateSubject(Mage::helper('core/data')->__('Vax %s Year Guarantee', $registration->getWarranty()));
            $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_support/name'));
            $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_support/email'));

            $emailTemplateVariables['c_name']     = $registration->getFirstName();
            $emailTemplateVariables['c_surname']  = $registration->getSurname();
            $emailTemplateVariables['c_code']     = $registration->getId();
            $emailTemplateVariables['c_model']    = $product->getName()." ".$product->getUkProductNo();
            $emailTemplateVariables['c_serial']   = $registration->getSerialNumber();
            $emailTemplateVariables['c_warranty'] = $registration->getWarranty();
            $emailTemplateVariables['c_purchase'] = date('d/m/Y', strtotime($registration->getPurDate()));
            $emailTemplateVariables['vax_phone']  = Mage::getStoreConfig('general/store_information/phone');
            $emailTemplateVariables['vax_email']  = Mage::getStoreConfig('trans_email/ident_support/email');

            $emailTemplate->send($registration->getEmail(), $registration->getCustomerName(), $emailTemplateVariables);
            
        } catch (Exception $ex) {
            Mage::log($ex->__toString(), null, 'registration.log');
        }
    }
}
