<?php
define('MAGENTO', realpath(__DIR__ . '/../../'));
require_once MAGENTO . '/app/Mage.php';
umask(0);
Mage::App();
// Retrieve DB Connection Details
$host = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/host');
$user = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/username');
$pwd = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/password');
$database = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
// http://www.dirtdevil.co.uk

$con = mysqli_connect($host, $user, $pwd, $database);

/*==========================================================================================================================*/
/*  SPARE PRODUCT PAGE
/*==========================================================================================================================*/
/*-------------------------------------------------------------*/
/*  Meta Title for Spares and Accessories Product pages
/*-------------------------------------------------------------*/
// attribute set id of Spares = 10
// store id for dirtdevil uk = 0
// product name = meta.attribute_id = 71
// meta_title attribute id = meta.attribute_id = 82

// select product names (attr 71 in meta) from catalog_product_entity_varchar where the product belongs to the dirtdevil website,
// its sku has an attribute set that says SPARES (10)
// and insert them into the value for the meta title (82)


//select the current row's product title'
$productTitleRowSelection = '
    SELECT meta.value FROM
     (SELECT catalog_product_entity_varchar.entity_id,
             catalog_product_entity_varchar.attribute_id,
             catalog_product_entity_varchar.value
      FROM catalog_product_entity_varchar)
      AS meta
           JOIN catalog_product_entity sku      ON meta.entity_id = sku.entity_id
           JOIN catalog_product_website website ON meta.entity_id = website.product_id
    WHERE website.website_id    = 1
      AND sku.attribute_set_id = 10
      AND meta.attribute_id    = 71
      AND meta.entity_id       = meta1.entity_id
    ORDER BY meta.entity_id
';

//add product names to the title field
$sqlProductTitle = "
    UPDATE catalog_product_entity_varchar AS meta1
    SET    meta1.value = (" . $productTitleRowSelection . ")
    WHERE  meta1.attribute_id = 82
           AND (" . $productTitleRowSelection . ") IS NOT NULL ;
";
mysqli_query($con, $sqlProductTitle);


// add '| Dirtdevil to each spares title field'
$sqlMetaTitle = "
    UPDATE catalog_product_entity_varchar AS meta
        SET meta.value =
          CASE WHEN length(concat(
          (" . $productTitleRowSelection . ")
          , ' | Dirt Devil UK Genuine Spares')) > 64
            THEN concat(meta.value,' | Dirt Devil UK')
            ELSE concat(meta.value, ' | Dirt Devil UK Genuine Spares')
          END
    WHERE meta.attribute_id = 82
          AND (" . $productTitleRowSelection . ") IS NOT NULL ;
";
mysqli_query($con, $sqlMetaTitle);

/*-------------------------------------------------------------*/
/*  Meta Description for Spares and Accessories Product pages
/*-------------------------------------------------------------*/
// attribute set id of Spares = 10
// store id for dirtdevil uk = 0
// product name = meta.attribute_id = 71
// meta_description attribute id = meta.attribute_id = 84

// select product names (attr 71 in meta) from catalog_product_entity_varchar where the product belongs to the dirtdevil website,
// its sku has an attribute set that says SPARES (10)
// and insert them into the value for the meta description (84)
// concatenate selected product name with "Buy genuine part ect."

//add product names to the description field
$sqlProductDescription = "
    UPDATE catalog_product_entity_varchar AS meta1
    SET meta1.value = concat('Buy Genuine Dirt Devil ',(" . $productTitleRowSelection . "),
         ' from Dirt Devil UK Official Spares Website. Find Spares and Accessories for any Dirt Devil Floor Cleaning Machine.')
    WHERE meta1.attribute_id = 84
    AND (" . $productTitleRowSelection . ") IS NOT NULL ;
";
mysqli_query($con, $sqlProductDescription);


/*==========================================================================================================================*/
/*  SUPPORT
/*==========================================================================================================================*/
/*-------------------------------------------------------------*/
/*  Meta Title for product support pages
/*-------------------------------------------------------------*/
// First copy the right product titles in the meta title field
//
// category parents for product support: Uprights, Cylinders, Cordless, Steamers, Carpet Washers, Handhelds - 47, 48, 49, 50, 51, 52
// product name = category name = meta.attribute_id = 41
// meta_title attribute id = meta.attribute_id = 46

// select product names (attr_id = 41 in meta) from catalog_category_entity_varchar where the product belongs to the dirtdevil uk website,
// it is a child category of one of the machine types under Support category
// and insert them into the value for the meta title (46)

//select the current row's associated product name'
$supportProductTitleRowSelection = '
 SELECT meta.value FROM
          (SELECT catalog_category_entity_varchar.value,
                  catalog_category_entity_varchar.attribute_id,
                  catalog_category_entity_varchar.store_id,
                  catalog_category_entity_varchar.entity_id
           FROM catalog_category_entity_varchar
          )  AS meta
        LEFT JOIN catalog_category_entity parentLink ON meta.entity_id = parentLink.entity_id
        WHERE parentLink.parent_id in (47, 48, 49, 50, 51, 52)
          AND meta.attribute_id = 41
          AND meta.store_id = 0
          AND meta.entity_id = meta1.entity_id
        ORDER BY meta.entity_id
';

$sqlSupportProductTitle = "
    UPDATE catalog_category_entity_varchar AS meta1
    SET meta1.value = (" . $supportProductTitleRowSelection . ")
    WHERE meta1.attribute_id = 46
      AND meta1.store_id = 0
      AND (" . $supportProductTitleRowSelection . ") IS NOT NULL ;
";
mysqli_query($con, $sqlSupportProductTitle);

// add 'Support | Dirt Devil' to each title field'
$sqlSupportProductMetaTitle = "
    UPDATE catalog_category_entity_varchar AS meta
        SET meta.value =
          CASE WHEN length(concat('Support | Dirt Devil ', (" . $supportProductTitleRowSelection . "))) > 65
            THEN concat('Support | ', meta.value)
            ELSE concat('Support | Dirt Devil ', meta.value)
          END
    WHERE meta.attribute_id = 46
      AND meta.store_id     = 0;
      AND (" . $supportProductTitleRowSelection . ") IS NOT NULL ;
";
mysqli_query($con, $sqlSupportProductMetaTitle);


/*-------------------------------------------------------------*/
/*  Meta Description for product support pages
/*-------------------------------------------------------------*/
// First copy the right product titles in the meta description field field
//
// category parents for product support: Uprights, Cylinders, Cordless, Steamers, Carpet Washers, Handhelds - 47, 48, 49, 50, 51, 52
// product name = category name = meta.attribute_id = 41 in catalog_category_entity_varchar
// meta_decription attribute id = catalog_category_entity_text.attribute_id = 48

// select product names (attr_id = 41 in meta) from catalog_category_entity_varchar where the product belongs to the dirtdevil uk website,
// it is a child category of one of the machine types under Support category
// and insert them into the value for the meta description (48) in catalog_category_entity_text

// then find product number for the products referenced in the support pages
$sqlSupportProductDescription = "
    UPDATE catalog_category_entity_text AS meta1
    SET meta1.value = concat('Dirt Devil ',
        (" . $supportProductTitleRowSelection . "), ' ',
        (
            SELECT productId.value FROM
              (SELECT catalog_product_entity_varchar.value,
                      catalog_product_entity_varchar.attribute_id,
                      catalog_product_entity_varchar.store_id,
                      catalog_product_entity_varchar.entity_id
               FROM catalog_product_entity_varchar
              )  AS productId
              LEFT JOIN catalog_category_entity_int catProduct ON productId.entity_id = catProduct.value
              LEFT JOIN catalog_category_entity parentLink     ON catProduct.entity_id = parentLink.entity_id
            WHERE productId.attribute_id = 185
             AND  catProduct.attribute_id = 184
             AND parentLink.parent_id in (47, 48, 49, 50, 51, 52)
             AND catProduct.store_id = 0
             AND catProduct.entity_id = meta1.entity_id
            ORDER BY catProduct.entity_id
        ),
        ' User Guides, Spares & Accessories, Helpful Videos, FAQs, and Technical Support. Dirt Devil UK Product Support Portal. '
    )
    WHERE meta1.attribute_id = 48
      AND meta1.store_id = 0;
      AND (" . $supportProductTitleRowSelection . ") IS NOT NULL ;
";
mysqli_query($con, $sqlSupportProductDescription);

mysqli_close($con);
?>