<!DOCTYPE html>
<html>
<head>
    <title>Dirt Devil UK</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="pragma" content="no-cache">
    <link rel="stylesheet" type="text/css" href="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/css/styles.css">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/js/blazy.min.js"></script>
    <script src="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/js/scripts.js"></script>
</head>
<body>
<div class="header">
    <div class="image-wrapper">
        <img class="logo b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/images/logo-980.png" data-src-medium="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/images/logo-770.png" data-src-small="http://8cad4b46fd454aa339e1-dde2451a9d687113c6d407a3acfbca54.r44.cf3.rackcdn.com/skin/frontend/dirtdevil_rwd/uk_holding/images/logo-320.png" alt="Dirt Devil UK">
    </div>
</div>
<div class="content">
    <div class="sub-header">
        <h1>New Website Coming Soon.</h1>
    </div>
    <p class="contact-advice">To register your warranty and for any product queries, please give us a call on:</p>
    <p class="contact-phone">Phone: <br style="display: none;" /> <span class="number">0330 026 2626</span> (Mon-Fri 9am-5pm)</p>
</div>
<div class="footer">
    <p class="footer-cta">
        Let's Go.
    </p>
</div>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-59407703-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>