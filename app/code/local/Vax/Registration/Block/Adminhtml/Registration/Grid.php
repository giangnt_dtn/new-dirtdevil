<?php

/**
 * Registration Grid Adminhtml Block.
 *
 * @copyright Vax Ltd
 */
class Vax_Registration_Block_Adminhtml_Registration_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize grid settings
     *
     */
    protected function _construct() {
        parent::_construct();

        $this->setId('vax_registration_list');
        $this->setDefaultSort('id');

        /*
         * Override method getGridUrl() in this
         * class to provide URL for ajax
         */
        $this->setUseAjax(true);
    }

    /**
     * Prepare registration collection
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('vax_registration/registration_collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns to those in the database
     *
     * @return Vax_Registration_Block_Adminhtml_Registration_Grid
     */
    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => $this->__('Reference'),
            'sortable' => true,
            'width' => '60px',
            'index' => 'entity_id'
        ));

        $this->addColumn('first_name', array(
            'header' => $this->__('First Name'),
            'index' => 'first_name'
        ));

        $this->addColumn('surname', array(
            'header' => $this->__('Surname'),
            'index' => 'surname'
        ));

        $this->addColumn('postcode', array(
            'header' => $this->__('Postcode'),
            'index' => 'postcode'
        ));

        $this->addColumn('model', array(
            'header' => $this->__('GPID/SKU'),
            'index' => 'model'
        ));

        $this->addColumn('pur_date', array(
            'header' => $this->__('Purchased'),
            'index' => 'pur_date'
        ));

        $this->addColumn('warranty', array(
            'header' => $this->__('Length'),
            'index' => 'warranty'
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('cms')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }

        // If they have permission to edit the guarantee
        if(Mage::getSingleton('admin/session')->isAllowed('registration/edit')) {
            $this->addColumn('action', array(
                'header' => $this->__('Action'),
                'width' => '100px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => $this->__('Edit Details'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'registration_id',
                    ),
                ),
                'filter' => false,
                'sortable' => false,
            ));
        }

        $this->addExportType('*/*/exportCsv', Mage::helper('vax_registration')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('vax_registration')->__('XML'));

        return parent::_prepareColumns();
    }

    /**
     * Return URL where to send the user when he clicks on a row
     */
    public function getRowUrl($row) {
        return $this->getUrl('*/*/view', array('registration_id' => $row->getId()));
    }

    /**
     * Return Grid URL for AJAX query
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }
}