<?php

class Vax_SalesRule_Model_Resource_SalesRule_Rule_Collection extends Mage_SalesRule_Model_Resource_Rule_Collection
{
    protected function _beforeLoad()
    {
        if (!$this->getFlag('include_free_gift_rules')) {
            // don't process free gift rules in total calculation
            $this->addFieldToFilter('is_free_gift_promotion', 0);
        } else {
            $this->addFieldToFilter('is_free_gift_promotion', 1);
        }

        return parent::_beforeLoad();
    }
}