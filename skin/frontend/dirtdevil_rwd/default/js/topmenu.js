$j(document).ready(function () {
    navDropdown();
});

function navDropdown() {
    enquire.register('screen and (min-width: ' + 771 + 'px)', {
        match: function () {
            navDropdownCall();
        },
        unmatch: function () {
            $j('.nav-products').show();
            $j('.nav-spares').show();
            $j('.nav-support').show();
            $j('.nav-primary').show();
            $j('.nav-primary ').css('display', '');
            $j('.nav-primary div').show();
        }
    });
}

function navDropdownCall() {
    $j(".nav-top li.products").on('mouseenter touchend click', function () {
        $j('.nav-products').show();
        $j('.nav-spares').hide();
        $j('.nav-support').hide();
        // to do link to unveil enabled in admin
        //$j('img').unveil();
    });

    $j(".nav-top li.spares").on('mouseenter touchend click', function () {
        $j('.nav-products').hide();
        $j('.nav-spares').show();
        $j('.nav-support').hide();
    });

    //desktop behavior
    if(! $j('.touch').length) {
        $j(".nav-top li.products, .nav-top li.spares").on('click mouseenter', function (e) {
            if($j( window ).width() >= 771)
                $j('.nav-primary').slideDown('fast');
        });
        $j("#header-nav").mouseleave(function () {
            if($j( window ).width() >= 771)
                $j('.nav-primary').slideUp('fast');
        });
    }

    //touchscreen behavior
    if($j('.touch').length && $j( window ).width() >= 771) {
        // if the user taps on a link, the menu opens;
        // if the user taps again on that link, the menu closes
        // if the user taps on another link, the other link is open and the menu remains open
        var origin = null;

        $j(".nav-top li.products, .nav-top li.spares").on('touchend', function (e) {
            if (origin == null || origin != e.target) {
                $j('.nav-primary').slideDown('fast');
                origin = e.target;
            } else if (origin == e.target){
                $j('.nav-primary').slideUp('fast');
                origin = null;
            }
        });
    }
}