<?php
/**
 * Vax Registration Edit form block class
 *
 * @category    code
 * @package     local
 * @copyright   Copyright (c) 2012 Vax Ltd.
 * @author      M Stephens
 */

class Vax_Registration_Block_Adminhtml_Registration_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare the inner form wrapper
     */
    protected function _prepareForm() {
        $form = new Varien_Data_Form(array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save', array('registration_id' => $this->getRequest()->getParam('registration_id'))),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ));
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}