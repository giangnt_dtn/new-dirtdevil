<?php

/**
 * Vac Checkout Observer.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_Checkout_Model_Observer
{
    protected $_quote        = null;
    protected $_shippingCode = 'flatrate_flatrate';

    /**
     * Returns the current quote.
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if ( ! $this->_quote) {
            $this->_quote = Mage::getSingleton('checkout/session')->getQuote();
        }

        return $this->_quote;
    }

    /**
     * Adds default shipping method to quote.
     *
     * @param null $params
     * @return bool
     */
    public function addShipping($params = null) {

        if (Mage::registry('checkout_addShipping')) {
            Mage::unregister('checkout_addShipping');
            return;
        }

        Mage::register('checkout_addShipping',true);

        $cart = Mage::getSingleton('checkout/cart');
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $cart->getQuote();

        // Coupon code
        if ($quote->getCouponCode() != '') {
            $c = Mage::getResourceModel('salesrule/rule_collection');
            $c->getSelect()->where("code=?", $quote->getCouponCode());

            foreach ($c->getItems() as $coupon) {
                if ($coupon->getSimpleFreeShipping() > 0) {
                    $quote->getShippingAddress()->setShippingMethod($this->_getCheapestShippingMethod()->getCode())->save();
                    return true;
                }
            }
        }

        try {
            // Sets country ID to be default if none is set
            if ($quote->getShippingAddress()->getCountryId() == '') {
                $quote->getShippingAddress()->setCountryId($this->_getDefaultShippingCountry());
            }

            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->getShippingAddress()->collectShippingRates();

            // Get avaliable store shipping options
            $rate = $this->_getCheapestShippingMethod();

            // If allowed then proceed in setting shipping cost in cart
            if ($rate && $rate->getCode()) {
                $this->_shippingCode = $rate->getCode();
                $address = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress();
                $address->setShippingMethod($rate->getCode())->setCollectShippingRates(true);
            }
        }
        catch (Mage_Core_Exception $e) {
            Mage::getSingleton('checkout/session')->addError($e->getMessage());
        }
        catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addException($e, Mage::helper('checkout')->__('Load customer quote error'));
        }
    }

    /**
     * Returns default delivery option.
     *
     * @return mixed
     */
    protected function _getDefaultShippingCountry()
    {
        return Mage::getStoreConfig('shipping/origin/country_id');
    }

    /**
     * Returns the cheapest shipping rate for delivery address.
     *
     * @return mixed
     */
    protected function _getCheapestShippingMethod()
    {
        $collection = $this->getQuote()->getShippingAddress()->getShippingRatesCollection()->addOrder('price', 'ASC');

        foreach ($collection as $method) {
            if (!$method->isDeleted()) {
                return $method;
            }
        }
    }
}
