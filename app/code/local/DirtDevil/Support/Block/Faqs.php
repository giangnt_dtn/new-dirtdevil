<?php
/**
 * FAQs block
 *
 * @category   DirtDevil
 * @package    DirtDevil_Page
 * @author     Vax
 */
class DirtDevil_Support_Block_Faqs extends Mage_Core_Block_Template
{
    protected $product = null;
    protected $category = null;

    protected function _construct() {
        parent::_construct();

        $this->product = $this->getProduct();
    }

    public function getProduct() {
        if($this->product) {
            return $this->product;
        }

        if(Mage::registry("current_category")) {
            $this->category = Mage::registry("current_category");
        }

        if($this->category && $this->category->getCategoryProductId()) {
            $this->product = Mage::getModel('catalog/product')->load($this->category->getCategoryProductId());

            return $this->product;
        }

        return false;
    }

    public function getProductId() {
        if($this->getProduct()) {
            return $this->getProduct()->getId();
        }

        return false;
    }

    public function getTopics($publicOnly = true, $questionType = null) {
        if(!$this->getProduct()) {
            return false;
        }

        return Mage::getModel('prodfaqs/prodfaqs')->getTopicsByProduct($this->getProductId(), $publicOnly, $questionType);
    }

    public function getProductRelatedFaqs($publicOnly = true, $questionType = null) {
        if(!$this->getProduct()) {
            return false;
        }

        $product_faqs = Mage::getModel('prodfaqs/prodfaqs')->getFaqsOfProduct($this->getProductId(), $publicOnly, $questionType);
        return $product_faqs;
    }

    public function getTopicTitle($topic) {
        return $topic['topic_title'];
    }

    public function getTopicCount($topic) {

        return count($topic);
    }

    public function getFaqInTopicCount($productId, $topic) {

        return count($this->getFaqsInTopic($productId, $topic));
    }

    public function getFaqsInTopic($productId, $topic) {

        $collection = Mage::getModel('prodfaqs/prodfaqs')->getFaqsByTopic($topic, true, false, $productId);

        return $collection;
    }

    public function getTopFaq($limit)
    {
        if(!$this->getProduct()) {
            return false;
        }

        $data = Mage::getModel('prodfaqs/prodfaqs')->getTopicsByProduct($this->product->getId());
        $faqIds = array();
        foreach ($data as $faq) {
            $faqIds[] = $faq['faqs_id'];
        }

        $collection = Mage::getResourceModel('prodfaqs/prodfaqs_collection')->addFieldToFilter('main_table.faqs_id', $faqIds);
        $collection->addStatusFilter(1)
                   ->addFieldToFilter('visibility', 'public')
                   ->addFieldToFilter('question_type', 'product_question');
        $collection->join(array('topic' => 'prodfaqs/topic'), 'main_table.topic_id=topic.topic_id', array('topic_title' => 'topic.title'));

        $collection->setOrder('topic.topic_order', 'DESC');
        $collection->setOrder('main_table.faq_order', 'DESC');

        $collection->setPageSize($limit);

        return $collection;
    }

    public function getFaqHtml($faq, $isLast = false, $line = null, $template = 'prodfaqs/singlefaq.phtml')
    {
        if (!$faq instanceof Vax_Prodfaqs_Model_Prodfaqs) {
            $faq = Mage::getModel('prodfaqs/prodfaqs')->load((int)$faq);
        }
        $topic = Mage::getModel('prodfaqs/topic')->load($faq->getTopicId());
        $faq->setTopic($topic);
        if (!$faq->getFaqsId()) {
            return '';
        }
        $block = $this->getLayout()->createBlock('vaxfaqs/faq', 'faq_block_' . $faq->getFaqsId());
        $block->setTemplate($template)
            ->setLineNumber($line)
            ->setIsLastFaq($isLast)
            ->setFaq($faq);
        return $block->toHtml();
    }
}
