<?php

/**
 * Vax Post Code Anywhere Controller.
 *
 * @copyright Vax Ltd 2013
 */
class Vax_PostcodeAnywhere_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Index Action.
     *
     * Searches for an address by post code.
     *
     * @return void
     */
    public function indexAction()
    {
        if ( ! Mage::helper('vax_postcodeanywhere')->isEnabled() || ! $this->getRequest()->getParam('postcode')) {
            return;
        }

        $emptyNo = 0;
        $addresses = Mage::helper('vax_postcodeanywhere')->getAddressesByPostcode(
            $this->getRequest()->getParam('postcode')
        );

        if (Mage::getStoreConfig('vax_postcodeanywhere/general/add_empty_option')) {
            array_unshift($addresses, array(
                'label' => Mage::helper('vax_postcodeanywhere')->__('Select Address'),
                'value' => -1)
            );
            $emptyNo = 1;
        }

        if (count($addresses) == $emptyNo) {
            $html = '<div class="control-group error"><div class="controls help-inline">' . Mage::helper('vax_postcodeanywhere')->__('Postcode not found.') . '</div></div>';
            $html .= '<a href="" class="btn-manual-entry btn-manual-entry-main">' . Mage::helper('vax_postcodeanywhere')->__('Enter address manually') . '</a>';
            $this->getResponse()->setBody($html);
            return;
        }

        $this->loadLayout();

        $select = $this->getLayout()->createBlock('vax_postcodeanywhere/addressSelect');

		if(Mage::getStoreConfig('vax_postcodeanywhere/general/address_multiple_select')) {
			$select->setExtraParams('multiple="multiple"');
		}

        $select->setOptions($addresses);
        $select->setName('address_id')->setId('address_id')->setClass('address-select')->setTitle(Mage::helper('core')->__('Select address'));
        $block  = $this->getLayout()->createBlock('vax_postcodeanywhere/addressSelectForm')->setSelectElement($select);
        $this->getResponse()->setBody($block->toHtml());
    }

    /**
     * Fetch Address Action.
     *
     * Searches for an address by ID.
     *
     * @return void
     */
    public function fetchAddressAction()
    {
        if ( ! Mage::helper('vax_postcodeanywhere')->isEnabled() || ! $this->getRequest()->getParam('address_id')) {
            return;
        }

        $address = Mage::helper('vax_postcodeanywhere')->getAddressByID(
            $this->getRequest()->getParam('address_id')
        );

        $quote           = Mage::getSingleton('checkout/session')->getQuote();
        $shippingAddress = $quote->getShippingAddress();

        if (Mage::app()->getStore()->getId() == 16) {
            $this->getResponse()->setBody(Zend_Json::encode($address));
            return;
        }

        if ( ! $quote->getShippingAddress()->getPostcode() && $quote->isCustomerInOutlyingRegion($address['postcode'])) {
            Mage::getSingleton('core/session')->addError('Sorry next day delivery is unavailable for customers living in outlying regions of the UK');
            $shippingAddress->setPostcode($address['postcode'])->save();
            $this->getResponse()->setBody(Zend_Json::encode(array('redirect' => Mage::getUrl('checkout/cart'))));
            return;
        }

        $this->getResponse()->setBody(Zend_Json::encode($address));
    }
}