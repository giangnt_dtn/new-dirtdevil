<?php
require_once "Mage/Newsletter/controllers/SubscriberController.php";

/**
 * Newsletter Subscriber Controller.
 *
 * @copyright DirtDevil 2015
 */
class DirtDevil_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController
{
    /**
     * New subscription action
     */
    public function newAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $session          = Mage::getSingleton('core/session');

            if( $referer = $this->_getRefererUrl() ) {
                $referer = htmlspecialchars_decode( $referer );
                $dummyRequest = Mage::helper( 'turpentine/esi' )->getDummyRequest( $referer );
                $route = $dummyRequest->getRouteName();
                if ($route == 'checkout')
                {
                  $session = Mage::getSingleton('dirtdevil_newsletter/newsletterSession');
                }
            }

            $customerSession  = Mage::getSingleton('customer/session');
            $email            = (string)$this->getRequest()->getPost('email');
            $source           = (string)$this->getRequest()->getPost('area');
            $newsletterSource = "";

            try {
                if (Mage::helper('email')->isFormKeyValidationEnabled() && !$this->_validateFormKey()) {
                    $session->addError($this->__('There was a problem with the subscription.'));
                    $this->_redirectReferer();
                    return;
                }
                
                /* added firstname and lastname to subscribe */
                $firstName = (string)$this->getRequest()->getPost('subscriber_firstname');
                $lastName  = (string)$this->getRequest()->getPost('subscriber_lastname');

                /*sets the newsletter source text*/
                if ($source === "newsletter_footer"){
                    $newsletterSource = "Footer";
                } else if ($source === "newsletterValidateDetail") {
                    $newsletterSource = "Newsletter Page";
                } else if ($source === "successNewsletterForm") {
                    $newsletterSource = "Checkout Success After";
                }
                
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    Mage::throwException($this->__('Subscription failed: Please enter a valid email address. For example johndoe@gmail.com'));
                }
                //check if the names provided are not empty strings, and are not random non-letter characters
                if (strlen(trim($firstName)) == 0 ||
                    strlen(trim($lastName)) == 0 ||
                    !preg_match('/[a-zA-Z]/', $firstName) ||
                    !preg_match('/[a-zA-Z]/', $lastName)
                ) {

                    Mage::throwException($this->__('Subscription failed: Please enter your first and last names. You can only use letters.'));
                }

                //check if newsletter subscription is enabled
                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                    !$customerSession->isLoggedIn()
                ) {
                    Mage::throwException($this->__('We are sorry, but subscriptions for guests are currently disabled. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                }

                //check if the subscriber already exists
                $ownerId = Mage::getModel('customer/customer')
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email)
                    ->getId();
                if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                    $session->addError($this->__('Subscription failed: This email is already in use.'));
                }

                //create a new subscription with the email and names of the user
                $status = Mage::getModel('dirtdevil_newsletter/subscriber')->subscribe($email, $firstName, $lastName, $newsletterSource);

                //success messages
                if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                    $session->addSuccess($this->__('Thank you! A confirmation request has been sent to your email.'));
                } else {
                    $session->addSuccess($this->__('Thank you! You have successfully subscribed to our newsletter.'));
                }
            } catch (Mage_Core_Exception $e) {
                $session->addError($this->__('%s', $e->getMessage()));
                Mage::logException($e);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                $session->addError($this->__('We are sorry, there was a problem with the subscription.'));
                Mage::logException($e);
                $this->_redirectReferer();
                return;
            }
        }
        $this->_redirectReferer();
    }

    /**
     * Subscription confirm action
     */
    public function confirmAction()
    {
        $id   = (int)$this->getRequest()->getParam('id');
        $code = (string)$this->getRequest()->getParam('code');

        if ($id && $code) {
            $subscriber = Mage::getModel('newsletter/subscriber')->load($id);
            $session    = Mage::getSingleton('core/session');

            if ($subscriber->getId() && $subscriber->getCode()) {
                if ($subscriber->confirm($code)) {
                    $session->addSuccess($this->__('Your subscription has been confirmed.'));
                    $subscriber->sendConfirmationSuccessEmail();
                } else {
                    $session->addError($this->__('Invalid subscription confirmation code.'));
                }
            } else {
                $session->addError($this->__('Invalid subscription ID.'));
            }
        }

        $this->_redirectUrl(Mage::getBaseUrl());
    }
}