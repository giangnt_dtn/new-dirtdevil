<?php
/**
 * @author Bazaarvoice, Inc.
 */
class DirtDevilUk_Bazaarvoice_Model_ProductFeed_Product extends Bazaarvoice_Connector_Model_ProductFeed_Product
{
    /**
     * @param Varien_Io_File $ioObject File object for feed file
     * @param Mage_Catalog_Model_Product $productDefault
     * @param array $productsByLocale
     */
    /*
     * modify plugin for ean_code and uk_product_no
     */
    protected function writeProduct(Varien_Io_File $ioObject, Mage_Catalog_Model_Product $productDefault,
        array $productsByLocale)

    {
        // Get ref to BV helper
        /* @var $bvHelper Bazaarvoice_Connector_Helper_Data */
        $bvHelper = Mage::helper('bazaarvoice');

        // Generate product external ID from SKU, this is the same for all groups / stores / views
        $productExternalId = $bvHelper->getProductId($productDefault);
        
        $website = $productDefault->getStore()->getWebsite()->getId();
        
        /* Make sure that CategoryExternalId is one written to Category section */
        if($productDefault->getVisibility() == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE) {
            if($productDefault->getData("product_families")){
                // if families are enabled and product is not visible, use parent categories
                $parentId = array_pop($productDefault->getData("product_families"));
                $parentProduct = $bvHelper->getProductFromProductExternalId($parentId);
                
                // skip product if parent is disabled
    			if (is_object($parentProduct)){
    				if($parentProduct->getVisiblity() == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE || $parentProduct->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED || !in_array($website, $parentProduct->getWebsiteIds())) {
    					Mage::log("        Skipping ".$productDefault->getSku()." because it is not visible and its parent product " . $parentProduct->getSku() . " is disabled.", Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
    					return true;
    				}
    				
    				if (!is_null($parentProduct->getCategoryIds())){
    					$parentCategories = $parentProduct->getCategoryIds();
    					Mage::log("        Product ".$productDefault->getSku()." using parent categories from ".$parentProduct->getSku(), Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
    				}
    			}
            } else {
				Mage::log("        Skipping ".$productDefault->getSku()." because it is not visible and has no parent product.", Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
				return true;
            }
        } else {
            // normal behavior
            $parentCategories = $productDefault->getCategoryIds();
            Mage::log("        Product ".$productDefault->getSku()." using its own categories", Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
        }

        $ioObject->streamWrite("<Product>\n" .
            '    <ExternalId>' . $productExternalId . "</ExternalId>\n" .
            '    <Name><![CDATA[' . htmlspecialchars($productDefault->getName(), ENT_QUOTES, 'UTF-8') . "]]></Name>\n" .
            '    <Description><![CDATA[' . htmlspecialchars($productDefault->getData('short_description'), ENT_QUOTES, 'UTF-8') .
            "]]></Description>\n");

        $brandId = $productDefault->getData('brand');
        if ($productDefault->hasData('brand') && !is_null($brandId) && !empty($brandId)) {
            $ioObject->streamWrite('    <BrandExternalId>' . $brandId . "</BrandExternalId>\n");
        }

        if (!is_null($parentCategories) && count($parentCategories) > 0) {
            $validCategoryIdList = $this->getCategoryIdList();
            foreach ($parentCategories as $parentCategoryId) {
                $parentCategory = Mage::getModel('catalog/category')->setStoreId($productDefault->getStoreId())->load($parentCategoryId);
                if ($parentCategory != null) {
                    $categoryExternalId = $bvHelper->getCategoryId($parentCategory, $productDefault->getStoreId());
                    if (is_array($validCategoryIdList) && in_array($categoryExternalId, $validCategoryIdList)) {
                        $ioObject->streamWrite('    <CategoryExternalId>' . $categoryExternalId .
                            "</CategoryExternalId>\n");
                        break;
                    } else {
                        Mage::log("        Category $categoryExternalId not found", Zend_Log::DEBUG, Bazaarvoice_Connector_Helper_Data::LOG_FILE);
                    }
                }
            }
        }

        $modelNoAttribute = 'uk_product_no';
        if($modelNoAttribute && $productDefault->getData($modelNoAttribute)) {
            $ioObject->streamWrite('    <ModelNumbers><ModelNumber>' . $productDefault->getData($modelNoAttribute) . "</ModelNumber></ModelNumbers>\n");
        }

        $manufacturerPartNumber = 'sku';
        if($manufacturerPartNumber && $productDefault->getData($manufacturerPartNumber)) {
            $ioObject->streamWrite('    <ManufacturerPartNumbers><ManufacturerPartNumber>' . $productDefault->getData($manufacturerPartNumber) . "</ManufacturerPartNumber></ManufacturerPartNumbers>\n");
        }

        //$upcAttribute = Mage::getStoreConfig("bazaarvoice/bv_config/product_feed_upc_attribute_code");
		$upcAttribute = 'upc_code';
        if($upcAttribute && $productDefault->getData($upcAttribute)) {
            $ioObject->streamWrite('    <UPCs><UPC>' . $productDefault->getData($upcAttribute) . "</UPC></UPCs>\n");            
        }
        
        $eanAttribute = "ean_code";
        if($eanAttribute && $productDefault->getData($eanAttribute)) {
            $ioObject->streamWrite('    <EANs><EAN>' . $productDefault->getData($eanAttribute) . "</EAN></EANs>\n");            
        }

        $ioObject->streamWrite('    <ProductPageUrl>' . "<![CDATA[" . $this->getProductUrl($productDefault) . "]]>" . "</ProductPageUrl>\n");
        $imageUrl = $productDefault->getData('localized_image_url');;
        if (strlen($imageUrl)) {
            $ioObject->streamWrite('    <ImageUrl>' . "<![CDATA[" . $imageUrl . "]]>" . "</ImageUrl>\n");
        }
        
        // Write out localized <Names>
        $ioObject->streamWrite("    <Names>\n");
        foreach ($productsByLocale as $curLocale => $curProduct) {
            $ioObject->streamWrite('        <Name locale="' . $curLocale . '"><![CDATA[' .
                htmlspecialchars($curProduct->getData('name'), ENT_QUOTES, 'UTF-8') . "]]></Name>\n");
        }
        $ioObject->streamWrite("    </Names>\n");
        // Write out localized <Descriptions>
        $ioObject->streamWrite("    <Descriptions>\n");
        foreach ($productsByLocale as $curLocale => $curProduct) {
            $ioObject->streamWrite('         <Description locale="' . $curLocale . '"><![CDATA[' .
                htmlspecialchars($curProduct->getData('short_description'), ENT_QUOTES, 'UTF-8') . "]]></Description>\n");
        }
        $ioObject->streamWrite("    </Descriptions>\n");
        // Write out localized <ProductPageUrls>
        $ioObject->streamWrite("    <ProductPageUrls>\n");
        foreach ($productsByLocale as $curLocale => $curProduct) {
            $ioObject->streamWrite('        <ProductPageUrl locale="' . $curLocale . '">' . "<![CDATA[" .
                $this->getProductUrl($curProduct) . "]]>" . "</ProductPageUrl>\n");
        }
        $ioObject->streamWrite("    </ProductPageUrls>\n");
        // Write out localized <ImageUrls>
        $ioObject->streamWrite("    <ImageUrls>\n");
        foreach ($productsByLocale as $curLocale => $curProduct) {
            $imageUrl = $curProduct->getData('localized_image_url');
            if (strlen($imageUrl)) {
                $ioObject->streamWrite('<ImageUrl locale="' . $curLocale . '">' . "<![CDATA[" . $imageUrl .
                    "]]>" . "</ImageUrl>\n");
            }
        }
        $ioObject->streamWrite("    </ImageUrls>\n");
        // Product Families
        if($productDefault->getData("product_families")){
            $ioObject->streamWrite("    <Attributes>\n");
            foreach($productDefault->getData("product_families") as $family){
                 $ioObject->streamWrite('        <Attribute id="BV_FE_FAMILY"><Value>'.$family.'</Value></Attribute>'."\n");
            }
            if($productDefault->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE || strtolower(Mage::getStoreConfig("bazaarvoice/bv_config/product_feed_export_bvfamilies_expand")) == 'true' || Mage::getStoreConfig("bazaarvoice/bv_config/product_feed_export_bvfamilies_expand") == '1'){
                $ioObject->streamWrite('        <Attribute id="BV_FE_EXPAND">'."\n");
                foreach($productDefault->getData("product_families") as $family){
                     $ioObject->streamWrite('            <Value>BV_FE_FAMILY:'.$family.'</Value>'."\n");
                }
                $ioObject->streamWrite("        </Attribute>\n");
            }
            $ioObject->streamWrite("    </Attributes>\n");
        }

        // Close this product
        $ioObject->streamWrite("</Product>\n");
    }
}
