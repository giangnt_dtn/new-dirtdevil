<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `{$this->getTable('realexExtended/chanel')}`
    ADD COLUMN `tss_rule_specific` varchar(255) AFTER `cvn_result`;
");
$installer->endSetup();