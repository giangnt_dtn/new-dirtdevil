<?php
/**
* DirtDevi Spares Helper
*
* @category   DirtDevi
* @package    DirtDevil_Spares
* @author     Vax
*/
class DirtDevil_Spares_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_SPARES_ROOT_CATEGORY    = 'dirtdevil_spares/config/spares_root_category';
    const XML_SUPPORT_ROOT_CATEGORY    = 'dirtdevil_spares/config/support_root_category';

    /**
     * Returns the root category for spares
     *
     * @return int
     */
    public function getSparesRootCategory()
    {
        return Mage::getStoreConfig(self::XML_SPARES_ROOT_CATEGORY);
    }

    /**
     * Returns the root category for support
     *
     * @return int
     */
    public function getSupportRootCategory()
    {
        return Mage::getStoreConfig(self::XML_SUPPORT_ROOT_CATEGORY);
    }

    public function getThumbnailImage($category) {
        if($category->getThumbnail()) {
            return Mage::helper('catalog/category')->getThumbnailUrl($category->getThumbnail());
        } else {
            return (string)Mage::helper('catalog/image')->init(
                Mage::getModel('catalog/product'),
                'small_image'
            )->resize(270, 270);
        }
    }

    /*
     * Check synonyms for similiar sku (machines only)
     */
    public function getMachineMatchingSynonym($sku) {
        $baseSkus = explode('-', $sku);
        $sku = $baseSkus[0];

        if (0 === preg_match('/[a-z]/i', $sku)) {
            $sku = substr($sku, 0, 4);
        }

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter(
                array(
                    array("attribute" => 'spare_search_synonym', 'like' => "%$sku%"),
                ))
            ->addAttributeToFilter(
                array(
                    array("attribute" => 'product_type_value', 'eq' => "Machine"),
                ))
            ->addStoreFilter(Mage::app()->getStore()->getId());

        $productCollection->addAttributeToFilter('status', 1);

        // Group product by entity id, this stops duplicates!
        $productCollection->getSelect()->group('entity_id');
        $productCollection->getSelect()->limit(1);
        $productCollection->load();

        return $productCollection;
    }

    /*
     * Check synonyms for similiar sku (spares only)
     */
    public function getSpareMatchingSynonym($sku) {

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter(
                array(
                    array("attribute" => 'spare_search_synonym', 'like' => "%$sku%"),
                ))
            ->addAttributeToFilter(
                array(
                    array("attribute" => 'product_type_value', 'eq' => "Spare"),
                ))
            ->addStoreFilter(Mage::app()->getStore()->getId());

        $productCollection->addAttributeToFilter('status', 1);

        // Group product by entity id, this stops duplicates!
        $productCollection->getSelect()->group('entity_id');
        $productCollection->getSelect()->limit(1);
        $productCollection->load();

        return $productCollection;
    }

    /**
     * Returns products related to product collection or group of product ids.
     *
     * @param $products
     * @return mixed
     */
    public function getRelatedProducts($products)
    {
        if ($products instanceof Mage_Catalog_Model_Resource_Product_Collection) {
            $prodIds = $products->getAllIds();
        } else {
            $prodIds = (array)$products;
        }
        $relatedProductLinks = Mage::getResourceModel('catalog/product_link_collection')
            ->addFieldToFilter('link_type_id', Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED)
            ->addFieldToFilter('product_id', array('in' => $prodIds));

        $relatedProductIds = array();
        foreach ($relatedProductLinks as $link) {
            $relatedProductIds[] = $link->getLinkedProductId();
        }

        $relatedProductCollection = Mage::getResourceModel('catalog/product_collection')
            ->addIdFilter($relatedProductIds);

        return $relatedProductCollection;
    }
}
