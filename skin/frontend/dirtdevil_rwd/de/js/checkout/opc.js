$j = jQuery.noConflict();

if (typeof Translator == 'object' && Translator instanceof Translate) {
    var opcTranslator = Translator;
} else {
    var opcTranslator = {
        translate: function(text) {
            return text;
        }
    };
}
$j(function(){

    // Address Model
    // ----------

    var Address = Backbone.Model.extend({
        firstname       : null,
        lastname        : null,
        street_1        : null,
        street_2        : null,
        street_3        : null,
        city            : null,
        region          : null,
        region_id       : null,
        postcode        : null,
        country_id      : null,
        shipping_method : null,
        same_as_shipping : false,
        address_id      : null,
        save_in_address_book:   false,

        getLine: function(attr) {
            if (_.isEmpty(this.get(attr))) {
                return '';
            }

            return this.escape(attr);
        },

        findAddressByID: function(type, id) {
            if (_.isEmpty(id) ) {
                return;
            }

            var address = this;
                address.trigger('loading', opcTranslator.translate('Loading Address'));

            $j.ajax({
                url: '/postcode-lookup/index/fetchAddress?address_id='+id,
                success: function(data){
                    if ( ! _.isEmpty(data.redirect)) {
                        window.location = data.redirect;
                    }

                    if ( ! _.isEmpty(data)) {
                        address.set(data);
                    }

                    address.trigger('addressSelectComplete');
                },
                dataType : 'json'
            });
        },

        findAddressesByPostCode: function(type, postcode) {
            if (_.isEmpty(postcode) ) {
                return;
            }

            var address = this;
                address.trigger('loading', opcTranslator.translate('Searching Address'));

            $j.ajax({
                url: '/postcode-lookup/index?postcode='+postcode+'&addEmpty=1',
                success: function(data){
                    address.trigger('addressSearchComplete', data);
                }
            });
        }
    });

    var ContactDetails = Backbone.Model.extend({
        prefix    : 'n/a',
        firstname : '',
        lastname  : '',
        email     : '',
        telephone : '',
        optIn     : false,
        same_as_shipping: false,
        street_1        : null,
        street_2        : null,
        street_3        : null,
        city            : null,
        region          : 'n/a',
        postcode        : null,
        country_id      : null,
        shipping_method : null,
        createAccount   : false,
        password            : '',
        password_confirm    : '',
        address_id          : null,
        save_in_address_book    : false,

        getFullName: function() {
            return this.escape('prefix')+' '+this.escape('firstname')+' '+this.escape('lastname');
        },

        getContactData: function()
        {
            return {
                prefix: this.has('prefix') ? this.get('prefix') : 'n/a',
                firstname: this.get('firstname'),
                lastname: this.get('lastname'),
                email: this.get('email'),
                telephone: this.get('telephone'),
                createAccount: this.get('createAccount'),
                customer_password: this.get('password'),
                confirm_password: this.get('password_confirm'),
                dob: this.get('year') + '-' + this.get('month') + '-' + this.get('day'),
                gender: this.get('gender'),
                day: this.get('day'),
                month: this.get('month'),
                year: this.get('year')
            }
        },

        getDeliveryData: function()
        {
            return {
                street_1: this.get('street_1'),
                street_2: this.get('street_2'),
                street_3: this.get('street_3'),
                city: this.get('city'),
                region_id: this.has('region_id') ? this.get('region_id') : 'n/a',
                postcode: this.get('postcode'),
                country_id: this.get('country_id'),
                shipping_method: this.get('shipping_method'),
                customer_address_id: this.get('shipping-address-select'),
                save_in_address_book: this.get('save_in_address_book')
            }
        },

        getBillingData: function()
        {
            return {
                prefix: this.has('prefix') ? this.get('prefix') : 'n/a',
                firstname: this.get('address_billing_firstname'),
                lastname: this.get('address_billing_lastname'),
                street_1: this.get('address_billing_street_1'),
                street_2: this.get('address_billing_street_2'),
                street_3: this.get('address_billing_street_3'),
                city: this.get('address_billing_city'),
                postcode: this.get('address_billing_postcode'),
                region_id: this.get('address_billing_region_id'),
                country_id: this.get('address_billing_country_id'),
                customer_address_id: this.get('billing-address-select'),
                save_in_address_book: this.get('address_billing_save_in_address_book'),
                same_as_shipping: this.get('same_as_shipping'),
                dob: this.get('year') + '-' + this.get('month') + '-' + this.get('day'),
                gender: this.get('gender'),
                day: this.get('day'),
                month: this.get('month'),
                year: this.get('year')
            }
        },

        getContactBillingData: function()
        {
            contact = this.getContactData();
            billing = this.getBillingData();
            for (var attrname in billing) {
                contact[attrname] = billing[attrname];
            }
            contact['street'] = [contact['street_1'], contact['street_2'], contact['street_3']];
            if (typeof contact['gender'] == 'undefined') {
                contact['gender'] = '';
            }

            return contact;
        }


    // Contact Details Model
    // ----------
    });

    // Progress Dialog Model
    // ----------

    var ProgressDialog = Backbone.Model.extend({
        show: function(msg) {
            this.trigger('show', msg);
        },

        hide: function() {
            this.trigger('hide');
        }
    });

    // Payment Details Model
    // ----------

    var PaymentDetails = Backbone.Model.extend({
        cc_type              : null,
        cc_owner             : null,
        cc_number            : null,
        cc_id                : null,
        cc_exp_year          : null,
        cc_exp_month         : null,
        currency             : null,
        method               : null,
        use_customer_balance : false
    });

    // Checkout Model
    // ----------

    var Checkout = Backbone.Model.extend({
        stepIndex       : 1,
        steps           : null,
        contactDetails  : null,
        paymentDetails  : null,
        deliveryAddress : null,
        billingAddress  : null,
        progressDialog  : null,
        complete        : false,
        isProcessing    : false,

        constructor: function() {
            var checkout         = this;

            checkout.steps           = new CheckoutStepCollection(getCheckoutSteps());
            checkout.contactDetails  = new ContactDetails;
            checkout.paymentDetails  = new PaymentDetails;
            checkout.billingAddress  = new Address;
            checkout.progressDialog  = new ProgressDialog;
            checkout.stepIndex       = getCurrentStepIndex();
            checkout.complete        = checkout.isFinalStep();

            // Loading Events
            checkout.progressDialog.listenTo(checkout, 'loading', this.progressDialog.show);
            checkout.progressDialog.listenTo(checkout.billingAddress, 'loading', this.progressDialog.show);

            // Complete Events
            checkout.progressDialog.listenTo(checkout, 'orderComplete', this.progressDialog.hide);
            checkout.progressDialog.listenTo(checkout.billingAddress, 'addressSearchComplete', this.progressDialog.hide);
            checkout.progressDialog.listenTo(checkout.billingAddress, 'addressSelectComplete', this.progressDialog.hide);

            Backbone.Model.apply(this, arguments);
        },

        getCurrentStep: function() {
            return this.steps.at(this.stepIndex - 1);
        },

        handleSuccess: function() {
            window.location.replace('/checkout/onepage/success');
        },

        handleRedirect: function(params) {
            this.trigger('redirect', params);
        },

        handleFailure: function(msg) {
            this.trigger('orderComplete');
            //this.set({stepIndex : this.stepIndex += 1});
            this.disable();
            this.trigger('failure', msg);
        },

        enable: function() {
            this.complete = false;
        },

        disable: function() {
            this.complete = true;
        },

        getMaxSteps: function () {
            return this.steps.length;
        },

        isFirstStep: function() {
            return this.stepIndex == 1;
        },

        isPenultimateStep: function() {
            return this.stepIndex == (this.getMaxSteps() - 1);
        },

        isFinalStep: function() {
            return this.stepIndex == this.getMaxSteps();
        },

        setStep: function(stepIndex) {
            while (stepIndex < this.stepIndex) {
                this.prevStep();
            }
        },

        nextStep: function() {
            if (typeof _gaq != 'undefined') {
                _gaq.push(['_trackEvent', 'Checkout Stages', 'continue', 'stage_'+this.stepIndex]);
            }
            if (this.stepIndex == this.getMaxSteps() -1 && !this.isProcessing) {
                this.submitPayment();
                this.isProcessing = true;
                this.set({stepIndex : this.stepIndex += 1});
            }

            if (this.stepIndex < this.getMaxSteps() -1) {
                this.set({stepIndex : this.stepIndex += 1});
            }

            this.updateStepInfo();
        },

        prevStep: function() {
            if (this.stepIndex > 1) {
                this.set({stepIndex : this.stepIndex -= 1});
            }

            this.updateStepInfo();
        },

        updateStepInfo: function() {
            $j("html, body").animate({
                scrollTop: $j("#checkout-header").offset().top
            },"slow");
            if (this.isFirstStep()) {
                $j('.order-summary').addClass('first-step');
            } else {
                $j('.order-summary').removeClass('first-step');
            }
        },

        isComplete: function() {
            return this.complete;
        },

        showProgressDialog: function() {
            this.progressDialog.show();
        },

        hideProgressDialog: function() {
            this.progressDialog.hide();
        },

        submitPayment: function() {
            if (this.isProcessing) {
                return;
            }
            this.isProcessing = true;
            var checkout = this;
                checkout.trigger('loading', opcTranslator.translate('Processing Order'));
            var data                 = {};
                data.contactDetails  = checkout.contactDetails.getContactData();
                data.deliveryAddress  = checkout.contactDetails.getDeliveryData();
                data.billingAddress  = checkout.contactDetails.getBillingData();
                data.paymentDetails  = checkout.paymentDetails.toJSON();
            $j.ajax({
                type: 'POST',
                url: '/checkout/onepage/submit',
                data: JSON.stringify(data),
                timeout: 120000,
                asynchronous: false,
                success: function(data){
                    if (_.isUndefined(data)) {
                        checkout.handleFailure(opcTranslator.translate('Unfortunately we cannot process your order at this time. Please try again later.'))
                    }
                    else if ( ! _.isUndefined(data.success)) {
                        checkout.handleSuccess(data.success.order);
                    } else if ( ! _.isUndefined(data.redirect)) {
                        checkout.handleRedirect(data.redirect);
                    }
                    else if ( ! _.isUndefined(data.failure)) {
                        checkout.handleFailure(data.failure.msg);
                    }
                },
                error: function(){
                    checkout.handleFailure(opcTranslator.translate('Unfortunately we cannot process your order at this time. Please try again later.'))
                    checkout.trigger('orderComplete');
                },
                complete: function() {
                    checkout.isProcessing = false;
                },
                dataType: 'json'
            });
        },

        returnToCart: function() {
            var checkout = this;
            checkout.trigger('loading', opcTranslator.translate('Processing Order'));

            var data                 = {};
            data.contactDetails  = checkout.contactDetails.toJSON();
            data.billingAddress  = checkout.billingAddress.toJSON();
            data.paymentDetails  = checkout.paymentDetails.toJSON();

            $j.ajax({
                type: 'POST',
                url: '/checkout/onepage/return',
                data: JSON.stringify(data),
                timeout: 120000,
                asynchronous: false,
                success: function(data){
                    document.location.href = data.return_url;
                },
                error: function(data){
                    document.location.href= '/checkout/cart';
                },
                dataType: 'json'
            });
        },

        reset: function() {
            var checkout = this;
                checkout.trigger('reset');
                checkout.setStep(1);

            _.delay(_.bind(checkout.enable, checkout), 1000);
        }
    });

    // Checkout Step Model
    // ----------

    var CheckoutStep = Backbone.Model.extend({
        title     : null,
        summary   : null,
        htmlClass : null,
        isValid   : false
    });

    // Check Step Collection
    // ---------------

    var CheckoutStepCollection = Backbone.Collection.extend({
        model        : CheckoutStep,
        localStorage : new Backbone.LocalStorage("checkout-step-backbone")
    });

    // The Checkout View
    // -----------------

    var CheckoutView = Backbone.View.extend({
        el: $j('#checkout-wrapper'),

        events: {
            'click  a[data-submit="billing-details"]'       : 'synchBillingAddress',
            'click  a[data-submit="billing-payment-details"]' : 'synchBillingPaymentDetails',
            'click  a[data-submit="contact-shipping"]'       : 'synchContactDetails',
            'change #address_delivery_shipping_date'        : 'synchDeliveryDate',
            'click  a[data-submit="payment-details"]'       : 'synchPaymentDetails',
            'click  button.btn-try-again'                         : 'reset',
            'click  .btn-prev'                              : 'prevStep',
            'click  .btn-edit'                              : 'editForm',
            'click .btn-billing-next'                       : 'revealPaymentForm',
            'change #billing-payment-details-form input'            : 'partSynchBillingAddress',
            'click .return-to-cart'                         : 'returnToCart',
            'blur input'                                   : 'initFormValidation',
            'focus input'                                   : 'initFormValidation',
            'blur select'                                  : 'initFormValidation',
            'focus select'                                  : 'initFormValidation'
        },

        initialize: function() {
            this.checkout                = new Checkout();
            this.contentView             = new CheckoutStepsContentView({ model : this.checkout });
            this.headerView              = new CheckoutStepsHeaderView({ model : this.checkout });
            this.actionsView             = new CheckoutStepsActionsView({ model : this.checkout });
            this.sidebarView             = new CheckoutStepsSidebarView({ model : this.checkout });
            this.progressDialogView      = new ProgressDialogView({ model: this.checkout.progressDialog });
            this.secure3DFormView        = new Secure3DFormView({ model : this.checkout });
            this.billingAddressView      = new BillingAddressView({ model : this.checkout.billingAddress });
            this.billingAddressFormView  = new BillingAddressFormView({ model : this.checkout.billingAddress });
            this.contactDetailsFormView  = new ContactDetailsFormView({ model : this.checkout.contactDetails });
            this.contactDetailsView      = new ContactDetailsView({ model : this.checkout.contactDetails });
            this.paymentDetailsFormView  = new PaymentDetailsFormView({ model : this.checkout.paymentDetails });

            this.listenTo(this.checkout, 'success', this.handleSuccess);
            this.listenTo(this.checkout, 'failure', this.handleFailure);
            this.listenTo(this.contactDetailsFormView, 'useDeliveryAddress', this.useDeliveryAddress);
            this.listenTo(this.billingAddressFormView, 'validate', this.isValid);
        },

        synchBillingAddress: function(event) {
            event.preventDefault();

            if (this.isValid()) {
                this.billingAddressFormView.synch();
                //this.nextStep();
            }
        },

        partSynchBillingAddress: function(event) {
            this.billingAddressFormView.synch();
        },

        synchContactDetails: function(event) {
            event.preventDefault();

            if (this.isValid()) {
                this.contactDetailsFormView.synch();
                this.contactDetailsFormView.synchContactDetails();
                this.nextStep();
            }
        },

        synchDeliveryAddress: function(event) {
            event.preventDefault();

            if (this.isValid()) {
                if ($j('#contactDetails_same_as_shipping').is(':checked')) {
                    this.useDeliveryAddress();
                }

                this.nextStep();
            }
        },

        synchDeliveryMethod: function(event) {
            var target = $j(event.target);

            $j('.delivery-date').hide();
            target.parent().parent().find('.delivery-date').show();

            var deliveryDate = $j('#address_delivery_shipping_date:visible option:selected').text();

            $j('#cart-summary tbody tr td.desc').each(function(idx, elt) {
                desc  = $j(target).parent().find('.title').text();

                if (deliveryDate) {
                    desc += ' - '+deliveryDate;
                }

                price = $j(target).parent().find('.price').text();
                if ($j(elt).html().indexOf('Versandoption')>=0) {
                    $j(elt).html('Versandoption - ' + desc);
                    $j(elt).parent().find('.price').html(price);
                    $j(elt).parent().find('.price').attr('data-price', "0.00");
                }
                var sum=0;
                $j('#cart-summary td.price:not(.grand-total)').each(
                    function(i, elt) {
                        sum += Number($j(elt).text().replace(/[^0-9\.\-]+/g,""));
                    });
                $j('#cart-summary td.grand-total .price').text('£' + sum.toFixed(2));
            });
        },

        synchDeliveryDate: function(event) {
            $j(event.target).parent().parent().find('#address_delivery_shipping_method').click();
        },

        synchPaymentDetails: function(event) {
            if (event) {
                event.preventDefault();
            }

            if (this.isValid()) {
                this.paymentDetailsFormView.synch();
                this.nextStep();
            }
        },

        synchBillingPaymentDetails: function(event) {
            if (event) {
                event.preventDefault();
            }

            this.synchBillingAddress(event);
            this.synchPaymentDetails(event);
        },

        handleSuccess: function() {
            this.$el.addClass('checkout-success');
            this.$('.order-success').show();
        },

        handleFailure: function(msg) {
            this.$el.addClass('checkout-failure');
            this.$('.order-failure').show()
                                    .find('.title')
                                    .text(msg);
        },

        useDeliveryAddress: function() {
            this.checkout.billingAddress.set(this.checkout.contactDetails.attributes);
        },

        reset: function(event) {
            event.preventDefault();
            this.checkout.reset();
            this.$el.removeClass('checkout-failure');
            this.$('.order-failure').hide();
        },

        nextStep: function() {
            if ( ! this.canProgress()) {
                return;
            }

            var prevStep = this.checkout.stepIndex;

            this.checkout.nextStep();
            this.initFormValidation();
        },

        prevStep: function(event) {
            if (typeof _gaq != 'undefined') {
                _gaq.push(['_trackEvent', 'Checkout Stages', 'previous', 'stage_'+App.checkout.stepIndex]);
            }
            if ( ! this.checkout.isFirstStep()) {
                event.preventDefault();
                this.checkout.prevStep();
            }
        },

        /*reveal checkout step by toggling parent element visibility */

        editForm: function(event) {
            event.preventDefault();
            $j('.step-4').addClass('step-inactive');
            var activeStep = $j(event.target).data("step");
            this.checkout.setStep(activeStep);
            $j('.step-'+activeStep).removeClass('step-visited');
        },

        /*reveal payment step on progressing through delivery, bump step index one further due to merged step3/4 forms */

        revealPaymentForm: function(event) {
            if ( ! this.isValid()) {
                return false;
            }

            $j('.step-4').removeClass('step-inactive');
        },

        canProgress: function() {
            if (this.checkout.isComplete()) {
                return false;
            }

            return ! $j('#checkout-wrapper').hasClass('checkout-failure');
        },

        isValid: function() {
            if ($j('#address_id').length > 0) {
                return false;
            }

            currentStep = this.checkout.getCurrentStep();

            if (typeof currentStep == 'undefined') {
                return;
            }

            var validator = $j('#'+currentStep.get('htmlClass')+'-form').validate({
                                errorClass: 'help-inline',
                                errorElement: 'span',
                                ignore: ".ignore",
                                showErrors: function(errorMap, errorList) {
                                    this.defaultShowErrors();

                                    _.each(errorList, function(error){
                                        element = $j(error.element);
                                        element.parents('.control-group').addClass('error');
                                        form = element.closest('form');
                                        if (!$j('.error-list .'+element.attr('id')).length) {
                                            form.parent().find('ul.error-list').append('<li class="' + $j(element).attr('id') + '">' + $j(element).attr('title') + '</li>');
                                            form.parent().find('.error-block').show();
                                        }
                                    });
                                },
                                unhighlight: function(element, successClass) {
                                    $j(element).parents('.control-group').removeClass('error');
                                    $j('.error-list .' + $j(element).attr('id')).remove();
                                    if (!$j('.error-list li').length) {
                                        $j('.error-block').hide();
                                    }
                                }
                            });
                validator.form();

            return validator.numberOfInvalids() === 0;
        },

        initFormValidation: function(element) {
            currentStep = this.checkout.getCurrentStep();

            if (typeof currentStep == 'undefined') {
                return;
            }

            var validator = $j('#'+currentStep.get('htmlClass')+'-form').validate({
                errorClass: 'help-inline',
                errorElement: 'span',
                ignore: ".ignore",
                showErrors: function(errorMap, errorList) {
                    this.defaultShowErrors();

                    _.each(errorList, function(error){
                        element = $j(error.element);
                        element.parents('.control-group').addClass('error');
                        form = element.closest('form');
                        if (!$j('.error-list .'+element.attr('id')).length) {
                            form.parent().find('ul.error-list').append('<li class="' + $j(element).attr('id') + '">' + $j(element).attr('title') + '</li>');
                            form.parent().find('.error-block').show();
                        }
                    });
                },
                unhighlight: function(element, successClass) {
                    $j(element).parents('.control-group').removeClass('error');
                    $j('.error-list .' + $j(element).attr('id')).remove();
                    if (!$j('.error-list li').length) {
                        $j('.error-block').hide();
                    }
                }
            });
        },

        returnToCart: function(event) {
            event.preventDefault();
            this.checkout.returnToCart();
        }
    });

    // Checkout Steps Actions View
    // ---------------------------

    var CheckoutStepsActionsView = Backbone.View.extend({
        el: $j("#checkout-steps-actions"),

        initialize: function() {
            this.listenTo(this.model, 'change:stepIndex', this.render);
            this.listenTo(this.model, 'reset', this.reset);
            this.listenTo(this.model, 'failure', this.handleFailure);
        },

        render: function() {
            this.updateActions();
            currentStep = this.model.getCurrentStep();

            if (typeof currentStep == 'undefined') {
                return;
            }

            this.$('.btn-next').attr('data-submit', currentStep.get('htmlClass'));
        },

        handleSuccess: function() {
            this.$('.btn-prev').hide();

            this.$('.btn-next').addClass('btn-success')
                               .find('p')
                               .text(opcTranslator.translate('Return to Shop'));
        },

        handleFailure: function() {
            this.$('.btn-next[data-submit=billing-payment-details]').attr('data-submit', 'order-retry')
                               .removeClass('btn-success')
                               .addClass('btn-danger')
                               .find('p')
                               .text(opcTranslator.translate('Try Again'));
        },

        reset: function() {
            this.$('.btn-prev').show()
                               .find('p')
                               .text(opcTranslator.translate('Back to Basket'));

            currentStep = this.model.getCurrentStep();

            if (typeof currentStep == 'undefined') {
                return;
            }

            this.$('.btn-next').attr('data-submit', currentStep.get('htmlClass'))
                               .addClass('btn-success')
                               .removeClass('btn-danger')
                               .find('p')
                               .text(opcTranslator.translate('Continue'));
        },

        updateActions: function () {
            if (this.model.isFirstStep()) {
                this.reset();
                return;
            }

            if (this.model.isPenultimateStep()) {
                $j('.step-' + this.model.getCurrentStep().get('index') + ' .btn-next').show().find('p').text(opcTranslator.translate('Place order'));
                return;
            }

            if (this.model.isFinalStep()) {
                this.handleSuccess();
                return;
            }

            this.$('.btn-prev').show().find('p').text(opcTranslator.translate('Back to Basket'));
        }
    });

    // Checkout Steps Header View
    // -----------------------------

    var CheckoutStepsHeaderView = Backbone.View.extend({
        el: $j("#checkout-steps-header"),

        initialize: function() {
            this.listenTo(this.model, 'change:stepIndex', this.render);
            this.listenTo(this.model, 'failure', this.handleFailure);
            this.listenTo(this.model, 'success', this.handleSuccess);
        },

        handleSuccess: function(order) {
            this.$('.title').text('Thank You For Your Order');
            this.$(".summary").text(opcTranslator.translate('Your unique order number is: ')+order.id);
        },

        handleFailure: function() {
            this.$('.title').text(opcTranslator.translate('Order Failed'));
            this.$('.summary').text(opcTranslator.translate('Your Card Has Not Been Charged'));
        },

        render: function() {
            currentStep = this.model.getCurrentStep();

            if (typeof currentStep == 'undefined') {
                return;
            }

            this.$(".title").text(currentStep.get('title'));

            this.$(".summary").text(currentStep.get('summary'));
        }
    });

    // Checkout Steps Sidebar View
    // -----------------------------

    var CheckoutStepsSidebarView = Backbone.View.extend({
        el: $j("#progress-bar"),

        events: {
            'click .checkout-steps-header' : 'setStep'
        },

        initialize: function() {
            this.listenTo(this.model, 'change:stepIndex', this.render);
            this.listenTo(this.model, 'reset', this.reset);
            this.listenTo(this.model, 'success', this.handleSuccess);
            this.listenTo(this.model, 'failure', this.handleFailure);
        },

        handleSuccess: function() {
            this.$('.step-last').addClass('step-success')
                                .find('p')
                                .text(opcTranslator.translate('Order Complete'));
        },

        handleFailure: function() {
            this.$('.step-last').addClass('step-error')
                                .find('p')
                                .text(opcTranslator.translate('Order Failed'));
        },

        reset: function() {
            this.$('.step-last').removeClass('step-error')
                                .find('p')
                                .text(opcTranslator.translate('Order Complete'));
        },

        setStep: function(event) {
            if (this.model.isComplete()) {
                return;
            }

            var stepIndex = $j(event.currentTarget).data('step');
            if ($j('.step-'+stepIndex).hasClass('step-visited')) {
                this.model.setStep(stepIndex);
            }
        },

        render: function() {
            if (this.model.get('stepIndex') > this.model.getMaxSteps()) {
                return;
            }
            this.$('.step-'+(this.model.get('stepIndex') - 2)).removeClass('step-prev');
            this.$('.step-'+(this.model.get('stepIndex') - 1)).removeClass('step-active').addClass('step-prev step-visited');
            this.$('.step-'+this.model.get('stepIndex')).removeClass('step-prev').addClass('step-active');
            this.$('.step-'+(this.model.get('stepIndex') + 1)).removeClass('step-active').addClass('step-inactive');
        }
    });

    // Checkout Steps Content View
    // -----------------------------

    var CheckoutStepsContentView = Backbone.View.extend({
        el: $j("#checkout-steps"),

        initialize: function() {
            this.listenTo(this.model, 'change:stepIndex', this.render);
        },

        render: function() {
            if (this.model.get('stepIndex') > this.model.getMaxSteps()) {
                return;
            }
            this.$('.step-'+(this.model.get('stepIndex') - 2)).removeClass('step-prev');
            this.$('.step-'+(this.model.get('stepIndex') - 1)).removeClass('step-active').addClass('step-prev step-visited');
            this.$('.step-'+this.model.get('stepIndex')).removeClass('step-prev step-inactive step-visited').addClass('step-active');
            this.$('.step-'+(this.model.get('stepIndex') + 1)).removeClass('step-active').addClass('step-inactive');
        }
    });

    // Address View
    // -----------------------------
    var AddressView = Backbone.View.extend({
        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
        },

        truncate: function(line) {
            if (line.length <= 65) {
                return line;
            }

            return '<abbr title="'+line+'">'+$j.trim(line).substring(0, 65)+'</abbr>';
        },

        render: function() {
            var view = this;
                view.$('div').text('');
                view.$('div').append(view.model.getLine('street_1')+'<br>');

                if ( ! _.isEmpty(view.model.getLine('street_2'))) {
                    view.$('div').append(view.model.getLine('street_2')+'<br>');
                }
                if ( ! _.isEmpty(view.model.getLine('street_3'))) {
                    view.$('div').append(view.model.getLine('street_3')+'<br>');
                }

                view.$('div').append(view.model.getLine('city')+'<br>');
                view.$('div').append(view.model.getLine('region')+'<br>');
                view.$('div').append(view.model.getLine('postcode')+'<br>');
                view.$('div').append(view.model.getLine('country'));
        }
    });

    // Form View
    // -----------------------------
    var FormView = Backbone.View.extend({
        synch: function() {
            var form = this;
                form.model.clear();

            form.$(':input').each(function(){
                var input = this;
                var attr  = input.id.replace(form.getInputPrefix(), '');

                if ($j(input).attr('type') == 'hidden' && attr !== 'quoteid') {
                    return true; // continue
                }

                if ($j(input).attr('type') == 'checkbox') {
                    form.model.set(attr , $j(input).is(':checked'));
                } else if ($j(input).attr('type') !== 'radio' || $j(input).is(':checked')) {
                    form.model.set(attr , $j.trim($j(input).val()));
                }
            });
        },

        getInputPrefix: function() {
            return this.inputPrefix;
        },

        reset: function() {
            this.model.clear()
            this.$(':input').val('');
        },

        render: function() {
            var form = this;

            _.each(form.model.attributes, function(value, index){
                if ( ! _.isEmpty(value) ) {
                    form.$('#'+form.getInputPrefix()+index).val(value);
                }
            });
        }
    });

    var AddressForm = FormView.extend({
        events: {
            'click .btn-search-address'      : 'searchAddress',
            'click .btn-select-address'      : 'selectAddress',
            'change select.address-selector' : 'selectAddress',
            'click .btn-manual-entry'        : 'manualEntry',
            'click .btn-show-search-address' : 'showSearchAddress'
        },

        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'addressSearchComplete', this.handleAddressSearchCompletion);
            this.listenTo(this.model, 'addressSelectComplete', this.handleAddressSelectCompletion);
        },

        getPostCode: function() {
            return this.$('.postcode:first').val();
        },

        getAddressID: function() {
            return this.$('#address_id').val();
        },

        getAddressType: function() {
            return this.addressType;
        },

        handleAddressSearchCompletion: function(data) {
            if ( ! _.isEmpty(data)) {
                this.$('.btn-manual-entry-main').hide();
                this.$('.address-data').show();
                this.$('.address-line').hide();
                this.$('.address-search').html(data).show();
            }
        },

        handleAddressSelectCompletion: function() {
            this.$('.address-line').show();
            this.$('.btn-search-address').hide();
            this.$('.address-search').html('').hide();
        },

        searchAddress: function(event) {
            try {
                _gaq.push(['_trackEvent', 'Checkout_Postcode_Lookup', 'click', 'stage_'+App.checkout.stepIndex]);
            } catch (e) {

            }

            if (!this.getPostCode()) {
                var validator = $j(this.el).validate({
                    errorClass: 'help-inline',
                    errorElement: 'span',
                    ignore: ".ignore",
                    showErrors: function(errorMap, errorList) {
                        this.defaultShowErrors();

                        _.each(errorList, function(error){
                            $j(error.element).parents('.control-group').addClass('error');
                        });
                    },
                    unhighlight: function(element, successClass) {
                        $j(element).parents('.control-group').removeClass('error');
                    }
                });
                validator.form();
            }

            event.preventDefault();
            this.model.findAddressesByPostCode(
                this.getAddressType(),
                this.getPostCode()
            );
        },

        selectAddress: function(event) {
            event.preventDefault();

            this.model.findAddressByID(
                this.getAddressType(),
                this.getAddressID()
            );
        },

        manualEntry: function(event) {
            event.preventDefault();

            this.$('.address-data').show();
            this.$('.address-line').show();
            this.$('.address-search').hide();
            this.$('.btn-search-address').hide();
            this.$('.btn-manual-entry').hide();
        },

        showSearchAddress: function(event) {
            event.preventDefault();

            this.$('.address-data').hide();
            this.$('.address-line').hide();
            this.$('.address-search').show();
            this.$('.btn-search-address').show();
            this.$('.btn-manual-entry').show();
        }
    });

    // Delivery Address Views
    // -----------------------------

    var DeliveryAddressView = AddressView.extend({
        el: $j("#contact-shipping-form"),

        render: function() {
            var view = this;
            view.$('div').text('');

            view.$('div').append('<strong>' + $j($j('.controls-methods span.title')[0]).html() + '</strong><br />');
            if ($j('#address_delivery_shipping_date option:selected').text()) {
                view.$('div').append($j('#address_delivery_shipping_date option:selected').text() + '<br />');
            }

            view.$('div').append('<br /><strong>Your Delivery Address is:</strong><br />');

            view.$('div').append(view.model.getLine('street_1')+'<br>');

            if ( ! _.isEmpty(view.model.getLine('street_2'))) {
                view.$('div').append(view.model.getLine('street_2')+'<br>');
            }
            if ( ! _.isEmpty(view.model.getLine('street_3'))) {
                view.$('div').append(view.model.getLine('street_3')+'<br>');
            }

            view.$('div').append(view.model.getLine('city')+'<br>');
            view.$('div').append(view.model.getLine('region')+'<br>');
            view.$('div').append(view.model.getLine('postcode')+'<br>');
            view.$('div').append(view.model.getLine('country'));
        }
    });

    var DeliveryAddressFormView = AddressForm.extend({
        el: $j("#contact-shipping-form"),

        addressType: 'delivery',

        inputPrefix: 'contactDetails_'
    });

    // Billing Address Views
    // -----------------------------

    var BillingAddressView = AddressView.extend({
        el: $j("#billing-details-summary")
    });

    var BillingAddressFormView = AddressForm.extend({
        el: $j("#billing-address-form"),

        addressType: 'billing',

        inputPrefix: 'address_billing_',

        events: {
            'click .btn-use-delivery:not(:checked)'      : 'showSearchAddress',
            'click .btn-use-delivery:checked'      : 'useDeliveryAddress',
            'click .btn-search-address'             : 'searchAddress',
            'click .btn-select-address'             : 'selectAddress',
            'change .address-selector'              : 'selectAddress',
            'click .btn-manual-entry'               : 'manualEntry',
            'click .btn-show-search-address'        : 'showSearchAddress'
        },

        showSearchAddress: function(event) {
            this.$('.address-data').show();
            this.$('.address-line').show();
            this.$('.control-group-search').show();
            this.$('.address-search').show();
            this.$('.btn-search-address').show();
            this.$('.btn-manual-entry').show();
            this.$('#billing-details-summary').hide();
        },

        useDeliveryAddress: function(event) {
            this.trigger('useDeliveryAddress');
            this.$('.control-group-search').hide();
            this.$('.address-search').hide();
            this.$('.btn-search-address').hide();
            this.$('.btn-manual-entry').hide();
            this.$('.address-data').hide();
            this.$('.address-line').hide();
            this.$('#billing-details-summary').show();
        }
    });

    // Contact Details Views
    // -----------------------------

    var ContactDetailsFormView = FormView.extend({
        el: $j("#contact-shipping-form"),

        events: {
            'change input#contactDetails_same_as_shipping:checked': 'hideBillingAddress',
            'change input#contactDetails_same_as_shipping:not(:checked)': 'showBillingAddress',
            'change input.delivery': 'syncBillingAddress',
            'change select.delivery': 'syncBillingAddress',
            'change select#shipping-address-select': 'selectCustomerShippingAddress',
            'change select#billing-address-select': 'selectCustomerBillingAddress'
        },

        initialize: function() {
            this.inputPrefix = 'contactDetails_';
            this.useDeliveryAddress();
        },

        hideBillingAddress: function(event) {
            this.trigger('useDeliveryAddress');
            $j('#billing-address-form').hide();
            $j('#billing-address-form input').addClass('ignore');
            this.useDeliveryAddress();
        },

        showBillingAddress: function() {
            $j('#billing-address-form').show();
            $j('#billing-address-form input').removeClass('ignore');
            this.useDeliveryAddress();
        },

        syncBillingAddress: function(event) {
            if ($j('#contactDetails_same_as_shipping').is(':checked')) {
                $j('#billing-address-form input[name="' + event.target.name.replace('contact_details', 'address[billing]') +'"]').val(event.target.value);
                $j('#billing-address-form input[name="' + event.target.name.replace('[delivery]', '[billing]') +'"]').val(event.target.value);
                $j('#billing-address-form select[name="' + event.target.name.replace('[delivery]', '[billing]') +'"]').val(event.target.value);
                try {
                    $j('#billing-address-select').val($j('#shipping-address-select').val());
                    if ($j('#billing-address-select').val()) {
                        $j('.new-address-form.billing-address').hide();
                        $j('.new-address-form.billing-address input').addClass('ignore');
                    } else {
                        $j('.new-address-form.billing-address').show();
                        $j('.new-address-form.billing-address input').removeClass('ignore');
                    }
                } catch (e) {

                }
            } else {
                $j('.billing-address input, .billing-address select:not("#address_billing_country_id")').each(function() {
                    $j(this).val('');
                });
            }
        },

        synchContactDetails: function(event) {
            $j('#invoice_method').closest('.payment-method').hide();
            $j.ajax('/paymentlimit', {
                data: {contact: this.model.getContactBillingData(), shipping: this.model.getDeliveryData()},
                method: 'post',
                async: false,
                success: function(transport) {
                    if (1 == transport) {
                        $j('#invoice_method').closest('.payment-method').remove();
                    } else {
                        $j('#invoice_method').closest('.payment-method').show();
                    }
                }
            });

            // remove for enabling bonicheck
            return;
            $j('div.payment-method').hide();
            $j.ajax('/ddbonicheck/index/validateMethods', {
                data: this.model.getContactBillingData(),
                method: 'post',
                async: false,
                success: function(transport) {
                    $j('div.payment-method').each(function() {
                        myInput = $j(this).find('input.pay-method');
                        if (myInput && myInput.val() && transport.indexOf(myInput.val()) == -1) {
                            $j(this).remove();
                        }
                    });

                    if (!$j('input.pay-method').length) {
                        $j('#no-payment-methods-alert').show();
                    }

                    $j('div.payment-method').show();
                }
            });
        },

        useDeliveryAddress: function() {
            if ($j('#contactDetails_same_as_shipping').is(':checked')) {
                $j('input.delivery,select.delivery').each(function() {
                    $j('#billing-address-form input[name="' + $j(this).attr('name').replace('contact_details', 'address[billing]') +'"]').val($j(this).val());
                    $j('#billing-address-form input[name="' + $j(this).attr('name').replace('[delivery]', '[billing]') +'"]').val($j(this).val());
                    $j('#billing-address-form select[name="' + $j(this).attr('name').replace('[delivery]', '[billing]') +'"]').val($j(this).val());
                    try {
                        $j('#billing-address-select').val($j('#shipping-address-select').val());
                        if ($j('#billing-address-select').val()) {
                            $j('.new-address-form.billing-address').hide();
                            $j('.new-address-form.billing-address input').addClass('ignore');
                        } else {
                            $j('.new-address-form.billing-address').show();
                            $j('.new-address-form.billing-address input').removeClass('ignore');
                        }
                    } catch (e) {

                    }
                });
            } else {
                $j('.billing-address input, .billing-address select:not("#address_billing_country_id")').each(function() {
                    $j(this).val('');
                });
                try {
                    $j('#billing-address-select').val($j('#shipping-address-select').val());
                    if ($j('#billing-address-select').val()) {
                        $j('.new-address-form.billing-address').hide();
                        $j('.new-address-form.billing-address input').addClass('ignore');
                    } else {
                        $j('.new-address-form.billing-address').show();
                        $j('.new-address-form.billing-address input').removeClass('ignore');
                    }
                } catch (e) {

                }
            }
        },

        selectCustomerShippingAddress: function(event) {
            if ($j(event.target).val()) {
                $j('.new-address-form.shipping-address').hide();
                $j('.new-address-form.shipping-address input').addClass('ignore');
            } else {
                $j('.new-address-form.shipping-address').show();
                $j('.new-address-form.shipping-address input').removeClass('ignore');
            }
        },

        selectCustomerBillingAddress: function(event) {
            if ($j(event.target).val()) {
                $j('.new-address-form.billing-address').hide();
                $j('.new-address-form.billing-address input').addClass('ignore');
            } else {
                $j('.new-address-form.billing-address').show();
                $j('.new-address-form.billing-address input').removeClass('ignore');
            }
        }
    });

    // Contact Details Summary
    //-------------------------------

    var ContactDetailsView = Backbone.View.extend({
        el: $j('#contact_shipping_summary'),

        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
        },

        render: function() {
            var view = this;
            view.$('div').text('');
            view.$('div').append(view.model.escape('firstname')+' ');
            view.$('div').append(view.model.escape('lastname')+'<br>');
            view.$('div').append(view.model.escape('email')+'<br>');
            view.$('div').append(view.model.escape('telephone')+'<br>');
        }
    });

    // Payment Details Views
    // -----------------------------

    var PaymentDetailsFormView = FormView.extend({
        el: $j("#payment-details-form"),

        events: {
            'click .pay-method'           : 'handlePaymentMethodSelect',
            'click .payment-method-label' : 'reactivateValidation',
            'click #use_customer_balance' : 'handleCustomerBalanceSelect'
        },

        initialize: function() {
            if (this.isUsingCustomerBalance()) {
                this.disablePaymentMethods();
                return;
            }

            this.reactivateValidation();
        },

        getInputPrefix: function() {
            return this.getMethodName()+'_';
        },

        getMethodName: function() {
            return this.$('input.pay-method:checked').val();
        },

        isUsingCustomerBalance: function() {
            return this.$('#use_customer_balance:checked').length > 0;
        },

        showPaymentMethodForm: function(method) {
            this.hidePaymentMethodForms();
            this.$('#payment_form_'+method).show().find(":input").removeClass('ignore');
        },

        hidePaymentMethodForms: function() {
            this.$('ul[id^=payment_form_]').hide().find(":input").addClass('ignore');
        },

        selectPaymentMethod: function(method) {
            this.$('#'+method+'_method').click();
            this.showPaymentMethodForm(method);
        },

        handlePaymentMethodSelect: function(event) {
            this.reactivateValidation();
            this.showPaymentMethodForm($j(event.currentTarget).val());
            this.uncheckCustomerBalance();
        },

        resetPaymentMethods: function() {
            this.$('ul[id^=payment_form_]').attr('checked', false);
        },

        disablePaymentMethods: function() {
            this.resetPaymentMethods();
            this.hidePaymentMethodForms();
        },

        handleCustomerBalanceSelect: function(event) {
            this.disablePaymentMethods();
            this.model.set({method : 'free'});
        },

        uncheckCustomerBalance: function() {
            this.$('#use_customer_balance:checked').attr('checked', false);
        },

        deactivatePaymentValidation: function() {
            $j('.pay-method').parent().parent().next().find('input,select,textarea').addClass('ignore');
        },

        reactivateValidation: function() {
            this.deactivatePaymentValidation();
            $j('.pay-method:checked').parent().parent().next().find('input,select,textarea').removeClass('ignore');
        }
    });

    // Progress Dialog Views
    // -----------------------------

    var ProgressDialogView = Backbone.View.extend({
        el: $j(".progress-container-wrapper"),

        initialize: function() {
            this.listenTo(this.model, 'show', this.show);
            this.listenTo(this.model, 'hide', this.hide);
        },

        show: function(msg) {
            $j('.step-active .progress-container-wrapper').hide();
            $j('.step-active .progress-container-wrapper').first().show();
            $j('.progress-bg').show().css({left: 0});
            this.$el.find('.msg').text(msg);
        },

        hide: function() {
            this.$el.hide();
            $j('.progress-bg').hide().css({left: -9999999 + 'px'});
        }
    });

    // 3D Secure Form View
    // -----------------------------

    var Secure3DFormView = Backbone.View.extend({
        el: $j("#3ds-form"),

        initialize: function() {
            this.listenTo(this.model, 'redirect', this.handleRedirect);
        },

        handleRedirect: function(params) {
            this.$('.pareq').val(params.pareq);
            this.$('.term').val(params.termURL);
            this.$('.md').val(params.md);

            this.$el.attr('action', params.acsURL).submit();
        }
    });

    var App = new CheckoutView;
});