<?php

class Vax_SalesRule_Model_Rule_Freegift_Validator
{
    /**
     * Product Rules collection
     *
     * @var array
     */
    protected $_rules;

    /**
     * get promotional gift rules for a product
     *
     * @param $websiteId
     * @param $customerGroupId
     * @param $productId
     * @return mixed
     */
    public function getRules($websiteId, $customerGroupId, $productId)
    {
        $key = $websiteId . '_' . $customerGroupId . '_' . $productId;
        if (!isset($this->_rules[$key])) {
            $this->_rules[$key] = Mage::getResourceModel('salesrule/rule_collection')
                ->setFlag('include_free_gift_rules', true)
                ->setValidationFilter($websiteId, $customerGroupId)
                ->join(['free_gift' => 'vax_salesrule/free_gift'], 'free_gift.rule_id = main_table.rule_id')
                ->addFieldToFilter('product_id', $productId)
                ->load();
        }

        return $this->_rules[$key];
    }
}