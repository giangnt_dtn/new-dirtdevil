$j(document).ready(function () {
    function handleScroll() {
        var divPosition = $j('#checkout-steps').offset();
        var blockHeight = $j('#order-summary').height();


        if ($j(window).width() <= 770) {
            $j('#order-summary').css({position: 'static'});
            return;
        }
        footerHeight = $j($j('.footer-container')[0]).height() +40;

        curOffset = $j(window).scrollTop();
        maxScrollBottom = $j(document).height() - footerHeight;
        if (curOffset >= divPosition.top - 10) {
            if (curOffset <= maxScrollBottom - blockHeight) {
                $j('#order-summary').css({position:'fixed', top: '10px', bottom: 'auto'});
            } else {
                bottomPos = $j(window).height() - ($j(document).height() - curOffset) + footerHeight;
                $j('#order-summary').css({position:'fixed', bottom: bottomPos + 'px', top: 'auto'});
            }
        } else {
            $j('#order-summary').css({position: 'fixed', top: (divPosition.top - curOffset) + 'px'});
        }
    }

    function handleScrollStopped() {
        curOffset = $j(window).scrollTop();

        if(curOffset <= 0){
            $j('#order-summary').css({position:'static', top: '10px', bottom: 'auto'});
        }
    }

    divPos = $j('#checkout-steps').offset();
    curOffset = $j(window).scrollTop();

    $j('#order-summary').css({position: 'fixed', top: (divPos.top - curOffset) + 'px'});

    $j('#checkout-content-wrapper').css('min-height', $j('#order-summary').height());

    $j(window).scroll(function(event) {
        handleScroll();
    });

    $j(window).scroll(_.debounce(function(){
        handleScrollStopped();
    }, 150));

    $j(window).on('delayed-resize', function (e, resizeEvent) {
        handleScroll();
    });

    handleScroll();

    // make sure that the payment method is selected upon clicking its label
    $j('.payment-method label').click(function() {
        $j(this).parent().find('input[type=radio]').click();
    });

    // toggle detailed summary in the sidebar
    $j('a.expand-summary').click(function() {
        $j('.expandable').animate({opacity: 'toggle'}, 'slow');
    });

    // Cookie handling
    restoreCookie();
    $j('button.save-for-later').click(function(e){
        e.preventDefault();
        saveCookie();
    });

    // customer form
    $j('#contactDetails_createAccount').change(function() {
        console.log('Change');
        console.log($j(this));
        if ($j(this).is(':checked')) {
            $j('.create-account-form').show();
            $j('.create-account-form input[type="password"]').removeClass('ignore');
        } else {
            $j('.create-account-form').hide();
            $j('.create-account-form input[type="password"]').addClass('ignore');
        }
    });
});

function saveCookie() {
    console.log('Saving form data');
    $j('form').each(function(i){
        x = $j(this).attr('name');
        str = $j(this).serialize();
        $j.cookie(x, str, {expires : 7});
    });
}

function restoreCookie() {
    $j('form').each(function(i){
        x = $j(this).attr('name');
        str = $j.cookie(x);

        if ('string' == typeof str) {
            data = decodeURIComponent(str).split('&');
            $j(data).each(function(element) {
                a = this.split('=');
                if (!$j('input[name="'+a[0]+'"]').val()) {
                    $j('input[name="'+a[0]+'"]').val(a[1].replace('+', ' '));
                }
            });
        }
    });
}

function resetForm(formName){
    $j(formName).reset();
    $j.removeCookie(formName);
}
