<?php
/**
 * Vax Limited 
 *
 * @copyright   Copyright (c) 2015 Vax Ltd.
**/
class DirtDevil_Catalog_Model_Layer_Filter_Category extends Mage_Catalog_Model_Layer_Filter_Category
{
    protected $_itemsCount = null;

    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = (int) $request->getParam($this->getRequestVar());
        if (!$filter) {
            return $this;
        }
        $this->_categoryId = $filter;

        Mage::register('current_category_filter', $this->getCategory(), true);

        $this->_appliedCategory = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($filter);

        if ($this->_isValidCategory($this->_appliedCategory)) {
            $this->getLayer()->getProductCollection()
                ->addCategoryFilter($this->_appliedCategory);

            $this->getLayer()->getState()->addFilter(
                $this->_createItem($this->_appliedCategory->getName(), $filter)
            );
        }

        return $this;
    }

    public function getCategory()
    {
        if (Mage::registry('current_category')) {
            return Mage::registry('current_category');
        }

        return parent::getCategory();
    }

    protected function _getItemsData()
    {
        $key = $this->getLayer()->getStateKey().'_SUBCATEGORIES';
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        if ($data === null) {
            $categoty   = $this->getCategory();
            /** @var $categoty Mage_Catalog_Model_Category */
            $categories = $categoty->getChildrenCategories();

            $this->getLayer()->getProductCollection()
                ->addCountToCategories($categories);

            $data = array();

            if(!Mage::app()->getRequest()->isAjax()) {
                foreach ($categories as $category) {
                    if ($category->getIsActive() && $category->getProductCount()) {
                        $data[] = array(
                            'label' => Mage::helper('core')->escapeHtml($category->getName()),
                            'value' => $category->getId(),
                            'count' => $category->getProductCount(),
                            'orig_count'    => $category->getProductCount()
                        );
                    }
                }
            } else {
                $origCategories = $this->getOrigCategories();
                foreach ($categories as $category) {
                    if ($category->getIsActive()) {

                        $origCategory = array_key_exists($category->getId(), $origCategories)
                            ? $origCategories[$category->getId()]
                            : new Varien_Object(array('product_count' => 0));

                        $data[] = array(
                            'label' => Mage::helper('core')->escapeHtml($category->getName()),
                            'value' => $category->getId(),
                            'count' => $category->getProductCount(),
                            'orig_count'    => $origCategory->getProductCount()
                        );
                    }
                }
            }

            // Count total number of items for entire filter
            $totalCount = 0;
            foreach ($data as $dataItem) {
                $totalCount += $dataItem['count'];
            }

            $this->_itemsCount = $totalCount;

            $tags = $this->getLayer()->getStateTags();
            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
        }
        return $data;
    }

    /**
     * Prepares list of originally selected categories.
     *
     * @return array
     */
    public function getOrigCategories()
    {
        $layer = clone $this->getLayer();
        $categories = $this->getCategory()->getChildrenCategories();
        $layer->getProductCollection()->addCategoryFilter($this->getCategory())->addCountToCategories($categories);

        return $categories;
    }

    protected function _initItems()
    {
        $data = $this->_getItemsData();
        $items=array();
        foreach ($data as $itemData) {
            $_item = $this->_createItem(
                $itemData['label'],
                $itemData['value'],
                $itemData['count']);
            $_item->setOrigCount($itemData['orig_count']);
            $items[] = $_item;
        }
        $this->_items = $items;
        return $this;
    }


    /**
     * Overwrites parent class to return calculated items of selected fileter.
     *
     * @return int|null
     */
    public function getItemsCount() {
        if (null !== $this->_itemsCount && is_numeric($this->_itemsCount)) {
            return $this->_itemsCount;
        }

        return parent::getItemsCount();
    }
}
