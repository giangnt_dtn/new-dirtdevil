<?php

/**
 * Product Helper.
 */
class DirtDevil_Catalog_Helper_Product extends Mage_Catalog_Helper_Product
{
    /**
     * Returns string url for 1x1 placeholder.
     *
     * @return String
     */
    public function getPlaceHolderImage()
    {
        $placeholder = Mage::getBaseUrl('skin') . "frontend/dirtdevil_rwd/default/images/1x1placeholder.png";

        return $placeholder;
    }

    /**
     * Returns url for the spares category page
     *
     * @var Mage_Catalog_Model_Product
     * @return bool;
     */
    public function getSparesCategoryUrl($product)
    {
        if($product->getSparesCategoryId() && $product->getAttributeText('spares_category_id')) {
            return Mage::getModel('catalog/category')->load($product->getAttributeText('spares_category_id'))->getUrl();
        }
        return false;
    }

    /**
     * Checks whether the product has a Spares section
     *
     *  @var Mage_Catalog_Model_Product
     *  @return boolean;
     */
    public function hasSpares(Mage_Catalog_Model_Product $product) {
        $relatedCollection = $product->getRelatedProductIds();
        if (count($relatedCollection) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether the product has a Downloads section
     *
     *  @var Mage_Catalog_Model_Product
     *  @return boolean;
     */
    public function hasDownloads(Mage_Catalog_Model_Product $product){
        if(Mage::helper('core')->isModuleEnabled('Uni_Fileuploader')){
            $attachments = Mage::getBlockSingleton('fileuploader/fileuploader')->getProductAttachments( $product->getId());
            if(count($attachments) > 0){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the product has a Features section
     *
     *  @var Mage_Catalog_Model_Product
     *  @return boolean;
     */
    public function hasFeatures(Mage_Catalog_Model_Product $product){
        $features = $product->getData('product_features');
        if($features != null) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether the product has a Whats in the box section
     *
     *  @var Mage_Catalog_Model_Product
     *  @return boolean;
     */
    public function hasBox(Mage_Catalog_Model_Product $product){
        $whatsInTheBoxList = $product->getData('whats_in_the_box_numbered');

        return ($whatsInTheBoxList && strlen(trim($whatsInTheBoxList)) > 0);
    }

    /**
     * Checks whether the  reviews are enabled
     *
     *  @var Mage_Catalog_Model_Product
     *  @return boolean;
     */
    public function hasReviews(){
        $allowed = false;
        if(Mage::helper('core')->isModuleEnabled('Mage_Review')){
            $allowed = Mage::getBlockSingleton('review/form')->getAllowWriteReviewFlag();
        }
        return $allowed;
    }

    /**
     * Finds the link to the All Products page
     *
     *  @var Mage_Catalog_Model_Product
     *  @return String;
     */
    public function viewAllLink($product) {

        //UK - check for existing spares category
        if($this->getSparesCategoryUrl($product) && Mage::app()->getStore()->getCode() == 'dirtdevil_uk') {
            return $this->getSparesCategoryUrl($product);
        }
        //DE - check whether the product has spares
        else {
            if($this->hasSpares($product)) {
                if(Mage::app()->getStore()->getCode() == 'dirtdevil_de_default') {
                    return Mage::getUrl().'ersatzteile-und-zubehor?sku='.$product->getSku();
                }
                if(Mage::app()->getStore()->getCode() == 'dirtdevil_de_english') {
                    return Mage::getUrl().'spares-and-accessories?sku='.$product->getSku();
                }
            }
            return false;
        }
    }
}
