<?php
class Vax_LiveChat_Block_Contact extends Vax_LiveChat_Block_General
{
    /*
     * Returns the helper in use
     * @return Mage_Core_Helper_Abstract
     */
    public function getTheHelper() {
        return Mage::helper('vax_livechat/contact');
    }
}
